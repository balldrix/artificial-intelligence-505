#ifndef _CUTSCENE_H_
#define _CUTSCENE_H_

#ifndef _WAYPOINTMANAGER_H_
#include "WaypointManager.h"
#endif // _WAYPOINTMANAGER_H_

class CutScene
{
public:
	/**
	 * Constructor
	 */
				CutScene();
	/**
	 * Destructor
	 */
				~CutScene();
	/**
	 * Kicks off the cutscene and spawns any required props / characters
	 */
	void		BeginCutScene();
	/**
	 * Ends the cutscene
	 */
	void		EndCutScene();
	/**
	 * Updates the cutscene
	 */
	void		Update(
					float elapsedTime
					);
	/**
	 * Sets up the cutscene information 
	 */
	void		SetUpScene(
					const char* ID,
					float duration,
					const char* cameraType,
					const char* cameraPos, 
					const char* Target,
					bool tracks
					);
	/**
	 * Adds a character / prop to be spawned with the cutscene
	 */
	void		SetUpCharacterSpawning(
					bool isCharacter,
					const char* characterType,
					const char* character, 
					const char* initialState,
					bool canDamagePlayer
					);
	/**
	 * Adds a waypoint to a spawned object with the specified ident
	 */
	void		AddWayPointToSpawner(
					const char* spawnCharacterID,
					const char* waypointName
					);
	/**
	 * Sets the cutscene ID
	 */
	void		SetCutSceneID(
					const char* ID
					);
	/**
	 * Returns the cutscene id
	 */
	const char* GetCutSceneID() const;
	/**
	 * Returns the duration of the cutscene remaining
	 */
	float		GetDuration() const;
private:
	MyString m_cutsceneID;
	
	bool m_cameraChange;
	MyString m_cameraType;
	
	float m_duration;
	
	Vector3D m_cameraPosition;
	MyString m_cameraWaypoint;

	bool m_cameraTracksBones;

	MyString m_target;
	Vector3D m_targetDirection;

	bool m_spawnsCharacter;

	struct SpawnData
	{
		bool m_isCharacter;
		MyString m_spawnCharacterID;
		MyString m_characterType;
		bool m_canDamage;
		MyString m_initialAnimation;
		MyLinkedList<const WayPoint*> m_waypoints;
	};

	MyLinkedList<SpawnData*> m_characterToSpawn;
};

#endif // _CUTSCENE_H_