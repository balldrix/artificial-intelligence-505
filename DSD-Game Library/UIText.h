#ifndef _UITEXT_H_
#define _UITEXT_H_

#ifndef _UIOBJECT_H_
#include "UIObject.h"
#endif //#ifndef _UIOBJECT_H_

class UIText : public UIObject
{
public:
	/**
	 * Constructor
	 */
							UIText();
	/**
	 * Destructor
	 */
	virtual					~UIText();
	/**
	 * Returns the UI object type
	 */
	virtual const char*		GetType(){return "TEXT";}
	/**
	 * Sets the value to be displayed to the specified string
	 */
	void					SetValue(
								const char* string
								);
	/**
	 * Sets the value to be displayed to the specified number
	 */
	void					SetValue(
								int number
								);
	/**
	 * Sets the colour to draw the text
	 */
	void					SetTextColour(
								int r, 
								int g, 
								int b
								);
	/**
	 * Updates the text object
	 */
	virtual void			Update(
								float elapsedTime
								);
	/**
	 * Renders the text
	 */
	virtual void			Render();

private:
	MyString m_storeString;
	int m_red;
	int m_green;
	int m_blue;
};

#endif