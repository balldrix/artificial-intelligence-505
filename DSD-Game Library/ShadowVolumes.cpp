#include "Types.h"
#include "Light.h"
#include "Model.h"
#include "SpotLight.h"
#include "ShadowVolumes.h"
#include "ShadowMesh.h"
#include "RenderManager.h"
#include "EffectManager.h"
#include "Effect.h"
#include "CameraController.h"
#include "Program.h"

/*****************************************************************************************/

ShadowVolumes::ShadowVolumes()
{
	Vector3D lightPosition(-3,10,0);
	Vector3D lightDirection;
	Vector3DNormalize(&lightDirection,&lightPosition);
	m_light = new SpotLight("Light1",0,lightPosition,-lightDirection,Vector3D(2.0,0.5f,0.1f),10.0f,(D3DX_PI * 2) / 2,1.0f,1.0f,1,1,1,1);
	m_tempMesh = new Model("Assets\\Models\\Air_Tank.x",Vector3D(5.0f,0,-5),Vector3D(0.0f,2.0f,0.2f),1.0f);
}

/*****************************************************************************************/

ShadowVolumes::~ShadowVolumes(void)
{
}

/*****************************************************************************************/

void
ShadowVolumes::CreateShadowMesh(Mesh* inMesh)
{
	ShadowMesh::CreateShadowMesh(inMesh,&m_ShadowMesh);
}

/*****************************************************************************************/

void
ShadowVolumes::Render()
{
	Vector3D rotation = m_tempMesh->GetRotation();
	rotation.y += 0.05f;

	m_tempMesh->SetRotation(rotation);

	Effect* shadowVolumeEffect = EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME);

	EffectSetAndCacheHandle1(shadowVolumeEffect, SetMatrix, "m_Proj", CameraController::GetInstance()->GetProjection());

	EffectSetAndCacheHandle0(shadowVolumeEffect, SetTechnique, "mTech");
	Vector4D Ambi(AMBIENT,AMBIENT,AMBIENT,1.0f);
	EffectSetAndCacheHandle1(shadowVolumeEffect, SetVector4, "m_Ambient", Ambi);

	EffectSetAndCacheHandle1(shadowVolumeEffect, SetFloat, "m_ZClip",  100.0f - 0.1f );
	RenderScene();

	m_D3DDevice->Clear(0, NULL, D3DCLEAR_STENCIL, D3DCOLOR_ARGB(0, 170, 170, 170), 1.0f, 0 );

	Vector4D lightPosition = Vector4D(m_light->GetPosition().x,m_light->GetPosition().y,m_light->GetPosition().z,1.0f);
	Matrix m;
	MatrixIdentity(&m);
	D3DXVec4Transform(&lightPosition,&lightPosition,&m);
	D3DXVec4Transform(&lightPosition,&lightPosition,&CameraController::GetInstance()->GetView());
	Vector4D lights[10];
	ZeroMemory(&lights,sizeof(Vector4D) * 10);
	lights[0] = lightPosition;
	shadowVolumeEffect->GetEffect()->SetValue(shadowVolumeEffect->GetHandle("m_LightPosition")->m_handle,&lights,sizeof(Vector4D) * 10);
	EffectSetAndCacheHandle1(shadowVolumeEffect, SetVector4, "m_LightColour", Vector4D(15,15,15,1));

	if(Program::GetInstance()->Allow2SidedShadows())
	{
		EffectSetAndCacheHandle0(shadowVolumeEffect, SetTechnique, "mTechShadow2");
	}
	else
	{
		EffectSetAndCacheHandle0(shadowVolumeEffect, SetTechnique, "mTechShadow");
	}

	EffectSetAndCacheHandle1(shadowVolumeEffect, SetVector4, "m_ShadowColour", Vector4D(0.0f,0.1f,0.0f,0.2f));

	if(m_ShadowMesh)
	{
		UINT cPasses;
		shadowVolumeEffect->GetEffect()->Begin( &cPasses, 0 );

		for( UINT i = 0; i < cPasses; ++i )
		{
			shadowVolumeEffect->GetEffect()->BeginPass( i );
			shadowVolumeEffect->GetEffect()->CommitChanges();
			m_ShadowMesh->DrawSubset( 0 );
			shadowVolumeEffect->GetEffect()->EndPass();
		}
		shadowVolumeEffect->GetEffect()->End();
	}
	
	EffectSetAndCacheHandle0(shadowVolumeEffect, SetTechnique, "mTechScene");
	RenderScene();
}

/*****************************************************************************************/

void
ShadowVolumes::RenderScene()
{
	RenderManager::GetInstance()->RenderEnvironment();
	m_tempMesh->RenderWithShadowVolume();
}

/*****************************************************************************************/