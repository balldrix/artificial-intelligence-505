#include "Types.h"
#include "Vertex.h"
#include "BillBoardRegion.h"
#include "TextureCache.h"
#include "EffectManager.h"
#include "Effect.h"
#include "RenderManager.h"
#include "EnvironmentManager.h"
#include "Frustum.h"
#include "CameraController.h"
#include "Box.h"

/********************************************************************************************************************************************************/

BillBoardRegion::BillBoardRegion(const char* id, const char* textureName, const AABB &region, int numBillboards, 
								 const Vector2D& size, bool randomize) :
	m_id(id),
	m_textureName(textureName),
	m_numBB(numBillboards),
	m_size(size),
	m_randomize(randomize),
	m_aabb(region),
	m_quadMesh(0)
{
	if(Create())
	{
		m_flags = BillboardEffect;
		RenderManager::GetInstance()->AddEvent(this, NULL);
	}
}

/********************************************************************************************************************************************************/

BillBoardRegion::~BillBoardRegion()
{
	Destroy();
}

/********************************************************************************************************************************************************/

bool
BillBoardRegion::Create()
{
	float tempL = m_size.x * 0.5f;
	float tempD = m_size.y * 0.5f;
	Vector3D quadPos = Vector3D(0.0f,0.0f,0.0f);

	D3DVERTEXELEMENT9 elems[MAX_FVF_DECL_SIZE];
	UINT numElems = 0;
	BillBoardVertex::Decl->GetDeclaration(elems,&numElems);

	BillBoardVertex* vertex = 0;
	WORD* index;
	int vertIndex = 0;
	int indexIndex = 0;

	Vector3D boxBottom = m_aabb.GetMin();
	Vector3D boxTop = m_aabb.GetMax();

	if(SUCCEEDED(D3DXCreateMesh(2 * m_numBB, 4 * m_numBB, D3DXMESH_MANAGED, elems, m_D3DDevice, &m_quadMesh)))
	{
		m_quadMesh->LockVertexBuffer(0,(void**)&vertex);
		m_quadMesh->LockIndexBuffer(0,(void**)&index);


		for(int i = 0; i < m_numBB; ++i)
		{
			if(m_randomize)
			{
				tempD = GetRandomFloat(1.0f,m_size.y) * 0.5f;
				tempL = GetRandomFloat(1.0f,m_size.x) * 0.5f;
			}

			vertex[vertIndex + 0]  = BillBoardVertex(Vector3D(-tempL, tempD, 0.0f),Vector2D(0.0f,0.0f));
			vertex[vertIndex + 1]  = BillBoardVertex(Vector3D( tempL, tempD, 0.0f),Vector2D(1.0f,0.0f));
			vertex[vertIndex + 2]  = BillBoardVertex(Vector3D(-tempL,-tempD, 0.0f),Vector2D(0.0f,1.0f));
			vertex[vertIndex + 3]  = BillBoardVertex(Vector3D( tempL,-tempD, 0.0f),Vector2D(1.0f,1.0f));


			/************************************************************************/
			/* Set the position of each billboard                                   */
			/************************************************************************/
			Vector3D top, bottom;
			do 
			{
				//Random x z
				quadPos.x = GetRandomFloat(m_aabb.GetMin().x,m_aabb.GetMax().x);
				quadPos.z = GetRandomFloat(m_aabb.GetMin().z,m_aabb.GetMax().z);

				top = Vector3D(quadPos.x,100,quadPos.z);
				bottom = Vector3D(quadPos.x,-100,quadPos.z);

			} while(!EnvironmentManager::GetInstance()->TestCollision(top, bottom, quadPos));


			if(quadPos.y < boxBottom.y)
			{
				boxBottom.y = quadPos.y;
			}

			if(quadPos.y > boxTop.y)
			{
				boxTop.y = quadPos.y;
			}


			vertex[vertIndex + 0].quadPosition = quadPos;
			vertex[vertIndex + 1].quadPosition = quadPos;
			vertex[vertIndex + 2].quadPosition = quadPos;
			vertex[vertIndex + 3].quadPosition = quadPos;

			index[indexIndex + 0] = vertIndex + 2; 
			index[indexIndex + 1] = vertIndex + 0; 
			index[indexIndex + 2] = vertIndex + 1;
			index[indexIndex + 3] = vertIndex + 2; 
			index[indexIndex + 4] = vertIndex + 1; 
			index[indexIndex + 5] = vertIndex + 3;


			vertIndex += 4;
			indexIndex += 6;
		}

		m_quadMesh->UnlockVertexBuffer();
		m_quadMesh->UnlockIndexBuffer();

		boxTop.y += m_size.y;

		m_aabb.SetMin(boxBottom);
		m_aabb.SetMax(boxTop);

		m_box.SetBox(m_aabb);


		m_texIndex = TextureCache::GetInstance()->AddTexture(m_textureName,m_textureName,true);
		return true;
	}

	return false;
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::Destroy()
{
	if(m_quadMesh)
	{
		m_quadMesh->Release();
		m_quadMesh = 0;
	}
}

/********************************************************************************************************************************************************/

void 
BillBoardRegion::Render()
{
	const Frustum& frustum = CameraController::GetInstance()->GetFrustum();
	if(frustum.BoxVisible(m_aabb))
	{
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD), SetTexture, "m_Texture", TextureCache::GetInstance()->GetTexture(m_texIndex));
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->GetEffect()->CommitChanges();
		m_quadMesh->DrawSubset(0);
	}
}

/********************************************************************************************************************************************************/

const char*	
BillBoardRegion::GetID() const
{
	return m_id.GetPointer();
}

/********************************************************************************************************************************************************/

const AABB&
BillBoardRegion::GetRegion() const
{
	return m_aabb;
}

/********************************************************************************************************************************************************/

int
BillBoardRegion::GetNumBillboards() const
{
	return m_numBB;
}

/********************************************************************************************************************************************************/

const Vector2D&
BillBoardRegion::GetSize() const
{
	return m_size;
}

/********************************************************************************************************************************************************/

bool
BillBoardRegion::GetRandomize() const
{
	return m_randomize;
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::SetID(const char* id)
{
	m_id = id;
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::SetRegion(const AABB& region)
{
	m_aabb = region;
	m_box.SetBox(m_aabb);
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::SetNumBillboards(int num)
{
	m_numBB = num;
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::SetSize(const Vector2D& size)
{
	m_size = size;
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::SetRandomize(bool randomize)
{
	m_randomize = randomize;
}

/********************************************************************************************************************************************************/

bool
BillBoardRegion::Rebuild()
{
	Destroy();
	return Create();
}

/********************************************************************************************************************************************************/

void
BillBoardRegion::Select(bool select)
{
	if(select != m_selected)
	{
		if(select)
		{
			RenderManager::GetInstance()->AddBox(&m_box);
		}
		else
		{
			RenderManager::GetInstance()->RemoveBox(&m_box);
		}
		m_selected = select;
	}
}

/********************************************************************************************************************************************************/