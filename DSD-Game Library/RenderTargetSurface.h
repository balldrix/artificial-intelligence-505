#ifndef _RENDERTARGETDURFACE_H_
#define _RENDERTARGETDURFACE_H_

class RenderTargetSurface
{
public:
	/**
	 * Constructor
	 */
							RenderTargetSurface(
								int width, ///< Width of the render target
								int height, ///< Height of the render target
								int mips, ///< Number of mipmaps
								D3DFORMAT texformat, ///< The texture format
								bool useDB, ///< Flags saying wether to use depth buffer
								D3DFORMAT depthFormat, ///< Depth buffer format
								D3DVIEWPORT9& viewport, ///< The view port to use
								bool autoMips ///< Aut generate mip maps
								);
	/**
	 * Destructor
	 */
							~RenderTargetSurface();
	/**
	 * Returns the texture for the render target
	 */
	IDirect3DTexture9*		GetTexture();
	/**
	 * Begins renderering to the render target
	 */
	void					BeginScene();
	/**
	 * Ends renderering to the render target
	 */
	void EndScene();
private:
	// Dont allow
	RenderTargetSurface(const RenderTargetSurface& rhs) {};
	RenderTargetSurface& operator=(const RenderTargetSurface& rhs) {};
	
	IDirect3DTexture9* m_texture;
	ID3DXRenderToSurface* m_rts;
	IDirect3DSurface9* m_surface;

	int m_width;
	int m_height;
	int m_mips;
	D3DFORMAT m_textureFormat;
	bool m_useDepthBuffer;
	D3DFORMAT m_depthFormat;
	D3DVIEWPORT9 m_viewPort;
	bool m_autoMips;

};


#endif //_RENDERTARGETDURFACE_H_
