#include "Types.h"
#include "UISprite.h"

#include "TextureCache.h"
#include "UIManager.h"

/****************************************************************************************************************************/

UISprite::UISprite() : 
	m_repeated(false)
{
}

/****************************************************************************************************************************/

UISprite::~UISprite()
{
}

/****************************************************************************************************************************/

void 
UISprite::Load(const char* fileName)
{
	TextureCache::GetInstance()->AddTexture(this->GetID(),fileName,true);
}

/****************************************************************************************************************************/ 

void 
UISprite::Render()
{
	if(m_render)
	{
		D3DSURFACE_DESC desc;
		TextureCache::GetInstance()->GetTexture(GetID())->GetLevelDesc(0,&desc);

		Matrix mat;
		MatrixIdentity(&mat);

		float width = m_width;
		float height = m_height;

		if (m_repeated)
		{
			UIManager::GetInstance()->GetSprite()->SetTransform(&mat);

			for (UINT x = 0; x < m_width; x += desc.Width)
			{
				for (UINT y = 0; y < m_height; y += desc.Height)
				{
					RECT r;
					r.top = 0;
					r.left = 0;
					r.bottom = (UINT)m_height - y < desc.Height ? (UINT)m_height - y : desc.Height;
					r.right = (UINT)m_width - x < desc.Width ? (UINT)m_width - x : desc.Width;
					UIManager::GetInstance()->GetSprite()->Draw(TextureCache::GetInstance()->GetTexture(GetID()),
						&r,
						NULL,
						&Vector3D(m_position.x + x, m_position.y + y,0.0f),
						UIManager::GetInstance()->GetColour());
				}
			}
		}
		else
		{
			D3DVIEWPORT9 view;
			m_D3DDevice->GetViewport(&view);

			
			if(m_position.x + width > view.Width)
			{
				width -= (m_position.x + width - view.Width);
			}

			if(m_height > view.Height)
			{
				height -= (m_position.y + height - view.Height);				
			}

			float fScaleX = (float) width / desc.Width;
			float fScaleY = (float) height / desc.Height;


			float posX = (float) m_position.x / fScaleX;
			float posY = (float) m_position.y / fScaleY;

			Matrix scale;
			MatrixScaling(&scale,fScaleX,fScaleY,1.0f);
			mat *= scale;
			UIManager::GetInstance()->GetSprite()->SetTransform(&mat);
			UIManager::GetInstance()->GetSprite()->Draw(TextureCache::GetInstance()->GetTexture(GetID()),
				NULL,
				NULL,
				&Vector3D(posX,posY,0.0f),
				UIManager::GetInstance()->GetColour());
		}
	}
	
}

/****************************************************************************************************************************/ 

void 
UISprite::Update(float elapsedTime)
{
}

/****************************************************************************************************************************/ 