#include "Types.h"

#include "PolygonData.h"
#include "NavigationMeshNode.h"

/****************************************************************************************************/

NavigationMeshNode::NavigationMeshNode(PolygonData* poly) :
	m_poly(poly),
	m_centre(0.0f, 0.0f, 0.0f),
	m_neighbourIndex(0)
{
	for (int index = 0; index < 3; ++index)
	{
		m_neighbours[index] = NULL;
		m_centre += m_poly->GetVert(index);
	}

	m_centre /= 3;
}

/****************************************************************************************************/

NavigationMeshNode::~NavigationMeshNode()
{
}

/****************************************************************************************************/

float
NavigationMeshNode::CostCalculate(AStarNode* to)
{
	NavigationMeshNode* otherNode = static_cast<NavigationMeshNode*>(to);
	return Vector3DLength(&(GetCentre() - otherNode->GetCentre()));
}

/************************************************************************/

float
NavigationMeshNode::CostEstimate(AStarNode* to)
{
	return CostCalculate(to);
}

/************************************************************************/

AStarNode* 
NavigationMeshNode::GetFirstNeighbour()
{
	m_neighbourIndex = 0;
	return GetNextNeighbour();
}

/************************************************************************/

AStarNode*
NavigationMeshNode::GetNextNeighbour()
{
	while (m_neighbourIndex < 3)
	{
		if (m_neighbours[m_neighbourIndex])
		{
			return m_neighbours[m_neighbourIndex++];
		}
		else
		{
			++m_neighbourIndex;
		}
	}

	return NULL;
}

/************************************************************************/

const PolygonData*
NavigationMeshNode::GetPoly() const
{
	return m_poly;
}

/************************************************************************/

const Vector3D&
NavigationMeshNode::GetCentre() const
{
	return m_centre;
}

/************************************************************************/

bool
NavigationMeshNode::GetCommonEdge(NavigationMeshNode* otherNode, int& ourEdge, int& theirEdge)
{
	if (otherNode != this)
	{
		int matchCount = 0;

		for (int ourVertIndex = 0; ourVertIndex < 3; ++ourVertIndex)
		{
			for (int theirVertIndex = 0; theirVertIndex < 3; ++theirVertIndex)
			{
				if(GetPoly()->GetVert(ourVertIndex) == otherNode->GetPoly()->GetVert(theirVertIndex))
				{
					++matchCount;

					if (matchCount == 1)
					{
						ourEdge = ourVertIndex;
						theirEdge = theirVertIndex;
						break;
					}
					else if (matchCount == 2)
					{
						ourEdge = (4 - (ourEdge + ourVertIndex)) % 3;
						theirEdge = (4 - (theirEdge + theirVertIndex)) % 3;
						return true;
					}
				}
			}
		}
	}

	return false;
}

/************************************************************************/

void
NavigationMeshNode::SetNeighbour(NavigationMeshNode* neighbour, int edge)
{
	m_neighbours[edge] = neighbour;
}

/************************************************************************/

NavigationMeshNode*
NavigationMeshNode::GetNeighbour(int edge)
{
	return m_neighbours[edge];
}

/************************************************************************/

Vector3D
NavigationMeshNode::GetJoinPosition(AStarNode* otherNode)
{
	for (int index = 0; index < 3; ++index)
	{
		if (m_neighbours[index] == otherNode)
		{
			return (m_poly->GetVert(index) + m_poly->GetVert((index+1)%3)) / 2;
		}
	}

	return Vector3D(0.0f, 0.0f, 0.0f);
}

/************************************************************************/

bool
NavigationMeshNode::PointInsideNode(const Vector3D& point)
{
	bool isSideA = false;
	bool isSideB = false;

	for (int edgeIndex = 0; edgeIndex < 3; ++edgeIndex)
	{
		const Vector3D& edgeStart = m_poly->GetVert(edgeIndex);
		const Vector3D& edgeEnd = m_poly->GetVert((edgeIndex+1)%3);

		// Get the perpendicular direction (Note the flip of x and z and the change of sign)
		Vector3D direction(edgeEnd.z - edgeStart.z, 0.0f, edgeStart.x - edgeEnd.x);

		float dot = Vector3DDotProduct(&direction, &(point - edgeStart));

		if (dot > 0)
		{
			isSideA = true;

			if (isSideB)
			{
				return false;
			}
		}
		else if (dot < 0)
		{
			isSideB = true;

			if (isSideA)
			{
				return false;
			}
		}
	}

	return true;
}

/************************************************************************/
