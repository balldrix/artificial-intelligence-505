#ifndef _SHADOWMESH_H_
#define _SHADOWMESH_H_

struct EdgeMapping
{
	int m_oldEdge[2];
	int m_newEdge[2][2];

	EdgeMapping()
	{
		FillMemory(m_oldEdge,sizeof(m_oldEdge),-1);
		FillMemory(m_newEdge,sizeof(m_newEdge),-1);
	}
};

class ShadowMesh
{
public:
	/**
	 * Constructor
	 */
						ShadowMesh();
	/**
	 * Destructor
	 */
						~ShadowMesh();
	/**
	 * Creates a new shadow mesh from an input mesh
	 */
	static HRESULT		CreateShadowMesh(
							Mesh* Object, ///< The mesh to generate the shadow mesh from
							Mesh** OutPutMesh ///< The resulting mesh
							);
	/**
	 * Searches through edge list trying to find an existing edge
	 */
	static int			FindEdgeInMappingTable(
							int nV1, ///< vertex index1
							int nV2, ///< vertex index2 
							EdgeMapping* pMapping, ///< edge mapping list
							int nCount ///< edge mapping count
							);
	/**
	 * Adds the inserts the extra veritices into the mesh for the edge
	 */
	static void			ComputeNewEdges(
							EdgeMapping* mapping, 
							DWORD* IndexData, 
							int& numberEdges, 
							int& nextIndex, 
							int V1, 
							int V2,
							int index,
							int vertCount, 
							int offset1, 
							int offset2
							);
	/**
	 * Returns the shadow volume vertex type declaration
	 */
	static IDirect3DVertexDeclaration9* GetShadowDecl();


private:

};

#endif //_SHADOWMESH_H_

