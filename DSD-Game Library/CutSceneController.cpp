#include "Types.h"
#include "CutSceneController.h"
#include "CutScene.h"
#include "MyFileReader.h"
#include "Reigon.h"
#include "CharacterManager.h"
#include "Character.h"

/************************************************************************/

DefineSingleton(CutSceneController);

/************************************************************************/

CutSceneController::CutSceneController() :
	m_currentCutScene(0)
{
	m_playerSphere = CharacterManager::GetInstance()->GetCharacter("Player")->GetBoundingSphere();
}

/************************************************************************/

CutSceneController::~CutSceneController()
{
	LinkedListNode<CutScene*>* node = m_cutscenes.GetFirst();

	while (node)
	{
		delete node->GetData();
		node = node->GetNext();
	}
	m_cutscenes.Clear();
	m_playerSphere = 0;
}

/************************************************************************/

void
CutSceneController::Loader(const char *fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		MyString SceneID = reader.GetNextToken();
		float duration = (float)atof(reader.GetNextToken());
		MyString cameraType = reader.GetNextToken();
		MyString cameraPosition = reader.GetNextToken();
		MyString CameraTarget = reader.GetNextToken();
		MyString CameraTracks = reader.GetNextToken();

		while (SceneID[0] != '\0')
		{
			CutScene* c = new CutScene();
			if (strcmp(CameraTracks,"TRUE") == 0 || strcmp(CameraTracks,"true") == 0)
			{
				c->SetUpScene(SceneID,duration,cameraType,cameraPosition,CameraTarget,true);
			}
			else
			{
				c->SetUpScene(SceneID,duration,cameraType,cameraPosition,CameraTarget,false);
			}

			m_cutscenes.Insert(c);

			SceneID = reader.GetNextToken();
			duration = (float)atof(reader.GetNextToken());
			cameraType = reader.GetNextToken();
			cameraPosition = reader.GetNextToken();
			CameraTarget = reader.GetNextToken();
			CameraTracks = reader.GetNextToken();
		}

		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Effect File Could not be found");
	}
}

/************************************************************************/

void 
CutSceneController::CharacterSpawnLoader(const char* fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		MyString SceneID = reader.GetNextToken();
		MyString SpawnType = reader.GetNextToken();
		MyString CharacterType;
		MyString CharacterID;
		int waypointCount;
		MyString CharacterWaypoint;
		MyString CharacterAni;
		MyString CharacterHurtsPlayer;

		while (SceneID[0] != '\0')
		{
			CutScene* cutScene = GetCutScene(SceneID);

			if(strcmp(SpawnType,"CHARACTER")==0)
			{
				CharacterType = reader.GetNextToken();
				CharacterID = reader.GetNextToken();
				CharacterAni = reader.GetNextToken();
				CharacterHurtsPlayer = reader.GetNextToken();
				waypointCount = atoi(reader.GetNextToken());
				
				if(strcmp("TRUE",CharacterHurtsPlayer) == 0)
				{
					cutScene->SetUpCharacterSpawning(true,CharacterType,CharacterID,CharacterAni,true);
				}
				else
				{
					cutScene->SetUpCharacterSpawning(true,CharacterType,CharacterID,CharacterAni,false);
				}
				
				for (int i = 0; i < waypointCount; i++)
				{
					CharacterWaypoint = reader.GetNextToken();
					cutScene->AddWayPointToSpawner(CharacterID,CharacterWaypoint);
				}
			}
			else if(strcmp(SpawnType,"PROP")==0)
			{
				CharacterType = reader.GetNextToken();
				CharacterID = reader.GetNextToken();
				CharacterWaypoint = reader.GetNextToken();
				cutScene->SetUpCharacterSpawning(false,CharacterType,CharacterID,"NONE",false);
				cutScene->AddWayPointToSpawner(CharacterID,CharacterWaypoint);
			}

			SceneID = reader.GetNextToken();
			SpawnType = reader.GetNextToken();
		}

		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Character spawn File Could not be found");
	}
}

/************************************************************************/

void 
CutSceneController::LoadReigonData(const char* fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		MyString ID = reader.GetNextToken();
		MyString Triggers = reader.GetNextToken();
		float x = (float)atof(reader.GetNextToken());
		float y = (float)atof(reader.GetNextToken());
		float z = (float)atof(reader.GetNextToken());
		float radius = (float)atof(reader.GetNextToken());

		while (ID[0] != '\0')
		{
			Reigon * r = new Reigon(ID,Triggers,Vector3D(x,y,z),radius);
			m_reigons.Insert(r);

			ID = reader.GetNextToken();
			Triggers = reader.GetNextToken();
			x = (float)atof(reader.GetNextToken());
			y = (float)atof(reader.GetNextToken());
			z = (float)atof(reader.GetNextToken());
			radius = (float)atof(reader.GetNextToken());
		}
		
		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Effect File Could not be found");
	}
}

/************************************************************************/

CutScene* 
CutSceneController::GetCutScene(const char* ID)
{
	LinkedListNode<CutScene*>* node = m_cutscenes.GetFirst();

	while (node)
	{
		if(strcmp(node->GetData()->GetCutSceneID(),ID) == 0)
		{
			return node->GetData();
		}
		node = node->GetNext();
	}

	ErrorMessage("No CutScene Found");
	return 0; 
}

/************************************************************************/

void 
CutSceneController::BeginCutScene(const char* sceneID)
{
	if(!m_currentCutScene)
	{
		m_currentCutScene = GetCutScene(sceneID);

		m_currentCutScene->BeginCutScene();
	}
	else
	{
		m_currentCutScene->EndCutScene();
		
		m_currentCutScene = GetCutScene(sceneID);

		m_currentCutScene->BeginCutScene();

	}
	
}

/************************************************************************/

void
CutSceneController::Update(float elapsedTime)
{
	if(!m_currentCutScene)
	{
		LinkedListNode<Reigon*>* node = m_reigons.GetFirst();

	//	Sphere* s = CharacterManager::GetInstance()->GetCharacter("Player")->GetSphere();
		
		while (node)
		{
			
			if(m_playerSphere->CollidingWithSphere(node->GetData()->GetSphere()))
			{
				BeginCutScene(node->GetData()->GetCutscene());
				m_reigons.Remove(node);
				delete node->GetData();
				node = 0;
			}
			else
			{
				node = node->GetNext();
			}
		}
	}
	
	if(m_currentCutScene)
	{
		m_currentCutScene->Update(elapsedTime);
		if (m_currentCutScene->GetDuration() < 0.0f)
		{
			m_currentCutScene->EndCutScene();
			m_cutscenes.Remove(m_currentCutScene);
			delete m_currentCutScene;
			m_currentCutScene = 0;
		}
	}
}

/************************************************************************/

bool
CutSceneController::IsCutSceneActive() const
{
	return m_currentCutScene ? true : false;
}

/************************************************************************/
