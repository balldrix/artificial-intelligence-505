#include "Types.h"
#include "Box.h"
#include "CameraController.h"

/*********************************************************************************************************/

Box::Box() :
	m_vertexBuffer(NULL)
{
	m_box.SetMin(Vector3D(-1,-1,-1));
	m_box.SetMax(Vector3D(1,1,1));
	CreateWireFrame();
}

/*********************************************************************************************************/

Box::Box(const AABB& box) :
	m_vertexBuffer(NULL)
{
	m_box = box;
	CreateWireFrame();
}

/*********************************************************************************************************/

Box::~Box()
{
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
	}
}

/*********************************************************************************************************/

void 
Box::SetBox(const AABB& box)
{
	m_box = box;
	CreateWireFrame();
}

/*********************************************************************************************************/

void
Box::Render()
{
	Matrix world;
	D3DXMatrixIdentity(&world);

	m_D3DDevice->SetVertexShader(NULL);
	m_D3DDevice->SetPixelShader(NULL);
	CameraController::GetInstance()->SetWorld(world);
	m_D3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	m_D3DDevice->SetStreamSource(0, m_vertexBuffer, 0 , sizeof(VertexColour));
	m_D3DDevice->SetVertexDeclaration(VertexColour::Decl);
	m_D3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_D3DDevice->DrawPrimitive( D3DPT_LINELIST, 0, 12 );
	m_D3DDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
}

/*********************************************************************************************************/

void
Box::CreateWireFrame()
{
	if(!m_D3DDevice)
	{
		return;
	}
	if(m_vertexBuffer)
	{
		m_vertexBuffer->Release();
	}

	const DWORD colour = 0xffffffff;

	m_D3DDevice->CreateVertexBuffer(24*sizeof(VertexColour), D3DUSAGE_WRITEONLY,
		0, D3DPOOL_MANAGED, &m_vertexBuffer, 0);

	VertexColour* vb = 0;
	m_vertexBuffer->Lock(0, 0, (void**)&vb, 0);

	Vector3D p0(m_box.GetMin().x,m_box.GetMax().y,m_box.GetMin().z);
	Vector3D p1(m_box.GetMin().x,m_box.GetMax().y,m_box.GetMax().z);
	Vector3D p2(m_box.GetMax().x,m_box.GetMax().y,m_box.GetMax().z);
	Vector3D p3(m_box.GetMax().x,m_box.GetMax().y,m_box.GetMin().z);
	Vector3D p4(m_box.GetMin().x,m_box.GetMin().y,m_box.GetMin().z);
	Vector3D p5(m_box.GetMin().x,m_box.GetMin().y,m_box.GetMax().z);
	Vector3D p6(m_box.GetMax().x,m_box.GetMin().y,m_box.GetMax().z);
	Vector3D p7(m_box.GetMax().x,m_box.GetMin().y,m_box.GetMin().z);

	int count = 0;
	vb[count+0] = VertexColour(p0, colour);
	vb[count+1] = VertexColour(p1, colour);
	count+=2;
	vb[count+0] = VertexColour(p1, colour);
	vb[count+1] = VertexColour(p2, colour);
	count+=2;
	vb[count+0] = VertexColour(p2, colour);
	vb[count+1] = VertexColour(p3, colour);
	count+=2;
	vb[count+0] = VertexColour(p3, colour);
	vb[count+1] = VertexColour(p0, colour);
	count+=2;
	vb[count+0] = VertexColour(p4, colour);
	vb[count+1] = VertexColour(p5, colour);
	count+=2;
	vb[count+0] = VertexColour(p5, colour);
	vb[count+1] = VertexColour(p6, colour);
	count+=2;
	vb[count+0] = VertexColour(p6, colour);
	vb[count+1] = VertexColour(p7, colour);
	count+=2;
	vb[count+0] = VertexColour(p7, colour);
	vb[count+1] = VertexColour(p4, colour);
	count+=2;
	vb[count+0] = VertexColour(p0, colour);
	vb[count+1] = VertexColour(p4, colour);
	count+=2;
	vb[count+0] = VertexColour(p1, colour);
	vb[count+1] = VertexColour(p5, colour);
	count+=2;
	vb[count+0] = VertexColour(p2, colour);
	vb[count+1] = VertexColour(p6, colour);
	count+=2;
	vb[count+0] = VertexColour(p3, colour);
	vb[count+1] = VertexColour(p7, colour);

	m_vertexBuffer->Unlock();
}

/*********************************************************************************************************/
