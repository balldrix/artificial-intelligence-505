#include "Types.h"
#include "UIText.h"

#include "UIManager.h"

#include "Text.h"

/*****************************************************************************************/

UIText::UIText() :
	m_red(255),
	m_green(255),
	m_blue(255)
{
}

/*****************************************************************************************/

UIText::~UIText()
{
}

/*****************************************************************************************/

void 
UIText::SetValue(const char* string)
{
	m_storeString = string;
}

/*****************************************************************************************/

void 
UIText::SetValue(int number)
{
	m_storeString.Format("%d",number);
}

/*****************************************************************************************/

void
UIText::Render()
{
	if(m_render)
	{
		UIManager::GetInstance()->GetTextWriter()->PrintText(m_position, m_storeString, D3DCOLOR_ARGB(255,m_red,m_green,m_blue));
	}
	
}

/*****************************************************************************************/

void
UIText::Update(float elapsedTime)
{
}

/*****************************************************************************************/

void 
UIText::SetTextColour(int r, int g, int b)
{
	m_red = r;
	m_green = g;
	m_blue = b;
}

/*****************************************************************************************/
