#ifndef _UIOBJECT_H_
#define _UIOBJECT_H_

class UIObject
{
public:
	/**
	 * Constructor
	 */

						UIObject();
	/**
	 * Destructor
	 */
	virtual				~UIObject();
	/**
	 * Updates the UI object
	 */
	virtual void		Update(
							float elapsedTime
							){};
	/**
	 * Renders the UI object
	 */
	virtual void		Render(){};
	/**
	 * Returns the type of UI object this is
	 */
	virtual const char* GetType(){return "OBJECT";};
	/**
	 * Sets the ID of the UI object
	 */
	void				SetID(
							const char* ID
							){m_ID = ID;}
	/**
	 * Returns the UI object id
	 */
	const char*			GetID() const {return m_ID;}
	/**
	 * Sets the position of the UI object
	 */
	void				SetPosition(
							const Vector2D& position
							){m_position = position;}
	/**
	 * Returns the position of the UI object
	 */
	const Vector2D&		GetPosition() const {return m_position;}
	/**
	 * Sets the associated state for the UI object
	 */
	void				SetAssignedState(	
							const char* assignedState
							){m_assignedState = assignedState;}
	/**
	 * Returns the state the UI object is associated with
	 */
	const char*			GetAssignedState() const{return m_assignedState;}
	/**
	 * Sets the UI object visible or not
	 */
	void				SetVisible(
							bool visible							){m_render = visible;}
protected:
	MyString m_ID;
	Vector2D m_position;
	MyString m_assignedState;

	bool m_render;
};

#endif