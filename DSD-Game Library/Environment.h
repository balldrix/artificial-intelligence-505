#ifndef _ENVIRONMENT_H_
#define _ENVIRONMENT_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif //_RENDEREVENT_H_

class Model;

class Environment : public RenderEvent
{
public:
	/**
	 * Constructor
	 */
					Environment();
	/**
	 * Destructor
	 */
					~Environment();
	/**
	 * Loads the model to use for the environment with its transform
	 */
	void			LoadEnvironment(
						const char* ID,
						const char* fileName, 
						const Vector3D& pos, 
						const Vector3D& rot, 
						float scale
						);
	/**
	 * Handles the rendering of the environment
	 */
	virtual void	Render();
	/**
	 * Sets the UV scale to be applied to the environment model effect
	 */
	void			SetUVMulitply(
						int UVM
						);
	/**
	 * Gets the environments id
	 */
	const char*		GetID() const;
	/**
	 * Sets the id of the environment
	 */
	void			SetID(
						const char* id
						);
	/**
	 * Returns the model of the environment
	 */
	Model*			GetModel();
private:

	Model* m_model;
	MyString m_ID;

	int m_UVMultiply;
};


#endif //_ENVIRONMENT_H_