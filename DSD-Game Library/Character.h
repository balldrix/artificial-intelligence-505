#ifndef _CHARACTER_H_
#define _CHARACTER_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif //_RENDEREVENT_H_

#ifndef _WAYPOINTMANAGER_H_
#include "WaypointManager.h"
#endif //_WAYPOINTMANAGER_H_

#ifndef _ASTAR_H_
#include "AStar.h"
#endif

#include <vector>

/// Helper Define to cache off an animation index to make setting an animation quick
#define SetAnimationAndCacheIndex(o,s) { static int animationIndexCache = o->GetAnimationIndex(s); o->SetAnimation(animationIndexCache); }

class Sphere;
class SkinnedMesh;
class AnimationInstance;
class Behavior;

// contains name of bone and bounding sphere data
struct PerBoneShere
{
	const char* name;
	Sphere* sphere; 

};

class Character : public RenderEvent
{
public:
	/**
	 * Constructor
	 */
						Character();
	/**
	 * Destructor
	 */
						~Character();
	/**
	 * Marks the character as its be hit if its vulnarable. And sets
	 * up the flashing on the character
	 */
	void				HurtCharacter(float modifier = 1.0f);
	/**
	 * Sets the mesh to use for renderering
	 */
	void				SetMesh(
							SkinnedMesh* meshPtr
							);
	/**
	 * Returns the id of the character
	 */
	const char*			GetID() const;
	/**
	 * Sets the id of the character
	 */
	void				SetID(
							const char* ID
							);
	/**
	 * Renders the character
	 */
	virtual void		Render();
	/**
	 * Updates the character
	 */
	void				Update(
							float elapsedTime ///< The elapsed time this frame
							);
	/**
	 * Gets the index of the animation
	 */
	int					GetAnimationIndex(
							const char* aniName
							) const;
	/**
	 * Sets the current animation to use
	 */
	void				SetAnimation(
							int index
							);
	/**
	 * Returns the animation the character is playing
	 */
	const char*			GetAnimationName() const;
	/**
	 * Returns the animation instance for this character
	 */
	AnimationInstance*	GetAnimationInst()const;
	/**
	 * Sets the position of the character
	 */
	void				SetPosition(
							const Vector3D& pos
							);
	/**
	 * Gets the characters current position
	 */
	const Vector3D&		GetPosition() const;
	/**
	 * Sets the characters current rotation
	 */
	void				SetRotation(
							const Vector3D& rot
							);
	/**
	 * Gets the characters current rotation
	 */
	const Vector3D&		GetRotation() const;
	/**
	 * Sets the scale on the character
	 */
	void				SetScale(
							float scale
							);
	/**
	 * Returns the scale of the character
	 */
	float				GetScale()const;
	/**
	 * Adds a waypoint for the character to follow
	 */
	void				AddWayPoint(
							const WayPoint* waypoint
							);
	/**
	 * Sets the current behaviour on the character
	 */
	void				SetBehavior(
							const char* newBehavior
							);
	/**
	 * Sets an offset onto the characters bounding sphere
	 */
	void				SetBoundOffset(
							const Vector3D& boundPos
							);
	/**
	 * Gets the current offset on the characters bounding sphere
	 */
	const Vector3D&		GetBoundOffset() const;
	/**
	 * Gets the characters current bahaviour
	 */
	const char*			GetBehaviourID() const;
	/**
	 * Returns the type of character this is
	 */
	const char*			GetType() const;
	/**
	 * Sets the type of character this is
	 */
	void				SetType(
							const char* type
							);
	/**
	 * Returns the mesh the chracater is using
	 */
	SkinnedMesh*		GetMesh();
	/**
	 * Gets the number of waypoints the character has assoicated with it
	 */
	int					GetWayPointCount() const;
	/**
	 * Gets the waypoint at specified index
	 */
	const WayPoint*		GetWayPoint(int index) const;
	/**
	 * Gets the waypoint they are currently moving from
	 */
	const WayPoint*		GetSourceWayPoint() const;
	/**
	 * Gets the waypoint they are currently moving to
	 */
	const WayPoint*		GetTargetWayPoint() const;
	/**
	 * Lets the character know its arrived at a waypoint so it can move
	 * onto its next waypoint
	 */
	void				ArrivedAtWaypoint();
	/**
	 * Marks the character as seleected or not
	 */
	void				Select(
							bool select
							);
	/**
	 * Sets the characters current velocity
	 */
	void				SetVelocity(
							const Vector3D& vel
							);
	/**
	 * Gets the characters current velocity
	 */
	const Vector3D&		GetVelocity() const;
	/**
	 * Returns the number of times the character has been hit
	 */
	unsigned int		GetHitCount() const;
	/**
	 * Returns the bounding sphere of the character
	 */
	Sphere*				GetBoundingSphere() const;
	
	// returns bounding sphere list
	std::vector<PerBoneShere> GetBoundingSphereList() const;
	
	/*
	 * Returns if the character is dead or not
	 */
	bool				IsDead() const {return m_isDead;}
	/**
	 * Returns if the character is dead or not
	 */
	void				SetDead(
							bool dead
							){m_isDead = dead;}
	/**
	 * Returns if the character is the player or not
	 */
	bool				IsPlayer() const {return m_isPlayer;}
	/**
	 * Sets how much damage the character does to the player
	 */
	void				SetDamageToPlayer(
							int damage
							){m_damageValue = damage;}
	/**
	 * Returns how much damage is caused by the character to the player
	 */
	int					GetDamageToPlayer() const {return m_damageValue;}
	/**
	 * Sets the character to flash or not
	 */
	void				SetFlash(
							bool flash
							){m_flash = flash;}
	/**
	 * Set the character visisble or not
	 */
	void				SetVisible(
							bool vis
							){m_isVisible = vis;}
	/**
	 * Sets if the character can damage the player
	 */
	void				SetCanDamagePlayer(
							bool damage
							){m_canDamagePlayer = damage;}
	/**
	 * Returns if the character can damage the player
	 */
	bool				GetCanDamagePlayer() const {return m_canDamagePlayer;}
	/**
	 * Sets the speed of the character
	 */
	void				SetSpeed(
							float speed
							){m_speed = speed;}
	/**
	 * Returns the speed of the character
	 */
	float				GetSpeed() const {return m_speed;}
	/**
	 * Returns the path of the character
	 */
	AStarPath&			GetPath() {return m_path;}
	/**
	 * Gets the current AStar path node of the character
	 */
	AStarNode*			GetCurrentNode() const { return m_currentNode; }
	/**
	 * Gets the target AStar path node of the character
	 */
	AStarNode*			GetTargetNode() const { return m_targetNode; }
	/**
	 * Gets the last AStar path node in their path which the character can see
	 */
	AStarNode*			GetLastPathNodeSeen() const {return m_lastPathNodeSeen;}
	/**
	 * Sets the current AStar path node of the character
	 */
	void				SetCurrentNode(
							AStarNode* node
							) { m_currentNode = node; }
	/**
	 * Sets the target AStar path node of the character
	 */
	void				SetTargetNode(
							AStarNode* node
							) { m_targetNode = node; }
	/**
	 * Sets the last AStar path node in their path which the character can see
	 */
	void				SetLastPathNodeSeen(
							AStarNode* node
							){m_lastPathNodeSeen = node;}
	/**
	 * Sets if the character is vulnarable
	 */
	void				SetVulnarability(
							bool canHarm
							){m_isVulnarable = canHarm;}
	/**
	 * Returns if the character is vulnarable
	 */
	bool				GetVulnrability() const {return m_isVulnarable;}
	/**
	 * Sets that the character needs to be deleted
	 */
	void				SetRemove(
							bool remove
							){m_removeMe = remove;}
	/**
	 * Returns if the character needs to be deleted
	 */
	bool				NeedsRemoving() const {return m_removeMe;}
	/**
	 * Sets the character fading for the specified amount of time
	 */
	void				StartFade(
							float secs
							);
	/**
	 * Returns the fade time
	 */
	float				GetFadeTime() const {return m_fadeTimer;}
	/**
	 * Sets the alpha of the character
	 */
	void				SetAlpha(
							float alpha
							){m_alpha = alpha;}

	// stores bounding spheres in list
	void				AddBoundingSphere(LPD3DXFRAME frame);

	void SetDestination(Vector3D destination);

	Vector3D GetDestination() const { return m_destination; }

private:
	MyString m_Type;
	MyString m_ID;
	SkinnedMesh* m_meshPtr;
	AnimationInstance* m_aniCon;

	Sphere* m_boundingSphere;
	std::vector<PerBoneShere> m_boundingSphereList; // list of bone Sphere structs for each bone
	float m_scale;

	Vector3D m_position;
	Vector3D m_rotation;
	Vector3D m_velocity;
	Vector3D m_destination;
	float m_speed;
	float m_alpha;
	AStarPath m_path;
	AStarNode* m_lastPathNodeSeen;
	AStarNode* m_currentNode;
	AStarNode* m_targetNode;

	unsigned int m_hitCount;
	bool m_isVulnarable;

	bool m_canDamagePlayer;
	
	float m_time;

	float m_fadeTimer;
	float m_maxFadeTime;

	bool m_isPlayer;

	MyString m_currentBehavior;
	const WayPoint* m_sourceWaypoint;
	const WayPoint* m_targetWaypoint;

	int m_waypointTracker;

	static const int c_MaxWayPointCount = 64;
	const WayPoint* m_waypointList[c_MaxWayPointCount];
	int m_numWaypoints;


	Vector3D m_boundPosition;
	float m_boundRadius;
	Vector3D m_boundPositionOffset;
	bool	m_selected;
	bool   m_flash;
	float	m_flashCounter;
	bool m_isDead;
	bool m_removeMe;
	bool m_isVisible;
	int m_damageValue;
};

#endif //_CHARACTER_H_
