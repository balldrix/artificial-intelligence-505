#include "Types.h"
#include "PolygonData.h"
#include "Line.h"
#include "Model.h"
#include "NavigationMeshNode.h"
#include "NavigationMesh.h"

/*****************************************************************/

NavigationMesh::NavigationMesh()
{
	Init();
}

/*****************************************************************/


NavigationMesh::~NavigationMesh()
{
	ClearSearchTree(&m_rootSearchTreeNode);

	LinkedListNode<NavigationMeshNode*>* meshNodeListNode = m_meshNodes.GetFirst();
	while (meshNodeListNode)
	{
		delete meshNodeListNode->GetData()->GetPoly();
		delete meshNodeListNode->GetData();
		meshNodeListNode = meshNodeListNode->GetNext();
	}
	m_meshNodes.Clear();
}

/*****************************************************************/

void
NavigationMesh::Init()
{
	for (int index = 0; index < 4; ++index)
	{
		m_rootSearchTreeNode.childNodes[index] = NULL;
	}

	m_rootSearchTreeNode.meshNodes = NULL;
}

/*****************************************************************/

void 
NavigationMesh::AddModelToNavigationMesh(Model* model, const Matrix& position)
{
	Mesh* mesh = model->GetMesh();
	int navmeshSubset = model->GetNavmeshSubset();

	BYTE* pVerts;
	HRESULT hr = mesh->LockVertexBuffer(D3DLOCK_READONLY, (void**)&pVerts);
	if (hr != S_OK)
	{
		ErrorMessage("LockVertexBuffer Failed");
	}

	WORD* ppData;
	hr = mesh->LockIndexBuffer(D3DLOCK_READONLY, (void**)&ppData);
	if (hr != S_OK)
	{
		ErrorMessage("LockIndexBufferFailed");
	}

	D3DVERTEXELEMENT9 Decl[MAX_FVF_DECL_SIZE];
	hr = mesh->GetDeclaration(Decl);
	if(hr != S_OK)
	{
		ErrorMessage("GetDeclaration Failed");
	}
	UINT step = D3DXGetDeclVertexSize(Decl,0);


	DWORD* pAttr;
	hr = mesh->LockAttributeBuffer(D3DLOCK_READONLY, &pAttr);
	if(hr != S_OK)
	{
		ErrorMessage("LockAttributeBuffer Failed");
	}

	DWORD faces = mesh->GetNumFaces();

	for (DWORD i = 0 ; i < faces; ++i)
	{
		if (navmeshSubset == -1 || pAttr[i] == navmeshSubset)
		{
			Vector3D vecA = *(Vector3D*)(pVerts+ppData[i*3]*step);
			Vector3D vecB = *(Vector3D*)(pVerts+ppData[i*3+1]*step);
			Vector3D vecC = *(Vector3D*)(pVerts+ppData[i*3+2]*step);

			Vector3DTransformCoord(&vecA, &vecA, &position);
			Vector3DTransformCoord(&vecB, &vecB, &position);
			Vector3DTransformCoord(&vecC, &vecC, &position);

			PolygonData* p = new PolygonData();
			p->SetVerts(vecA, vecB, vecC);

			// Only add navigation polys that aren't too steep
			if (p->GetNormal().y > 0.7f)
			{
				NavigationMeshNode* meshNode = new NavigationMeshNode(p);
				AddNode(meshNode);
			}
			else
			{
				delete p;
			}
		}
	}

	mesh->UnlockAttributeBuffer();
	mesh->UnlockIndexBuffer();
	mesh->UnlockVertexBuffer();
}

/*****************************************************************/

void
NavigationMesh::CreateSearchTree()
{
	ClearSearchTree(&m_rootSearchTreeNode);
	MakeSearchTree(&m_rootSearchTreeNode, m_box, m_meshNodes, 8);
}

/*****************************************************************/

NavigationMeshNode*
NavigationMesh::FindNode(const Vector3D& position, NavigationMeshNode* hint)
{
	Vector3D start(position.x, m_box.GetMax().y + 1.0f, position.z);
	Vector3D end(position.x, m_box.GetMin().y - 1.0f, position.z);
	Vector3D thisIntersectionPoint;

	if (hint && LinePolyTest(start, end, *hint->GetPoly(), thisIntersectionPoint))
	{
		return hint;
	}
 	else
	{
		AABB searchBoundingBox(end, start);

		MyLinkedList<NavigationMeshNode*> meshNodes;
		BuildOctreeNodeList(&m_rootSearchTreeNode, m_box, searchBoundingBox, meshNodes);

		NavigationMeshNode* bestNode = NULL;
		float bestDistance = -FLT_MAX;

		LinkedListNode<NavigationMeshNode*>* listNode = meshNodes.GetFirst();
		
		while (listNode)
		{
			if(LinePolyTest(start, end, *listNode->GetData()->GetPoly(), thisIntersectionPoint))
			{
				float distance = position.y + 2.0f - thisIntersectionPoint.y;
				if (bestDistance < 0.0f)
				{
					if (distance > bestDistance)
					{
						bestDistance = distance;
						bestNode = listNode->GetData();
					}
				}
				else
				{
					if (distance > 0.0f && distance < bestDistance)
					{
						bestDistance = distance;
						bestNode = listNode->GetData();
					}
				}
			}

			listNode = listNode->GetNext();
		}

		return bestNode;
	}
}

/*****************************************************************/

bool
NavigationMesh::LinePolyTest(const Vector3D& LA, const Vector3D& LB, const PolygonData& poly, Vector3D& intersectionPoint)
{
	Vector3D dir = LB - LA;
	
	// Find vectors for two edges sharing vert0
	Vector3D edge1 = poly.GetVert(1) - poly.GetVert(0);
	Vector3D edge2 = poly.GetVert(2) - poly.GetVert(0);

	// Begin calculating determinant - also used to calculate U parameter
	Vector3D pvec;
	Vector3DCrossProduct(&pvec, &dir, &edge2);

	// If determinant is near zero, ray lies in plane of triangle
	float det = Vector3DDotProduct(&edge1, &pvec);

	Vector3D tvec;

	if( det > 0.f )
	{
		tvec = LA - poly.GetVert(0);
	}
	else
	{
		tvec = poly.GetVert(0) - LA;
		det = -det;
	}

	if (det < 0.0001f)
	{
		return false;
	}

	// Calculate U parameter and test bounds
	float u = Vector3DDotProduct(&tvec, &pvec);
	if (u < 0.0f || u > det)
	{	
		return false;
	}

	// Prepare to test V parameter
	Vector3D qvec;
	Vector3DCrossProduct(&qvec, &tvec, &edge1);

	// Calculate V parameter and test bounds
	float v = Vector3DDotProduct(&dir, &qvec);
	if (v < 0.0f || u + v > det)
	{
		return false;
	}
	
	// Calculate t, scale parameters, ray intersects triangle
	float t = Vector3DDotProduct(&edge2, &qvec);

	if (t < 0.0f || t > det)
	{	
		return false;
	}

	intersectionPoint = LA + dir * t / det;

	return true;
}

/*****************************************************************/

void
NavigationMesh::MakeSearchTree(SearchTreeNode* searchTreeNode, const AABB& boundingBox, const MyLinkedList<NavigationMeshNode*>& meshNodes, int levels)
{
	int numEntries = 0;

	LinkedListNode<NavigationMeshNode*>* listNode = meshNodes.GetFirst();
	while (listNode)
	{
		numEntries++;
		listNode = listNode->GetNext();
	}

	if (levels == 0 || numEntries < 5)
	{
		// This is a leaf node so copy the mesh node list into the node
		searchTreeNode->meshNodes = new MyLinkedList<NavigationMeshNode*>;

		LinkedListNode<NavigationMeshNode*>* listNode = meshNodes.GetFirst();
		while (listNode)
		{
			searchTreeNode->meshNodes->Insert(listNode->GetData());
			listNode = listNode->GetNext();
		}

		for (int regionIndex = 0; regionIndex < 4; ++regionIndex)
		{
			searchTreeNode->childNodes[regionIndex] = NULL;
		}
	}
	else
	{
		MyLinkedList<NavigationMeshNode*> subList;

		for (int regionIndex = 0; regionIndex < 4; ++regionIndex)
		{
			subList.Clear();

			AABB subBoundingBox;
			GetSubRegion(boundingBox, regionIndex, subBoundingBox);

			numEntries = 0;

			LinkedListNode<NavigationMeshNode*>* listNode = meshNodes.GetFirst();
			while (listNode)
			{
				if (IsTriangleInBox(listNode->GetData()->GetPoly(), subBoundingBox))
				{
					subList.Insert(listNode->GetData());
					numEntries ++;
				}

				listNode = listNode->GetNext();
			}

			if (numEntries)
			{
				searchTreeNode->childNodes[regionIndex] = new SearchTreeNode;
				searchTreeNode->childNodes[regionIndex]->meshNodes = NULL;
				MakeSearchTree(searchTreeNode->childNodes[regionIndex], subBoundingBox, subList, levels-1);
			}
			else
			{
				searchTreeNode->childNodes[regionIndex] = NULL;
			}
		}
	}
}

/*****************************************************************/

void
NavigationMesh::ClearSearchTree(SearchTreeNode* searchTreeNode)
{
	if (searchTreeNode->meshNodes)
	{
		delete searchTreeNode->meshNodes;
		searchTreeNode->meshNodes = NULL;
	}

	for (int regionIndex = 0; regionIndex < 4; ++regionIndex)
	{
		if (searchTreeNode->childNodes[regionIndex])
		{
			ClearSearchTree(searchTreeNode->childNodes[regionIndex]);
			delete searchTreeNode->childNodes[regionIndex];
		}
	}
}

/*****************************************************************/

void
NavigationMesh::GetSubRegion(const AABB& boundingBox, int regionIndex, AABB& subBoundingBox)
{
	Vector3D min, max;

	if (regionIndex & 1)
	{
		min.x = (boundingBox.GetMin().x + boundingBox.GetMax().x) / 2;
		max.x = boundingBox.GetMax().x;
	}
	else
	{
		min.x = boundingBox.GetMin().x;
		max.x = (boundingBox.GetMin().x + boundingBox.GetMax().x) / 2;
	}

	if (regionIndex & 2)
	{
		min.z = (boundingBox.GetMin().z + boundingBox.GetMax().z) / 2;
		max.z = boundingBox.GetMax().z;
	}
	else
	{
		min.z = boundingBox.GetMin().z;
		max.z = (boundingBox.GetMin().z + boundingBox.GetMax().z) / 2;
	}

	min.y = boundingBox.GetMin().y;
	max.y = boundingBox.GetMax().y;

	subBoundingBox.SetMin(min);
	subBoundingBox.SetMax(max);
}

/*****************************************************************/

bool
NavigationMesh::IsTriangleInBox(const PolygonData* triangle, const AABB& boundingBox)
{
	AABB box(triangle);

	return boundingBox.CheckCollisionAABB(box);
}

/*****************************************************************/

void
NavigationMesh::BuildOctreeNodeList(const SearchTreeNode* searchTreeNode, const AABB& boundingBox, 
									const AABB& searchBoundingBox, MyLinkedList<NavigationMeshNode*>& polygons)
{
	if (boundingBox.CheckCollisionAABB(searchBoundingBox))
	{
		if (searchTreeNode->meshNodes)
		{
			LinkedListNode<NavigationMeshNode*>* listNode = searchTreeNode->meshNodes->GetFirst();

			while (listNode)
			{
				bool inList = false;

				LinkedListNode<NavigationMeshNode*>* polyNode = polygons.GetFirst();

				NavigationMeshNode* navMeshNode = listNode->GetData();

				while (polyNode)
				{
					if (polyNode->GetData() == navMeshNode)
					{
						inList = true;
						break;
					}
					polyNode = polyNode->GetNext();
				}

				if (!inList)
				{
					polygons.Insert(navMeshNode);
				}

				listNode = listNode->GetNext();
			}
		}
		else
		{
			for (int regionIndex = 0; regionIndex < 4; ++regionIndex)
			{
				if (searchTreeNode->childNodes[regionIndex])
				{
					AABB subBoundingBox;
					GetSubRegion(boundingBox, regionIndex, subBoundingBox);
					BuildOctreeNodeList(searchTreeNode->childNodes[regionIndex], subBoundingBox, searchBoundingBox, polygons);
				}
			}
		}
	}
}

/*****************************************************************/

void
NavigationMesh::SetID(const char* id)
{
	m_id = id;
}

/*****************************************************************/

const char* 
NavigationMesh::GetID() const
{
	return m_id.GetPointer();
}

/*****************************************************************/

void
NavigationMesh::SetFileName(const char* name)
{
	m_fileName = name;
}

/*****************************************************************/

const char*
NavigationMesh::GetFileName() const
{
	return m_fileName;
}

/*****************************************************************/

void
NavigationMesh::AddNode(NavigationMeshNode* meshNode)
{
	LinkedListNode<NavigationMeshNode*>* meshNodeListNode = m_meshNodes.GetFirst();
	while (meshNodeListNode)
	{
		NavigationMeshNode* otherMeshNode = meshNodeListNode->GetData();

		int ourEdge, theirEdge;

		if (meshNode->GetCommonEdge(otherMeshNode, ourEdge, theirEdge))
		{
			meshNode->SetNeighbour(otherMeshNode, ourEdge);
			otherMeshNode->SetNeighbour(meshNode, theirEdge);
		}

		meshNodeListNode = meshNodeListNode->GetNext();
	}

	for (int index = 0; index < 3; ++index)
	{
		m_box.AddPoint(meshNode->GetPoly()->GetVert(index));
	}

	m_meshNodes.Insert(meshNode);
}

/*****************************************************************/


bool
NavigationMesh::LineOfSightCheck(const Vector3D& startPosition3D, NavigationMeshNode* currentNode, const Vector3D& endPosition3D, NavigationMeshNode* endNode)
{
	Vector2D startPos(startPosition3D.x, startPosition3D.z);
	Vector2D endPos(endPosition3D.x, endPosition3D.z);

	NavigationMeshNode* previousNode = NULL;

	bool result = (currentNode == endNode);

	while (currentNode && currentNode != endNode)
	{
		NavigationMeshNode* neighbourNode = NULL;
		bool found = false;
		float bestPathRatio = -1.0f;

		for (int neighbourIndex = 0; neighbourIndex < 3; ++neighbourIndex)
		{
			// If this neighbour is the previous node...
			if (previousNode && currentNode->GetNeighbour(neighbourIndex) == previousNode)
			{
				// ...skip it because we don't want to go back on ourselves
				continue;
			}

			const Vector3D& vertexA3D = currentNode->GetPoly()->GetVert(neighbourIndex);
			const Vector3D& vertexB3D = currentNode->GetPoly()->GetVert((neighbourIndex+1)%3);

			Vector2D vertA(vertexA3D.x, vertexA3D.z);
			Vector2D vertB(vertexB3D.x, vertexB3D.z);

			float pathRatio, neighbourRatio;
			if (DoLinesIntersect(vertA, vertB, startPos, endPos, false, true, &neighbourRatio, &pathRatio))
			{
				if (!found || pathRatio > bestPathRatio)
				{
					found = true;
					bestPathRatio = pathRatio;
					neighbourNode = static_cast<NavigationMeshNode*>(currentNode->GetNeighbour(neighbourIndex));
				}
			}
		}

		if (!neighbourNode)
		{
			break;
		}

		previousNode = currentNode;
		currentNode = neighbourNode;

		if (currentNode == endNode)
		{
			result = true;
		}
	}

	return result;
}

/*****************************************************************/

bool
NavigationMesh::DoLinesIntersect(const Vector2D& lineP1, const Vector2D& lineP2, const Vector2D& lineQ1, const Vector2D& lineQ2, bool infiniteP, bool infiniteQ, float* ratioP, float* ratioQ)
{
	float AxMinusBx = lineP1.x - lineP2.x;
	float AxMinusCx = lineP1.x - lineQ1.x;
	float AyMinusCy = lineP1.y - lineQ1.y;
	float BxMinusAx = lineP2.x - lineP1.x;
	float ByMinusAy = lineP2.y - lineP1.y;
	float DxMinusCx = lineQ2.x - lineQ1.x;
	float DyMinusCy = lineQ2.y - lineQ1.y;

	float denominatorEquationOne = (BxMinusAx * DyMinusCy) - (ByMinusAy * DxMinusCx);
	float numeratorEquationOne = (AyMinusCy * DxMinusCx) - (AxMinusCx * DyMinusCy);
	float numeratorEquationTwo = (AyMinusCy * BxMinusAx) - (AxMinusCx * ByMinusAy);

	if (denominatorEquationOne != 0.0f)
	{
		float ratioOne = numeratorEquationOne / denominatorEquationOne;
		float ratioTwo = numeratorEquationTwo / denominatorEquationOne;

		if ((infiniteP || (ratioOne >= 0.0f && ratioOne <= 1.0f)) && (infiniteQ || (ratioTwo >= 0.0f && ratioTwo <= 1.0f)))
		{
			if (ratioP)
			{
				(*ratioP) = ratioOne;
			}
			if (ratioQ)
			{
				(*ratioQ) = ratioTwo;
			}

			return true;
		}
	}

	return false;
}

/*****************************************************************/

void
NavigationMesh::DebugRender(const Vector3D& focus)
{
	LinkedListNode<NavigationMeshNode*>* listNode = m_meshNodes.GetFirst();
	while (listNode)
	{
		const PolygonData* poly = listNode->GetData()->GetPoly();
		const Vector3D& centre = listNode->GetData()->GetCentre();
		if (Vector3DLength(&(centre-focus)) < 20.0f)
		{
			for (int index = 0; index < 3; ++index)
			{
				Line l(poly->GetVert(index), poly->GetVert((index+1)%3), 0xFFFFFF00, 0xFFFFFF00);
				l.Render();
				NavigationMeshNode* neighbour = listNode->GetData()->GetNeighbour(index);
				if (neighbour)
				{
					Line n(centre, neighbour->GetCentre()+Vector3D(0.0f, 0.2f, 0.0f), 0xFFFF0000, 0xFF00FF00);
					n.Render();
				}
			}
		}

		listNode = listNode->GetNext();
	}
}

/*****************************************************************/
