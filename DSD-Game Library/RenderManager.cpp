#include "Types.h"
#include "RenderEvent.h"
#include "RenderManager.h"
#include "EffectManager.h"
#include "Effect.h"
#include "ShadowManager.h"
#include "CameraController.h"
#include "Sphere.h"
#include "Line.h"
#include "Box.h"

#include "CharacterManager.h"
#include "Character.h"
#include "NavigationMesh.h"
#include "NavigationMeshNode.h"
#include "EnvironmentManager.h"
#include "AStar.h"

/************************************************************************/

DefineSingleton(RenderManager);

/************************************************************************/

RenderManager::RenderManager() :
	m_firstEvent(NULL)
{
}

/************************************************************************/

RenderManager::~RenderManager()
{
	m_debugSphere.Clear();
	m_debugLines.Clear();
	m_debugBoxes.Clear();
}

/************************************************************************/

void
RenderManager::AddEvent(RenderEvent* e, RenderEvent* parent)
{
	if (parent)
	{
		if (parent->m_firstChild)
		{
			RenderEvent* sibling = parent->m_firstChild;
			while (sibling->m_nextSibling)
			{
				sibling = sibling->m_nextSibling;
			}

			sibling->m_nextSibling = e;
		}
		else
		{
			parent->m_firstChild = e;
		}
	}
	else
	{
		if (m_firstEvent)
		{
			RenderEvent* sibling = m_firstEvent;
			while (sibling->m_nextSibling)
			{
				sibling = sibling->m_nextSibling;
			}

			sibling->m_nextSibling = e;
		}
		else
		{
			m_firstEvent = e;
		}

	}
}

/************************************************************************/

void
RenderManager::RemoveEvent(RenderEvent* e)
{
	if (m_firstEvent)
	{
		if (m_firstEvent == e)
		{
			m_firstEvent = NULL;
		}
		else
		{
			RemoveEvent(e, m_firstEvent);
		}
	}
}

/************************************************************************/

void
RenderManager::RemoveEvent(RenderEvent* e, RenderEvent* searchPosition)
{
	if (searchPosition->m_firstChild)
	{
		if (searchPosition->m_firstChild == e)
		{
			searchPosition->m_firstChild = e->m_nextSibling;
		}
		else
		{
			RemoveEvent(e, searchPosition->m_firstChild);
		}
	}

	if (searchPosition->m_nextSibling)
	{
		if (searchPosition->m_nextSibling == e)
		{
			searchPosition->m_nextSibling = e->m_nextSibling;
		}
		else
		{
			RemoveEvent(e, searchPosition->m_nextSibling);
		}
	}
}

/************************************************************************/

void
RenderManager::RenderShadowMap()
{
	if (m_firstEvent)
	{
		//Firstly Begin the shadow map pass, this will create a texture of the depth buffer
		ShadowManager::GetInstance()->SetUpForShadowMapDepthRead();
		UINT numPasses = 0;
		UINT numPass1 = 0;
		//begin effect
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->Begin(&numPass1,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->BeginPass(0);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->SetActive(true);
		{
			//Render the environment
			RecursiveRender(m_firstEvent, RenderEvent::EnvironmentShadowMap);
		}
		
		//end the shader
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->SetActive(false);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->End();


		//Render the characters in there currect animations, so we need to use the technique with utilises the skinning matricies
		EffectSetAndCacheHandle0(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP), SetTechnique, "mTech2");	

		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->Begin(&numPass1,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->BeginPass(0);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->SetActive(true);
		{
			RecursiveRender(m_firstEvent, RenderEvent::ShadowMapRender);
		}

		//end the shader
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->SetActive(false);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->End();

		//set up the shadow map shader
		ShadowManager::GetInstance()->SetUpForShadowMapRenderToScene();
	}
}

/************************************************************************/

void
RenderManager::Render()
{
	if (m_firstEvent)
	{
		UINT numPasses = 1;
		UINT numPass1 = 1;

		//Begin the scene render of the shadowmap shader
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->Begin(&numPass1,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->BeginPass(0);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->SetActive(true);
		{
			//Render the environment

			RecursiveRender(m_firstEvent, RenderEvent::EnvironmentShadowMap);

		}

		//end the shader
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->SetActive(false);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->End();

		numPasses = 0;

		//Begin Billboard Shader and render pass
		Matrix world;
		MatrixIdentity(&world);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD), SetMatrix, "m_WVP", world * CameraController::GetInstance()->GetView() * CameraController::GetInstance()->GetProjection());
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD), SetVector3, "m_EyePosition", CameraController::GetInstance()->GetCurrentCameraPosition());
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->GetEffect()->CommitChanges();
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->GetEffect()->Begin(&numPasses,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->GetEffect()->BeginPass(0);
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->SetActive(true);

		RecursiveRender(m_firstEvent, RenderEvent::BillboardEffect);

		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->GetEffect()->End();
		EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD)->SetActive(false);
		
		numPasses = 0;
		//begin Character (Skinned) Shader pass
		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->SetActive(true);
		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->GetEffect()->Begin(&numPasses,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->GetEffect()->BeginPass(0);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetVector3, "m_EyePosition", CameraController::GetInstance()->GetCurrentCameraPosition());

		RecursiveRender(m_firstEvent, RenderEvent::Skinned);

		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->GetEffect()->End();
		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->SetActive(false);
		
		//begin the fog shader pass
		EffectManager::GetInstance()->GetEffect(EffectManager::FOG)->GetEffect()->Begin(&numPasses,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::FOG)->GetEffect()->BeginPass(0);
		EffectManager::GetInstance()->GetEffect(EffectManager::FOG)->SetActive(true);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::FOG), SetVector3, "m_EyePosition", CameraController::GetInstance()->GetCurrentCameraPosition());
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::FOG), SetMatrix, "m_View", CameraController::GetInstance()->GetView());
		RecursiveRender(m_firstEvent, RenderEvent::NoEffects);
		RecursiveRender(m_firstEvent, RenderEvent::UIEffect);
		EffectManager::GetInstance()->GetEffect(EffectManager::FOG)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::FOG)->GetEffect()->End();
		EffectManager::GetInstance()->GetEffect(EffectManager::FOG)->SetActive(false);

		//begin the particle shader pass
		EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE)->GetEffect()->Begin(&numPasses,0);
		EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE)->GetEffect()->BeginPass(0);
		EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE)->SetActive(true);

		RecursiveRender(m_firstEvent, RenderEvent::ParticleEffect);

		EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE)->SetActive(false);
		EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE)->GetEffect()->EndPass();
		EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE)->GetEffect()->End();

		
		m_D3DDevice->SetVertexShader(NULL);
		m_D3DDevice->SetPixelShader(NULL);
	
		//RenderNavigationDebug();
		LinkedListNode<Sphere*>* sphereNode = m_debugSphere.GetFirst();

		while (sphereNode)
		{
			sphereNode->GetData()->Render();
			sphereNode = sphereNode->GetNext();
		}

		LinkedListNode<Line*>* lineNode = m_debugLines.GetFirst();

		while (lineNode)
		{
			lineNode->GetData()->Render();
			lineNode = lineNode->GetNext();
		}

		LinkedListNode<Box*>* boxNode = m_debugBoxes.GetFirst();

		while (boxNode)
		{
			boxNode->GetData()->Render();
			boxNode = boxNode->GetNext();
		}
	}
}

/************************************************************************/

void
RenderManager::RecursiveRender(RenderEvent* e, RenderEvent::RenderFlag renderFlag)
{
	m_current = renderFlag;

	while (e)
	{
		if (e->GetFlag(renderFlag))
		{
			e->Render();
		}

		if (e->m_firstChild)
		{
			RecursiveRender(e->m_firstChild, renderFlag);
		}

		e = e->m_nextSibling;
	}
}

/************************************************************************/

const RenderEvent::RenderFlag 
RenderManager::CurrentRenderFlag() const
{
	return m_current;
}

/************************************************************************/

void 
RenderManager::AddSphere(Sphere* sphere)
{
	LinkedListNode<Sphere*>* node = m_debugSphere.GetFirst();

	while (node)
	{
		if (node->GetData() == sphere)
		{
			/// Already in list
			return;
		}

		node = node->GetNext();
	}

	m_debugSphere.Insert(sphere);
}

/************************************************************************/

void
RenderManager::RemoveSphere(Sphere* sphere)
{
	m_debugSphere.Remove(sphere);
}

/************************************************************************/

void
RenderManager::AddLine(Line* line)
{
	LinkedListNode<Line*>* node = m_debugLines.GetFirst();

	while (node)
	{
		if (node->GetData() == line)
		{
			/// Already in list
			return;
		}

		node = node->GetNext();
	}

	m_debugLines.Insert(line);
}

/************************************************************************/

void
RenderManager::RemoveLine(Line* line)
{
	m_debugLines.Remove(line);
}

/************************************************************************/

void
RenderManager::AddBox(Box* box)
{
	LinkedListNode<Box*>* node = m_debugBoxes.GetFirst();

	while (node)
	{
		if (node->GetData() == box)
		{
			/// Already in list
			return;
		}

		node = node->GetNext();
	}

	m_debugBoxes.Insert(box);
}

/************************************************************************/

void
RenderManager::RemoveBox(Box* box)
{
	m_debugBoxes.Remove(box);
}

/************************************************************************/

void
RenderManager::RenderNavigationDebug()
{
	EnvironmentManager::GetInstance()->GetNavigationMesh()->DebugRender(CharacterManager::GetInstance()->GetCharacter(0)->GetPosition());

	int numChars = CharacterManager::GetInstance()->GetNumberCharacters();
	for (int index = 0; index < numChars; ++index)
	{
		Character* character = CharacterManager::GetInstance()->GetCharacter(index);
		AStarPath& path = character->GetPath();
		LinkedListNode<AStarNode*>* listNode = path.GetFirst();
		while (listNode && listNode->GetNext())
		{
			Vector3D end = (listNode->GetNext() == path.GetLast() && character->GetTargetWayPoint()) ? character->GetTargetWayPoint()->m_Position : static_cast<NavigationMeshNode*>(listNode->GetNext()->GetData())->GetCentre();
			Line l(static_cast<NavigationMeshNode*>(listNode->GetData())->GetCentre(), end, 0xFFFFFFFF, 0xFFFFFFFF);
			l.Render();
			listNode = listNode->GetNext();
		}

		if (path.GetFirst())
		{
			LinkedListNode<AStarNode*>* lastSeen = path.GetFirst();

			if (character->GetLastPathNodeSeen())
			{
				while (lastSeen && lastSeen->GetData() != character->GetLastPathNodeSeen())
				{
					lastSeen = lastSeen->GetNext();
				}
			}
			
			NavigationMeshNode* lastSeenData = static_cast<NavigationMeshNode*>(lastSeen->GetData());
			Vector3D wayPoint = lastSeen->GetNext() ? lastSeenData->GetJoinPosition(lastSeen->GetNext()->GetData()) : (character->GetTargetWayPoint() ? character->GetTargetWayPoint()->m_Position : lastSeenData->GetCentre());
			Line s(character->GetPosition()+Vector3D(0.0f, 0.2f, 0.0f), wayPoint+Vector3D(0.0f, 0.2f, 0.0f), 0xFF00FFFF, 0xFF00FFFF);
			s.Render();
		}
	}
}

/************************************************************************/