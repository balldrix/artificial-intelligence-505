
#ifndef _UISPRITE_H_
#define _UISPRITE_H_

#ifndef _UIOBJECT_H_
#include "UIObject.h"
#endif //#ifndef _UIOBJECT_H_

class UISprite : public UIObject
{
public:
	/**
	 * Constructor
	 */
						UISprite();
	/**
	 * Destrcutor
	 */
	virtual				~UISprite();
	/**
	 * Loads the specified image into the sprite
	 */
	void				Load(
							const char* fileName
							);
	/**
	 * Returns the UI object type
	 */
	virtual const char* GetType(){return "SPRITE";}
	/**
	 * Updates the UI sprite
	 */
	virtual void		Update(
							float elapsedTime
							);
	/**
	 * Renders the sprite
	 */
	virtual void		Render();
	/**
	 * Sets the display width
	 */
	void				SetWidth(
							float width
							){m_width = width;}
	/**
	 * Sets the display height
	 */
	void				SetHeight(
							float height
							){m_height = height;}
	/**
	 * Sets the image to be repeated
	 */
	void				SetRepeated(
							bool repeated
							){m_repeated = repeated;}
private:
	float m_width;
	float m_height;
	MyString m_spriteTexture;
	bool m_repeated;
	int m_textureId;
};

#endif //#ifndef _UISPRITE_H_