#include "Types.h"
#include "Model.h"
#include "Environment.h"
#include "RenderManager.h"
#include "EffectManager.h"
#include "Effect.h"
#include "CameraController.h"
#include "BaseCamera.h"

/************************************************************************/

Environment::Environment() :
	m_model(0)
{
	m_flags = AmibentEnvironment;
	m_flags |= EnvironmentShadowMap;
	m_flags |= EnvironmentShadowVolume;
}

/************************************************************************/

Environment::~Environment()
{
	if (m_model)
	{
		delete m_model;
	}

	RenderManager::GetInstance()->RemoveEvent(this);
}

/************************************************************************/

void
Environment::LoadEnvironment(const char* ID, const char* fileName, const Vector3D& pos, const Vector3D& rot, float scale)
{
	m_ID = ID;
	m_model = new Model(fileName,pos,rot,scale);
	RenderManager::GetInstance()->AddEvent(this, NULL);
}

/************************************************************************/

void
Environment::SetUVMulitply(int UVM)
{
	m_UVMultiply = UVM;
}

/************************************************************************/

void
Environment::Render()
{
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::NORMALMAP), SetVector3, "m_EyePosition", CameraController::GetInstance()->GetCurrentCamera()->GetPosition());
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::NORMALMAP), SetVector3, "m_LightPosition", Vector3D(0,1,0));
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::NORMALMAP), SetMatrix, "m_View", CameraController::GetInstance()->GetView());
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::NORMALMAP), SetInt, "m_UVMultiply", m_UVMultiply);

	if(RenderManager::GetInstance()->CurrentRenderFlag() == AmibentEnvironment)
	{
		m_model->Render();
	}

	if(RenderManager::GetInstance()->CurrentRenderFlag() == EnvironmentShadowMap)
	{
		m_model->RenderWithShadow();
	}

	if(RenderManager::GetInstance()->CurrentRenderFlag() == EnvironmentShadowVolume)
	{
		m_model->RenderWithShadowVolume();
	}
}

/************************************************************************/

const char* 
Environment::GetID() const
{
	return m_ID.GetPointer();
}

/************************************************************************/

void
Environment::SetID(const char* id)
{
	m_ID = id;
}

/************************************************************************/

Model*
Environment::GetModel()
{
	return m_model;
}

/************************************************************************/
