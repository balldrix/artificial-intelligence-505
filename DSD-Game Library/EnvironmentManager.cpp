#include "Types.h"
#include "Model.h"
#include "CollisionMesh.h"
#include "NavigationMesh.h"
#include "EnvironmentManager.h"
#include "Environment.h"
#include "BillBoardRegion.h"

/************************************************************************/

DefineSingleton(EnvironmentManager);

/************************************************************************/

EnvironmentManager::EnvironmentManager() :
	m_numModels(0),
	m_numBillboardRegions(0),
	m_collisionMesh(NULL),
	m_navigationMesh(NULL)
{
}

/************************************************************************/

EnvironmentManager::~EnvironmentManager()
{
	for (int index = 0; index < m_numModels; index ++)
	{
		delete m_models[index];
	}

	for (int index = 0; index < m_numBillboardRegions; index ++)
	{
		delete m_billboardRegions[index];
	}

	if (m_collisionMesh)
	{
		delete m_collisionMesh;
	}

	if (m_navigationMesh)
	{
		delete m_navigationMesh;
	}
}

/************************************************************************/

void
EnvironmentManager::AddModel(const char* ID, const char* fileName, const Vector3D& position, const Vector3D& rotation, float scale, int UVMultiply)
{
	Environment* newModel = new Environment();
	
	assert(m_numModels < c_MaxEnvironmentModels);
	m_models[m_numModels] = newModel;
	m_numModels ++;
	newModel->LoadEnvironment(ID,fileName,position,rotation,scale);
	newModel->SetUVMulitply(UVMultiply);
}

/************************************************************************/

void 
EnvironmentManager::AddCollisionMesh(const char* ID, const char* fileName)
{
	if (m_collisionMesh)
	{
		delete m_collisionMesh;
	}

	Model* temp = new Model(fileName);
	m_collisionMesh = new CollisionMesh();

	m_collisionMesh->SetID(ID);
	m_collisionMesh->SetFileName(fileName);
	Matrix mat;
	MatrixIdentity(&mat);
	m_collisionMesh->AddModelToCollisionMesh(temp, mat);

	delete temp;
}

/************************************************************************/

void 
EnvironmentManager::AddNavigationMesh(const char* ID, const char* fileName)
{
	if (m_navigationMesh)
	{
		delete m_navigationMesh;
	}

	Model* temp = new Model(fileName);
	m_navigationMesh = new NavigationMesh();

	m_navigationMesh->SetID(ID);
	m_navigationMesh->SetFileName(fileName);
	Matrix mat;
	MatrixIdentity(&mat);
	m_navigationMesh->AddModelToNavigationMesh(temp, mat);

	delete temp;
}

/************************************************************************/

bool
EnvironmentManager::TestCollision(const Vector3D& lineSrc, const Vector3D& lineDes, Vector3D& intersectionPoint)
{
	if (m_collisionMesh)
	{
		Vector3D normal;
		return m_collisionMesh->LineMeshTest(lineSrc, lineDes, intersectionPoint, normal);
	}
	else
	{
		return false;
	}
}

/************************************************************************/

bool
EnvironmentManager::TestCollision(const Vector3D& start, const Vector3D& end, float radius, Vector3D& intersectionPoint, float& intersectionTime)
{
	if (m_collisionMesh)
	{
		return m_collisionMesh->SphereSweepMeshTest(start, end, radius, intersectionPoint, intersectionTime);
	}
	else
	{
		return false;
	}
}

/************************************************************************/

void 
EnvironmentManager::Render()
{
	for (int i = 0; i < m_numModels; i++)
	{
		m_models[i]->Render();
	}
}

/************************************************************************/

void 
EnvironmentManager::AddBillBoardRegion(BillBoardRegion* billboards)
{
	assert(m_numBillboardRegions < c_MaxBillBoardRegions);
	m_billboardRegions[m_numBillboardRegions] = billboards;
	m_numBillboardRegions ++;
}

/************************************************************************/

CollisionMesh* 
EnvironmentManager::GetCollisionMesh()
{
	return m_collisionMesh;
}

/************************************************************************/

NavigationMesh*
EnvironmentManager::GetNavigationMesh()
{
	return m_navigationMesh;
}

/************************************************************************/

void
EnvironmentManager::UpdateEnvID(const char* oldName, const char* newName)
{
	for (int i = 0; i < m_numModels; i++)
	{
		if(_stricmp(oldName,m_models[i]->GetID())==0)
		{
			m_models[i]->SetID(newName);
		}
	}
}

/************************************************************************/

void
EnvironmentManager::UpdateCollisionID(const char* oldName, const char* newName)
{
	if (m_collisionMesh)
	{
		m_collisionMesh->SetID(newName);
	}
}

/************************************************************************/

int
EnvironmentManager::GetBillBoardRegionCount() const
{
	return m_numBillboardRegions;
}

/************************************************************************/

BillBoardRegion*
EnvironmentManager::GetBillBoardRegion(int index)
{
	if(index < m_numBillboardRegions)
	{
		return m_billboardRegions[index];
	}
	return NULL;
}

/************************************************************************/

void
EnvironmentManager::DeleteModel(Environment* env)
{
	for (int index = 0; index < m_numModels; index ++)
	{
		if (m_models[index] == env)
		{
			delete m_models[index];
			m_models[index] = m_models[m_numModels - 1];
			m_numModels --;
			break;
		}
	}
}

/************************************************************************/
