#ifndef _SHADOWVOLUME_H_
#define _SHADOWVOLUME_H_

#define AMBIENT 0.10f

class SpotLight;
class Model;

class ShadowVolumes
{
public:
	/** 
	 * Constrcutor
	 */
					ShadowVolumes();
	/**
	 * Destructor
	 */
					~ShadowVolumes();
	/**
	 * Generates the shadow volume mesh from the input mesh
	 */
	void			CreateShadowMesh(
						Mesh* inMesh ///< Mesh to generate the shadow volume mesh from
						);
	/**
	 * Returns the shadow volume mesh
	 */
	Mesh*			GetShadowMesh() const;
	/**
	 * Renders the shadow volume mesh example
	 */
	void			Render();
	/**
	 * Renders the shadow volume mesh example scene
	 */
	void			RenderScene();

private:

	Mesh* m_ShadowMesh;
	SpotLight* m_light;
	Model* m_tempMesh;
};

#endif _SHADOWVOLUME_H_