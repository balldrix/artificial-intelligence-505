#include "Types.h"
#include "Behavior.h"
#include "Character.h"
#include "CharacterManager.h"
#include "EnvironmentManager.h"
#include "NavigationMesh.h"
#include "NavigationMeshNode.h"
#include "CollisionMesh.h"

/*********************************************************************************************************************/

Behavior::Behavior(const char* ID) :
	m_ID(ID)
{
	m_playerCharacter =  CharacterManager::GetInstance()->GetCharacter("Player");
}

/*********************************************************************************************************************/

Behavior::~Behavior()
{
	m_playerCharacter = 0;
}

/*********************************************************************************************************************/

void 
Behavior::FollowTerrain(Character *owner, float elapsedTime)
{
	Vector3D position = owner->GetPosition();
	float radius = owner->GetBoundingSphere()->GetRadius();

	Vector3D top = position;
	top.y += 1.0f;

	Vector3D bottom = position;
	bottom.y = EnvironmentManager::GetInstance()->GetCollisionMesh()->GetBox().GetMin().y;

	Vector3D intersectionPoint;

	if (EnvironmentManager::GetInstance()->TestCollision(top, bottom, intersectionPoint))
	{
		position.y = intersectionPoint.y;
	}
	else
	{
		top.y = EnvironmentManager::GetInstance()->GetCollisionMesh()->GetBox().GetMax().y;
		if (EnvironmentManager::GetInstance()->TestCollision(top, bottom, intersectionPoint))
		{
			position.y = intersectionPoint.y;
		}
	}

	owner->SetPosition(position);
}

/*********************************************************************************************************************/

float
Behavior::AngleToTarget(Character *owner, const Vector3D& targetPos)
{
	const Vector3D& position = owner->GetPosition();

	float angle = atan2f(-(targetPos.x - position.x), -(targetPos.z - position.z));

	return angle;
}

/*********************************************************************************************************************/

float
Behavior::GetPathAngleToTarget(Character* owner, const Vector3D& inputTargetPos)
{
	Vector3D direction;
	Vector3D targetPos = inputTargetPos;
	const Vector3D& position = owner->GetPosition();
	const Vector3D& velocity = owner->GetVelocity();

	NavigationMesh* navMesh = EnvironmentManager::GetInstance()->GetNavigationMesh();
	NavigationMeshNode* currentNode = navMesh->FindNode(position, static_cast<NavigationMeshNode*>(owner->GetCurrentNode()));
	NavigationMeshNode* targetNode = navMesh->FindNode(targetPos, static_cast<NavigationMeshNode*>(owner->GetTargetNode()));
	owner->SetCurrentNode(currentNode);
	owner->SetTargetNode(targetNode);

	

	if (currentNode && targetNode && currentNode != targetNode)
	{
		AStarPath& path = owner->GetPath();

		if (path.GetFirst() == 0 || path.GetLast()->GetData() != targetNode)
		{
			AStar::GetInstance()->FindPath(currentNode, targetNode, path);
			owner->SetLastPathNodeSeen(NULL);
		}

		if (path.GetFirst())
		{
			LinkedListNode<AStarNode*>* lastSeen = path.GetFirst();
			
			if (owner->GetLastPathNodeSeen())
			{
				while (lastSeen && lastSeen->GetData() != owner->GetLastPathNodeSeen())
				{
					lastSeen = lastSeen->GetNext();
				}
			}
	
			bool found = false;
			while (lastSeen)
			{
				NavigationMeshNode* lastSeenData = static_cast<NavigationMeshNode*>(lastSeen->GetData());
				Vector3D wayPoint = lastSeen->GetNext() ? lastSeenData->GetJoinPosition(lastSeen->GetNext()->GetData()) : inputTargetPos;

				if (navMesh->LineOfSightCheck(position, currentNode, wayPoint, lastSeenData))
				{
					found = true;
					owner->SetLastPathNodeSeen(lastSeenData);
					targetPos = wayPoint;
				}
				else
				{
					break;
				}

				lastSeen = lastSeen->GetNext();
			}

			if (!found && lastSeen)
			{
				lastSeen = lastSeen->GetPrevious();

				while (lastSeen)
				{
					NavigationMeshNode* lastSeenData = static_cast<NavigationMeshNode*>(lastSeen->GetData());
					Vector3D wayPoint = lastSeen->GetNext() ? lastSeenData->GetJoinPosition(lastSeen->GetNext()->GetData()) : inputTargetPos;

					if (navMesh->LineOfSightCheck(position, currentNode, wayPoint, lastSeenData))
					{
						found = true;
						owner->SetLastPathNodeSeen(lastSeenData);
						targetPos = wayPoint;
						break;
					}

					lastSeen = lastSeen->GetPrevious();
				}
			}
		}
	}
	else
	{
		owner->GetPath().Clear();

		if (currentNode && targetNode && currentNode == targetNode)
		{
			owner->GetPath().Insert(currentNode);
			owner->SetLastPathNodeSeen(NULL);
		}
	}

	direction.x = targetPos.x - position.x;
	direction.z = targetPos.z - position.z;
	direction.y = 0;

	return atan2f(-direction.x, -direction.z);
}

/*********************************************************************************************************************/

void 
Behavior::MoveInDirection(Character* owner, float elapsedTime)
{
	float angle = owner->GetRotation().y;

	Vector3D position = owner->GetPosition();
	Vector3D velocity(-sin(angle), 0.0f, -cos(angle));
	velocity *= owner->GetSpeed();

	position += velocity * elapsedTime;

	owner->SetPosition(position);
	owner->SetVelocity(velocity);

	FollowTerrain(owner,elapsedTime);
}

/*********************************************************************************************************************/

float 
Behavior::DistanceToCharacterSq(Character* owner,const Character& character)
{
	Sphere& temp = *((Character&)character).GetBoundingSphere();
	return DistanceToTargetSq(owner,temp.GetPosition());
}

/*********************************************************************************************************************/

float 
Behavior::DistanceToTargetSq(Character* owner, const Vector3D& targetPos)
{
	Vector3D vecTo =  owner->GetBoundingSphere()->GetPosition() - targetPos;
	vecTo.y = 0.0f;
	return Vector3DDotProduct(&vecTo,&vecTo);	
}

/*********************************************************************************************************************/

TurnDirection 
Behavior::TurnTo(Character* owner, float angle, float speed, float elapsedTime)
{
	Vector3D rotation = owner->GetRotation();
	float targetAngle = angle;
	TurnDirection directionToTurn = TD_FACING;
	if( angle > rotation.y )
	{
		targetAngle -= 2 * D3DX_PI;
	}

	float fDiff = rotation.y - targetAngle;

	if(fDiff < 0.001f )
	{
		return  TD_FACING;
	}

	if( fDiff < D3DX_PI )       // cw turn
	{
		// if we're overturning
		if( rotation.y - speed * elapsedTime <= targetAngle  )
		{
			rotation.y = angle;
			directionToTurn =  TD_FACING;
		}
		else
		{
			rotation.y = float( rotation.y - speed * elapsedTime );
			directionToTurn =  TD_LEFT;
		}
	}
	else                        // ccw turn
	{
		// if we're overturning
		if( rotation.y + speed * elapsedTime - 2 * D3DX_PI >= targetAngle )
		{
			rotation.y = angle;
			directionToTurn =  TD_FACING;
		}
		else
		{
			rotation.y = float( rotation.y + speed * elapsedTime );
			directionToTurn =  TD_RIGHT;	
		}
	}
	owner->SetRotation(rotation);
	return directionToTurn;
}

/*********************************************************************************************************************/