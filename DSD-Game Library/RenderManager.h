#ifndef _RENDERMANAGER_H_
#define _RENDERMANAGER_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif

//////////////////////////////////////////////////////////////////////////
//The Render Manager Class
//
//This class is used to control the rendering of objects,
//////////////////////////////////////////////////////////////////////////

class ShadowMap;
class Sphere;
class Line;
class Box;
class ShadowMap;

class RenderManager : public Singleton<RenderManager>
{
public:
	/**
	 * Constructor
	 */
									RenderManager();
	/**
	 * Destructor
	 */
									~RenderManager();
	/**
	 * Adds an new render event listener
	 */
	void							AddEvent(
										RenderEvent* e, 
										RenderEvent* parent
										);
	/**
	 * Removes an existing render event from the tree
	 */
	void							RemoveEvent(
										RenderEvent* e
										);
	/**
	 * Render the scene
	 */
	void							Render();

	/**
	 * Render the shadow map for the scene
	 */
	void							RenderShadowMap();
	/**
	 * Render the environment
	 */
	void							RenderEnvironment();
	/**
	 * Return the flag of whats currently being rendered
	 */
	const RenderEvent::RenderFlag	CurrentRenderFlag() const;
	/**
	 * Adds a debug sphere to render
	 */
	void							AddSphere(
										Sphere* sphere
										);
	/**
	 * Removes a debug sphere to render
	 */
	void							RemoveSphere(
										Sphere* sphere
										);
	/**
	 * Add a debug line to render
	 */
	void							AddLine(
										Line* sphere
										);
	/**
	 * Removes a debug line to render
	 */
	void							RemoveLine(
										Line* sphere
										);
	/**
	 * Adds a debug box to render
	 */
	void							AddBox(
										Box* box
										);
	/**
	 * Removes a debug box
	 */
	void							RemoveBox(
										Box* box
										);
private:
	/**
	 * Recusrively looks through the tree looking the render event to remove
	 */
	void							RemoveEvent(
										RenderEvent* e, 
										RenderEvent* searchPosition
										);
	/**
	 * Recusrively looks through the tree rendering each tree which has a matching render flag
	 */
	void							RecursiveRender(
										RenderEvent* e, 
										RenderEvent::RenderFlag renderFlag
										);

	void RenderNavigationDebug();

	RenderEvent*	m_firstEvent;
	RenderEvent::RenderFlag m_current;
	MyLinkedList<Sphere*>	m_debugSphere;
	MyLinkedList<Line*>		m_debugLines;
	MyLinkedList<Box*>		m_debugBoxes;
};

#endif //_RENDERMANAGER_H_