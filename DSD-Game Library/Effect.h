#ifndef _EFFECT_H_
#define _EFFECT_H_

/// Helpers for caching off effect bind handles
#define EffectSetAndCacheHandle0(e,t,i) { static EffectHandle* handlePointerCache = e->GetHandle(i); e->t(handlePointerCache); }
#define EffectSetAndCacheHandle1(e,t,i,p1) { static EffectHandle* handlePointerCache = e->GetHandle(i); e->t(handlePointerCache,p1); }
#define EffectSetAndCacheHandle2(e,t,i,p1,p2) { static EffectHandle* handlePointerCache = e->GetHandle(i); e->t(handlePointerCache,p1,p2); }
#define EffectSetAndCacheHandle3(e,t,i,p1,p2,p3) { static EffectHandle* handlePointerCache = e->GetHandle(i); e->t(handlePointerCache,p1,p2,p3); }
#define EffectSetAndCacheHandle4(e,t,i,p1,p2,p3,p4) { static EffectHandle* handlePointerCache = e->GetHandle(i); e->t(handlePointerCache,p1,p2,p3,p4); }

struct EffectHandle
{
	D3DXHANDLE m_handle;
	MyString m_ID;
};

class Effect
{
public:
	/**
	 * Constructor
	 */
										Effect();
	/**
	 * Destructor
	 */
										~Effect();
	/**
	 * Load the effect
	 */
	HRESULT								LoadEffect(
											const char* effectID, ///< The effect id
											const char* fileName ///< Effect filename
											);
	/**
	 * Adds an handle for the technique
	 */
	void								AddEffectHandleTechnique(
											const char* ID, ///< Effect param name
											const char* variableName ///< Technique id
											);
	/**
	 * Adds an handle for effect parameter
	 */
	void								AddEffectHandleParameter(
											const char* ID, ///< Effect param name
											const char* variableName, ///< Technique param name
											bool isTexture = false ///< if the parameter is a texture
											);
	/**
	 * Returns the D3D effect
	 */
	ID3DXEffect*						GetEffect();
	/**
	 * Returns the effect id
	 */
	const char*							GetEffectID() const;
	/**
	 * Returns the effect handle for the specified id.
	 */
	EffectHandle*						GetHandle(
											const char* ID ///< Effect param name
											);
	/**
	 * Sets the current technique to use
	 */
	void								SetTechnique(
											EffectHandle* handle ///< Handle to the technique to use
											);
	/**
	 * Sets a matrix parameter
	 */
	void								SetMatrix(
											EffectHandle* handle, ///< handle to the effect parameter
											const Matrix& mat ///< Value to store
											);
	/**
	 * Set an int parameter
	 */
	void								SetInt(
											EffectHandle* handle, ///< handle to the effect parameter
											int number ///< Value to store
											);
	/**
	 * Set a float parameter
	 */
	void								SetFloat(
											EffectHandle* handle, ///< handle to the effect parameter
											float value ///< Value to store
											);
	/**
	 * Set a bool paramater
	 */
	void								SetBool(
											EffectHandle* handle, ///< handle to the effect parameter
											bool flag ///< Value to store
											);
	/**
	 * Set a Vector2 paramater
	 */
	void								SetVector2(
											EffectHandle* handle, ///< handle to the effect parameter
											const Vector2D& value ///< Value to store
											);
	/**
	 * Set a vector3 parameter
	 */
	void								SetVector3(
											EffectHandle* handle, ///< handle to the effect parameter
											const Vector3D& v ///< Value to store
											);
	/**
	 * Set a vector4 parameter
	 */
	void								SetVector4(
											EffectHandle* handle, ///< handle to the effect parameter
											const Vector4D& v ///< Value to store
											);
	/**
	 * Set a vector4 parameter, from a vector3 & float
	 */
	void								SetVector4(
											EffectHandle* handle, ///< handle to the effect parameter
											const Vector3D& v, ///< The vector 3 value
											float a ///< Value to make the 4th component
											);
	/**
	 * Set a colour parameter
	 */
	void								SetColour(
											EffectHandle* handle, ///< handle to the effect parameter
											float r, ///< The red component
											float g, ///< The green component
											float b, ///< The blue component
											float a ///< The alpha component
											);
	/**
	 * Set a texture parameter
	 */
	void								SetTexture(
											EffectHandle* handle,///< handle to the effect parameter
											LPDIRECT3DBASETEXTURE9 tex ///< The texture to store
											);
	/**
	 * Set a matrix array parameter
	 */
	void								SetMatrixArray(
											EffectHandle* handle,///< handle to the effect parameter
											Matrix* theMatrix, ///< Matrix values to store
											unsigned int numberBones ///< Number of matrices
											);
	/**
	 * Sets the effect active
	 */
	void								SetActive(
											bool active ///< if the effect is active
											);

	/**
	 *	Returns if effect is active
	 */
	bool								GetActive() const;
	/**
	 * Returns if the effect uses a texture
	 */
	bool								GetUsesTexture() const;

private:
	MyString m_effectID;
	MyLinkedList<EffectHandle*> m_handles;
	ID3DXEffect* m_effect;

	bool m_active;
	bool m_usesTexture;
};

#endif // _EFFECT_H_
