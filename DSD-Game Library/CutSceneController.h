#ifndef _CUTSCENECONTROLLER_H_
#define _CUTSCENECONTROLLER_H_

class CutScene;
class Reigon;
class Sphere;
class CutSceneController : public Singleton<CutSceneController>
{
public:
	/**
	 * Constructor
	 */
					CutSceneController();
	/**
	 * Destructor
	 */
					~CutSceneController();
	/**
	 * Loads the cutscene definition file
	 */
	void			Loader(
						const char* fileName ///< Filename of the cutscene definitions
						);
	/**
	 * Loads the object spawning definition file which defined what objects
	 * to spawn for each cutscene
	 */
	void			CharacterSpawnLoader(
						const char* fileName ///<object spawning definition filename
						);
	/**
	 * Loads the cutscene trigger region definition file
	 */
	void			LoadReigonData(
						const char* fileName ///< Filename of the trigger file
						);
	/**
	 * Kicks off the specified cutscene
	 */
	void			BeginCutScene(
						const char* sceneID ///< ID of the cutscene to start
						);
	/**
	 * Ends the current cutscene
	 */
	void			EndCutScene();
	/**
	 * Gets the cutscene with the specified id
	 */
	CutScene*		GetCutScene(
						const char* ID ///< The id of the cutscene to retreive
						);
	/**
	 * Updates the trigger regions and any currently running cutscenes.
	 */
	void			Update(
						float elapsedTime ///< The amount of elapsed time this frame
						);
	/**
	 * Returns uf a cutscene is active
	 */
	bool			IsCutSceneActive() const;
private:
	Sphere* m_playerSphere;
	CutScene* m_currentCutScene;
	MyLinkedList<CutScene*> m_cutscenes;
	MyLinkedList<Reigon*> m_reigons;
};

#endif // _CUTSCENECONTROLLER_H_
