#ifndef _REIGON_H_
#define _REIGON_H_

#ifndef _SPHERE_H_
#include "Sphere.h"
#endif //#ifndef _SPHERE_H_

class Reigon
{
public:
	/**
	 * Constructor
	 */
					Reigon(
						const char* ID, ///< The id of the region
						const char* cutsceneTrigger, ///< The cutscene to trigger from this region
						const Vector3D& position, ///< The position of the region
						float radius ///< The radius of the region
						);
	/**
	 * Destructor
	 */
					~Reigon();
	/**
	 * Returns the sphere for the region
	 */
	const Sphere&	GetSphere() const;
	/**
	 * Returns the cutscene that this region triggers
	 */
	const char*		GetCutscene() const;
private:
	MyString m_ID;
	MyString m_cutsceneToTrigger;
	Sphere m_triggerSphere;
};



#endif //_REIGON_H_