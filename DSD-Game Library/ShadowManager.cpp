#include "Types.h"
#include "ShadowManager.h"
#include "EffectManager.h"
#include "Effect.h"
#include "CameraController.h"
#include "Program.h"
#include "ShadowMesh.h"
#include "ShadowMap.h"
#include "CharacterManager.h"
#include "Character.h"
#include "TextureCache.h"
#include "EnvironmentManager.h"

/************************************************************************/

DefineSingleton(ShadowManager);

/************************************************************************/

ShadowManager::ShadowManager()
{
	m_shadowMap = new ShadowMap();
	m_shadowMap->Create();
}

/************************************************************************/

ShadowManager::~ShadowManager()
{
	delete m_shadowMap;
	m_shadowMap = 0;
}

/************************************************************************/

void
ShadowManager::SetUpForAmbientSceneRender()
{
	//Set up light positions
	Vector4D lightPosition = Vector4D(3,10,0,1.0f);
	Matrix m;
	MatrixIdentity(&m);
	D3DXVec4Transform(&lightPosition,&lightPosition,&m);
	D3DXVec4Transform(&lightPosition,&lightPosition,&CameraController::GetInstance()->GetView());
	
	Vector3D lights;
	ZeroMemory(&lights,sizeof(Vector3D));
	
	lights = Vector3D(lightPosition.x,lightPosition.y,lightPosition.z);
	
	//pass information through to the shader.
	EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME)->GetEffect()->SetValue(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME)->GetHandle("m_LightPosition")->m_handle,&lights,sizeof(Vector3D)* 10);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetVector4, "m_LightColour", Vector4D(15,15,15,1));

	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetMatrix, "m_Proj", CameraController::GetInstance()->GetProjection());
	EffectSetAndCacheHandle0(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetTechnique, "mTech");
	Vector4D Ambi(0.1f,0.1f,0.1f,1.0f);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetVector4, "m_Ambient", Ambi);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetFloat, "m_ZClip",  100.0f - 0.1f );
}

/************************************************************************/

void
ShadowManager::SetUpForShadowVolumeRender()
{
	//clear stencil buffer
	m_D3DDevice->Clear(0, NULL, D3DCLEAR_STENCIL, D3DCOLOR_ARGB(0, 170, 170, 170), 1.0f, 0 );

	if(Program::GetInstance()->Allow2SidedShadows())
	{
		EffectSetAndCacheHandle0(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetTechnique, "mTechShadow2");
	}
	else
	{
		EffectSetAndCacheHandle0(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetTechnique, "mTechShadow");
	}

	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetVector4, "m_ShadowColour", Vector4D(0.0f,0.1f,0.0f,0.2f));
}

/************************************************************************/

void
ShadowManager::SetUpForSceneLightingRender()
{
	EffectSetAndCacheHandle0(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWVOLUME), SetTechnique, "mTechScene");
}

/************************************************************************/

void 
ShadowManager::SetUpForShadowMapDepthRead()
{
	m_shadowMap->SetUpDepthRender();
}

/************************************************************************/

void 
ShadowManager::SetUpForShadowMapRenderToScene()
{
	m_shadowMap->SetUpSceneRender();
}

/************************************************************************/

ShadowMap*
ShadowManager::GetShadowMap()
{
	return m_shadowMap;
}

/************************************************************************/