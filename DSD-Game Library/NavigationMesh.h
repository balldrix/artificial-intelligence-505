#ifndef _NAVIGATIONMESH_H_
#define _NAVIGATIONMESH_H_

class PolygonData;
class NavigationMeshNode;
class Model;

class NavigationMesh
{
public:
	/**
	 * Constructor
	 */
							NavigationMesh();
	/**
	 * Destructor
	 */
							~NavigationMesh();
	/**
	 * Adds a model to the navigation mesh, with all the polygons being turned into new nodes which will be inserted
	 * into the navigation mesh
	 */
	void					AddModelToNavigationMesh(
								Model* model, ///< The model to add
								const Matrix& position ///< The world transformation to apply to this model
								);
	/**
	 * Finds the node for the specified position on the mesh. If a hint node is passed in it will check to see if the 
	 * position is still on this node and early out if its the case, to stop the searches through the tree everytime 
	 * an object moves.
	 */
	NavigationMeshNode*		FindNode(
								const Vector3D& position, 
								NavigationMeshNode* hint
								);
	/**
	 * Regenerates the search tree
	 */
	void					CreateSearchTree();
	/**
	 * Gets the id of the navigation mesh
	 */
	const char*				GetID() const;
	/**
	 * Sets the id of the navigation mesh
	 */
	void					SetID(
								const char* id ///< the id of the navigation mesh
								);
	/**
	 * Gets the filename of the navigation mesh
	 */
	const char*				GetFileName() const;
	/**
	 * Sets the filename the navigation mesh was loaded from
	 */
	void					SetFileName(
								const char* name
								);
	/**
	 * Returns if you can see another point of the mesh from another without coming off the mesh.
	 */
	bool					LineOfSightCheck(
								const Vector3D& startPosition3D, ///< The position to check from
								NavigationMeshNode* currentNode, ///< The node to check from
								const Vector3D& endPosition3D, ///< The position to test
								NavigationMeshNode* endNode ///< The node for the position to test
								);
	/**
	 * Function which determines if 2 lines intersect 
	 */
	static bool				DoLinesIntersect(
								const Vector2D& lineP1, 
								const Vector2D& lineP2, 
								const Vector2D& lineQ1, 
								const Vector2D& lineQ2, 
								bool infiniteP, 
								bool infiniteQ, 
								float* ratioP, 
								float* ratioQ
								);
	/**
	 * Visualises the mesh to make easier to debug issues, only renders triangles within
	 * 20 metres from the focus point
	 */
	void					DebugRender(
								const Vector3D& focus ///< The focus point
								);

private:
	struct SearchTreeNode
	{
		SearchTreeNode*						childNodes[4];
		MyLinkedList<NavigationMeshNode*>*	meshNodes;
	};
	/**
	 * Initialises the navigation mesh.
	 */
	void					Init();
	/**
	 * Adds an new node into the mesh and links it in with any existing nodes which its touching
	 */
	void					AddNode(
								NavigationMeshNode* meshNode ///< The new node
								);
	/**
	 * Generates the search tree splitting the nodes down into a spacial regions to make searching regions/intersections more efficent.
	 */
	void					MakeSearchTree(
								SearchTreeNode* searchTreeNode, ///< The search tree node we are currently making the tree for
								const AABB& boundingBox, ///< The bound volume of this search tree node
								const MyLinkedList<NavigationMeshNode*>& meshNodes, ///< A list of all the navigation mesh nodes which could be contained within this region
								int levels ///< The current level of the tree were are at
								);
	/**
	 * Destroys the current search tree nodes
	 */
	void					ClearSearchTree(
								SearchTreeNode* searchTreeNode ///< The node for which to destroy all the sub search tree data
								);
	/**
	 * Generates a sub regions bounding volume from its parent nodes bound region
	 */
	void					GetSubRegion(
								const AABB& boundingBox, ///< The current bounding box
								int regionIndex,  ///< The child search tree node to generate a bounding box for
								AABB& subBoundingBox ///< The resulting bounding box
								);
	/**
	 * Helper function to determine if a triangle is contained within a bounding box
	 */
	bool					IsTriangleInBox(
								const PolygonData* triangle, ///< The triangle to test
								const AABB& boundingBox ///< The bounding box to test
								);
	/**
	 * Searches through the search tree building a list of nodes which could possibly lie within a specified volume
	 */
	void					BuildOctreeNodeList(
								const SearchTreeNode* searchTreeNode, 
								const AABB& boundingBox, 
								const AABB& searchBoundingBox, 
								MyLinkedList<NavigationMeshNode*>& meshNodes
								);
	/**
	 * Performs a test to see if the line intersects with the specified polygon and calculates the intersecion point
	 */
	static bool				LinePolyTest(
								const Vector3D& lineStart, 
								const Vector3D& lineEnd, 
								const PolygonData& poly, 
								Vector3D& intersectionPoint
								);

	MyString							m_id;
	MyString							m_fileName;
	MyLinkedList<NavigationMeshNode*>	m_meshNodes;
	AABB								m_box;
	SearchTreeNode						m_rootSearchTreeNode;
};

#endif // _NAVIGATIONMESH_H_

