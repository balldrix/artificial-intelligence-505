#include "Types.h"
#include "AnimationInstance.h"
#include "WaypointManager.h"
#include "SkinnedMesh.h"
#include "Character.h"
#include "CameraController.h"
#include "EffectManager.h"
#include "Effect.h"
#include "RenderManager.h"
#include "BaseCamera.h"
#include "Vertex.h"
#include "ShadowMesh.h"
#include "Sphere.h"
#include "Frustum.h"
#include "ShadowManager.h"
#include "ShadowMap.h"
#include "ModelManager.h"
#include "SkinnedMesh.h"
#include "InputManager.h"
#include "Behavior.h"
#include "BehaviorManager.h"
#include "CharacterManager.h"

/************************************************************************/

Character::Character() : 
	m_velocity(Vector3D(0,0,0)), 
	m_numWaypoints(0),
	m_hitCount(0),
	m_flash(false),
	m_isDead(false),
	m_flashCounter(0.0f),
	m_isVisible(true),
	m_canDamagePlayer(true),
	m_removeMe(false),
	m_lastPathNodeSeen(NULL),
	m_alpha(1.0f),
	m_aniCon(0),
	m_boundingSphere(NULL),
	m_fadeTimer(-1.0f),
	m_damageValue(0),
	m_currentNode(NULL),
	m_targetNode(NULL)
{
	m_meshPtr = 0;
	m_position = Vector3D(0,0,0);
	m_rotation = Vector3D(0,0,0);
	m_destination = Vector3D(0,0,0);
	m_boundPositionOffset = Vector3D(0,0,0);
	m_scale = 1.0f;

	m_sourceWaypoint = 0;
	m_targetWaypoint = 0;
	m_waypointTracker = 0;

	m_selected = false;
	m_flags = Skinned;
	m_flags |= ShadowMapRender; 
	m_flags |= AmibentEnvironment;
	m_flags |= Debug;

	m_currentBehavior = "";

	RenderManager::GetInstance()->AddEvent(this, NULL);
}

/************************************************************************/

Character::~Character()
{
	RenderManager::GetInstance()->RemoveEvent(this);
	m_meshPtr = 0;
	if(m_aniCon)
	{
		delete m_aniCon;
		m_aniCon = 0;
	}

	// if there is a list of bounding spheres
	if(!m_boundingSphereList.empty())
	{
		m_boundingSphere = NULL;

		// delete list
		m_boundingSphereList.clear();
	}

}

/************************************************************************/

void 
Character::HurtCharacter(float modifier)
{
	if(m_isVulnarable)
	{
		this->m_hitCount = m_hitCount + 1 * modifier;
		m_flash = true;
		m_flashCounter = 0.05f;
	}	
}

/************************************************************************/

void
Character::SetMesh(SkinnedMesh *meshPtr)
{
	//delete animation controller if one exists already
	if(m_aniCon)
	{
		delete m_aniCon;
		m_aniCon = 0;
	}
	//delete bounding Sphere if one exists already
	if(m_boundingSphere)
	{
		RenderManager::GetInstance()->RemoveSphere(m_boundingSphere);
		delete m_boundingSphere;
		m_boundingSphere = 0;
	}
	//set m_meshPtr to the mesh pointer passed into the function
	m_meshPtr = meshPtr;
	//get a clone of its animation controller
	m_meshPtr->CloneAniCon(&m_aniCon);
	//if it requires a shadow volume, get that too
	if(m_meshPtr->GetShadowMesh())
	{
		m_flags |= ShadowVolume;
	}

	if(!m_isPlayer)
	{
		// create a bounding sphere list bone collision
		AddBoundingSphere(meshPtr->GetFrameRoot());
	}

	// create bounding sphere to check character is in viewing frustum
	D3DXFrameCalculateBoundingSphere(meshPtr->GetFrameRoot(), &m_boundPosition, &m_boundRadius);
	if(m_boundingSphere)
	{
		m_boundingSphere->SetPosition(m_position + m_boundPosition);
		m_boundingSphere->SetRadius(m_boundRadius*m_scale);
	}
	else
	{
		m_boundingSphere = new Sphere(m_boundPosition, m_boundRadius);
	}

	m_aniCon->Update(0, m_meshPtr);
}

/************************************************************************/

void
Character::Render()
{
	if(!m_isVisible)
	{
		return;
	}
	//check the character is in the viewing frustum
	const Frustum& frustum = CameraController::GetInstance()->GetFrustum();
	Vector3D pos = m_position + m_boundPosition;
	float radius = m_boundRadius * m_scale;
	bool visible = frustum.SphereVisible(pos, radius);
	
	if(!visible)
	{
		return;
	}

	//if render flag is Skinned
	if(RenderManager::GetInstance()->CurrentRenderFlag() == Skinned)
	{
		//transform mesh to position
        Matrix tempWorld, tempScale, tempPosition, tempRot;
		Matrix rotateX, rotateY, rotateZ;

		MatrixScaling(&tempScale,m_scale,m_scale,m_scale);

		MatrixRotationX(&rotateX,m_rotation.x);
		MatrixRotationY(&rotateY,m_rotation.y);
		MatrixRotationZ(&rotateZ,m_rotation.z);

		MatrixTranslation(&tempPosition,m_position.x,m_position.y,m_position.z);
		MatrixMultiply(&tempRot, &rotateX, &rotateY);
		MatrixMultiply(&tempRot, &tempRot, &rotateZ);

		MatrixMultiply(&tempWorld, &tempScale, &rotateY);
		MatrixMultiply(&tempWorld, &tempWorld, &rotateX);
		MatrixMultiply(&tempWorld, &tempWorld, &rotateZ);
		MatrixMultiply(&tempWorld, &tempWorld, &tempPosition);
		
		//Set world in shadow map
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP), SetMatrix, "m_World", tempWorld);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->CommitChanges();

		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetMatrix, "m_World", tempWorld);
		

		//create a WVP
		tempWorld = tempWorld * CameraController::GetInstance()->GetViewProjection();

		//set the vertexblend shader
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetMatrix, "m_WVP", tempWorld);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetVector4, "m_LightPosition", Vector4D(0,1,0,0));
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetMatrix, "m_View", CameraController::GetInstance()->GetView());
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetMatrix, "m_Rotation", tempRot);


		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetBool, "m_negative", false);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetBool, "m_flash", m_flash);
			
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetFloat, "m_alpha",  m_alpha);
		
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetBool, "m_Render", true);

		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->GetEffect()->CommitChanges();

		/*Skinned meshes that have multiple instances
		MUST be rendered via the animation controller
		as the controller will set the bones to the one in the current anim set
		for its particular instance :s*/
		
		{
			m_meshPtr->Render(m_aniCon);
		}
	}

	//if a ShadowMap render call
	else if(RenderManager::GetInstance()->CurrentRenderFlag() == ShadowMapRender)
	{
		//transform mesh to position
		Matrix tempWorld, tempScale, tempPosition, tempRot;
		Matrix rotateX, rotateY, rotateZ;

		MatrixScaling(&tempScale,m_scale,m_scale,m_scale);

		MatrixRotationX(&rotateX,m_rotation.x);
		MatrixRotationY(&rotateY,m_rotation.y);
		MatrixRotationZ(&rotateZ,m_rotation.z);

		MatrixTranslation(&tempPosition,m_position.x,m_position.y,m_position.z);
		MatrixMultiply(&tempRot, &rotateX, &rotateY);
		MatrixMultiply(&tempRot, &tempRot, &rotateZ);

		MatrixMultiply(&tempWorld, &tempScale, &rotateY);
		MatrixMultiply(&tempWorld, &tempWorld, &rotateX);
		MatrixMultiply(&tempWorld, &tempWorld, &rotateZ);
		MatrixMultiply(&tempWorld, &tempWorld, &tempPosition);

		//set shadow map shader
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP), SetMatrix, "m_World", tempWorld);
		EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP)->GetEffect()->CommitChanges();

		tempWorld = tempWorld * ShadowManager::GetInstance()->GetShadowMap()->GetCurrentView() * ShadowManager::GetInstance()->GetShadowMap()->GetCurrentProj();

		//set vertex blend shader
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetMatrix, "m_WVP", tempWorld);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetVector4, "m_LightPosition", Vector4D(0,1,0,0));
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetMatrix, "m_Rotation", tempRot);
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetBool, "m_Render", false);

		EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND)->GetEffect()->CommitChanges();
		
		m_meshPtr->RenderShadow(m_aniCon);
	}
	else if(RenderManager::GetInstance()->CurrentRenderFlag() == Debug)
	{
		m_boundingSphere->Render(false);
	}
}

/************************************************************************/

void
Character::Update(float elapsedTime)
{
	m_time = elapsedTime;
	m_aniCon->Update(m_time,m_meshPtr);

	//sort fade if required
	if(m_fadeTimer > 0)
	{
		m_fadeTimer -= elapsedTime;

		m_alpha = (1.0f / m_maxFadeTime) * m_fadeTimer;
	}

	if(!m_isPlayer)
	{
	// for all bones in sphere list
		for(unsigned int index = 0; index < m_boundingSphereList.size() - 3; index++)
		{
			// get bone position
			const Matrix& bonePos = m_meshPtr->GetJointMatrix(m_boundingSphereList[index].name, m_aniCon);
			m_boundPosition = Vector3D(bonePos._41, bonePos._42, bonePos._43);

			// get character rotation
			Matrix rot;
			MatrixRotationY(&rot, this->GetRotation().y);
			Vector3DTransformCoord(&m_boundPosition, &m_boundPosition, &rot);

			// set sphere offset position
			Vector3D offsetRot;
			Vector3DTransformCoord(&offsetRot, &m_boundPositionOffset, &rot);
			m_boundingSphereList[index].sphere->SetPosition(m_position + m_boundPosition + offsetRot);
		}
	}

	/**
		set stand alone bounding sphere for frustrum calculations
	**/
	//find a key bone that we can use as a center point for the bounding sphere
	const Matrix& hipPos = m_meshPtr->GetJointMatrix("spine_hips",m_aniCon);

	m_boundPosition = Vector3D(hipPos._41,hipPos._42,hipPos._43);
	Matrix rot;	
	MatrixRotationY(&rot,this->GetRotation().y);
	Vector3DTransformCoord(&m_boundPosition,&m_boundPosition,&rot);
	Vector3D offsetRot;
	Vector3DTransformCoord(&offsetRot,&m_boundPositionOffset,&rot);
	//update sphere position
	if(m_boundingSphere)
	{
		m_boundingSphere->SetPosition(m_position+m_boundPosition+offsetRot);	
	}

	//update behavior
	if(m_currentBehavior.Length() > 0)
	{
		BehaviorManager::GetInstance()->GetBehavior(m_currentBehavior)->Update(this,elapsedTime);
	}

	if(m_flashCounter > 0.0f)
	{
		m_flashCounter -= elapsedTime;
	}
	else
	{
		m_flashCounter = 0.0f;
		m_flash = false;
	}
}

/************************************************************************/

int
Character::GetAnimationIndex(const char* aniName) const
{
	return m_aniCon->GetIndex(aniName);
}

/************************************************************************/

void
Character::SetAnimation(int index)
{
	m_aniCon->SetAnimation(index);
}

/************************************************************************/

const char*
Character::GetAnimationName() const
{
	return m_aniCon->GetAnimationName();
}

/************************************************************************/

void
Character::SetPosition(const Vector3D& pos)
{
	m_position = pos;

	if(m_boundingSphere)
	{
		Vector3D spherePos = m_position + m_boundPosition + m_boundPositionOffset;
		m_boundingSphere->SetPosition(spherePos);
	}
}

/************************************************************************/

const Vector3D& 
Character::GetPosition() const
{
	return m_position;
}

/************************************************************************/

void
Character::SetRotation(const Vector3D&  rot)
{
	m_rotation = rot;
}

/************************************************************************/

const Vector3D& 
Character::GetRotation() const
{
	return m_rotation;
}

/************************************************************************/

void 
Character::SetScale(float scale)
{
	m_scale = scale;

	if(m_boundingSphere)
	{
		m_boundingSphere->SetRadius(m_boundRadius*m_scale);
	}
}

/************************************************************************/

float
Character::GetScale() const
{
	return m_scale;
}

/************************************************************************/

const char*
Character::GetID() const
{
	return m_ID;
}

/************************************************************************/

void 
Character::SetID(const char* ID)
{
	m_ID = ID;
	if(strcmp(ID, "Player") == 0 || strcmp(ID, "Skeleton") == 0)
	{
		m_isPlayer = true;
	}
	else
	{
		m_isPlayer = false;
	}
}

/************************************************************************/

void
Character::AddWayPoint(const WayPoint* waypoint)
{
	if(m_sourceWaypoint && m_targetWaypoint == 0)
	{
		m_targetWaypoint = waypoint;
		m_waypointTracker = 1;
	}
	if(m_sourceWaypoint == 0)
	{
		m_sourceWaypoint = waypoint;
		SetPosition(m_sourceWaypoint->m_Position);
	}

	assert(m_numWaypoints < c_MaxWayPointCount);
	m_waypointList[m_numWaypoints] = waypoint;
	m_numWaypoints ++;
}

/************************************************************************/

void 
Character::SetBehavior(const char* newBehavior)
{
	if(m_currentBehavior.Length() > 0)
	{
		BehaviorManager::GetInstance()->GetBehavior(m_currentBehavior)->OnExit(this);
	}
	m_currentBehavior = newBehavior;
	BehaviorManager::GetInstance()->GetBehavior(m_currentBehavior)->OnEnter(this);
}

/************************************************************************/

const char* 
Character::GetBehaviourID() const
{
	return m_currentBehavior;
}

/************************************************************************/

SkinnedMesh*	
Character::GetMesh()
{
	return m_meshPtr;
}

/************************************************************************/

const WayPoint*
Character::GetSourceWayPoint() const
{
	return m_sourceWaypoint;
}
/************************************************************************/

const WayPoint*
Character::GetTargetWayPoint() const
{
	if(!m_targetWaypoint)
	{
		return m_sourceWaypoint;
	}
	return m_targetWaypoint;
}

/************************************************************************/

void
Character::ArrivedAtWaypoint()
{
	m_sourceWaypoint = m_targetWaypoint;
	m_waypointTracker++;
	
	if(m_waypointTracker >= m_numWaypoints)
	{
		m_waypointTracker = 0;
	}

	m_targetWaypoint = m_waypointList[m_waypointTracker];
}

/************************************************************************/

void
Character::Select(bool select)
{
	if(select != m_selected && m_boundingSphere)
	{
		if(select)
		{
			RenderManager::GetInstance()->AddSphere(m_boundingSphere);
		}
		else
		{
			RenderManager::GetInstance()->RemoveSphere(m_boundingSphere);
		}
		m_selected = select;
	}
}

/************************************************************************/

Sphere*
Character::GetBoundingSphere() const
{
	return m_boundingSphere;
}

std::vector<PerBoneShere>
Character::GetBoundingSphereList() const
{
	return m_boundingSphereList;
}

/************************************************************************/

int
Character::GetWayPointCount() const
{
	return m_numWaypoints;
}

/************************************************************************/

const WayPoint*
Character::GetWayPoint(int index) const
{
	if(index < m_numWaypoints)
	{
		return m_waypointList[index];
	}
	return NULL;
}
/************************************************************************/

void 
Character::StartFade(float secs)
{
	m_maxFadeTime = secs;
	m_fadeTimer = secs;
}

void Character::AddBoundingSphere(LPD3DXFRAME frame)
{
	// if frame has a first child
	if(frame->pFrameFirstChild != NULL)
	{
		// call recursion
		AddBoundingSphere(frame->pFrameFirstChild);
	}

	// if frame has a sibling
	if(frame->pFrameSibling != NULL)
	{
		// call recursion
		AddBoundingSphere(frame->pFrameSibling);
	}

	// calculate centre of bone
	Vector3D centrePosition;
	centrePosition = Vector3D(frame->TransformationMatrix._41,
							  frame->TransformationMatrix._42,
							  frame->TransformationMatrix._43);

	// calculate radius 
	float radius;
	if(frame->pFrameFirstChild != NULL)
	{
		Vector3D nextBonePos = Vector3D(frame->pFrameFirstChild->TransformationMatrix._41,
						  frame->pFrameFirstChild->TransformationMatrix._42,
						  frame->pFrameFirstChild->TransformationMatrix._43);

		radius = Vector3DLength(&D3DXVECTOR3(nextBonePos - centrePosition)) / 2;
	}
	else
	{
		radius = 0.05f;
	}

	// add bone sphere data to list of bounding spheres
	PerBoneShere boneSphere;
	boneSphere.name = frame->Name;
	boneSphere.sphere = new Sphere(centrePosition, radius);
	m_boundingSphereList.push_back(boneSphere);
	
	return;
}

void 
Character::SetDestination(Vector3D destination)
{
	m_destination = destination;
}

/************************************************************************/

void 
Character::SetBoundOffset(const Vector3D& boundPos)
{
	m_boundPositionOffset = boundPos;
}

/************************************************************************/

const Vector3D&
Character::GetBoundOffset() const
{
	return m_boundPositionOffset;
}

/************************************************************************/

AnimationInstance*	
Character::GetAnimationInst()const
{ 
	return m_aniCon;
}

/************************************************************************/

unsigned int		
Character::GetHitCount() const
{
	return m_hitCount;
}

/************************************************************************/

void				
Character::SetVelocity(const Vector3D& vel)
{
	m_velocity = vel;
}

/************************************************************************/

const Vector3D&		
Character::GetVelocity() const
{
	return m_velocity;
}

/************************************************************************/

const char*			
Character::GetType() const
{
	return m_Type;
}

/************************************************************************/

void				
Character::SetType(const char* type)
{
	m_Type = type;
}

/************************************************************************/
