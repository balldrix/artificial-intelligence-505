#include "Types.h"
#include "Effect.h"

/*****************************************************************************************/

Effect::Effect() :
	m_effect(0),
	m_usesTexture(false),
	m_active(false)
{
}

/*****************************************************************************************/

Effect::~Effect()
{
	m_effect->Release();

	LinkedListNode<EffectHandle*>* node = m_handles.GetFirst();

	while (node)
	{
	 delete node->GetData();
	 node = node->GetNext();
	}

	m_handles.Clear();
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//LoadEffect Loads the .fx file through direct x bu using the D3DXCreateEffectFromFile command
//the file is then tested for syntax errors, the result is stored in a ID3DXEffect object 
//////////////////////////////////////////////////////////////////////////
HRESULT
Effect::LoadEffect(const char* effectID,const char* fileName)
{
	m_effectID = effectID;

	ID3DXBuffer* errors =  0;

	HRESULT hr = D3DXCreateEffectFromFile(m_D3DDevice,fileName,0,0,D3DXSHADER_DEBUG,0,&m_effect,&errors);

	//error checking will return a popup box if the shader contains syntax errors
	if(hr != S_OK || errors)
	{
		MessageBox(0, (char*)errors->GetBufferPointer(), 0, 0);
	}

	return hr;
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//AddEffectHandleTechnique adds a technique handle to the effect,
//techniques control what parts of the shader is executed
//////////////////////////////////////////////////////////////////////////

void
Effect::AddEffectHandleTechnique(const char *ID, const char *variableName)
{
	EffectHandle* temp = new EffectHandle;
	m_handles.Insert(temp);
	m_handles.GetLast()->GetData()->m_ID = ID;
	m_handles.GetLast()->GetData()->m_handle = m_effect->GetTechniqueByName(variableName);
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//AddEffectHandleParameter adds a Parameter handle to the effect,
//Parameter handles links the global variables of the shader to the game code
//////////////////////////////////////////////////////////////////////////

void
Effect::AddEffectHandleParameter(const char *ID, const char *variableName,bool isTexture)
{
	EffectHandle* temp = new EffectHandle;
	m_handles.Insert(temp);
	m_handles.GetLast()->GetData()->m_ID = ID;
	m_handles.GetLast()->GetData()->m_handle = m_effect->GetParameterByName(0,variableName);

	if(isTexture)
	{
		m_usesTexture = true;
	}
}

/*****************************************************************************************/

ID3DXEffect*
Effect::GetEffect()
{
	return m_effect;
}

/*****************************************************************************************/

const char*
Effect::GetEffectID() const 
{
	return m_effectID;
}

/*****************************************************************************************/

EffectHandle*
Effect::GetHandle(const char *ID)
{
	LinkedListNode<EffectHandle*>* node = m_handles.GetFirst();

	while (node)
	{
		if(_stricmp(node->GetData()->m_ID,ID) == 0)
		{
			return node->GetData();
		}
		node = node->GetNext();
	}
	return 0;
}

/*****************************************************************************************/

void
Effect::SetTechnique(EffectHandle* handle)
{
	m_effect->SetTechnique(handle->m_handle);
}

/*****************************************************************************************/

void
Effect::SetMatrix(EffectHandle* handle, const Matrix& mat)
{
	m_effect->SetMatrix(handle->m_handle,&mat);
}

/*****************************************************************************************/

void
Effect::SetInt(EffectHandle* handle, int number)
{
	m_effect->SetInt(handle->m_handle,number);
}

/*****************************************************************************************/

void
Effect::SetFloat(EffectHandle* handle, float flo)
{
	m_effect->SetFloat(handle->m_handle,flo);
}

/*****************************************************************************************/

void 
Effect::SetBool(EffectHandle* handle, bool flag)
{
	m_effect->SetBool(handle->m_handle,flag);
}

/*****************************************************************************************/

void
Effect::SetVector2(EffectHandle* handle, const Vector2D& v)
{
	m_effect->SetValue(handle->m_handle,&v,sizeof(Vector2D));
}

/*****************************************************************************************/

void
Effect::SetVector3(EffectHandle* handle, const Vector3D& v)
{
	m_effect->SetValue(handle->m_handle,&v,sizeof(Vector3D));
}

/*****************************************************************************************/

void
Effect::SetVector4(EffectHandle* handle, const Vector4D& v)
{
	m_effect->SetValue(handle->m_handle,&v,sizeof(Vector4D));
}

/*****************************************************************************************/

void
Effect::SetVector4(EffectHandle* handle, const Vector3D& v,float a)
{
	Vector4D temp = Vector4D(v.x,v.y,v.z,a);
	SetVector4(handle,temp);
}

/*****************************************************************************************/

void
Effect::SetColour(EffectHandle* handle, float r, float g, float b, float a)
{
	SetVector4(handle, Vector4D(r,g,b,a));
}

/*****************************************************************************************/

void
Effect::SetTexture(EffectHandle* handle,LPDIRECT3DBASETEXTURE9 tex)
{
	m_effect->SetTexture(handle->m_handle,tex);
}

/*****************************************************************************************/

void
Effect::SetMatrixArray(EffectHandle* handle,Matrix* theMatrix,unsigned int numberBones)
{
	m_effect->SetMatrixArray(handle->m_handle,theMatrix,numberBones);
}

/*****************************************************************************************/

void
Effect::SetActive(bool active)
{
	m_active = active;
}

/*****************************************************************************************/

bool
Effect::GetActive() const
{
	return m_active;
}

/*****************************************************************************************/

bool
Effect::GetUsesTexture() const
{
	return m_usesTexture;
}

/*****************************************************************************************/
