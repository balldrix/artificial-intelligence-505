#ifndef _BILLBOARDREGION_H_
#define _BILLBOARDREGION_H_


#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif

#ifndef _BOX_H_
#include "Box.h"
#endif //#ifndef _BOX_H_

/**
 *	Creates a region containing billboards all using the
 *	same texture.
 *	Billboards will be placed randomly within the AABB
 *	and rendered using single draw
 */

class BillBoardRegion : public RenderEvent
{
public:
	/**
	 * Constructor
	 */
						BillBoardRegion(
							const char* id,
							const char* textureName, 
							const AABB &region, 
							int numBillboards, 
							const Vector2D& size,
							bool randomize = true
							);
	/**
	 * Destructor
	 */
						~BillBoardRegion();
	/**
	 * Renders the billboard region
	 */
	void				Render();
	/**
	 * Returns the id of the billboard region
	 */
	const char*			GetID() const;
	/**
	 * Returns the bounding region
	 */
	const AABB&			GetRegion() const;
	/**
	 * Returns the number of billboards contained within the region
	 */
	int					GetNumBillboards() const;
	/**
	 * Returns the size of the billboards
	 */
	const Vector2D&		GetSize() const;
	/**
	 * Returns if the billboards are randomised
	 */
	bool				GetRandomize() const;
	/**
	 * Sets the ID of the billboard region
	 */
	void				SetID(
							const char* id
							);
	/**
	 * Sets the region Bound
	 */
	void				SetRegion(
							const AABB& region
							);
	/**
	 * Sets the number of billboards
	 */
	void				SetNumBillboards(
							int num
							);
	/**
	 * Sets the size of the billboards
	 */
	void				SetSize(
							const Vector2D& size
							);
	/**
	 * Sets if the billboards should be randomised
	 */
	void				SetRandomize(
							bool randomize
							);
	/**
	 * Sets if the region is selected
	 */
	void				Select(
							bool select
							);
	/**
	 * Rebuilds the billboards
	 */
	bool		Rebuild();
private:
	/**
	 * Creates the internal data for the region
	 */
	bool		Create();
	/**
	 * Destroys the internal data for the region
	 */
	void		Destroy();

	MyString	m_id;
	Mesh*		m_quadMesh;

	int			m_texIndex;

	MyString	m_textureName;
	int			m_numBB;
	Vector2D	m_size;
	bool		m_randomize;
	AABB		m_aabb;

	Box			m_box;
	bool		m_selected;
};

#endif