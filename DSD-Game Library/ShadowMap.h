#ifndef _SHADOWMAP_H_
#define _SHADOWMAP_H_

#define SHADOWMAP_SIZE 2048

class SpotLight;

//////////////////////////////////////////////////////////////////////////
//ShadowMap Class
//
//This Class along with the shadow map shader, creates a shadow map
//from a light source which is then used to display a shadow across the
//environment
//////////////////////////////////////////////////////////////////////////

class ShadowMap
{
public:
	/**
	 * Constructor
	 */
					ShadowMap();
	/**
	 * Destructor
	 */
					~ShadowMap();
	/**
	 * Creates and setups the shadow map
	 */
	void			Create();
	/**
	 * Renders the shadow map and then the scene using the shadow map
	 */
	void			Render();
	/**
	 * Renders the shadow map
	 */
	void			SetUpDepthRender();
	/**
	 * Renders the scene using the shadow map
	 */
	void			SetUpSceneRender();
	/**
	 * Returns the shadow maps current scene view matrix
	 */
	const Matrix&	GetCurrentView() const;
	/**
	 * Returns the shadow maps current scene projection matrix
	 */
	const Matrix&	GetCurrentProj() const;
	/**
	 * Sets the lights position
	 */
	void			UpdateLightPosition(
						const Vector3D& cameraPos
						);
private:	
	
	void RenderScene(bool renderShadow,const Matrix* view, const Matrix* Proj);

	//Our Shadow Texture and surface
	LPDIRECT3DTEXTURE9 m_shadowMap;
	LPDIRECT3DSURFACE9 m_shadowMapSurface;

	//the Shadow Projection matrix
	Matrix m_shadowMatrix;
	//the SpotLight
	SpotLight* m_light;

	Matrix m_view;
	Matrix m_proj;

};

#endif //_SHADOWMAP_H_
