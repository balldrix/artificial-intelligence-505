#ifndef _ENVIRONMENTMAMANGER_H_
#define _ENVIRONMENTMAMANGER_H_

class Environment;
class CollisionMesh;
class NavigationMesh;
class BillBoard;
class AnimatingSprite;

class BillBoardRegion;

class EnvironmentManager : public Singleton<EnvironmentManager>
{
public:
	/**
	 * Constructor
	 */
						EnvironmentManager();
	/**
	 * Destructor
	 */
						~EnvironmentManager();
	/**
	 * Adds an new environment model
	 */
	void				AddModel(
							const char* ID, ///< Id of the environment model
							const char* fileName, ///< Filename of the environment model
							const Vector3D& position = Vector3D(0,0,0), ///< Position of the model
							const Vector3D& rotation = Vector3D(0,0,0), ///< Rotation of the model
							float scale = 1.0f, ///< Scale of the model
							int UVMultiply = 1.0f ///< UV multiply scale
							);
	/**
	 * Adds an new collision mesh
	 */
	void				AddCollisionMesh(
							const char* ID, ///< Id of the collision mesh
							const char* fileName ///< Filename of the collision mesh
							);
	/**
	 * Adds an new navigation pathfinding mesh
	 */
	void				AddNavigationMesh(
							const char* ID, ///< The id of the new nav mesh
							const char* fileName ///< Filename of the new nav mesh
							);
	/**
	 * Adds an new billboard region
	 */
	void				AddBillBoardRegion(
							BillBoardRegion* billboards ///< The new billboard region
							);
	/**
	 * Removes a current environment model
	 */
	void				DeleteModel(
							Environment* env ///< Model to delete
							);
	/**
	 * Performs intersection tests along a line, returns true if an intersection occured
	 */
	bool				TestCollision(
							const Vector3D& lineSrc, ///< Line start
							const Vector3D& lineDes, ///< Line end
							Vector3D& intersectionPoint ///< Intersection result
							);
	/**
	 * Performs intersection tests of a sphere along a line, returns true if an intersection occured
	 */
	bool				TestCollision(
							const Vector3D& start, ///< Sphere line start
							const Vector3D& end, ///< Sphere line end
							float radius, ///< sphere radius
							Vector3D& intersectionPoint, ///< Intersection result
							float& intersectionTime ///< Normalised intersection time
							);
	/**
	 * Renders the environment models
	 */
	void				Render();
	/**
	 * Returns the numder of billboard regions
	 */
	int					GetBillBoardRegionCount() const;
	/**
	 * Returns the specified billboard region
	 */
	BillBoardRegion*	GetBillBoardRegion(
							int index ///< Index of the region to return
							);
	/**
	 * Returns the collision mesh
	 */
	CollisionMesh*		GetCollisionMesh();
	/**
	 * Returns the navigation mesh
	 */
	NavigationMesh*		GetNavigationMesh();
	/**
	 * Changes a environment models id
	 */
	void				UpdateEnvID(
							const char* oldName, 
							const char* newName
							);
	/**
	 * Changes a collision models id
	 */
	void				UpdateCollisionID(
							const char* oldName, 
							const char* newName
							);
private:
	static const int c_MaxEnvironmentModels = 32;
	static const int c_MaxBillBoardRegions = 128;

	int m_numModels;
	Environment* m_models[c_MaxEnvironmentModels];
	

	CollisionMesh* m_collisionMesh;
	NavigationMesh* m_navigationMesh;

	
	BillBoardRegion* m_billboardRegions[c_MaxBillBoardRegions];
	int m_numBillboardRegions;
};

#endif //_ENVIRONMENTMAMANGER_H_
