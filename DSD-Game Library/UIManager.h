#ifndef _UIMANAGER_H_
#define _UIMANAGER_H_

class UIObject;
class Text;
class UIManager : public Singleton<UIManager>
{
public:
	/**
	 * Constructor
	 */
						UIManager();
	/**
	 * Destructor
	 */
						~UIManager();
	/**
	 * Loads the UI definition file
	 */
	void				LoadFile(
							const char* fileName ///< The filename of the UI definition
							);
	/**
	 * Adds an new UI Object
	 */
	void				AddObject(
							UIObject* object ///< The new UI object
							);
	/**
	 * Gets the UI Objects with the specified Id
	 */
	UIObject*			GetUIObject(
							const char* ID ///< Id of the UI object to retrieve
							) const;
	/**
	 * Renders the UI
	 */
	void				Render();
	/**
	 * Updates the UI
	 */
	void				Update(
							float elapsedTime ///< The amount of elapsed time this frame
							);
	/**
	 * Sets the current UI state, which makes all ui objects with the mathing state visible
	 */
	void				SetCurrentUI(
							const char* state ///< The new UI state
							);
	/**
	 * Gets the current UI state
	 */
	const char*			GetCurrentUI() const;
	/**
	 * Returns the text renderering object
	 */
	Text*				GetTextWriter() const {return m_stringWriter;}
	/**
	 * D3DX Sprite object for batching all the UI Objects
	 */
	LPD3DXSPRITE		GetSprite() const {return m_spriteBatcher;}
	/**
	 * Sets the colour to render all UI sprite objects
	 */
	void				SetColor(
							D3DCOLOR color
							){m_color = color;}
	/**
	 * Gets the colour to render all UI sprite objects
	 */
	D3DCOLOR			GetColour() const {return m_color;}
private:
	static const int c_MaxNumObjects = 256;
	UIObject* m_objects[c_MaxNumObjects];
	int m_numObjects;

	MyString m_state;
	LPD3DXSPRITE m_spriteBatcher;
	Text* m_stringWriter;
	D3DCOLOR m_color;
};

#endif