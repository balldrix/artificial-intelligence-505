#ifndef _ENTITYLOADER_H_
#define _ENTITYLOADER_H_


class EntityLoader
{
public:
	/**
	 * Constructor
	 */
						EntityLoader();
	/**
	 * Destructor
	 */
						~EntityLoader();
	/**
	 * Loads the games parameters and returns the results into references passed in
	 */
	void				LoadGameParameters(
							const char* fileName, ///< Filename of the file which contains the game parameters
							int& Coins, ///< The number of coins to collect for a health bonus
							int& Cargo, ///< The amount of cargo the player must collect
							float& fogMinDist, ///< The minimum fog distance
							float& fogMaxDist ///< The maximum fog distance
							);
	/**
	 * Loads the list of model assets to load with their game id's and scales
	 */
	void				LoadAssetList(
							const char* fileName ///< Filename of the asset file
							);
	/**
	 * Loads the list of characters for the game with their associated list of waypoints
	 */
	void				LoadCharacterList(
							const char* fileName ///< Filename of the character list
							);
	/**
	 * Loads in the list of props for the game with their corresponding positions
	 */
	void				LoadPropList(
							const char* fileName ///< Filename of the prop list
							);
	/**
	 * Loads in a list of billboard regions for the game and their corresponding textures / regions
	 */
	void				LoadBillBoardRegion(
							const char* fileName ///< Filename of the billboard region file
							);
	/**
	 * Loads in a list of sounds which will be utilised by the game and sets their corresponding game id
	 */
	void				LoadSounds(
							const char* fileName ///< Filename of the sound list
							);
	/**
	 * Sets up the loading bar progress
	 */
	static void			ResetLoadingCounter();
	/**
	 * Sets up how much each call to IncrementLoadingCounter should move along the loading bar
	 */
	static void			SetLoadingCounterIncrement(
							int increment
							);
	/**
	 * Sets how many increment values are required to get the bar to its end point
	 */
	static void			SetLoadingCounterEndPoint(
							int endPoint
							);
	/**
	 * Sets the display width of the loading bar, so the increment value can be scaled to this scale 
	 * for display
	 */
	static void			SetLoadingBarMaxWidth(
							int maxWidth
							);
	/**
	 * Moves along the loading bar by the current increment amount, different assets types will move the bar 
	 * on by different amounts
	 */
	static void			IncrementLoadingCounter();
	/**
	 * Returns the current loading bar increment count
	 */
	static int			GetLoadingCounter();

private:
	/**
	 * Updates the loading bars current display
	 */
	static void			UpdateBar();

	static int ms_loadingCounter;
	static int ms_loadingCounterIncrement;
	static int ms_loadingCounterEndPoint;
	static int ms_loadingBarMaxWidth;
};

#endif