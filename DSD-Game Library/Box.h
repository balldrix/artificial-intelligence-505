#ifndef _BOX_H_
#define _BOX_H_

#ifndef _VERTEX_H_
#include "Vertex.h"
#endif

/**
 * Helper class for rendering box volumes
 */

class Box
{
public:
	/**
	 * Constructor
	 */
					Box();
	/**
	 * Constructor from aabb
	 */
					Box(
						const AABB& box
						);
	/**
	 * Destructor
	 */
					~Box();
	/**
	 * Update the volume
	 */
	void			SetBox(
						const AABB& box
						);
	/**
	 * Render the volume
	 */
	void			Render();
protected:
private:
	/**
	 * Create the wireframe vertex buffer
	 */
	void			CreateWireFrame();

	AABB						m_box;
	IDirect3DVertexBuffer9*		m_vertexBuffer;

};
#endif