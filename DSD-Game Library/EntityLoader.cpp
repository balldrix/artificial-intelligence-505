#include "Types.h"
#include "EntityLoader.h"
#include "MyFileReader.h"
#include "ModelManager.h"
#include "EnvironmentManager.h"
#include "CharacterManager.h"
#include "Character.h"
#include "WaypointManager.h"
#include "PropManager.h"
#include "PropItem.h"
#include "BillBoardRegion.h"
#include "Program.h"
#include "UIManager.h"
#include "UISprite.h"
#include "CollisionMesh.h"
#include "NavigationMesh.h"

#include "SoundManager.h"
#include "SoundEmitter.h"

/************************************************************************/

int EntityLoader::ms_loadingCounter = 0;
int EntityLoader::ms_loadingCounterEndPoint = 100;
int EntityLoader::ms_loadingBarMaxWidth = 100;
int EntityLoader::ms_loadingCounterIncrement = 1;

/************************************************************************/

EntityLoader::EntityLoader()
{
}

/************************************************************************/

EntityLoader::~EntityLoader()
{
}

/************************************************************************/

void
EntityLoader::LoadGameParameters(const char* fileName, int& Coins, int& Cargo, float& fogMinDist, float& fogMaxDist)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		int fogR = atoi(reader.GetNextToken());
		int fogG = atoi(reader.GetNextToken());
		int fogB = atoi(reader.GetNextToken());
		Coins = atoi(reader.GetNextToken());
		Cargo = atoi(reader.GetNextToken());
		fogMinDist = (float)atof(reader.GetNextToken());
		fogMaxDist = (float)atof(reader.GetNextToken());
		
		Program::GetInstance()->SetClearBufferColour(fogR,fogG,fogB);

		IncrementLoadingCounter();

		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Effect File Could not be found");
	}
}

/************************************************************************/

void
EntityLoader::LoadAssetList(const char* fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");
		
		MyString type = reader.GetNextToken();
		MyString Id = reader.GetNextToken();
		MyString filePath = reader.GetNextToken();
		int Scale = atoi(reader.GetNextToken());

		while (type[0] != '\0')
		{
			if(strcmp(type,"CHARACTER")== 0)
			{
				EntityLoader::SetLoadingCounterIncrement(250);
				ModelManager::GetInstance()->CreateModel(Id,filePath,(float)Scale);
			}
			else if(strcmp(type,"PROP")== 0)
			{
				EntityLoader::SetLoadingCounterIncrement(50);
				ModelManager::GetInstance()->CreateMesh(Id,filePath,(float)Scale);
			}
			else if(strcmp(type,"ENVIRONMENT")== 0)
			{
				EntityLoader::SetLoadingCounterIncrement(250);
				EnvironmentManager::GetInstance()->AddModel(Id,filePath);
			}
			else if(strcmp(type,"ENVIRONMENTCOLLISION")== 0)
			{
				EntityLoader::SetLoadingCounterIncrement(250);
				EnvironmentManager::GetInstance()->AddCollisionMesh(Id,filePath);
			}
			else if(strcmp(type,"ENVIRONMENTNAVIGATION")== 0)
			{
				EntityLoader::SetLoadingCounterIncrement(250);
				EnvironmentManager::GetInstance()->AddNavigationMesh(Id, filePath);
			}

			IncrementLoadingCounter();

			type = reader.GetNextToken();
			Id = reader.GetNextToken();
			filePath = reader.GetNextToken();
			Scale = atoi(reader.GetNextToken());
		}

		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Effect File Could not be found");
	}
}

/************************************************************************/

void
EntityLoader::LoadCharacterList(const char *fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");
		
		MyString modelType = reader.GetNextToken();
		MyString characterID  = reader.GetNextToken();
		int waypointCounter = atoi(reader.GetNextToken());
		MyString waypoint;


		while (modelType[0] != '\0')
		{
			Character* newCharacter = new Character();
			newCharacter->SetID(characterID);
			newCharacter->SetType(modelType);
			ObjectModel* model = ModelManager::GetInstance()->GetObjectModel(modelType.GetPointer());
			
			newCharacter->SetMesh(model->m_mesh);
			newCharacter->SetScale(model->m_scale);
			newCharacter->SetAnimation(0);
			
			for(int i = 0; i < waypointCounter; i++)
			{
				waypoint = reader.GetNextToken();
				newCharacter->AddWayPoint(WaypointManager::GetInstance()->GetWayPoint(waypoint));
			}
			CharacterManager::GetInstance()->AddCharacter(newCharacter);

			IncrementLoadingCounter();

			modelType = reader.GetNextToken();
			characterID  = reader.GetNextToken();
			waypointCounter = atoi(reader.GetNextToken());
		}
		
		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Effect File Could not be found");
	}
}

/************************************************************************/

void 
EntityLoader::LoadPropList(const char* fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");
		
		MyString type = reader.GetNextToken();
		MyString uniqueID = reader.GetNextToken();
		float secs = (float)atof(reader.GetNextToken());
		float x = (float)atof(reader.GetNextToken());
		float y = (float)atof(reader.GetNextToken());
		float z = (float)atof(reader.GetNextToken());
		MyString bakeInBool = reader.GetNextToken();

		while (type[0] != '\0')
		{
			PropItem* p = new PropItem();
			p->SetType(type);
			p->SetID(uniqueID);
			p->SetPosition(Vector3D(x, y, z));

			if (_stricmp(bakeInBool.GetPointer(),"true")==0)
			{
				p->SetRotation(Vector3D(0.0f, secs, 0.0f));
			}
			if(secs > 0)
			{
				p->SetRespawn(true);
				p->SetRespawnTime((float)secs);
			}
			else
			{
				p->SetRespawn(false);
			}
			
			p->SetMesh(ModelManager::GetInstance()->GetStaticMeshPointer(type));

			p->GenerateRadius();
			
			if(strlen(p->GetID()) > 0)
			{
				if(strcmp(bakeInBool,"TRUE") == 0)
				{
					p->SetBakedIntoWorld(true);
					p->BakeIntoCollisionMesh();
				}
				else
				{
					p->SetBakedIntoWorld(false);
				}
				PropManager::GetInstance()->AddProp(p);
			}
			else
			{
				delete p;
			}

			IncrementLoadingCounter();

			type = reader.GetNextToken();
			uniqueID = reader.GetNextToken();
			secs = (float)atof(reader.GetNextToken());
			x = (float)atof(reader.GetNextToken());
			y = (float)atof(reader.GetNextToken());
			z = (float)atof(reader.GetNextToken());
			bakeInBool = reader.GetNextToken();
		}

		reader.CloseFile();

		EnvironmentManager::GetInstance()->GetCollisionMesh()->CreateSearchTree();
		EnvironmentManager::GetInstance()->GetNavigationMesh()->CreateSearchTree();
	}
	else
	{
		ErrorMessage("PropItem File Could not be found");
	}
}

/************************************************************************/

void 
EntityLoader::LoadBillBoardRegion(const char* fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		AABB box;

		Vector3D topLeft;
		Vector3D bottomRight;
		Vector2D bsize;

		MyString uniqueID  = reader.GetNextToken();
		MyString textureName = reader.GetNextToken();

		bottomRight.x = (float)atof(reader.GetNextToken());
		bottomRight.y = (float)atof(reader.GetNextToken());
		bottomRight.z = (float)atof(reader.GetNextToken());

		topLeft.x = (float)atof(reader.GetNextToken());
		topLeft.y = (float)atof(reader.GetNextToken());
		topLeft.z = (float)atof(reader.GetNextToken());

		int num = atoi(reader.GetNextToken());

		bsize.x = (float)atof(reader.GetNextToken());
		bsize.y = (float)atof(reader.GetNextToken());
	
		MyString trueFalse = reader.GetNextToken();
		bool randomize = false;


		while (uniqueID[0] != '\0')
		{
			randomize = (_stricmp(trueFalse.GetPointer(),"true") == 0);

			box.SetMin(bottomRight);
			box.SetMax(topLeft);

			BillBoardRegion* newBillboard = new BillBoardRegion(uniqueID.GetPointer(), textureName.GetPointer(), box, num, bsize, randomize);
			EnvironmentManager::GetInstance()->AddBillBoardRegion(newBillboard);

			IncrementLoadingCounter();

			uniqueID  = reader.GetNextToken();
			textureName = reader.GetNextToken();

			bottomRight.x = (float)atof(reader.GetNextToken());
			bottomRight.y = (float)atof(reader.GetNextToken());
			bottomRight.z = (float)atof(reader.GetNextToken());

			topLeft.x = (float)atof(reader.GetNextToken());
			topLeft.y = (float)atof(reader.GetNextToken());
			topLeft.z = (float)atof(reader.GetNextToken());

			num = atoi(reader.GetNextToken());

			bsize.x = (float)atof(reader.GetNextToken());
			bsize.y = (float)atof(reader.GetNextToken());
		
			trueFalse = reader.GetNextToken();
		}
		
		reader.CloseFile();
	}
	else
	{
		ErrorMessage("PropItem File Could not be found");
	}
}

/************************************************************************/

void 
EntityLoader::LoadSounds(const char* fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		MyString uniqueID = reader.GetNextToken();
		MyString filename = reader.GetNextToken();

		while (uniqueID[0] != '\0')
		{
			SoundEmitter* se = new SoundEmitter(uniqueID,filename,DS3DALG_NO_VIRTUALIZATION);
			SoundManager::GetInstance()->AddEmitter(se); 
			SoundManager::GetInstance()->GetEmitter(uniqueID)->SetPosition(Vector3D(1,0,0));

			IncrementLoadingCounter();

			uniqueID = reader.GetNextToken();
			filename = reader.GetNextToken();
		}

		reader.CloseFile();
	}
	else
	{
		ErrorMessage("Sounds File Couldn't Be found");
	}

}

/************************************************************************/

void
EntityLoader::ResetLoadingCounter()
{
	ms_loadingCounter = 0;
	UpdateBar();
}

/************************************************************************/

void
EntityLoader::SetLoadingCounterIncrement(int increment)
{
	ms_loadingCounterIncrement = increment;
}

/************************************************************************/

void
EntityLoader::SetLoadingCounterEndPoint(int endPoint)
{
	ms_loadingCounterEndPoint = endPoint;
}

/************************************************************************/

void
EntityLoader::SetLoadingBarMaxWidth(int maxWidth)
{
	ms_loadingBarMaxWidth = maxWidth;
}

/************************************************************************/

int
EntityLoader::GetLoadingCounter()
{
	return ms_loadingCounter;
}

/************************************************************************/

void
EntityLoader::IncrementLoadingCounter()
{
	ms_loadingCounter += ms_loadingCounterIncrement;
	UpdateBar();
}

/************************************************************************/

void
EntityLoader::UpdateBar()
{
	if (_stricmp(UIManager::GetInstance()->GetCurrentUI(), "LOADING") == 0)
	{
		UISprite* loadingBar = static_cast<UISprite*>(UIManager::GetInstance()->GetUIObject("LoadingBar"));

		int width = ms_loadingBarMaxWidth * ms_loadingCounter / ms_loadingCounterEndPoint;
		if (width > ms_loadingBarMaxWidth)
		{
			width = ms_loadingBarMaxWidth;
		}
		loadingBar->SetRepeated(true);
		loadingBar->SetWidth((float)width);

		m_D3DDevice->BeginScene();
		UIManager::GetInstance()->Render();
		m_D3DDevice->EndScene();
		m_D3DDevice->Present(NULL,NULL,NULL,NULL);
	}
}

/************************************************************************/
