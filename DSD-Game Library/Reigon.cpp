#include "Types.h"
#include "Reigon.h"
#include "Sphere.h"

/****************************************************************************************************************************/

Reigon::Reigon(const char* ID, const char* cutsceneTrigger, const Vector3D& position, float radius) :
	m_ID(ID),
	m_cutsceneToTrigger(cutsceneTrigger),
	m_triggerSphere(position, radius)
{
}

/****************************************************************************************************************************/

Reigon::~Reigon()
{
}

/****************************************************************************************************************************/

const Sphere&
Reigon::GetSphere() const
{
	return m_triggerSphere;
}

/****************************************************************************************************************************/

const char*
Reigon::GetCutscene() const
{
	return m_cutsceneToTrigger;
}

/****************************************************************************************************************************/
