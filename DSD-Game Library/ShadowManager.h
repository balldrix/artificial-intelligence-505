#ifndef _SHADOWMANAGER_H_
#define _SHADOWMANAGER_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif //_RENDEREVENT_H_

class ShadowMap;


class ShadowManager : public Singleton<ShadowManager>
{
public:
	/**
	 * Constrcutor
	 */
					ShadowManager();
	/**
	 * Destructor
	 */
					~ShadowManager();
	/**
	 * Set up shadow volumes AmbientSceneRender
	 */
	void			SetUpForAmbientSceneRender();
	/**
	 * Set Up for Shadow Volume Render
	 */
	void			SetUpForShadowVolumeRender();
	/**
	 * SetUp For Scene Lighting Render on shadow volume
	 */
	void			SetUpForSceneLightingRender();
	/**
	 * SetUp For ShadowMap Depth Read
	 */
	void			SetUpForShadowMapDepthRead();
	/**
	 * SetUp For ShadowMap Render To Scene
	 */
	void			SetUpForShadowMapRenderToScene();
	/**
	 * Returns the shadow map object
	 */
	ShadowMap*		GetShadowMap();
private:
	ShadowMap* m_shadowMap;
};

#endif //_SHADOWMANAGER_H_
