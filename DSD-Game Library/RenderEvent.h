#ifndef _RENDEREVENT_H_
#define _RENDEREVENT_H_

/***********************************************************************
 Anything that needs rendering within this game, i.e. characters, props. 
 there class inherits RenderEvent, a flag is assigned to the class
 to determine at what point they will be rendered, this is managed in the render manager.

 Render is a pure virtual function, meaning that any class that inherits Render Event
 must have a render function.
***********************************************************************/

class RenderEvent
{
public:
	enum RenderFlag
	{
		NoEffects	=				1,
		Skinned		=				1 << 1,
		ParticleEffect	=			1 << 2,
		ShadowVolume =				1 << 3,
		AmibentEnvironment =		1 << 4,
		BillboardEffect =			1 << 5,
		ShadowMapRender =			1 << 6,
		EnvironmentShadowVolume =	1 << 7,
		EnvironmentShadowMap =		1 << 8,
		UIEffect =					1 << 9,
		Debug =						1 << 10,
	};

	/**
	 * Constructor
	 */
					RenderEvent() { m_flags = 0; m_firstChild = 0; m_nextSibling = 0; }
	/**
	 * Destructor
	 */
	virtual			~RenderEvent() {};
	/**
	 * Render interface
	 */
	virtual void	Render() = 0;
	/**
	 * Returns if the flag is set
	 */
	bool			GetFlag(RenderFlag flag) const { return (m_flags & flag) != 0 ; }
protected:
	unsigned int	m_flags;

private:
	friend class RenderManager;

	RenderEvent*	m_firstChild;
	RenderEvent*	m_nextSibling;
};

#endif //_RENDEREVENT_H_
