#include "Types.h"
#include "Text.h"
#include "UIManager.h"
#include "UIObject.h"
#include "MyFileReader.h"

#include "Text.h"
#include "UIText.h"
#include "UISprite.h"

/****************************************************************************************************************************/

DefineSingleton(UIManager);

/****************************************************************************************************************************/

UIManager::UIManager() :
	m_numObjects(0)
{
	LoadFile("Assets\\Scripts\\UI.txt");
	m_state = "";
	m_stringWriter = new Text("Arial",35,true,false);
	D3DXCreateSprite(m_D3DDevice,&m_spriteBatcher);
	m_color = 0xFFFFFFFF;
}

/****************************************************************************************************************************/

UIManager::~UIManager()
{
	for (int index = 0; index < m_numObjects; index ++)
	{
		delete m_objects[index];
	}

	if(m_stringWriter)
	{
		delete m_stringWriter;
	}
	
	m_spriteBatcher->Release();
}

/****************************************************************************************************************************/

void 
UIManager::LoadFile(const char *fileName)
{
	MyFileReader reader;

	if(reader.Exists(fileName))
	{
		reader.OpenFile(fileName,"r");

		MyString details;
		MyString type = reader.GetNextToken();
		float x = 0.0f;
		float y = 0.0f;

		while (type[0] != '\0')
		{
			UIObject* newObject = 0;

			if(strcmp(type,"TEXT")== 0)
			{
				newObject = new UIText;
			}
			else if(strcmp(type,"SPRITE")== 0)
			{
				newObject = new UISprite;
			}
			else
			{
				continue;
			}

			assert(m_numObjects < c_MaxNumObjects);
			m_objects[m_numObjects] = newObject;
			m_numObjects ++;
			
			details = reader.GetNextToken();
			newObject->SetID(details);

			Vector2D pos;

			pos.x = (float)atof(reader.GetNextToken());
			pos.y = (float)atof(reader.GetNextToken());
			
			newObject->SetPosition(pos);

			if(strcmp(type,"SPRITE")== 0)
			{
				float width = (float)atof(reader.GetNextToken());
				float height = (float)atof(reader.GetNextToken());
				static_cast<UISprite*>(newObject)->SetWidth(width);
				static_cast<UISprite*>(newObject)->SetHeight(height);
			}

			details = reader.GetNextToken();
			newObject->SetAssignedState(details);

			if(strcmp(type,"TEXT")== 0)
			{
				details = reader.GetNextToken();
				static_cast<UIText*>(newObject)->SetValue(details);
			}
			else if(strcmp(type,"SPRITE")== 0)
			{
				details = reader.GetNextToken();
				static_cast<UISprite*>(newObject)->Load(details);
			}

			type = reader.GetNextToken();		
		}

		reader.CloseFile();
	}
	else
	{
		ErrorMessage("UIFileCouldn't be found :(");
	}
}

/****************************************************************************************************************************/

void 
UIManager::AddObject(UIObject* object)
{
	assert(m_numObjects < c_MaxNumObjects);
	m_objects[m_numObjects] = object;
	m_numObjects ++;
}

/****************************************************************************************************************************/

UIObject* 
UIManager::GetUIObject(const char *ID) const
{
	for (int index = 0; index < m_numObjects; index ++)
	{
		if(strcmp(m_objects[index]->GetID(),ID)==0)
		{
			return m_objects[index];
		}
	}

	return 0;
}

/****************************************************************************************************************************/

void 
UIManager::Render()
{
	m_spriteBatcher->Begin(D3DXSPRITE_DO_NOT_ADDREF_TEXTURE | D3DXSPRITE_ALPHABLEND );

	for (int index = 0; index < m_numObjects; index ++)
	{
		if(strcmp(m_objects[index]->GetType(),"SPRITE")==0)
		{
			m_objects[index]->Render();
		}
	} 
	m_spriteBatcher->End();

	for(int index = 0; index < m_numObjects; index++)
	{
		if(strcmp(m_objects[index]->GetType(), "TEXT") == 0)
		{
			m_objects[index]->Render();
		}
	}
}

/****************************************************************************************************************************/

void 
UIManager::Update(float elapsedTime)
{
	for (int index = 0; index < m_numObjects; index ++)
	{
		m_objects[index]->Update(elapsedTime);
	}	
}

/****************************************************************************************************************************/

void 
UIManager::SetCurrentUI(const char* state)
{
	if(strcmp(m_state,state)!=0)
	{
		m_state = state;

		for (int i = 0; i < m_numObjects; i++)
		{
			if(strcmp(m_objects[i]->GetAssignedState(),state) != 0)
			{
				m_objects[i]->SetVisible(false);
			}
			else
			{
				m_objects[i]->SetVisible(true);
			}
		}
	}
}

/*****************************************************************************************/

const char*
UIManager::GetCurrentUI() const
{
	return m_state.GetPointer();
}

/*****************************************************************************************/
