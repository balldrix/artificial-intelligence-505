#include "Types.h"
#include "RenderTargetSurface.h"

/**************************************************************************************/

RenderTargetSurface::RenderTargetSurface(int width, int height, int mips, D3DFORMAT texformat, bool useDB, 
										 D3DFORMAT depthFormat, D3DVIEWPORT9& viewport, bool autoMips) : 
	m_texture(0), 
	m_rts(0), 
	m_width(width), 
	m_height(height), 
	m_mips(mips),
	m_textureFormat(texformat),
	m_useDepthBuffer(useDB),
	m_depthFormat(depthFormat),
	m_viewPort(viewport),
	m_autoMips(autoMips)
{
	UINT usage = D3DUSAGE_RENDERTARGET;
	
	if(m_autoMips)
	{
		usage |= D3DUSAGE_AUTOGENMIPMAP;
	}

	D3DXCreateTexture(m_D3DDevice, m_width, m_height, m_mips, usage, m_textureFormat, D3DPOOL_DEFAULT, &m_texture);
	D3DXCreateRenderToSurface(m_D3DDevice, m_width, m_height, m_textureFormat, m_useDepthBuffer, m_depthFormat, &m_rts);
	m_texture->GetSurfaceLevel(0, &m_surface);

	if (!SUCCEEDED(m_rts->BeginScene(m_surface,&m_viewPort)))
	{
		m_surface->Release();
		m_texture->Release();

		// Fall back for intel chipsets and others that only support powers of two
		m_width = 512;
		m_height = 512;
		m_viewPort.Width = 512;
		m_viewPort.Height = 512;

		D3DXCreateTexture(m_D3DDevice, m_width, m_height, m_mips, usage, m_textureFormat, D3DPOOL_DEFAULT, &m_texture);
		D3DXCreateRenderToSurface(m_D3DDevice, m_width, m_height, m_textureFormat, m_useDepthBuffer, m_depthFormat, &m_rts);
		m_texture->GetSurfaceLevel(0, &m_surface);
	}
	else
	{
		m_rts->EndScene(D3DX_FILTER_NONE);
	}
}

/**************************************************************************************/

RenderTargetSurface::~RenderTargetSurface()
{
	m_surface->Release();
	m_texture->Release();
	m_rts->Release();
}

/**************************************************************************************/

IDirect3DTexture9*
RenderTargetSurface::GetTexture()
{
	return m_texture;
}

/**************************************************************************************/

void
RenderTargetSurface::BeginScene()
{
	m_rts->BeginScene(m_surface,&m_viewPort);
}

/**************************************************************************************/

void
RenderTargetSurface::EndScene()
{
	m_rts->EndScene(D3DX_FILTER_NONE);
}

/**************************************************************************************/