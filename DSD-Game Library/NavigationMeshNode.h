#ifndef _NAVIGATIONMESHNODE_H_
#define _NAVIGATIONMESHNODE_H_

#ifndef _ASTARNODE_H_
#include "AStarNode.h"
#endif //#ifndef _ASTARNODE_H_

class NavigationMeshNode : public AStarNode
{
public:
						NavigationMeshNode(PolygonData* poly);
	virtual				~NavigationMeshNode();

	virtual float		CostCalculate(AStarNode* to);
	virtual float		CostEstimate(AStarNode* to);
	virtual AStarNode*	GetFirstNeighbour();
	virtual AStarNode*	GetNextNeighbour();

	const PolygonData*	GetPoly() const;
	const Vector3D&		GetCentre() const;

	Vector3D			GetJoinPosition(AStarNode* otherNode);

	bool				PointInsideNode(const Vector3D& point);

private:
	friend class NavigationMesh;

	bool				GetCommonEdge(NavigationMeshNode* otherNode, int& ourEdge, int& theirEdge);
	void				SetNeighbour(NavigationMeshNode* neighbour, int edge);
	NavigationMeshNode*	GetNeighbour(int edge);

	PolygonData*		m_poly;
	Vector3D			m_centre;
	NavigationMeshNode*	m_neighbours[3];
	int					m_neighbourIndex;
};

#endif
