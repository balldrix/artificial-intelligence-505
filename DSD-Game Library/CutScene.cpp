#include "Types.h"
#include "CutScene.h"
#include "Character.h"
#include "CharacterManager.h"
#include "WaypointManager.h"
#include "Model.h"
#include "ModelManager.h"
#include "CameraController.h"
#include "StaticCamera.h"
#include "WaypointManager.h"
#include "EffectManager.h"
#include "Effect.h"
#include "SkinnedMesh.h"
#include "PropItem.h"
#include "PropManager.h"

/************************************************************************/

CutScene::CutScene():
	m_cameraChange(false), 
	m_duration(0.0f), 
	m_spawnsCharacter(false),
	m_cameraTracksBones(false)
{
	m_cameraPosition = m_targetDirection = Vector3D(0,0,0);
	m_cutsceneID = " ";
	m_target = " ";
}

/************************************************************************/

CutScene::~CutScene()
{
	LinkedListNode<SpawnData*>* node = m_characterToSpawn.GetFirst();

	while (node)
	{
		delete node->GetData();
		node = node->GetNext();
	}
	m_characterToSpawn.Clear();
}

/************************************************************************/

void
CutScene::SetUpScene(const char* ID,float duration,const char* cameraType, const char* cameraPos, const char* Target,bool tracks)
{
	m_cutsceneID = ID;
	m_duration = duration;
	m_cameraWaypoint = cameraPos;
	m_cameraType = cameraType;
	m_target = Target;
	m_cameraTracksBones = tracks;
}

/************************************************************************/

void
CutScene::SetUpCharacterSpawning(bool isCharacter,const char* characterType,const char *character, const char *initialState,bool canDamagePlayer)
{
	m_spawnsCharacter = true;
	
	SpawnData* c = new SpawnData;

	if(isCharacter)
	{
		c->m_isCharacter = true;
		c->m_characterType = characterType;
		c->m_spawnCharacterID = character;
		c->m_initialAnimation = initialState;
		c->m_canDamage = canDamagePlayer;
	}
	else
	{
		c->m_isCharacter = false;
		c->m_characterType = characterType;
		c->m_spawnCharacterID = character;
	}
	m_characterToSpawn.Insert(c);
}

/************************************************************************/

void
CutScene::AddWayPointToSpawner(const char* spawnCharacterID,const char* waypointName)
{
	LinkedListNode<SpawnData*>* node = m_characterToSpawn.GetFirst();
	
	while (node)
	{
		if(strcmp(spawnCharacterID,node->GetData()->m_spawnCharacterID) == 0)
		{
			node->GetData()->m_waypoints.Insert(WaypointManager::GetInstance()->GetWayPoint(waypointName));
		}
		node = node->GetNext();
	}
}

/************************************************************************/

void 
CutScene::BeginCutScene()
{
	//spawn characters / props if required
	if(m_spawnsCharacter)
	{
		LinkedListNode<SpawnData*>* node = m_characterToSpawn.GetFirst();
		
		while(node)
		{
			SpawnData* temp = node->GetData();
			
			//if the spawning object is a character
			if (temp->m_isCharacter)
			{
				//create the character
				Character* c = new Character();
				
				c->SetID(temp->m_spawnCharacterID);
				c->SetType(temp->m_characterType);
				ObjectModel* model = ModelManager::GetInstance()->GetObjectModel(temp->m_characterType.GetPointer());
				c->SetMesh(model->m_mesh);
				c->SetScale(model->m_scale);
				c->SetCanDamagePlayer(temp->m_canDamage);
				c->SetBehavior(temp->m_initialAnimation);
				
				//add its waypoints
				LinkedListNode<const WayPoint*>* waypointNode = node->GetData()->m_waypoints.GetFirst();
				while (waypointNode)
				{
					c->AddWayPoint(waypointNode->GetData());
					waypointNode = waypointNode->GetNext();
				}
				//add the character to the character manager
				CharacterManager::GetInstance()->AddCharacter(c);
				node = node->GetNext();
				m_characterToSpawn.Remove(temp);
				delete temp;
				temp = 0;
			}
			//if the spawning object is a prop
			else
			{
				//create the prop object
				PropItem *p = new PropItem();
				p->SetType(temp->m_characterType);
				p->SetID(temp->m_spawnCharacterID);
				const WayPoint* waypoint = node->GetData()->m_waypoints.GetFirst()->GetData();
				p->SetPosition(waypoint->m_Position);
				p->SetRespawn(false);
				p->SetMesh(ModelManager::GetInstance()->GetStaticMeshPointer(temp->m_characterType));
				p->SetBakedIntoWorld(false);
				p->GenerateRadius();
				PropManager::GetInstance()->AddProp(p);
				node = node->GetNext();
				m_characterToSpawn.Remove(temp);
				delete temp;
				temp = 0;
			}
		}
	}
		
	//Set Up Camera
	if(strcmp(m_cameraType,"STATICCAMERA") == 0)
	{
		m_cameraChange = true;
		CameraController::GetInstance()->AddCamera(new StaticCamera);
	}

	const WayPoint* way = WaypointManager::GetInstance()->GetWayPoint(m_cameraWaypoint);

	//Try Character.
	Character* sceneCharacter = CharacterManager::GetInstance()->GetCharacter(m_target);
	Vector3D pos;
	
	if(sceneCharacter)
	{
		pos = sceneCharacter->GetPosition();
	}
	else // Try from Prop
	{
		PropItem* sceneProp = PropManager::GetInstance()->GetPropItem(m_target);
		pos = sceneProp->GetPosition();
	}

	//if we want the camera to track the animation of the object
	if(m_cameraTracksBones)
	{
		SkinnedMesh* mesh = sceneCharacter->GetMesh();
		const Matrix& m = mesh->GetJointMatrix("Head", sceneCharacter->GetAnimationInst());

		Vector3DTransformCoord(&pos,&pos,&m);
	}

	pos.y += 3.0f;
	
	CameraController::GetInstance()->GetCurrentCamera()->SetupCamera(way->m_Position,pos,Vector3D(0,1,0));

	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetBool, "m_CutScene", true);
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->CommitChanges();

}

/************************************************************************/

void
CutScene::EndCutScene()
{
	if(m_cameraChange)
	{
		CameraController::GetInstance()->RemoveCamera();
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetBool, "m_CutScene", false);
		EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->CommitChanges();
	}
}

/************************************************************************/

void 
CutScene::Update(float elapsedTime)
{
	m_duration -= elapsedTime;

	if(m_cameraTracksBones)
	{
		Vector3D pos = CharacterManager::GetInstance()->GetCharacter(m_target)->GetPosition();
		const Matrix& m = CharacterManager::GetInstance()->GetCharacter(m_target)->GetMesh()->GetJointMatrix("Head", CharacterManager::GetInstance()->GetCharacter(m_target)->GetAnimationInst());
		pos.y += m._42;

		CameraController::GetInstance()->GetCurrentCamera()->SetTarget(pos);
	}
}

/************************************************************************/

const char*
CutScene::GetCutSceneID() const
{
	return m_cutsceneID;
}

/************************************************************************/

float
CutScene::GetDuration() const
{
	return m_duration;
}

/************************************************************************/