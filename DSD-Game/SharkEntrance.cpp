#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "SoundManager.h"
#include "SoundEmitter.h"
#include "SharkBehaviors.h"
#include "CutSceneController.h"
#include "CharacterManager.h"
#include "SharkEntrance.h"

/**********************************************************************************************************************************/

SharkEntrance::SharkEntrance() : 
	Behavior("SHARK_ENTRANCE"),
	m_SharkSphereRadius(0.f),
	m_SharkSphereAttackRadius(0.f)
{

}

/**********************************************************************************************************************************/

SharkEntrance::~SharkEntrance()
{

}

/**********************************************************************************************************************************/

void 
SharkEntrance::OnEnter(Character* owner)
{
	owner->SetDamageToPlayer(60);
	SetAnimationAndCacheIndex(owner, "SharkAttack");
	owner->SetRotation(Vector3D(0,3.14f,0));
	m_timer = 3.0f;

	if(m_SharkSphereRadius == 0.0f)
	{
		m_SharkSphereRadius = owner->GetBoundingSphere()->GetRadius() - 15.0f;
		m_SharkSphereAttackRadius = owner->GetBoundingSphere()->GetRadius() - 8.0f;
	}

	owner->GetBoundingSphere()->SetRadius(m_SharkSphereRadius);
	Vector3D p = owner->GetBoundOffset();
	p.y += 5;
	p.z += 10;

	owner->SetBoundOffset(p);

	SoundManager::GetInstance()->GetEmitter("GAMEMUSIC")->Stop();
	SoundManager::GetInstance()->GetEmitter("ENDBOSS")->Play(true);
}

/**********************************************************************************************************************************/

void 
SharkEntrance::Update(Character* owner, float elapsedTime)
{
	m_timer -= elapsedTime;

	if(m_timer < 0.0f)
	{
		CutSceneController::GetInstance()->BeginCutScene("CutScene6");
		owner->SetBehavior("SHARK_PATROL");
		CharacterManager::GetInstance()->GetCharacter("CAGE1")->SetBehavior("CAGE_PULLUP");
	}
}

/**********************************************************************************************************************************/

void	
SharkEntrance::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
