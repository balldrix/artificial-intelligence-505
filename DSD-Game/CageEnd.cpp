
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "GameStateManager.h"
#include "CageEnd.h"

/**********************************************************************************************************************************/

CageEnd::CageEnd() : 
	Behavior("CAGE_END"),
	m_sphere(0)
{
}

/**********************************************************************************************************************************/

CageEnd::~CageEnd()
{
	if(m_sphere)
	{
		delete m_sphere;
		m_sphere = 0;
	}
}

/**********************************************************************************************************************************/

void 
CageEnd::OnEnter(Character* owner)
{
	m_sphere = new Sphere(owner->GetPosition(),2);
}

/**********************************************************************************************************************************/

void 
CageEnd::Update(Character* owner, float elapsedTime)
{
	if(m_sphere->CollidingWithSphere(*m_playerCharacter->GetBoundingSphere()))
	{
		GameStateManager::GetInstance()->SwitchState("GAMECOMPLETED");
	}
}

/**********************************************************************************************************************************/

void 
CageEnd::OnExit(Character* owner)
{
	if(m_sphere)
	{
		delete m_sphere;
		m_sphere = 0;
	}
	
}

/**********************************************************************************************************************************/