#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CutSceneController.h"
#include "SharkBehaviors.h"
#include "SharkChase.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

SharkChase::SharkChase() : 
	Behavior("SHARK_CHASE")
{

}

/**********************************************************************************************************************************/

SharkChase::~SharkChase()
{

}

/**********************************************************************************************************************************/

void
SharkChase::OnEnter(Character* owner)
{
	Vector3D p = Vector3D(0,0,0);

	owner->SetBoundOffset(p);
	SetAnimationAndCacheIndex(owner, "SharkSwim_L");
}

/**********************************************************************************************************************************/

void 
SharkChase::Update(Character* owner, float elapsedTime)
{
	if(!CutSceneController::GetInstance()->IsCutSceneActive())
	{
		//Path find to player
		float tempAngle = GetPathAngleToTarget(owner,m_playerCharacter->GetPosition());

		TurnDirection dirToTurn  = TurnTo(owner,tempAngle,SHARKBANKSPEED,elapsedTime);

		switch(dirToTurn)
		{
		case TD_FACING:
			SetAnimationAndCacheIndex(owner, "SharkSwim_L");
			if(owner->GetPath().GetFirst())
			{
				MoveInDirection(owner, elapsedTime);
			}
			//MoveInDirection(owner, elapsedTime);
			break;
		case TD_LEFT:
			SetAnimationAndCacheIndex(owner, "SharkBank_L");
			break;
		case TD_RIGHT:
			SetAnimationAndCacheIndex(owner, "SharkBank_R");
			break;
		default: 
			break;
		};

		
		float distSq = DistanceToCharacterSq(owner,*m_playerCharacter);
		if( distSq > SHARKCHASE * SHARKCHASE)
		{		
			owner->SetBehavior("SHARK_PATROL");
		}
		else if(distSq < SHARKATTACK * SHARKATTACK)
		{
			owner->SetBehavior("SHARK_ATTACK");
		}
		//kill it
		if(owner->GetHitCount() > NUM_SHARK_HITS * ScoreManager::GetInstance()->GetDifficulty())
		{
			owner->SetBehavior("SHARK_DEATH");
			owner->SetDead(true);

			// add points
			ScoreManager::GetInstance()->AddPoints(PointSystem::SHARK);
		}
	}
}

/**********************************************************************************************************************************/

void	
SharkChase::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
