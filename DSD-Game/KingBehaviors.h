#ifndef _KINGBEHAVIORS_H_
#define _KINGBEHAVIORS_H_

const int NUM_KING_HITS = 20;
const float KINGDISTPERSEC = 2.0f;
const float KINGROTPERSEC = 2.0f;
const float KINGCHASEDIST = 25.0f;
const float KINGATTACKDIST = 18.0f;

#include "KingAttack.h"
#include "KingCrabChase.h"
#include "KingCrabEntrance.h"
#include "KingCrabPatrol.h"
#include "KingCrabPatrol.h"
#include "KingVulnerable.h"






#endif