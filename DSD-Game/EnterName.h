// EnterName.h
// Christopher Ball 2017
// Game state for entering name on high score table

#ifndef _ENTER_NAME_
#define _ENTER_NAME_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif //#ifndef _GAMESTATE_H_

class EnterName : public GameState
{
public:
	/**
	* Constructor
	*/
	EnterName();
	/**
	* Destructor
	*/
	~EnterName();
	/**
	* On entry handler, called when you enter the game state
	*/
	virtual void		OnEntry();
	/**
	* On exit handler, called when you exit the game state
	*/
	virtual void		OnExit();
	/**
	* Update handler, called once a frame for the state to update its logic.
	*/
	virtual void		Update(
		float elapsedTime ///< The elapsed time this frame
	);
	/**
	*	Render handler, handles renderering the game for its current state
	*/
	virtual void		Render();

	// display high score table
	void DisplayTable();

private:
	// player name buffer
	MyString m_name;

	// timer before state switches from displaying table to front end
	float m_timer;

	// different states within this game state
	enum CurrentScreen
	{
		NAME_ENTRY,
		DISPLAY_TABLE
	}m_currentScreen;
};

#endif _ENTER_NAME_