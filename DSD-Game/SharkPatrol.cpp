#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CutSceneController.h"
#include "SharkBehaviors.h"
#include "SharkPatrol.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

SharkPatrol::SharkPatrol() : 
	Behavior("SHARK_PATROL")
{

}

/**********************************************************************************************************************************/

SharkPatrol::~SharkPatrol()
{

}

/**********************************************************************************************************************************/

void 
SharkPatrol::OnEnter(Character* owner)
{
	Vector3D p = Vector3D(0,0,0);

	owner->SetBoundOffset(p);
	owner->SetSpeed(SHARKSWIMSPEED);
	SetAnimationAndCacheIndex(owner, "SharkSwim_L");
}

/**********************************************************************************************************************************/

void 
SharkPatrol::Update(Character* owner, float elapsedTime)
{
	if(!CutSceneController::GetInstance()->IsCutSceneActive())
	{
		float tempAngle = GetPathAngleToTarget(owner,owner->GetTargetWayPoint()->m_Position);

		TurnDirection dirToTurn  = TurnTo(owner,tempAngle,SHARKBANKSPEED,elapsedTime);

		switch(dirToTurn)
		{
		case TD_FACING:
			SetAnimationAndCacheIndex(owner, "SharkSwim_L");
			if(owner->GetPath().GetFirst())
			{
				MoveInDirection(owner, elapsedTime);
			}
			//MoveInDirection(owner, elapsedTime);
			break;
		case TD_LEFT:
			SetAnimationAndCacheIndex(owner, "SharkBank_L");
			break;
		case TD_RIGHT:
			SetAnimationAndCacheIndex(owner, "SharkBank_R");
			break;
		default: 
			break;
		};

		if(DistanceToTargetSq(owner,owner->GetTargetWayPoint()->m_Position) < 0.5f)
		{
			owner->ArrivedAtWaypoint();
		}

		//Patrol between way points
		if(DistanceToCharacterSq(owner,*m_playerCharacter) < SHARKCHASE * SHARKCHASE)
		{
			owner->SetBehavior("SHARK_CHASE");
		}

		//kill it
		if(owner->GetHitCount() > NUM_SHARK_HITS * ScoreManager::GetInstance()->GetDifficulty())
		{
			owner->SetBehavior("SHARK_DEATH");
			owner->SetDead(true);

			// add points
			ScoreManager::GetInstance()->AddPoints(PointSystem::SHARK);
		}
	}
}

/**********************************************************************************************************************************/

void	
SharkPatrol::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
