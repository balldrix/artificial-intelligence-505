
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CrabDeath.h"

/**********************************************************************************************************************************/

CrabDeath::CrabDeath() : 
	CrabBehavior("CRAB_DEATH")
{

}

/**********************************************************************************************************************************/

CrabDeath::~CrabDeath()
{

}

/**********************************************************************************************************************************/

void 
CrabDeath::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "DIE");
}

/**********************************************************************************************************************************/

void 
CrabDeath::Update(Character* owner, float elapsedTime)
{
	//remove character

	FollowTerrain(owner,elapsedTime);
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
    LPD3DXANIMATIONSET pASTrack;
    
	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

   
    pAC->GetTrackAnimationSet( track, &pASTrack );
	if(strcmp(anim,"DIE")==0)
	{
		 D3DXTRACK_DESC td;
         pAC->GetTrackDesc( track, &td );
		 
		 double period = pASTrack->GetPeriod();
		 if( td.Position >= period - 0.1f)
		 {			 
			  SetAnimationAndCacheIndex(owner, "DEAD");			 
			  owner->StartFade(3.0f);
		 }		
	}

	if(strcmp(anim,"DEAD")==0)
	{
		if(owner->GetFadeTime() <= 0.0f)
		{
			owner->SetRemove(true);
		}
	}
   
	if(pASTrack)
	{
		pASTrack->Release();
		pASTrack = 0;
	}
}

/**********************************************************************************************************************************/

void 
CrabDeath::OnExit(Character* owner)
{
	
}

/**********************************************************************************************************************************/
