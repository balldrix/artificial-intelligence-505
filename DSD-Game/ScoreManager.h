// ScoreManager.h
// Christopher Ball 2016
// keeps track of the score and points

#ifndef _SCOREMANAGER_H_
#define _SCOREMANAGER_H_
 
// constants of point values in namespace
namespace PointSystem
{
	const unsigned int CARGO = 100;
	const unsigned int COIN = 200;
	const unsigned int CROWN = 1000;
	const unsigned int FISH = 10;
	const unsigned int CRAB = 50;
	const unsigned int SQUID = 200;
	const unsigned int KING_CRAB = 5000;
	const unsigned int SHARK = 10000;
}

// constant values for difficulty level
namespace Difficulty
{
	const float NORMAL = 1.0f;
	const float HARD = 1.6f;
}

class ScoreManager : public Singleton<ScoreManager>
{
public:
	ScoreManager();
	~ScoreManager();

	void SetDifficulty(float multiplier);
	float GetDifficulty() const { return m_difficulty; }

	void AddPoints(unsigned int points); // award points to player score
	void ResetScore(); // reset player score to 0
	unsigned int GetScore() const { return m_currentScore; } // get current player score

private:
	unsigned int m_currentScore; // current player score
	float m_difficulty;
};

#endif _SCOREMANAGER_H_