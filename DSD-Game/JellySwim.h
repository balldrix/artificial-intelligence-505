
#ifndef _JELLYSWIM_H_
#define _JELLYSWIM_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class EnemyProjectile;
class Player;

class JellySwim : public Behavior
{
public:
	/**
	 * Constructor
	 */
					JellySwim();

					// overloaded constructor
					JellySwim(Player* player);
	/**
	 * Destructor
	 */
	virtual			~JellySwim();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);

	// shoot bolt
	void ShootBolt(Character* owner);

	Vector3D GetRandomPosition();

private:
	EnemyProjectile* m_electricBolt; // projectile
	float m_shootTimer; // timer for shooting1
	Player* m_player; // pointer to player class
};


#endif //#ifndef _JELLYSWIM_H_