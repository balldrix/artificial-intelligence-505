#ifndef _SQUIDBEHAVIORS_H_
#define _SQUIDBEHAVIORS_H_

const int NUM_SQUID_HITS = 5;
const float SQUIDROTPERSEC = 2;
const float SQUID_SPEED = 4;

const float SQUIDCHASEDIST = 15.0f;
const float SQUIDATTACKDIST = 6.0f;

const Vector3D INKLOCATOR = Vector3D(0.0f, 2.0f, 0.0f);

#include "SquidAttack.h"
#include "SquidChase.h"
#include "SquidDie.h"
#include "SquidPatrol.h"
#include "SquidFlee.h"

#endif //#ifndef _SQUIDBEHAVIORS_H_