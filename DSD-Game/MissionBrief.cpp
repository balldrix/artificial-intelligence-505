#include "Types.h"
#include "MissionBrief.h"
#include "UIManager.h"
#include "UIObject.h"
#include "UISprite.h"
#include "InputManager.h"
#include "GameStateManager.h"
#include "Program.h"
#include "Game.h"
#include "SoundManager.h"
#include "SoundEmitter.h"

#define SPACEBAR 0x39

/************************************************************************/
const int MAX_BRIEF_SCREENS = 5; // number of screens to display
/************************************************************************/

/**********************************************************************************************************************************/

MissionBrief::MissionBrief() :
	GameState("BRIEF"),
	m_currentScreen(0)
{
}

/**********************************************************************************************************************************/

MissionBrief::~MissionBrief()
{
}

/**********************************************************************************************************************************/

void
MissionBrief::OnEntry()
{
	UIManager::GetInstance()->SetCurrentUI("BRIEF");
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->SetPosition(Vector3D(0, 0, 0));
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Play(true);

	// set current screen to the first one
	m_currentScreen = 0;

	ChangeScreen(); // set screen using m_currentScreen
}

/**********************************************************************************************************************************/

void
MissionBrief::OnExit()
{
	UIManager::GetInstance()->SetCurrentUI("LOADING");
}

/**********************************************************************************************************************************/

void
MissionBrief::Update(float elapsedTime)
{
	UIManager* uiManager = UIManager::GetInstance();

	if(InputManager::GetInstance()->IsKeyClicked(false, SPACEBAR))
	{
		// increment screen
		m_currentScreen++;

		// only change screen if it's one that can be displayed
		if(m_currentScreen < MAX_BRIEF_SCREENS)
		{
			// change the current screen
			ChangeScreen();
		}
		else if(m_currentScreen == MAX_BRIEF_SCREENS) // all screens have been viewed
		{
			// stop music while state changes
			SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Stop();

			// switch states to the game state
			GameStateManager::GetInstance()->SwitchState("GAME");
		}
	}

	InputManager::GetInstance()->Update();
	uiManager->Update(elapsedTime);
	SoundManager::GetInstance()->Update();
}

/**********************************************************************************************************************************/

void
MissionBrief::Render()
{
	UIManager::GetInstance()->Render();
}

void
MissionBrief::ChangeScreen()
{
	// get pointer to UI manager to change UI object settings
	UIManager* uiManager = UIManager::GetInstance();

	// check with screen to display then turn that screen visible and
	// turn visible off for all the other screens
	switch(m_currentScreen)
	{
		case(0):
			uiManager->GetUIObject("Mission1")->SetVisible(true);
			uiManager->GetUIObject("Mission2")->SetVisible(false);
			uiManager->GetUIObject("Mission3")->SetVisible(false);
			uiManager->GetUIObject("Mission4")->SetVisible(false);
			uiManager->GetUIObject("Mission5")->SetVisible(false);
			break;
		case(1):
			uiManager->GetUIObject("Mission1")->SetVisible(false);
			uiManager->GetUIObject("Mission2")->SetVisible(true);
			uiManager->GetUIObject("Mission3")->SetVisible(false);
			uiManager->GetUIObject("Mission4")->SetVisible(false);
			uiManager->GetUIObject("Mission5")->SetVisible(false);
			break;
		case(2):
			uiManager->GetUIObject("Mission1")->SetVisible(false);
			uiManager->GetUIObject("Mission2")->SetVisible(false);
			uiManager->GetUIObject("Mission3")->SetVisible(true);
			uiManager->GetUIObject("Mission4")->SetVisible(false);
			uiManager->GetUIObject("Mission5")->SetVisible(false);
			break;
		case(3):
			uiManager->GetUIObject("Mission1")->SetVisible(false);
			uiManager->GetUIObject("Mission2")->SetVisible(false);
			uiManager->GetUIObject("Mission3")->SetVisible(false);
			uiManager->GetUIObject("Mission4")->SetVisible(true);
			uiManager->GetUIObject("Mission5")->SetVisible(false);
			break;
		case(4):
			uiManager->GetUIObject("Mission1")->SetVisible(false);
			uiManager->GetUIObject("Mission2")->SetVisible(false);
			uiManager->GetUIObject("Mission3")->SetVisible(false);
			uiManager->GetUIObject("Mission4")->SetVisible(false);
			uiManager->GetUIObject("Mission5")->SetVisible(true);
			break;
	}
}

/**********************************************************************************************************************************/
