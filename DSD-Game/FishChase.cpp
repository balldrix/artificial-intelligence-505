#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "FishBehaviors.h"
#include "FishChase.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

FishChase::FishChase() :
	Behavior("FISH_CHASE")
{

}

/**********************************************************************************************************************************/

FishChase::~FishChase()
{

}

/**********************************************************************************************************************************/

void
FishChase::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "SWIM");
}

/**********************************************************************************************************************************/

void
FishChase::Update(Character* owner, float elapsedTime)
{
	//Path find to player
	float tempAngle = GetPathAngleToTarget(owner, m_playerCharacter->GetPosition());

	if(TurnTo(owner, tempAngle, FISHROTPERSEC, elapsedTime) == TD_FACING)
	{
		if(owner->GetPath().GetFirst())
		{
			MoveInDirection(owner, elapsedTime);
		}
	}

	// check fish is within distance
	float distSq = DistanceToCharacterSq(owner, *m_playerCharacter);
	if(distSq > FISHCHASEDIST * FISHCHASEDIST)
	{
		owner->SetBehavior("FISH_PATROL");
	}

	if(owner->GetHitCount() > NUM_FISH_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("FISH_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::SQUID);
	}
}

/**********************************************************************************************************************************/

void
FishChase::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
