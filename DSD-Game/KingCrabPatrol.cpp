
#include "Types.h"
#include "Character.h"
#include "CutSceneController.h"
#include "KingBehaviors.h"
#include "KingCrabPatrol.h"

/**********************************************************************************************************************************/

KingCrabPatrol::KingCrabPatrol() : 
	CrabBehavior("KINGCRAB_PATROL")
{

}

/**********************************************************************************************************************************/

KingCrabPatrol::~KingCrabPatrol()
{

}

/**********************************************************************************************************************************/

void 
KingCrabPatrol::OnEnter(Character* owner)
{
	owner->SetSpeed(KINGDISTPERSEC);
	SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
}

/**********************************************************************************************************************************/

void 
KingCrabPatrol::Update(Character* owner, float elapsedTime)
{
	if(!CutSceneController::GetInstance()->IsCutSceneActive())
	{
		float tempAngle = SideAngleToTarget(owner,owner->GetTargetWayPoint()->m_Position);

		TurnDirection dirToTurn  = TurnTo(owner,tempAngle,KINGROTPERSEC,elapsedTime);

		switch(dirToTurn)
		{
		case TD_FACING:
			SetAnimationAndCacheIndex(owner, "WALK_RIGHT");		
			WalkSideways(owner,owner->GetTargetWayPoint()->m_Position,elapsedTime);
			break;
		case TD_LEFT:
			SetAnimationAndCacheIndex(owner, "WALK_LEFT");
			break;
		case TD_RIGHT:
			SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
			break;
		default: 
			break;
		};

		FollowTerrain(owner,elapsedTime);	

		if(DistanceToTargetSq(owner,owner->GetTargetWayPoint()->m_Position) < 0.1f)
		{
			owner->ArrivedAtWaypoint();
		}

		
		if(DistanceToCharacterSq(owner,*m_playerCharacter) < KINGCHASEDIST * KINGCHASEDIST)
		{
			owner->SetBehavior("KING_CHASE");
		}	
	}
}

/**********************************************************************************************************************************/

void	
KingCrabPatrol::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
