
#ifndef _CAGEIDLE_H_
#define _CAGEIDLE_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class CageIdle : public Behavior
{
public:
	/**
	 * Constructor
	 */
					CageIdle();
	/**
	 * Destructor
	 */
	virtual			~CageIdle();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
private: 

	bool m_triggered;
	Sphere* m_boundingSphere;
};

#endif //#ifndef _CAGEIDLE_H_