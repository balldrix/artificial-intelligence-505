#ifndef _SHARKBEHAVIORS_H_
#define _SHARKBEHAVIORS_H_

const int NUM_SHARK_HITS = 50;

const float SHARKBANKSPEED = 5.0f;
const float SHARKSWIMSPEED = 6.0f;
const float SHARKCHASE = 40.0f;
const float SHARKATTACK = 18.0f;

#include "SharkAttack.h"
#include "SharkChase.h"
#include "SharkDeath.h"
#include "SharkEntrance.h"
#include "SharkPatrol.h"

#endif //#ifndef _SHARKBEHAVIORS_H_