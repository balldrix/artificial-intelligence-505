#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "SquidBehaviors.h"
#include "SquidChase.h"

#include "ScoreManager.h"
#include "ParticleEmitter.h"

/**********************************************************************************************************************************/

SquidChase::SquidChase() : 
	Behavior("SQUID_CHASE")
{

}

/**********************************************************************************************************************************/

SquidChase::~SquidChase()
{

}

/**********************************************************************************************************************************/

void
SquidChase::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "SWIM");
}

/**********************************************************************************************************************************/

void 
SquidChase::Update(Character* owner, float elapsedTime)
{
	//Path find to player
	float tempAngle = GetPathAngleToTarget(owner,m_playerCharacter->GetPosition());

	if(TurnTo(owner,tempAngle,SQUIDROTPERSEC,elapsedTime) == TD_FACING)
	{
		if(owner->GetPath().GetFirst())
		{
			MoveInDirection(owner, elapsedTime);
		}
	}
	
	float distSq = DistanceToCharacterSq(owner,*m_playerCharacter);
	if( distSq > SQUIDCHASEDIST * SQUIDCHASEDIST)
	{		
		owner->SetBehavior("SQUID_PATROL");
	}
	else if(distSq < SQUIDATTACKDIST * SQUIDATTACKDIST)
	{
		owner->SetBehavior("SQUID_ATTACK");
	}

	// if health is below 50%
	if(owner->GetHitCount() >(NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty()) / 2)
	{
		// release ink cloud
		ParticleEmitter* ink = new ParticleEmitter();
		ink->SetUpEmitter(1000, owner->GetPosition(), owner->GetPosition(), Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 1.0f, 3.0f, 5.0f, 50);
		ink->SetID("INK");
		ink->LoadTexture("ink", "Ink.tga");
		ink->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(ink);

		// set to flee
		owner->SetBehavior("SQUID_FLEE");

		// set target position to away from 
		Matrix rotation;
		Vector3D facing = owner->GetPosition() - m_playerCharacter->GetPosition();
		MatrixRotationY(&rotation, D3DX_PI);
		Vector3DTransformCoord(&facing, &facing, &rotation);
		Vector3DNormalize(&facing, &facing);

		owner->SetDestination(facing * SQUIDCHASEDIST);
	}

	if(owner->GetHitCount() > NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("SQUID_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::SQUID);
	}
}

/**********************************************************************************************************************************/

void	
SquidChase::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
