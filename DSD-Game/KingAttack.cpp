#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "KingBehaviors.h"
#include "KingAttack.h"

/**********************************************************************************************************************************/

KingAttack::KingAttack() : 
	CrabBehavior("KING_ATTACK")
{
	m_normalRadius = 0;
	m_attackRadius = 0;
}

/**********************************************************************************************************************************/

KingAttack::~KingAttack()
{
	
}

/**********************************************************************************************************************************/

void 
KingAttack::OnEnter(Character* owner)
{
 	if(m_normalRadius == 0)
	{
		m_normalRadius = owner->GetBoundingSphere()->GetRadius();
		m_attackRadius = m_normalRadius * 1.5f;
	}

	SetAnimationAndCacheIndex(owner, "ATTACK");
}

/**********************************************************************************************************************************/

void 
KingAttack::Update(Character* owner, float elapsedTime)
{
	float tempAngle = AngleToTarget(owner,m_playerCharacter->GetPosition());
	TurnTo(owner,tempAngle,KINGROTPERSEC,elapsedTime);

	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );

	if(strcmp(anim,"ATTACK")==0)
	{
		owner->SetVulnarability(false);
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();

		period /= 2;

		if( td.Position > period - 0.1f && td.Position < period + 0.1f)
		{
			owner->GetBoundingSphere()->SetRadius(m_attackRadius);
		}
		else
		{
			owner->GetBoundingSphere()->SetRadius(m_normalRadius);
		}

		if( td.Position >= period * 2 - 0.1f)
		{
			pAC->SetTrackPosition(track,0);
			
		
			if(DistanceToCharacterSq(owner,*m_playerCharacter) > KINGATTACKDIST * KINGATTACKDIST)
			{		
				owner->SetBehavior("KING_CHASE");	
			}
			else
			{
				int randomAttack = rand() % 2;
				if(randomAttack == 1)
				{
					owner->SetBehavior("KING_VULNARABLE");
				}
			}
		}
	}
}

/**********************************************************************************************************************************/

void	
KingAttack::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
