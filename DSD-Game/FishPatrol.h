#ifndef _FISHPATROL_H_
#define _FISHPATROL_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class FishPatrol : public Behavior
{
public:
	/**
	 * Constructor
	 */
					FishPatrol();
	/**
	 * Destructor
	 */
	virtual			~FishPatrol();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
private:
};

#endif //#ifndef _FISHPATROL_H_