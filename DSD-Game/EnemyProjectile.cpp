#include "Types.h"
#include "EnemyProjectile.h"
#include "Model.h"
#include "Player.h"
#include "RenderManager.h"
#include "ModelManager.h"
#include "CharacterManager.h"
#include "Character.h"
#include "Sphere.h"
#include "SoundManager.h"
#include "SoundEmitter.h"
#include "ScoreManager.h"

#include "PropManager.h"

const float PROJECTILESPEED = 12.0f;
const float PROJECTILELIFETIME = 1.0f;

EnemyProjectile::EnemyProjectile() :
	m_position(0, 2, 0),
	m_rotation(0, 0, 0),
	m_forward(0, 0, 1),
	m_life(PROJECTILELIFETIME),
	m_alive(false),
	m_model(0),
	m_player(NULL)
{
	m_flags |= NoEffects;
}

EnemyProjectile::~EnemyProjectile()
{
	m_model = 0;
	RenderManager::GetInstance()->RemoveEvent(this);
}

void 
EnemyProjectile::CreateProjectile(const char* ID, const char* type, Player* player)
{
	m_ID = ID;
	m_model = ModelManager::GetInstance()->GetStaticMeshPointer(type);
	m_model->SetID(m_ID);
	m_model->SetPosition(m_position);
	m_model->SetRotation(m_rotation);
	m_model->SetScale(3.0f);
	RenderManager::GetInstance()->AddEvent(this, NULL);

	m_player = player;
}

void 
EnemyProjectile::Render()
{
	if(m_alive)
	{
		m_model->SetPosition(m_position);
		m_model->SetRotation(m_rotation);
		m_model->Render();
	}
}

void 
EnemyProjectile::Update(float elapsedTime)
{
	if(m_alive)
	{
		Sphere& sphere = *m_player->GetCharacter()->GetBoundingSphere();
		Vector3D vecTo = sphere.GetPosition() - m_position;
		float radius = sphere.GetRadius();
		float distance = Vector3DLengthSq(&vecTo);

		if(distance < radius * radius)
		{		
			//tell character it's been hit
			m_player->SetHurt((int)(10 * ScoreManager::GetInstance()->GetDifficulty()));

			//remove the bullet
			m_alive = false;
			m_life = PROJECTILELIFETIME;
			return;
		}
	}

	//update position
	Vector3D newPosition = m_position;
	newPosition -= m_forward * (PROJECTILESPEED * elapsedTime);
	m_position = newPosition;

	m_life -= elapsedTime;

	if(m_life < 0)
	{
		m_alive = false;
		m_life = PROJECTILELIFETIME;
	}
}

void 
EnemyProjectile::SetPosition(const Vector3D& pos)
{
	m_position = pos;
}

const Vector3D& 
EnemyProjectile::GetPosition() const
{
	return m_position;
}

void 
EnemyProjectile::SetRotation(const Vector3D& rot)
{
	m_rotation = rot;
}

const Vector3D&
EnemyProjectile::GetRotation() const
{
	return m_rotation;
}

void 
EnemyProjectile::SetForward(const Vector3D& forward)
{
	m_forward = forward;
}

const Vector3D&
EnemyProjectile::GetForward() const
{
	return m_forward;
}

void 
EnemyProjectile::SetAlive(bool alive)
{
	m_alive = alive;
}

bool
EnemyProjectile::GetAlive() const
{
	return m_alive;
}
