
#ifndef _CRABDEATH_H_
#define _CRABDEATH_H_

#ifndef _CRABBEHAVIOR_H_
#include "CrabBehavior.h"
#endif //#ifndef _CRABBEHAVIOR_H_

class CrabDeath : public CrabBehavior
{
public:
	/**
	 * Constructor
	 */
					CrabDeath();
	/**
	 * Destructor
	 */
	virtual			~CrabDeath();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
};

#endif //#ifndef _CRABDEATH_H_