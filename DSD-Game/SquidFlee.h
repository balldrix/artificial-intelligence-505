#ifndef _SQUIDFLEE_H_
#define _SQUIDFLEE_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class SquidFlee : public Behavior
{
public:
	/**
	* Constructor
	*/
	SquidFlee();
	/**
	* Destructor
	*/
	virtual			~SquidFlee();
	/**
	* Notifies the behaviour that the following character has now entered the behaviour
	*/
	virtual void	OnEnter(
		Character* owner
	);
	/**
	* Updates the characters logic for this behaviour
	*/
	virtual void	Update(
		Character* owner,
		float elapsedTime
	);
	/**
	* Notifies the behaviour that the following character has now left the behaviour
	*/
	virtual void	OnExit(
		Character* owner
	);
};

#endif //#ifndef _SQUIDFLEE_H_