
#ifndef _KINGATTACK_H_
#define _KINGATTACK_H_

#ifndef _CRABBEHAVIOR_H_
#include "CrabBehavior.h"
#endif //#ifndef _CRABBEHAVIOR_H_

class KingAttack : public CrabBehavior
{
public:
	/**
	 * Constructor
	 */
					KingAttack();
	/**
	 * Destructor
	 */
	virtual			~KingAttack();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);

private:
	float m_attackRadius;
	float m_normalRadius;
};

#endif //#ifndef _KINGATTACK_H_