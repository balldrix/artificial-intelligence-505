#include "EnterName.h"
#include "UIManager.h"
#include "UIText.h"

#include "InputManager.h"
#include "GameStateManager.h"

#include "ScoreManager.h"
#include "HighScores.h"

#define ENTER 0x1C

EnterName::EnterName() :
	GameState("ENTERNAME"),
	m_name(""),
	m_timer(0.0f),
	m_currentScreen(NAME_ENTRY)
{
}

EnterName::~EnterName()
{
}

void
EnterName::OnEntry()
{
	// set current UI
	UIManager::GetInstance()->SetCurrentUI("ENTERNAME");

	// hide high score BG and table
	UIManager::GetInstance()->GetUIObject("HighScoreTableBG")->SetVisible(false);
	UIManager::GetInstance()->GetUIObject("Table")->SetVisible(false);

	// reset name
	m_name = "";
}

void
EnterName::OnExit()
{
	// hide high score BG and table
	UIManager::GetInstance()->GetUIObject("HighScoreTableBG")->SetVisible(false);
	UIManager::GetInstance()->GetUIObject("Table")->SetVisible(false);
}

void
EnterName::Update(float elapsedTime)
{
	switch(m_currentScreen)
	{
		case NAME_ENTRY:
			// if enter is pressed
			if(InputManager::GetInstance()->IsKeyClicked(false, ENTER))
			{
				// create new score structure and add it to the high score table
				Score playerScore;
				playerScore.name = m_name; // set new player score with new name
				playerScore.score = ScoreManager::GetInstance()->GetScore();
				HighScores::GetInstance()->InsertScore(playerScore);

				// change screen to display table
				DisplayTable();
				break;
			}

			// if any other key is pressed
			if(InputManager::GetInstance()->IsAnyKeyClicked())
			{
				// add the last pressed key to the string buffer
				m_name.Concatenate(InputManager::GetInstance()->GetChar());
				break;
			}

		case DISPLAY_TABLE:

			// update timer
			m_timer += elapsedTime;

			if(m_timer > 7.0f)
			{
				// go back to menu
				GameStateManager::GetInstance()->SwitchState("FRONTEND");
			}
			break;
	}

	// set name on screen to the value of m_name
	static_cast<UIText*>(UIManager::GetInstance()->GetUIObject("Name"))->SetValue(m_name);

	// update input manager
	InputManager::GetInstance()->Update();
}

void
EnterName::Render()
{
	UIManager::GetInstance()->Render();
}

void
EnterName::DisplayTable()
{
	// change screen state
	m_currentScreen = DISPLAY_TABLE;

	// hide enter name screen
	UIManager::GetInstance()->GetUIObject("NewHighScoreBG")->SetVisible(false);
	UIManager::GetInstance()->GetUIObject("Name")->SetVisible(false);

	// display high score table and BG
	UIManager::GetInstance()->GetUIObject("HighScoreTableBG")->SetVisible(true);
	UIManager::GetInstance()->GetUIObject("Table")->SetVisible(true);

	// create table to display
	MyString table = HighScores::GetInstance()->GetTable();

	// set UI to display table
	static_cast<UIText*>(UIManager::GetInstance()->GetUIObject("Table"))->SetValue(table);
}
