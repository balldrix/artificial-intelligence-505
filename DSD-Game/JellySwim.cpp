
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "JellySwim.h"
#include "JellyBehaviors.h"

#include "SoundManager.h"
#include "SoundEmitter.h"

#include "EnemyProjectile.h"
#include "Player.h"

#include "WaypointManager.h"

/**********************************************************************************************************************************/

JellySwim::JellySwim() : 
	Behavior("JELLY_SWIM")
{
	m_electricBolt = NULL;
}

JellySwim::JellySwim(Player* player) :
	Behavior("JELLY_SWIM"),
	m_shootTimer(0.0f)
{
	// copy player pointer
	m_player = player;

	// create new projectile
	m_electricBolt = new EnemyProjectile();
	m_electricBolt->CreateProjectile("Electric_Bolt", "ELECTRIC_BOLT", player);
}

/**********************************************************************************************************************************/

JellySwim::~JellySwim()
{
	//Destory projectile
	delete m_electricBolt;
	m_electricBolt = NULL;
}

/**********************************************************************************************************************************/

void 
JellySwim::OnEnter(Character* owner)
{
	owner->SetAlpha(0.5f);
	SetAnimationAndCacheIndex(owner, "Swim");

	owner->SetSpeed(JELLYSPEED);

	// set random target position
	owner->SetDestination(GetRandomPosition());
	
	// reset timer
	m_shootTimer = 0.0f;
}

/**********************************************************************************************************************************/

void 
JellySwim::Update(Character* owner, float elapsedTime)
{
	// rotate and move jelly fish in direction of destination point
	float tempAngle = GetPathAngleToTarget(owner, owner->GetDestination());

	if(TurnTo(owner, tempAngle, JELLYROTPERSEC, elapsedTime) == TD_FACING)
	{
		MoveInDirection(owner, elapsedTime);
	}

	if(DistanceToTargetSq(owner, owner->GetDestination()) < 0.1f)
	{
		owner->SetDestination(GetRandomPosition());
	}

	// if jelly fish is close enough
	if(DistanceToCharacterSq(owner, *m_playerCharacter) < JELLYATTACKDIST * JELLYATTACKDIST)
	{
		// if can shoot
		if(m_shootTimer > SHOOTINGDELAY)
		{
			// shoot and reset timer
			ShootBolt(owner);
			m_shootTimer = 0.0f;
		}
	}

	if(m_electricBolt != NULL)
	{
		m_electricBolt->Update(elapsedTime);
	}

	// update shooting timer
	m_shootTimer += elapsedTime;
}

/**********************************************************************************************************************************/

void	
JellySwim::OnExit(Character* owner)
{
}

void 
JellySwim::ShootBolt(Character* owner)
{
	if(m_electricBolt != NULL)
	{

	//if the projectile is not alive
		if(!m_electricBolt->GetAlive())
		{
			//set it alive
			m_electricBolt->SetAlive(true);

			//compute Position 
			Vector3D pos, forward;
			Matrix tempRot;
			Matrix rotateX, rotateY, rotateZ;

			//set positions and rotations for the bullets
			MatrixRotationX(&rotateX, owner->GetPosition().x - m_player->GetCharacter()->GetPosition().x);
			MatrixRotationY(&rotateY, owner->GetPosition().y - m_player->GetCharacter()->GetPosition().x);
			MatrixRotationZ(&rotateZ, owner->GetPosition().z - m_player->GetCharacter()->GetPosition().x);
			MatrixMultiply(&tempRot, &rotateX, &rotateY);
			MatrixMultiply(&tempRot, &tempRot, &rotateZ);

			Vector3DTransformCoord(&pos, &BOLTLOCATOR, &tempRot);

			pos += owner->GetPosition();
			m_electricBolt->SetPosition(pos);
			m_electricBolt->SetRotation(owner->GetRotation());
			Vector3DNormalize(&forward, &Vector3D(owner->GetPosition() - m_player->GetPosition()));
			m_electricBolt->SetForward(forward);

			//play shoot sound
			SoundManager::GetInstance()->GetEmitter("SHOOT")->Play();
			SoundManager::GetInstance()->GetEmitter("SHOOT")->SetPosition(owner->GetPosition());
			SoundManager::GetInstance()->GetEmitter("SHOOT")->SetVolume(200);

			return;
		}
	}
}

Vector3D JellySwim::GetRandomPosition()
{
	// get random number using max waypoints in waypoint list
	int num = rand() % WaypointManager::GetInstance()->GetWayPointCount();

	// set target position using random number
	return WaypointManager::GetInstance()->GetWayPoint(num)->m_Position;
}

/**********************************************************************************************************************************/
