
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CageLower.h"

/**********************************************************************************************************************************/

CageLower::CageLower() : 
	Behavior("CAGE_LOWER")
{

}

/**********************************************************************************************************************************/

CageLower::~CageLower()
{

}

/**********************************************************************************************************************************/

void 
CageLower::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "LOWER");
}

/**********************************************************************************************************************************/

void 
CageLower::Update(Character* owner, float elapsedTime)
{
	LPD3DXANIMATIONSET pASTrack;
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );

	if(strcmp(anim,"LOWER")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{
			SetAnimationAndCacheIndex(owner, "IDLE");
			owner->SetBehavior("CAGE_END");

		}		
	}
}

/**********************************************************************************************************************************/

void 
CageLower::OnExit(Character* owner)
{

}

/**********************************************************************************************************************************/
