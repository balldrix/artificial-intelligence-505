#ifndef _PLAYER_H_
#define _PLAYER_H_

#ifndef _PROPCOLLECTOR_H_
#include "PropCollector.h"
#endif //#ifndef _PROPCOLLECTOR_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

enum UpgradeState;
class Line;
class Bullets;
class TargetIndicator;
class Character;

class Player : public PropCollector, public Behavior
{
public:

	// different ai states for the diver
	enum AIState
	{
		COLLECTING,
		ATTACKING,
		RETREATING
	};
	
	enum CharacterState
	{
		ONFLOOR,
		JUMPING,
		ROCKETJUMPING,
		HURT,
		ELECTROCUTED,
		DEAD
	};
	/**
	 * Constructor
	 */
								Player();
	/**
	 * Destructor
	 */
								~Player();
	/**
	 * Returns the position of the player
	 */
	const Vector3D&				GetPosition() const;
	/**
	 * Sets the current state of the player
	 */
	void						SetState(
									CharacterState newState
									);
	/**
	 * Returns the current state of the player
	 */
	CharacterState				GetCharacterState() const;
	/**
	 * Updates the players logic and movement
	 */
	bool						Update(	
									float elapsedTime
									);
	/**
	 * Handles the player colliding with a prop which can be picked up
	 */
	virtual void				OnCollected(
									PropItem* item
									);
	/**
	 * Set the number coins required to increase the players health
	 */ 
	void						SetCoinsRequiredForHealthBoost(
									int coins
									);
	// sets player to hurt status
	void						SetHurt(int damage);

	/**
	 * Returns if the player has currently got cheats enabled.
	 */
	bool						Cheating() const {return m_cheat;}
	/**
	 * Returns the number of coins the player has collected
	 */
	int							GetCoinsCollected() const {return m_coins;}
	/**
	 * Returns the amount of cargo that the player has collected
	 */
	int							GetCargoCollected() const {return m_cargo;}
	
	// returns character pointer
	Character*					GetCharacter() const { return m_playerCharacter; }
	
	/**
	 * Returns if the player is currently flahsing
	 */
	bool						IsFlashing() const {return m_flash;}

	// set demo mode on or off
	void SetDemo(bool demo);

	// virtual functions overriden to allow behaviour base class
	virtual void	OnEnter(Character* owner) {};
	virtual void	Update(Character* owner, float elapsedTime) {};
	virtual void	OnExit(Character* owner) {};

	// sets random spawn location
	void SpawnPlayer();

private:
	/**
	 * Updates the players state for the currently pressed keys
	 */
	void						ProcessKeys(
									float elapsedTime
									);

	/**
	* Updates the players AI state
	*/
	void						ProcessAI(
		float elapsedTime
	);
	
	/**
	 * Updates the players movent and handles all the collisions with the environment
	 */
	void						ProcessMovement(
									float elapsedTime
									);
	/*
	* processes player movement with AI code
	*/
	void						ProcessDemoMovement(float elapsedTime);

	/**
	 * Recalculates the players forward vector from the current rotations
	 */
	void						CalculateRotations();
	/**
	 * Handles the firing of a bullet
	 */
	void						ShootBullet(float rotateMod = 0.0f);
	/**
	 * Moves the particle emitters to be inline with the new characters position
	 */
	void						UpdateParticlePositions();
	/**
	 * Handles the update of the rocket jump
	 */
	void						RocketJump(
									float elapsedTime
									);
	/**
	 * Kills the player
     */
	void						KillPlayer();
	/**
	 * Updates the UI display values.
	 */
	void						UpdateStats();

	// diver state methods
	AIState Collecting(float elapsedTime);
	AIState Attacking(float elapsedTime);
	AIState Retreating(float elapsedTime);
	
	// set target to lock missile to 
	void SetTargetLock();

	
	//Player orientation
	Vector3D m_position;
	Vector3D m_velocity;
	Vector3D m_rotation;
	Vector3D m_forward;

	//Player Stats
	int m_ammo;
	int m_health;
	int m_air;
	int m_cargo;
	int m_coins;
	CharacterState m_state;
	CharacterState m_prevState;

	int m_coinsRequiredForHealth;
	int m_nextHealthIncrement;

	bool m_flash;
	bool m_hurtPlayed;
	bool m_allowCollision;


	//Player Timers
	float m_hurtCounter;
	float m_electicuteTimer;
	float m_flashTimer;

	float m_idleTimer;
	float m_airTimer;

	bool m_noInputMovement;

	bool m_hadFloorCollision;
	bool m_allowAirControl;

	float m_jumpTimer;
	float m_jumpDelay;
	
	//////////////////CHEAT/////////////////////
	bool m_cheat;
	
	Character* m_playerCharacter;
	Character* m_playerSkeleton;
	static const int NUMBEROFBULLETS = 10;
	Bullets* m_bullets[NUMBEROFBULLETS];
	TargetIndicator* m_cargoTarget;

	// flag for demo ai control
	 bool m_demoMode;

	 // keep track of what state ai diver is at
	 AIState m_AIState;
	
	// target position
	 Vector3D m_targetPosition;
	 Character* m_NPCTarget;

	// is shield active
	bool m_shield;

	// upgrade state and timer
	UpgradeState m_currentUpgrade;
	float m_upgradeTimer;
	float m_shieldTimer;
	float m_rapidFireTimer;

	// has locked target
	bool m_lockedTarget;

	// lock target for smart missle
	Character* m_targetLock;
};

#endif //_PLAYER_H_
