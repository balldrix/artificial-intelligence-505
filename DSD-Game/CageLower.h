
#ifndef _CAGELOWER_H_
#define _CAGELOWER_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class CageLower : public Behavior
{
public:
	/**
	 * Constructor
	 */
					CageLower();
	/**
	 * Destructor
	 */
	virtual			~CageLower();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
};

#endif //#ifndef _CAGELOWER_H_