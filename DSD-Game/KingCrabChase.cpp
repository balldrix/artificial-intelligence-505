
#include "Types.h"
#include "Character.h"
#include "CutSceneController.h"
#include "AnimationInstance.h"
#include "KingBehaviors.h"
#include "KingCrabChase.h"


/**********************************************************************************************************************************/

KingCrabChase::KingCrabChase() : 
	CrabBehavior("KING_CHASE")
{

}

/**********************************************************************************************************************************/

KingCrabChase::~KingCrabChase()
{

}

/**********************************************************************************************************************************/

void 
KingCrabChase::OnEnter(Character* owner)
{
	owner->SetSpeed(KINGDISTPERSEC * 2);
	SetAnimationAndCacheIndex(owner, "WALK_FORWARD");
}

/**********************************************************************************************************************************/

void 
KingCrabChase::Update(Character* owner, float elapsedTime)
{
	//Path find to player
	float tempAngle = GetPathAngleToTarget(owner,m_playerCharacter->GetPosition());

	TurnDirection dirToTurn  = TurnTo(owner,tempAngle,KINGROTPERSEC,elapsedTime);

	switch(dirToTurn)
	{
	case TD_FACING:
		SetAnimationAndCacheIndex(owner, "WALK_FORWARD");
		MoveInDirection(owner, elapsedTime);		
		break;
	case TD_LEFT:
		SetAnimationAndCacheIndex(owner, "WALK_LEFT");
		break;
	case TD_RIGHT:
		SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
		break;
	default: 
		break;
	};
	
	float distSq = DistanceToCharacterSq(owner,*m_playerCharacter);

	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );

	if(strcmp(anim,"WALK_FORWARD")==0)
	{
		owner->SetVulnarability(false);
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();


		if( td.Position >= period - 0.1f)
		{
			pAC->SetTrackPosition(track,0);

			if( distSq > KINGCHASEDIST * KINGCHASEDIST)
			{		
				owner->SetBehavior("KINGCRAB_PATROL");
			}
			else if(distSq < KINGATTACKDIST * KINGATTACKDIST)
			{
				int randomAttack = rand() % 2;
				if(randomAttack == 1)
				{
					owner->SetBehavior("KING_VULNARABLE");
				}
				else
				{
					owner->SetBehavior("KING_ATTACK");
				}
			}
		}
	}
}

/**********************************************************************************************************************************/

void	
KingCrabChase::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
