#include "Types.h"

#include "Program.h"

#include "Timer.h"
#include "Main.h"

//Game states
#include "GameStateManager.h"
#include "Demo.h"
#include "Game.h"
#include "FrontEnd.h"
#include "Options.h"
#include "MissionBrief.h"
#include "GameOver.h"
#include "GameCompleted.h"
#include "EnterName.h"

// my headers
#include "ScoreManager.h"
#include "HighScores.h"

#ifdef _DEBUG
int MemoryDebugAllocHook( int allocType, void *userData, size_t size, int blockType, long requestNumber, const unsigned char *filename, int lineNumber)
{
	if (size == 1536)
	{
		// A line to add a breakpoint to
		int dummy = 42;
	}

	return true;
}
#endif

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )
{
#ifdef _DEBUG
	_crtBreakAlloc = -1;
	_CrtSetAllocHook(MemoryDebugAllocHook);
	//_CrtSetDbgFlag(  _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	Timer timer;
	unsigned __int64 lastTime;
	unsigned __int64 timerFrequency;

	Program::Create();
	
	MSG msg;
	Program::GetInstance()->GenerateProgram(hInstance,hPrevInstance,lpCmdLine,nShowCmd);
	
	// create score manager singleton
	ScoreManager::Create();

	// create a high score table singleton
	HighScores::Create();

	GameStateManager::Create();
	GameStateManager::GetInstance()->AddState(new FrontEnd);
	GameStateManager::GetInstance()->AddState(new Options);
	GameStateManager::GetInstance()->AddState(new MissionBrief);
	GameStateManager::GetInstance()->AddState(new Game);
	GameStateManager::GetInstance()->AddState(new Demo);
	GameStateManager::GetInstance()->AddState(new GameOver);
	GameStateManager::GetInstance()->AddState(new GameCompleted);
	GameStateManager::GetInstance()->AddState(new EnterName);
	GameStateManager::GetInstance()->SwitchState("DEMO");
	
	timerFrequency = timer.GetFrequency();
	lastTime = timer.GetTime();

	while(Program::GetInstance()->GetRunning())
	{
		unsigned __int64 timeNow = timer.GetTime();
		unsigned __int64 timeDiff = timeNow - lastTime;
		lastTime = timeNow;

		if (timeDiff > timerFrequency)
		{
			timeDiff = timerFrequency;
		}

		float timeDiffFloat = (float)timeDiff / (float)timerFrequency;


		while(PeekMessage(&msg, NULL, 0,0, PM_NOREMOVE))
		{
			BOOL bGetResult = GetMessage(&msg, NULL, 0, 0);
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		Program::GetInstance()->Update(timeDiffFloat);
		Program::GetInstance()->Render();
	}
	
	// destory singletons
	HighScores::Destroy();
	ScoreManager::Destroy();
	Program::Destroy();
	GameStateManager::Destroy();

#ifdef _DEBUG
	
	_CrtDumpMemoryLeaks();
#endif

	return (int)msg.wParam;
}

