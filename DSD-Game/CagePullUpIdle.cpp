
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CharacterManager.h"
#include "CagePullUpIdle.h"

/**********************************************************************************************************************************/

CagePullUpIdle::CagePullUpIdle() : 
	Behavior("CAGE_PULLUPIDLE")
{

}

/**********************************************************************************************************************************/

CagePullUpIdle::~CagePullUpIdle()
{

}

/**********************************************************************************************************************************/

void 
CagePullUpIdle::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "HIDE_IDLE");
}

/**********************************************************************************************************************************/

void 
CagePullUpIdle::Update(Character* owner, float elapsedTime)
{
	if(strcmp(CharacterManager::GetInstance()->GetCharacter("Shark1")->GetBehaviourID(),"SHARK_DEAD") == 0)
	{
		owner->SetBehavior("CAGE_LOWER");
	}
}

/**********************************************************************************************************************************/

void 
CagePullUpIdle::OnExit(Character* owner)
{

}

/**********************************************************************************************************************************/
