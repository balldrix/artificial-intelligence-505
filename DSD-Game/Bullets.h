#ifndef _BULLETS_H_
#define _BULLETS_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif //_RENDEREVENT_H_

enum UpgradeState;
class Model;
class Character;

class Bullets : public RenderEvent
{
public:
	/**
	 * Constructor
	 */
	Bullets();
/**
 * Destructor
 */
	~Bullets();
/**
 * Sets up the model and sets the bullets id
 */
	void				CreateBullet(
		const char* ID
	);
/**
 * Renders the bullet if it is active
 */
	virtual void		Render();
	/**
	 * Updates the movement of the bullet and detects if it has hit any characters
	 * if it is still active
	 */
	void				Update(
		float elapsedTime
	);
/**
 * Sets the position of the bullet
 */
	void				SetPosition(
		const Vector3D& pos
	);
/**
 * Gets current position of the bullet
 */
	const Vector3D&		GetPosition() const;
	/**
	 * Sets the current rotation of the bullet
	 */
	void				SetRotation(
		const Vector3D& rot
	);
/**
 * Gets the current rotation of the bullet
 */
	const Vector3D&		GetRotation() const;
	/**
	 * Sets the direction the bullet should move
	 */
	void				SetForward(
		const Vector3D& forward
	);
/**
 * Gets the direction of the bullet
 */
	const Vector3D&		GetForward() const;
	/**
	 * Sets the bullet active
	 */
	void				SetAlive(
		bool alive
	);
/**
 * Returns if the bullet is currently active
 */
	bool				GetAlive() const;

	// set upgrade state
	void				SetState(UpgradeState state);

	// set homing missile target
	void				SetTarget(Character* target);

	// turn towards target around Y axis
	void TurnToTargetY(float elapsedTime);

	// turn towards target around X axis
	void AdjustHeight(float elapsedTime);

	// set lifespan
	void SetLife(float life);


private:
	MyString m_ID; 

	Model* m_model;
	Vector3D m_position;
	Vector3D m_rotation;
	Vector3D m_forward;
	Vector3D m_up;
	float m_life;

	bool m_alive;

	UpgradeState m_currentUpgrade;
	Character* m_target;

	float m_speed;
};

#endif //_BULLET_H_
