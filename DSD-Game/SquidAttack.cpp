
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "SquidBehaviors.h"
#include "SquidAttack.h"

#include "ScoreManager.h"
#include "SoundManager.h"
#include "SoundEmitter.h"
#include "ParticleEmitter.h"

#include "EnemyProjectile.h"
#include "Player.h"


/**********************************************************************************************************************************/

SquidAttack::SquidAttack() :
	Behavior("SQUID_ATTACK")
{
	m_inkProjectile = NULL;
	
}

/**********************************************************************************************************************************/

SquidAttack::SquidAttack(Player* player) :
	Behavior("SQUID_ATTACK"),
	m_shootFlag(false)
{
	// copy player pointer
	m_player = player;

	// create new projectile
	m_inkProjectile = new EnemyProjectile();
	m_inkProjectile->CreateProjectile("Ink_Projectile", "INK_PROJECTILE", player);
}

SquidAttack::~SquidAttack()
{
	//Destory projectile
	delete m_inkProjectile;
	m_inkProjectile = NULL;
}

/**********************************************************************************************************************************/

void
SquidAttack::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "ATTACK");
	m_shootFlag = true;
}

/**********************************************************************************************************************************/

void 
SquidAttack::Update(Character* owner, float elapsedTime)
{
	//attack the player
	float tempAngle = AngleToTarget(owner,m_playerCharacter->GetPosition());

	if(TurnTo(owner,tempAngle,SQUIDROTPERSEC,elapsedTime) == TD_FACING)
	{
		SetAnimationAndCacheIndex(owner, "ATTACK");

		if(m_shootFlag)
		{
			// shoot and reset flag
			m_shootFlag = false;
			ShootInk(owner);
		}
	}
	else
	{
		SetAnimationAndCacheIndex(owner, "SWIM");
	}

	FollowTerrain(owner,elapsedTime);
	
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );
	if(strcmp(anim,"ATTACK")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();

		if( td.Position >= period - 0.1f)
		{
			pAC->SetTrackPosition(track,0);

			
			float distSq = DistanceToCharacterSq(owner,*m_playerCharacter);
			if( distSq > SQUIDATTACKDIST * SQUIDATTACKDIST)
			{		
				owner->SetBehavior("SQUID_CHASE");
			}
		}
	}

	//update projectile
	if(m_inkProjectile != NULL)
	{
		m_inkProjectile->Update(elapsedTime);
	}

	if(owner->GetHitCount() > NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("SQUID_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::SQUID);
	}

	// if health is below 50%
	if(owner->GetHitCount() > (NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty()) / 2)
	{
		// release ink cloud
		ParticleEmitter* ink = new ParticleEmitter();
		ink->SetUpEmitter(1000, owner->GetPosition(), owner->GetPosition(), Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 1.0f, 3.0f, 5.0f, 50);
		ink->SetID("INK");
		ink->LoadTexture("ink", "Ink.tga");
		ink->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(ink);

		// set to flee
		owner->SetBehavior("SQUID_FLEE");

		// set target position to away from 
		Matrix rotation;
		Vector3D facing = owner->GetPosition() - m_player->GetPosition();
		MatrixRotationY(&rotation, D3DX_PI);
		Vector3DTransformCoord(&facing, &facing, &rotation);
		Vector3DNormalize(&facing, &facing);

		owner->SetDestination(facing * SQUIDCHASEDIST);
	}
}

/**********************************************************************************************************************************/

void	
SquidAttack::OnExit(Character* owner)
{
}

void
SquidAttack::ShootInk(Character* owner)
{
	if(m_inkProjectile != NULL)
	{
	//if the projectile is not alive
		if(!m_inkProjectile->GetAlive())
		{
			//set it alive
			m_inkProjectile->SetAlive(true);

			//compute Position 
			Vector3D pos, forward;
			Matrix tempRot;
			Matrix rotateX, rotateY, rotateZ;

			//set positions and rotations for the bullets
			MatrixRotationX(&rotateX, owner->GetRotation().x);
			MatrixRotationY(&rotateY, owner->GetRotation().y);
			MatrixRotationZ(&rotateZ, owner->GetRotation().z);
			MatrixMultiply(&tempRot, &rotateX, &rotateY);
			MatrixMultiply(&tempRot, &tempRot, &rotateZ);

			Vector3DTransformCoord(&pos, &INKLOCATOR, &tempRot);

			pos += owner->GetPosition();
			m_inkProjectile->SetPosition(pos);
			m_inkProjectile->SetRotation(owner->GetRotation());
			Vector3DNormalize(&forward, &Vector3D(owner->GetPosition() - m_player->GetPosition()));
			m_inkProjectile->SetForward(forward);

			//play shoot sound
			SoundManager::GetInstance()->GetEmitter("SHOOT")->Play();
			SoundManager::GetInstance()->GetEmitter("SHOOT")->SetPosition(owner->GetPosition());
			SoundManager::GetInstance()->GetEmitter("SHOOT")->SetVolume(200);

			return;
		}
	}
}

/**********************************************************************************************************************************/
