
#ifndef _SHARKDEATH_H_
#define _SHARKDEATH_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class SharkDeath : public Behavior
{
public:
	/**
	 * Constructor
	 */
					SharkDeath();
	/**
	 * Destructor
	 */
	virtual			~SharkDeath();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
private:
	float m_timer;
	bool m_triggered;
};

#endif //#ifndef _SHARKDEATH_H_