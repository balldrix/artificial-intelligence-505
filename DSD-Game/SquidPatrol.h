#ifndef _SQUIDSQUIDPATROL_H_
#define _SQUIDSQUIDPATROL_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class SquidPatrol : public Behavior
{
public:
	/**
	 * Constructor
	 */
					SquidPatrol();
	/**
	 * Destructor
	 */
	virtual			~SquidPatrol();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);

	// check if player is in field of view
	bool CharacterInFov(Character* owner, Character* player, float fov);
};

#endif //#ifndef _SQUIDSQUIDPATROL_H_