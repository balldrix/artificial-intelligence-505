#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CutSceneController.h"
#include "CharacterManager.h"
#include "CutScene.h"
#include "SharkBehaviors.h"
#include "SharkDeath.h"

/**********************************************************************************************************************************/

SharkDeath::SharkDeath() : 
	Behavior("SHARK_DEATH")
{

}

/**********************************************************************************************************************************/

SharkDeath::~SharkDeath()
{

}

/**********************************************************************************************************************************/

void
SharkDeath::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "SharkDIe");
	m_timer = CutSceneController::GetInstance()->GetCutScene("CutScene7")->GetDuration();
	CutSceneController::GetInstance()->BeginCutScene("CutScene7");
	m_triggered = false;
}

/**********************************************************************************************************************************/

void 
SharkDeath::Update(Character* owner, float elapsedTime)
{
	//remove character

	FollowTerrain(owner,elapsedTime);
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();


	pAC->GetTrackAnimationSet( track, &pASTrack );
	if(strcmp(anim,"SharkDIe")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{
			SetAnimationAndCacheIndex(owner, "DEAD");
			owner->StartFade(3.0f);

		}		
	}

	if(strcmp(anim,"DEAD")==0)
	{
		if(owner->GetFadeTime() <= 0.0f)
		{
			owner->SetRemove(true);
		}
	}


	m_timer -= elapsedTime;

	if(m_timer < 0 && m_triggered == false)
	{
		m_triggered = true;
		CutSceneController::GetInstance()->BeginCutScene("CutScene8");
		CharacterManager::GetInstance()->GetCharacter("CAGE1")->SetBehavior("CAGE_LOWER");
	}

	if(pASTrack)
	{
		pASTrack->Release();
		pASTrack = 0;
	}
}

/**********************************************************************************************************************************/

void	
SharkDeath::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
