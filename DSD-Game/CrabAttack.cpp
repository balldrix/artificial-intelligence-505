
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CrabAttack.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

CrabAttack::CrabAttack() : 
	CrabBehavior("CRAB_ATTACK")
{

}

/**********************************************************************************************************************************/

CrabAttack::~CrabAttack()
{

}

/**********************************************************************************************************************************/

void 
CrabAttack::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "ATTCK");
}

/**********************************************************************************************************************************/

void 
CrabAttack::Update(Character* owner, float elapsedTime)
{
	//attack the player
	
	float tempAngle = AngleToTarget(owner,m_playerCharacter->GetPosition());

	TurnDirection dirToTurn  = TurnTo(owner,tempAngle,CRABROTPERSEC,elapsedTime);

	switch(dirToTurn)
	{
	case TD_FACING:
		SetAnimationAndCacheIndex(owner, "ATTCK");
		break;
	case TD_LEFT:
		SetAnimationAndCacheIndex(owner, "WALK_LEFT");
		break;
	case TD_RIGHT:
		SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
		break;
	default: 
		break;
	};


	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );
	if(strcmp(anim,"ATTCK")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{	
			float distSq = DistanceToCharacterSq(owner,*m_playerCharacter);
			if( distSq > CRABATTACKDIST * CRABATTACKDIST)
			{		
				owner->SetBehavior("CRAB_CHASE");
			}
			pAC->SetTrackPosition(track,0);
		}		
	}
	
	//kill it
	if(owner->GetHitCount() > NUM_CRAB_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("CRAB_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::CRAB);
	}
}

/**********************************************************************************************************************************/

void 
CrabAttack::OnExit(Character* owner)
{
	
}

/**********************************************************************************************************************************/
