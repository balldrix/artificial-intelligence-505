#ifndef _JELLYBEHAVIORS_H_
#define _JELLYBEHAVIORS_H_

// jelly fish constants
const float JELLYATTACKDIST = 15.0f;
const float SHOOTINGDELAY = 12.0f;
const Vector3D BOLTLOCATOR = Vector3D(0.0f, 2.0f, 0.0f);
const int JELLYROTPERSEC = 2;
const float JELLYSPEED = 3.0f;
const float TENTACLE_LENGTH = 5.0f;

#include "JellySwim.h"

#endif //#ifndef _JELLYBEHAVIORS_H_