// PlayerConstants.h
// Christopher Ball 2017
// moved from player.cpp to share with demo diver character

#ifndef _PLAYER_CONSTANTS_H_
#define _PLAYER_CONSTANTS_H_

#include "Types.h"

// player character constants
const Vector3D FORWARD = Vector3D(0, 0, 1);
const float DISTANCEPERSECOND = 8.0f;
const float ROTATIONPERSECOND = 2.0f;
const float ROCKETJUMPDURATION = 1.0f;

const float PLAYERHURTDURATION = 2.0f;
const float HURTFALLTIME = 0.8f;

const float ROCKET_AIR_LIMIT = 5.0f;

const Vector3D GUNLOCATOR = Vector3D(-0.357f, 1.4654f, -1.1402f);
const Vector3D HELMETLOCATOR = Vector3D(-0.0556f, 2.2031f, -0.5089f);

const Vector3D GUNPARTICLEMINVOL = Vector3D(-0.5f, -1.5f, -0.0f);
const Vector3D GUNPARTICLEMAXVOL = Vector3D(0.5f, 1.5f, -10.0f);

const Vector3D JETPACKLOCATOR = Vector3D(0.0f, 0.941f, 0.299f);
const Vector3D JETPACKPARTICLEMINVOL = Vector3D(-0.25f, -1.0f, -0.25f);
const Vector3D JETPACKPARTICLEMAXVOL = Vector3D(1.5f, -5.0f, 1.5f);

const float DEATHHEIGHT = -30.0f;
const float CEILINGHEIGHT = 54.0f;

// enemy detection range
const float DETECTION_RANGE = 40.0f;

// max random shots
const int MAX_SHOTS = 3;

// player weapon upgrades
enum UpgradeState
{
	NONE,
	HEATSEEKER,
	SMARTMISSILE,
	RAPIDFIRE,
	THREEWAY,
	EXPLOSIVE
};

// upgrade time limits
// change updgrade timer back
const float UPGRADE_TIMER = 25.0f;
const float SHIELD_TIMER = 40.0f;

// harpoon shot delay time
const float RAPIDFIRE_DELAY = 0.2f;

// missile and harpoon lifetimes
const float MISSILELIFE = 5.0f;
const float HARPOONLIFETIME = 1.0f;

#endif _PLAYER_CONSTANTS_H_