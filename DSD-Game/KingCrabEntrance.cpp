

#include "Types.h"
#include "Character.h"
#include "CutSceneController.h"
#include "KingCrabEntrance.h"

/**********************************************************************************************************************************/

KingCrabEntrance::KingCrabEntrance() : 
	CrabBehavior("KINGCRAB_ENTER")
{

}

/**********************************************************************************************************************************/

KingCrabEntrance::~KingCrabEntrance()
{
}

/**********************************************************************************************************************************/

void 
KingCrabEntrance::OnEnter(Character* owner)
{
	owner->SetDamageToPlayer(30);
	owner->SetVulnarability(false);
	SetAnimationAndCacheIndex(owner, "ENTRANCE");
	m_timer = 3.0f;
}

/**********************************************************************************************************************************/

void 
KingCrabEntrance::Update(Character* owner, float elapsedTime)
{
	m_timer -= elapsedTime;
	
	if(m_timer < 0 && !CutSceneController::GetInstance()->IsCutSceneActive())
	{
		//call the standard crab update from the behavior manager
		owner->SetBehavior("KINGCRAB_PATROL");
	}
}

/**********************************************************************************************************************************/

void	
KingCrabEntrance::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
