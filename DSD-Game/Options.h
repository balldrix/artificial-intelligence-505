// Options.h
// Christopher Ball 2017
// difficulty options screen

#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif //#ifndef _GAMESTATE_H_

class Options : public GameState
{
public:
	/**
	* Constructor
	*/
	Options();
	/**
	* Destructor
	*/
	~Options();
	/**
	* On entry handler, called when you enter the game state
	*/
	virtual void		OnEntry();
	/**
	* On exit handler, called when you exit the game state
	*/
	virtual void		OnExit();
	/**
	* Update handler, called once a frame for the state to update its logic.
	*/
	virtual void		Update(
		float elapsedTime ///< The elapsed time this frame
	);
	/**
	*	Render handler, handles renderering the game for its current state
	*/
	virtual void		Render();

private:

	enum DifficultyOptions
	{
		NORMAL,
		HARD
	}m_difficultOption;
};

#endif _OPTIONS_H_