
#ifndef _KINGCRABPATROL_H_
#define _KINGCRABPATROL_H_

#ifndef _CRABBEHAVIOR_H_
#include "CrabBehavior.h"
#endif //#ifndef _CRABBEHAVIOR_H_

class KingCrabPatrol : public CrabBehavior
{
public:
	/**
	 * Constructor
	 */
					KingCrabPatrol();
	/**
	 * Destructor
	 */
	virtual			~KingCrabPatrol();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
};


#endif //#ifndef _KINGCRABPATROL_H_