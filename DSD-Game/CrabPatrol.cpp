
#include "Types.h"
#include "Character.h"
#include "CrabPatrol.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

CrabPatrol::CrabPatrol() : 
	CrabBehavior("CRAB_PATROL")
{

}

/**********************************************************************************************************************************/

CrabPatrol::~CrabPatrol()
{

}

/**********************************************************************************************************************************/

void 
CrabPatrol::OnEnter(Character* owner)
{
	owner->SetSpeed(CRABDISTPERSEC);
	SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
}

/**********************************************************************************************************************************/

void 
CrabPatrol::Update(Character* owner, float elapsedTime)
{
	float tempAngle = SideAngleToTarget(owner,owner->GetTargetWayPoint()->m_Position);
	
	TurnDirection dirToTurn  = TurnTo(owner,tempAngle,CRABROTPERSEC,elapsedTime);

	switch(dirToTurn)
	{
	case TD_FACING:
		SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
		WalkSideways(owner,owner->GetTargetWayPoint()->m_Position,elapsedTime);
		break;
	case TD_LEFT:
		SetAnimationAndCacheIndex(owner, "WALK_LEFT");
		break;
	case TD_RIGHT:
		SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
		break;
	default: 
		break;
	};

	FollowTerrain(owner,elapsedTime);	

	if(DistanceToTargetSq(owner,owner->GetTargetWayPoint()->m_Position) < 0.1f)
	{
		owner->ArrivedAtWaypoint();
	}
	
	if(DistanceToCharacterSq(owner,*m_playerCharacter) <= CRABCHASEDIST * CRABCHASEDIST)
	{
		owner->SetBehavior("CRAB_CHASE");		
	}
	
	//kill it
	if(owner->GetHitCount() > NUM_CRAB_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("CRAB_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::CRAB);
	}
}

/**********************************************************************************************************************************/

void 
CrabPatrol::OnExit(Character* owner)
{
	
}

/**********************************************************************************************************************************/
