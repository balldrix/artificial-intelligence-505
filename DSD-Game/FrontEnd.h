#ifndef _FRONTEND_H_
#define _FRONTEND_H_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif //#ifndef _GAMESTATE_H_

class FrontEnd : public GameState
{
public:
	/**
	 * Constructor
	 */
						FrontEnd();
	/**
	 * Destructor
	 */
						~FrontEnd();
	/**
	 * On entry handler, called when you enter the game state
	 */
	virtual void		OnEntry();
	/**
	 * On exit handler, called when you exit the game state
	 */
	virtual void		OnExit();
	/**
	 * Update handler, called once a frame for the state to update its logic.
	 */
	virtual void		Update(
							float elapsedTime ///< The elapsed time this frame
							);
	/**
	 *	Render handler, handles renderering the game for its current state
	 */
	virtual void		Render();

	// display Highscores
	void DisplayHighScores();
	void DisplayMenu();

private:
	
	enum SelectedOption
	{
		START = 0,
		HIGH_SCORES_OPTION,
		QUIT
	}m_selectedOption;

	enum CurrentScreen
	{
		MENU = 0,
		HIGH_SCORES_SCREEN
	}m_currentScreen;
};

#endif