#include "HighScores.h"
#include "Singleton.h"

// set HighScores class as a singleton class
DefineSingleton(HighScores);

HighScores::HighScores()
{
	// sets default high score table at beginning of game
	m_highScoreList[9] = { "RALPH", 100000 };
	m_highScoreList[8] = { "DILLON", 50000 };
	m_highScoreList[7] = { "KEN", 20000 };
	m_highScoreList[6] = { "NATHAN", 15000 };
	m_highScoreList[5] = { "ZAK", 10000 };
	m_highScoreList[4] = { "HAYLEY", 8000 };
	m_highScoreList[3] = { "CARLTON", 5000 };
	m_highScoreList[2] = { "MATT", 2000 };
	m_highScoreList[1] = { "EMMA", 100 };
	m_highScoreList[0] = { "GAVIN", 0 };
}

HighScores::~HighScores()
{
}

bool 
HighScores::IsHighScore(unsigned int newScore)
{
	for(unsigned int i = 0; i < MAX_HIGH_SCORES; i++)
	{
		if(newScore > m_highScoreList[i].score)
		{
			return true;
		}
	}
	return false;
}

void
HighScores::InsertScore(Score newScore)
{
	// create a new temp var to store new position in table
	unsigned int newPosition = 0;

	// loop through the table to find new position for new score
	for(unsigned int i = 0; newScore > m_highScoreList[i]; i++)
	{
		newPosition = i;
	}

	// shift lower scores down
	for(unsigned int i = 0; i < newPosition; i++)
	{
		m_highScoreList[i] = m_highScoreList[i + 1];
	}

	// insert new score
	m_highScoreList[newPosition] = newScore;
}

MyString HighScores::GetTable()
{
	MyString table("");
	MyString buffer(""); // buffer string
	for(int i = MAX_HIGH_SCORES - 1; i >= 0; i--)
	{
		// get name
		const char* name = m_highScoreList[i].name;

		// get score 
		unsigned int score = m_highScoreList[i].score;
		// create line from score in index i
		buffer.Format("%-10s \t %10u \n", name, score);
		// add line to table string
		table.Concatenate(buffer);
	}
	return table;
}
