#include "Types.h"
#include "FrontEnd.h"
#include "UIManager.h"
#include "UIObject.h"
#include "UISprite.h"
#include "UIText.h"
#include "InputManager.h"
#include "GameStateManager.h"
#include "Program.h"
#include "Game.h"
#include "SoundManager.h"
#include "SoundEmitter.h"

#include "HighScores.h"

#define UPKEY 0xC8
#define DOWNKEY 0xD0
#define SPACEBAR 0x39
#define ENTER 0x1C

/**********************************************************************************************************************************/

FrontEnd::FrontEnd() : 
	GameState("FRONTEND"),
	m_selectedOption(START),
	m_currentScreen(MENU)
{
}

/**********************************************************************************************************************************/

FrontEnd::~FrontEnd()
{
}

/**********************************************************************************************************************************/

void 
FrontEnd::OnEntry()
{
	UIManager::GetInstance()->SetCurrentUI("FRONTEND");
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->SetPosition(Vector3D(0,0,0));
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Play(true);

	// hide high score table and BG
	UIManager::GetInstance()->GetUIObject("HighScoresBG")->SetVisible(false);
	UIManager::GetInstance()->GetUIObject("HighScoreTable")->SetVisible(false);
}

/**********************************************************************************************************************************/

void 
FrontEnd::OnExit()
{
	UIManager::GetInstance()->SetCurrentUI("OPTIONS");
}

/**********************************************************************************************************************************/

void 
FrontEnd::Update(float elapsedTime)
{
	UIManager* uiManager = UIManager::GetInstance();

	// if menu is active
	if(m_currentScreen == MENU)
	{

		if(m_selectedOption == START)
		{
			uiManager->GetUIObject("Start")->SetVisible(false);
			uiManager->GetUIObject("StartClick")->SetVisible(true);
			uiManager->GetUIObject("Quit")->SetVisible(true);
			uiManager->GetUIObject("QuitClick")->SetVisible(false);

			// set high scores button
			uiManager->GetUIObject("HighScoresButton")->SetVisible(true);
			uiManager->GetUIObject("HighScoresButtonClick")->SetVisible(false);
		}
		else if(m_selectedOption == QUIT)
		{
			uiManager->GetUIObject("Start")->SetVisible(true);
			uiManager->GetUIObject("StartClick")->SetVisible(false);
			uiManager->GetUIObject("Quit")->SetVisible(false);
			uiManager->GetUIObject("QuitClick")->SetVisible(true);

			// set high scores button
			uiManager->GetUIObject("HighScoresButton")->SetVisible(true);
			uiManager->GetUIObject("HighScoresButtonClick")->SetVisible(false);
		}
		else if(m_selectedOption == HIGH_SCORES_OPTION)
		{
			uiManager->GetUIObject("Start")->SetVisible(true);
			uiManager->GetUIObject("StartClick")->SetVisible(false);
			uiManager->GetUIObject("Quit")->SetVisible(true);
			uiManager->GetUIObject("QuitClick")->SetVisible(false);

			// set high scores button
			uiManager->GetUIObject("HighScoresButton")->SetVisible(false);
			uiManager->GetUIObject("HighScoresButtonClick")->SetVisible(true);
		}

		if(InputManager::GetInstance()->IsKeyClicked(false, UPKEY))
		{
			if(m_selectedOption == QUIT)
			{
				m_selectedOption = HIGH_SCORES_OPTION; // select high scores
			}
			else if(m_selectedOption == HIGH_SCORES_OPTION)
			{
				m_selectedOption = START; // select start
			}
		}

		if(InputManager::GetInstance()->IsKeyClicked(false, DOWNKEY))
		{
			if(m_selectedOption == START)
			{
				m_selectedOption = HIGH_SCORES_OPTION; // select high scores
			}
			else if(m_selectedOption == HIGH_SCORES_OPTION)
			{
				m_selectedOption = QUIT; // select quit
			}
		}

		if(InputManager::GetInstance()->IsKeyClicked(false, SPACEBAR))
		{
			SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Stop();
			if(m_selectedOption == START)
			{
				GameStateManager::GetInstance()->SwitchState("OPTIONS");
			}
			else if(m_selectedOption == HIGH_SCORES_OPTION)
			{
				DisplayHighScores();
			}
			else if(m_selectedOption == QUIT)
			{
				Program::GetInstance()->SetRunning(false);
			}
		}
	}
	else if(m_currentScreen == HIGH_SCORES_SCREEN)
	{
		if(InputManager::GetInstance()->IsKeyClicked(false, SPACEBAR) ||
		   InputManager::GetInstance()->IsKeyClicked(false, ENTER))
		{
			DisplayMenu();
		}
	}

	InputManager::GetInstance()->Update();
	uiManager->Update(elapsedTime);
	SoundManager::GetInstance()->Update();
}

/**********************************************************************************************************************************/

void 
FrontEnd::Render()
{
	UIManager::GetInstance()->Render();
}

void 
FrontEnd::DisplayHighScores()
{
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Play(true);

	UIManager* uiManager = UIManager::GetInstance();

	uiManager->GetUIObject("Start")->SetVisible(false);
	uiManager->GetUIObject("StartClick")->SetVisible(false);
	uiManager->GetUIObject("Quit")->SetVisible(false);
	uiManager->GetUIObject("QuitClick")->SetVisible(false);

	// set high scores button
	uiManager->GetUIObject("HighScoresButton")->SetVisible(false);
	uiManager->GetUIObject("HighScoresButtonClick")->SetVisible(true);

	// display high score table and BG
	UIManager::GetInstance()->GetUIObject("HighScoresBG")->SetVisible(true);
	UIManager::GetInstance()->GetUIObject("HighScoreTable")->SetVisible(true);

	// string for the table
	MyString table = HighScores::GetInstance()->GetTable();

	// set UI to display table
	static_cast<UIText*>(uiManager->GetUIObject("HighScoreTable"))->SetValue(table);

	// change state of screen
	m_currentScreen = HIGH_SCORES_SCREEN;
}

void 
FrontEnd::DisplayMenu()
{
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Play(true);

	UIManager* uiManager = UIManager::GetInstance();

	uiManager->GetUIObject("Start")->SetVisible(true);
	uiManager->GetUIObject("StartClick")->SetVisible(false);
	uiManager->GetUIObject("Quit")->SetVisible(true);
	uiManager->GetUIObject("QuitClick")->SetVisible(false);

	// set high scores button
	uiManager->GetUIObject("HighScoresButton")->SetVisible(false);
	uiManager->GetUIObject("HighScoresButtonClick")->SetVisible(true);

	// display high score table and BG
	UIManager::GetInstance()->GetUIObject("HighScoresBG")->SetVisible(false);
	UIManager::GetInstance()->GetUIObject("HighScoreTable")->SetVisible(false);

	// change state to menu
	m_currentScreen = MENU;
}

/**********************************************************************************************************************************/
