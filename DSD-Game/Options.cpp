#include "Types.h"
#include "UIManager.h"
#include "UIObject.h"
#include "UISprite.h"
#include "InputManager.h"
#include "GameStateManager.h"
#include "Program.h"
#include "Game.h"
#include "SoundManager.h"
#include "SoundEmitter.h"
#include "ScoreManager.h"

#include "Options.h"

#define UPKEY 0xC8
#define DOWNKEY 0xD0
#define SPACEBAR 0x39

Options::Options() :
	GameState("OPTIONS"),
	m_difficultOption(NORMAL)
{
}

Options::~Options()
{
}

void
Options::OnEntry()
{
	UIManager::GetInstance()->SetCurrentUI("OPTIONS");
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->SetPosition(Vector3D(0, 0, 0));
	SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Play(true);

	// hide some buttons
	UIManager::GetInstance()->GetUIObject("Normal")->SetVisible(false);
	UIManager::GetInstance()->GetUIObject("HardClick")->SetVisible(false);
}

void
Options::OnExit()
{
	UIManager::GetInstance()->SetCurrentUI("BRIEF");
}

void
Options::Update(float elapsedTime)
{
	UIManager* uiManager = UIManager::GetInstance();

	// set correct buttons to be visible
	if(m_difficultOption == NORMAL)
	{
		uiManager->GetUIObject("Normal")->SetVisible(false);
		uiManager->GetUIObject("NormalClick")->SetVisible(true);
		uiManager->GetUIObject("Hard")->SetVisible(true);
		uiManager->GetUIObject("HardClick")->SetVisible(false);
	}
	else if(m_difficultOption == HARD)
	{
		uiManager->GetUIObject("Normal")->SetVisible(true);
		uiManager->GetUIObject("NormalClick")->SetVisible(false);
		uiManager->GetUIObject("Hard")->SetVisible(false);
		uiManager->GetUIObject("HardClick")->SetVisible(true);
	}

	// if up is pressed
	if(InputManager::GetInstance()->IsKeyClicked(false, UPKEY))
	{
		// if difficulty is hard switch to normal
		if(m_difficultOption == HARD)
		{
			m_difficultOption = NORMAL;
		}
	}

	// if down is pressed
	if(InputManager::GetInstance()->IsKeyClicked(false, DOWNKEY))
	{
		// if difficulty is normal switch to hard
		if(m_difficultOption == NORMAL)
		{
			m_difficultOption = HARD;
		}
	}

	// if enter key is pressed
	if(InputManager::GetInstance()->IsKeyClicked(false, SPACEBAR))
	{
		// stop music
		SoundManager::GetInstance()->GetEmitter("TITLEMUSIC")->Stop();

		// check difficulty selected
		if(m_difficultOption == NORMAL)
		{
			ScoreManager::GetInstance()->SetDifficulty(Difficulty::NORMAL);
		}
		else if(m_difficultOption == HARD)
		{
			ScoreManager::GetInstance()->SetDifficulty(Difficulty::HARD);
		}
		
		// switch to mission brief state
		GameStateManager::GetInstance()->SwitchState("BRIEF");
	}

	InputManager::GetInstance()->Update();
	uiManager->Update(elapsedTime);
	SoundManager::GetInstance()->Update();
}

void 
Options::Render()
{
	UIManager::GetInstance()->Render();
}
