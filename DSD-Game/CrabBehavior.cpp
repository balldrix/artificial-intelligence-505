
#include "Types.h"
#include "Character.h"
#include "CrabBehavior.h"

/**********************************************************************************************************************************/

CrabBehavior::CrabBehavior(const char* ID) : 
	Behavior(ID)
{

}

/**********************************************************************************************************************************/

CrabBehavior::~CrabBehavior()
{

}

/**********************************************************************************************************************************/

void 
CrabBehavior::WalkSideways(Character *owner, const Vector3D& targetPos, float elapsedTime)
{
	float angle = owner->GetRotation().y;

	Vector3D position = owner->GetPosition();
	Vector3D velocity(-cos(angle), 0.0f, sin(angle));
	velocity *= owner->GetSpeed();

	position += velocity * elapsedTime;

	owner->SetPosition(position);
	owner->SetVelocity(velocity);

	FollowTerrain(owner,elapsedTime);
}

/**********************************************************************************************************************************/

float 
CrabBehavior::SideAngleToTarget(Character *owner, const Vector3D& targetPos)
{
	float angle1 = (GetPathAngleToTarget(owner,owner->GetTargetWayPoint()->m_Position) + (D3DX_PI * 0.5f)) - D3DX_PI;
	return angle1;
}

/**********************************************************************************************************************************/

