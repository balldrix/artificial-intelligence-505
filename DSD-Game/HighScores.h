// HighScores.h
// Christopher Ball 2017
// holds high scores in a table along with player names

#ifndef _HIGH_SCORES_H_
#define _HIGH_SCORES_H_

#include "MyString.h"
#include "Singleton.h"

const unsigned int MAX_HIGH_SCORES = 10;

// basic structure of score entry
struct Score
{
	MyString name; // player's name
	unsigned int score; // player's score
	bool operator >(const Score& rhs)
	{
		if(score > rhs.score)
		{
			return true;
		}
		return false;
	}
};


class HighScores : public Singleton<HighScores>
{
public:
	HighScores();
	~HighScores();
	bool IsHighScore(unsigned int newScore); // returns true if newScore needs to be added to table
	void InsertScore(Score newScore); // adds new score into the table
	Score GetScore(int index) const { return m_highScoreList[index]; } // returns score at index
	MyString GetTable();

private:
	Score m_highScoreList[MAX_HIGH_SCORES]; // container of high scores
};

#endif _HIGH_SCORES_H_

