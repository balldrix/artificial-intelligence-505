#include "Types.h"
#include "Stats.h"
#include "Model.h"
#include "Effect.h"
#include "Character.h"
#include "Game.h"
#include "InputManager.h"
#include "CameraController.h"
#include "ThirdPersonCamera.h"
#include "BaseCamera.h"
#include "Text.h"
#include "ModelManager.h"
#include "FreeCamera.h"
#include "EffectManager.h"
#include "RenderManager.h"
#include "TextureCache.h"
#include "CharacterManager.h"
#include "Player.h"
#include "EnvironmentManager.h"
#include "ParticleSystem.h"
#include "ParticleEmitter.h"
#include "RenderTargetSurface.h"
#include "Program.h"
#include "Vertex.h"
#include "ShadowMap.h"
#include "SoundManager.h"
#include "SoundListener.h"
#include "SoundEmitter.h"
#include "PropManager.h"
#include "PropItem.h"
#include "ShadowManager.h"
#include "EntityLoader.h"
#include "WaypointManager.h"
#include "BillBoardRegion.h"
#include "BehaviorManager.h"
#include "CutSceneController.h"
#include "CrabBehaviors.h"
#include "SquidBehaviors.h"
#include "FishBehaviors.h"
#include "KingBehaviors.h"
#include "SharkBehaviors.h"
#include "JellyBehaviors.h"
#include "CageBehaviours.h"
#include "UIManager.h"
#include "UIObject.h"
#include "ScoreManager.h"

const Vector3D INITIALSHADOWPOSITION = Vector3D(-100,75,-0);


/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Game Constructor
//////////////////////////////////////////////////////////////////////////

Game::Game() :
	GameState("GAME"),
	m_KingCrabTriggered(false),
	m_KingCrabKilled(false),
	m_CrownShown(false),
	m_CrownCollected(false),
	m_treasure(0),
	m_player(0),
	m_stats(0)
{
	AStar::Create();
	

	//Create a polygon Quad for the screen
	D3DVIEWPORT9 vp = {0, 0, 800, 600, 0.0f, 1.0f};
	m_renderTarget = new RenderTargetSurface(800, 600, 0, D3DFMT_X8R8G8B8, true, D3DFMT_D24S8, vp, true);
	TextureCache::GetInstance()->AddTexture("Noise","noise.tga");

	m_D3DDevice->CreateVertexBuffer(6*sizeof(VertexPositionTexCoord), D3DUSAGE_WRITEONLY,
		0, D3DPOOL_MANAGED, &m_renderTargetVertex, 0);

	VertexPositionTexCoord* v = 0;
	m_renderTargetVertex->Lock(0, 0, (void**)&v, 0);
	v[0] = VertexPositionTexCoord(-1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
	v[1] = VertexPositionTexCoord(1.0f, 1.0f, 0.0f, 1.0f, 0.0f);
	v[2] = VertexPositionTexCoord(-1.0f, -1.0f, 0.0f, 0.0f, 1.0f);
	v[3] = VertexPositionTexCoord(-1.0f, -1.0f, 0.0f, 0.0f, 1.0f);
	v[4] = VertexPositionTexCoord(1.0f, 1.0f, 0.0f, 1.0f, 0.0f);
	v[5] = VertexPositionTexCoord(1.0f, -1.0f, 0.0f, 1.0f, 1.0f);
	m_renderTargetVertex->Unlock();

	//Create The Stats Object
	m_stats = new Stats();

	
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Game Destructor
//////////////////////////////////////////////////////////////////////////

Game::~Game()
{
	//Delete stats and render target quad
	delete m_stats;
	delete m_renderTarget;
	
	m_renderTargetVertex->Release();
	AStar::Destroy();
}
/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//OnEntry method, creates singletons specific to the Game gamestate.
//////////////////////////////////////////////////////////////////////////

void
Game::OnEntry()
{
	/// Reset the flags
	m_KingCrabTriggered = false;
	m_KingCrabKilled = false;
	m_CrownShown = false;	
	m_CrownCollected = false;

	//Create Singleton
	UIManager::GetInstance()->SetCurrentUI("LOADING");
	EntityLoader::ResetLoadingCounter();
	RECT clientRect;
	GetClientRect(Program::GetInstance()->GetWindow(), &clientRect);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::PARTICLE), SetInt, "m_ScreenSize", clientRect.bottom);
	RenderManager::Create();
	EnvironmentManager::Create();
	PropManager::Create();
	CharacterManager::Create();
	ModelManager::Create();
	ShadowManager::Create();
	CameraController::Create();
	ParticleSystem::Create();
	WaypointManager::Create();
	WaypointManager::GetInstance()->LoadWayPointFile("Assets\\Scripts\\WAYPOINTS.txt");
	//SetUp Camera
	CameraController::GetInstance()->AddCamera(new ThirdPersonCamera);
	CameraController::GetInstance()->GetCurrentCamera()->SetPosition(Vector3D(0,5,0));
	static_cast<ThirdPersonCamera*>(CameraController::GetInstance()->GetCurrentCamera())->SetUpThirdPersonCamera(0.0f,0.5f);

	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetBool, "m_CutScene", false);
	//Set Loading Bar Information
	EntityLoader::SetLoadingCounterEndPoint(5240);
	EntityLoader::SetLoadingBarMaxWidth(342);
	EntityLoader* loader = new EntityLoader();
	//Load In Assets
	loader->LoadAssetList("Assets\\Scripts\\ASSETS.txt");
	EntityLoader::SetLoadingCounterIncrement(2);
	loader->LoadCharacterList("Assets\\Scripts\\ENEMIES.txt");
	EntityLoader::SetLoadingCounterIncrement(1);
	loader->LoadPropList("Assets\\Scripts\\PROPS.txt");
	EntityLoader::SetLoadingCounterIncrement(75);
	loader->LoadBillBoardRegion("Assets\\Scripts\\BillBoardRegion.txt");

	//Load in Cut scene information
	CutSceneController::Create();
	CutSceneController::GetInstance()->Loader("Assets\\Scripts\\CUTSCENES.txt");
	CutSceneController::GetInstance()->CharacterSpawnLoader("Assets\\Scripts\\CUTSCENESSPAWNLIST.txt");
	CutSceneController::GetInstance()->LoadReigonData("Assets\\Scripts\\Reigons.txt");
	//Create player
	m_player = new Player();
	m_player->SetDemo(false);
	int coins;
	float fogMinDistance = 5.0f;
	float fogMaxDistance = 80.0f;
	loader->LoadGameParameters("Assets\\Scripts\\PARAMS.txt",coins,m_treasure,fogMinDistance,fogMaxDistance);
	m_player->SetCoinsRequiredForHealthBoost(coins);
	delete loader;
	//Set the UI to the Game UI State
	UIManager::GetInstance()->SetCurrentUI("GAME");

	BehaviorManager::Create();

	//Set Background fog colour to both DirectX Device and shaders
	int ClearBufferColour[3];
	Program::GetInstance()->GetClearBufferColour(ClearBufferColour[0],ClearBufferColour[1],ClearBufferColour[2]);
	Vector3D fogColour;
	fogColour.x = (float)((1.0f / 255) * ClearBufferColour[0]);
	fogColour.y = (float)((1.0f / 255) * ClearBufferColour[1]);
	fogColour.z = (float)((1.0f / 255) * ClearBufferColour[2]);

	EffectSetAndCacheHandle2(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP), SetVector4, "m_FogColour", fogColour,1.0f);
	EffectSetAndCacheHandle2(EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD), SetVector4, "m_FogColour", fogColour,1.0f);
	EffectSetAndCacheHandle2(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetVector4, "m_FogColour", fogColour,1.0f);
	EffectSetAndCacheHandle2(EffectManager::GetInstance()->GetEffect(EffectManager::FOG), SetVector4, "m_FogColour", fogColour,1.0f);

	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP), SetFloat, "m_FogMinDist", fogMinDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD), SetFloat, "m_FogMinDist", fogMinDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetFloat, "m_FogMinDist", fogMinDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::FOG), SetFloat, "m_FogMinDist", fogMinDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::SHADOWMAP), SetFloat, "m_FogMaxDist", fogMaxDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::BILLBOARD), SetFloat, "m_FogMaxDist", fogMaxDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetFloat, "m_FogMaxDist", fogMaxDistance);
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::FOG), SetFloat, "m_FogMaxDist", fogMaxDistance);

	//Create particle effects
	
	ParticleEmitter* spray = new ParticleEmitter();
	spray->SetUpEmitter(15,Vector3D(0,0,0),Vector3D(0,0,0),Vector3D(-0.5f,1.0f,-0.5f),Vector3D(0.5f,3.0f,0.5f),0.1f,2.9f,3.0f,1);
	spray->LoadTexture("partical1","bubble_sprite.tga");
	spray->SetLooping(true);
	spray->SetID("HelemetParticle");
	ParticleSystem::GetInstance()->AddEmitter(spray);


	ParticleEmitter* spray1 = new ParticleEmitter();
	spray1->SetUpEmitter(150,Vector3D(-70,-10,-70),Vector3D(70,10,70),Vector3D(-0.5f,-0.005f,-0.5f),Vector3D(0.5f,0.005f,0.5f),0.1f,0.1f,10.0f,2);
	spray1->LoadTexture("partical1","bubble_sprite.tga");
	spray1->SetLooping(true);
	spray1->SetID("EnvironmentParticle");
	ParticleSystem::GetInstance()->AddEmitter(spray1);

	//Set Sound positions
	SoundManager::GetInstance()->AddListener("PlayerListener");
	SoundManager::GetInstance()->GetListener("PlayerListener")->SetParams(1.0f,5.0f,9.0f,true);
    SoundManager::GetInstance()->GetEmitter("BUBBLE")->Play(true);
	SoundManager::GetInstance()->GetEmitter("BUBBLE")->SetPosition(Vector3D(-50,5,0));
	SoundManager::GetInstance()->GetEmitter("BUBBLE")->SetVelocity(Vector3D(0,0,0));
	
	SoundManager::GetInstance()->GetEmitter("GAMEMUSIC")->Play(true);

	CreateBehaviors();

	// reset score to zero
	ScoreManager::GetInstance()->ResetScore();
}
/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//OnExit method, Deletes singletons specific to the Game gamestate.
//////////////////////////////////////////////////////////////////////////

void
Game::OnExit()
{
	//Delete Singletons
	SoundManager::GetInstance()->GetEmitter("BUBBLE")->Stop();
	SoundManager::GetInstance()->GetEmitter("GAMEMUSIC")->Stop();
	SoundManager::GetInstance()->GetEmitter("ENDBOSS")->Stop();
	delete m_player;
	EnvironmentManager::Destroy();
	WaypointManager::Destroy();
	ShadowManager::Destroy();
	CutSceneController::Destroy();
	ModelManager::Destroy();
	CharacterManager::Destroy();
	PropManager::Destroy();
	BehaviorManager::Destroy();
	ParticleSystem::Destroy();
	CameraController::Destroy();
	RenderManager::Destroy();

}
/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Update
//////////////////////////////////////////////////////////////////////////

void 
Game::Update(float elapsedTime)
{
	//Check Game Events
	TriggerGameEvents();

	//Update the cutscene controller
	CutSceneController::GetInstance()->Update(elapsedTime);
	
	//update a static timer
	static float time = 0;
	time +=elapsedTime;
	//Update the RenderTarget shader (for screen wibble effect)
	EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetFloat, "m_Time", (time));
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->SetTexture(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetHandle("m_Noise")->m_handle,TextureCache::GetInstance()->GetTexture("Noise"));
	//update stats
	m_stats->Update(elapsedTime);
	//update input manager
	InputManager::GetInstance()->Update();
	//Update characters
	CharacterManager::GetInstance()->UpdateCharacters(elapsedTime);
	if(!m_player->Update(elapsedTime))
	{
		//if the player has been killed exit the update
		return;
	}
	
	//Update all other key systems
	PropManager::GetInstance()->Update(elapsedTime);

	SoundManager::GetInstance()->Update();

	ParticleSystem::GetInstance()->Update(elapsedTime);


	if(strcmp(CameraController::GetInstance()->GetCurrentCamera()->GetType(),"THIRDPERSON") == 0)
	{
		static_cast<ThirdPersonCamera*>(CameraController::GetInstance()->GetCurrentCamera())->SetTarget(m_player->GetPosition());
	}

	CameraController::GetInstance()->Update(elapsedTime);

	CameraController::GetInstance()->ComputeCamera();
	UIManager::GetInstance()->Update(elapsedTime);

	//update render positions for particles, shadows and effects
	const Vector3D& cameraPos = CameraController::GetInstance()->GetCurrentCameraPosition();
	ShadowManager::GetInstance()->GetShadowMap()->UpdateLightPosition(m_player->GetPosition());
	//EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::VERTEXBLEND), SetVector4, "m_LightPosition", Vector4D(INITIALSHADOWPOSITION.x + cameraPos.x,INITIALSHADOWPOSITION.y,INITIALSHADOWPOSITION.z + cameraPos.z,0));
	
	Vector3D emitRegionMin(-70,-10,-70);
	Vector3D emitRegionMax(70,10,70);
	emitRegionMin += cameraPos;
	emitRegionMax += cameraPos;
	ParticleSystem::GetInstance()->GetEmitter("EnvironmentParticle")->SetEmitRegion(emitRegionMin, emitRegionMax);
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Render
//////////////////////////////////////////////////////////////////////////

void
Game::Render()
{
	RenderManager::GetInstance()->RenderShadowMap();

	// Start rendering to target
	m_D3DDevice->EndScene();
	m_renderTarget->BeginScene();
	//Create screne
	Program::GetInstance()->ClearBuffer();
	//render the game
	RenderManager::GetInstance()->Render();

	// Stop rendering to target
	m_renderTarget->EndScene();
	m_D3DDevice->BeginScene();
	
	//Set Scene Negative

	if(m_player->GetCharacterState() == Player::ELECTROCUTED)
	{
		if(m_player->IsFlashing())
		{
			EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetBool, "m_negative", false);
		}
		else
		{
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetBool, "m_negative", true);
	}
		
	}
	else
	{
		EffectSetAndCacheHandle1(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetBool, "m_negative", false);
	}

	//Set RenderTarget Shader effect textures
	EffectSetAndCacheHandle0(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET), SetTechnique, "mTech");
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->SetTexture(EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetHandle("m_Texture")->m_handle,m_renderTarget->GetTexture());
	m_D3DDevice->SetStreamSource(0, m_renderTargetVertex, 0, sizeof(VertexPositionTexCoord));
	m_D3DDevice->SetVertexDeclaration (VertexPositionTexCoord::Decl);
	UINT numPasses = 0;
	//Begin Effect
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->Begin(&numPasses,0);
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->BeginPass(0);
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->SetActive(true);
	//Render quad that contains the scene texture
	m_D3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);
	//End quad
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->EndPass();
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->GetEffect()->End();
	EffectManager::GetInstance()->GetEffect(EffectManager::RENDERTARGET)->SetActive(false);

	//render stats
	m_stats->Render();

	//render UI if there is no cutscene
	if(!CutSceneController::GetInstance()->IsCutSceneActive())
	{
		UIManager::GetInstance()->Render();
    }
	//display cheating text id the cheat mode is active
	if(m_player->Cheating())
	{
		UIManager::GetInstance()->GetTextWriter()->PrintText(Vector2D(300,100), "CHEAT MODE_ON");
	}
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//CreateBehaviors adds the behaviors classes to the behaviour manager
//and sets an initial behavior to each NPC
//////////////////////////////////////////////////////////////////////////

void 
Game::CreateBehaviors()
{
	//Create the behavior states
	BehaviorManager::GetInstance()->AddBehavior(new CrabPatrol());
	BehaviorManager::GetInstance()->AddBehavior(new CrabChase());
	BehaviorManager::GetInstance()->AddBehavior(new CrabAttack());
	BehaviorManager::GetInstance()->AddBehavior(new CrabDeath());

	BehaviorManager::GetInstance()->AddBehavior(new SquidPatrol());
	BehaviorManager::GetInstance()->AddBehavior(new SquidChase());
	BehaviorManager::GetInstance()->AddBehavior(new SquidAttack(m_player));
	BehaviorManager::GetInstance()->AddBehavior(new SquidDie());
	BehaviorManager::GetInstance()->AddBehavior(new SquidFlee());

	BehaviorManager::GetInstance()->AddBehavior(new FishPatrol());
	BehaviorManager::GetInstance()->AddBehavior(new FishDeath());
	BehaviorManager::GetInstance()->AddBehavior(new FishChase());

	BehaviorManager::GetInstance()->AddBehavior(new KingAttack());
	BehaviorManager::GetInstance()->AddBehavior(new KingVulnerable());
	BehaviorManager::GetInstance()->AddBehavior(new KingCrabEntrance());
	BehaviorManager::GetInstance()->AddBehavior(new KingCrabPatrol());
	BehaviorManager::GetInstance()->AddBehavior(new KingCrabChase());

	BehaviorManager::GetInstance()->AddBehavior(new SharkPatrol());
	BehaviorManager::GetInstance()->AddBehavior(new SharkEntrance());
	BehaviorManager::GetInstance()->AddBehavior(new SharkDeath());
	BehaviorManager::GetInstance()->AddBehavior(new SharkChase());
	BehaviorManager::GetInstance()->AddBehavior(new SharkAttack());

	BehaviorManager::GetInstance()->AddBehavior(new JellySwim(m_player));

	BehaviorManager::GetInstance()->AddBehavior(new CageEntrance());
	BehaviorManager::GetInstance()->AddBehavior(new CageIdle());
	BehaviorManager::GetInstance()->AddBehavior(new CagePullUp());
	BehaviorManager::GetInstance()->AddBehavior(new CagePullUpIdle());
	BehaviorManager::GetInstance()->AddBehavior(new CageLower());
	BehaviorManager::GetInstance()->AddBehavior(new CageEnd());

	
	//Set the enemies starting states
	for(int i = 0; i < CharacterManager::GetInstance()->GetNumberCharacters(); ++i)
	{
		const char* type = CharacterManager::GetInstance()->GetCharacter(i)->GetType();
		if(strcmp(type,"CRAB")==0)
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("CRAB_PATROL");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(true);
			CharacterManager::GetInstance()->GetCharacter(i)->SetDamageToPlayer(20 * ScoreManager::GetInstance()->GetDifficulty());
			continue;
		}
		if(strcmp(type,"SQUID")==0)
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("SQUID_PATROL");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(true);
			CharacterManager::GetInstance()->GetCharacter(i)->SetDamageToPlayer(10 * ScoreManager::GetInstance()->GetDifficulty());
			continue;
		}
		if((strcmp(type,"PURPLE")==0) || (strcmp(type,"CLOWN")==0))
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("FISH_PATROL");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(true);
			CharacterManager::GetInstance()->GetCharacter(i)->SetDamageToPlayer(5 * ScoreManager::GetInstance()->GetDifficulty());
			continue;
		}

		if(strcmp(type,"KING")==0)
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("KINGCRAB_ENTER");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(false);
			CharacterManager::GetInstance()->GetCharacter(i)->SetDamageToPlayer(30 * ScoreManager::GetInstance()->GetDifficulty());
			continue;
		}

		if(strcmp(type,"SHARK")==0)
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("SHARK_PATROL");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(true);
			CharacterManager::GetInstance()->GetCharacter(i)->SetDamageToPlayer(50 * ScoreManager::GetInstance()->GetDifficulty());
			continue;
		}

		if(strcmp(type,"JELLY")==0)
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("JELLY_SWIM");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(false);
			CharacterManager::GetInstance()->GetCharacter(i)->SetDamageToPlayer(50 * ScoreManager::GetInstance()->GetDifficulty());
			continue;
		}

		if(strcmp(type,"CAGE")==0)
		{
			CharacterManager::GetInstance()->GetCharacter(i)->SetBehavior("CAGE_ENTER");
			CharacterManager::GetInstance()->GetCharacter(i)->SetVulnarability(false);
			continue;
		}
	}
}

/*****************************************************************************************/

//////////////////////////////////////////////////////////////////////////
//GameEvents is a seires of flags that controls the triggering of cutscene within the game
//////////////////////////////////////////////////////////////////////////

void
Game::TriggerGameEvents()
{
	//trigger the king crab cutscene
	if(!m_KingCrabTriggered && m_player->GetCargoCollected() >= m_treasure)
	{
		CutSceneController::GetInstance()->BeginCutScene("CutScene1");
		m_KingCrabTriggered = true;
	}
	//trigger from king dead cutscene
	if(m_KingCrabTriggered  && !m_KingCrabKilled)
	{
		if(CharacterManager::GetInstance()->GetCharacter("KING1")->IsDead())
		{
			
			//Swap characters to remove the crown from the crab model, then reset the 
			CharacterManager::GetInstance()->GetCharacter("KING1")->SetMesh(ModelManager::GetInstance()->GetSkinnedMeshPointer("CRAB"));
			CharacterManager::GetInstance()->GetCharacter("KING1")->SetBehavior("CRAB_DEATH");
			
			m_KingCrabKilled = true;
			CutSceneController::GetInstance()->BeginCutScene("CutScene2");
		}
	}
	//trigger for show crown cutscene
	if(m_KingCrabKilled && !m_CrownShown && !CutSceneController::GetInstance()->IsCutSceneActive())
	{
		CutSceneController::GetInstance()->BeginCutScene("CutScene3");
		m_CrownShown = true;
	}
	//trigger for crown collected cutscene.
	if(m_CrownShown && !m_CrownCollected)
	{
		if(PropManager::GetInstance()->GetPropItem("CROWN1")->IsCollected())
		{
			CutSceneController::GetInstance()->BeginCutScene("CutScene4");
			m_CrownCollected = true;
		}
	}
}

/*****************************************************************************************/
