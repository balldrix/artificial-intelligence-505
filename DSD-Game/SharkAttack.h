
#ifndef _SHARKATTACK_H_
#define _SHARKATTACK_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class SharkAttack : public Behavior
{
public:
	/**
	 * Constructor
	 */
					SharkAttack();
	/**
	 * Destructor
	 */
	virtual			~SharkAttack();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
private:
	Vector3D m_playerPos; 
};

#endif //#ifndef _SHARKATTACK_H_