#ifndef _SQUIDDIE_H_
#define _SQUIDDIE_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class SquidDie : public Behavior
{
public:
	/**
	 * Constructor
	 */
					SquidDie();
	/**
	 * Destructor
	 */
	virtual			~SquidDie();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
};

#endif //#ifndef _SQUIDDIE_H_