#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "SquidBehaviors.h"
#include "SquidFlee.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

SquidFlee::SquidFlee() :
	Behavior("SQUID_FLEE")
{

}

/**********************************************************************************************************************************/

SquidFlee::~SquidFlee()
{

}

/**********************************************************************************************************************************/

void
SquidFlee::OnEnter(Character* owner)
{
	owner->SetSpeed(SQUID_SPEED);
	SetAnimationAndCacheIndex(owner, "SWIM");
}

/**********************************************************************************************************************************/

void
SquidFlee::Update(Character* owner, float elapsedTime)
{
	float tempAngle = GetPathAngleToTarget(owner, owner->GetDestination());

	if(TurnTo(owner, tempAngle, SQUIDROTPERSEC, elapsedTime) == TD_FACING)
	{
		MoveInDirection(owner, elapsedTime);
	}

	if(DistanceToTargetSq(owner, owner->GetDestination()) < 0.1f)
	{
		owner->ArrivedAtWaypoint();
		
		// go back to patroling
		owner->SetBehavior("SQUID_PATROL");
	}

	if(owner->GetHitCount() > NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("SQUID_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::SQUID);
	}
}

/**********************************************************************************************************************************/

void
SquidFlee::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
