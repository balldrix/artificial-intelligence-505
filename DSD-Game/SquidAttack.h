
#ifndef _SQUIDATTACK_H_
#define _SQUIDATTACK_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class EnemyProjectile;
class Player;

class SquidAttack : public Behavior
{
public:
	SquidAttack();
	/**
	 * Constructor
	 */
					// overloaded constructor
					SquidAttack(Player* player);
	/**
	 * Destructor
	 */
	virtual			~SquidAttack();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);

	// shoot ink
	void ShootInk(Character* owner);


private:
	EnemyProjectile* m_inkProjectile; // projectile
	bool m_shootFlag; // flag for allowing shooting
	Player* m_player; // pointer to player
};

#endif //#ifndef _SQUIDATTACK_H_