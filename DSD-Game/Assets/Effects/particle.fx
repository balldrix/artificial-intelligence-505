uniform extern float4x4 gWVP;
uniform extern float3 gEyePosition;
uniform extern float3 gWorldAccel;
uniform extern int gViewportHeight;
uniform extern float gTime;
uniform extern texture gTexture;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = CLAMP;
	AddressV  = CLAMP;
};

struct VS_INPUT
{
	float3 position	: POSITION0;
	float3 velocity	: TEXCOORD0;
	float size		: TEXCOORD1;
	float time		: TEXCOORD2;
	float life 		: TEXCOORD3;
	float mass		: TEXCOORD4;
	float4 colour 	: COLOR0;
};

struct VS_OUTPUT
{
	float4 position: POSITION;
	float2 texcoord: TEXCOORD0;
	float size : PSIZE;
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT) 0;
	
	float t = gTime - input.time;
	
	input.position = input.position + input.velocity * t +((0.5f * gWorldAccel * t * t) * input.mass);

	output.position = mul(float4(input.position,1.0f),gWVP);

	float d = distance(input.position, gEyePosition);
	
	output.size = gViewportHeight*input.size/(1.0f + 8.0f*d);
	
	return output;
}


float4 ps_main(float2 texcoord:TEXCOORD0) : COLOR
{
	return tex2D(TexS,texcoord);
}

technique Particle
{
    pass P0
    {
        vertexShader = compile vs_2_0 vs_main();
        pixelShader  = compile ps_2_0 ps_main();
        
        PointSpriteEnable = true;
        AlphaBlendEnable = true;
	SrcBlend     = One;
	DestBlend    = One;
	ZWriteEnable = false;
    }
}

