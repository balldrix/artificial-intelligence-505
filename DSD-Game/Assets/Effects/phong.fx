uniform extern float4x4 gWVP;
uniform extern float4x4 gView;
uniform extern float3 gLightPosition;
uniform extern float3 gEyePosition;
uniform extern texture gTexture;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

struct VS_INPUT
{
	float4 Position: POSITION0;
	float2 Texcoord: TEXCOORD0;
	float3 Normal: NORMAL0;
};


struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Texcoord: TEXCOORD0;
	float3 ViewDirection: TEXCOORD1;
	float3 LightDirection: TEXCOORD2;
	float3 Normal: TEXCOORD3;
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	VS_OUTPUT Output;
	
	Output.Position = mul(input.Position,gWVP);
	Output.Texcoord = input.Texcoord;
	
	Output.ViewDirection = gEyePosition - Output.Position;
	Output.LightDirection = gLightPosition - Output.Position;
	Output.Normal = mul( input.Normal, gView );
	
	return( Output );
}

struct PS_INPUT
{
	float2 Texcoord: TEXCOORD0;
	float3 ViewDirection: TEXCOORD1;
	float3 LightDirection: TEXCOORD2;
	float3 Normal: TEXCOORD3;
};

float4 ps_main(PS_INPUT input) : COLOR0
{
	float3 light = normalize(input.LightDirection);
	float3 normal = normalize(input.Normal);
	float nDOTl = dot(normal,light);
	
	float3 reflection = normalize( ( ( 2.0f * normal ) * ( nDOTl ) ) - light ); 
	float3 ViewDirection = normalize( input.ViewDirection );
	float  RDotV = max( 0.0f, dot( reflection, ViewDirection ) );
	   
	float4 BaseColor = tex2D( TexS, input.Texcoord );
	   
	float4 TotalAmbient = float4(0.35,0.35,0.35,1.0f) * BaseColor; 
	float4 TotalDiffuse =  float4(0.88,0.88,0.88,1.0f)  * nDOTl * BaseColor; 
	float4 TotalSpecular =  float4(0.5,0.5,0.5,1.0f)  * pow( RDotV, 10 );
	
	float4 returnvalue = saturate( TotalAmbient + TotalDiffuse + TotalSpecular );
	   
	return (returnvalue);
	
}

technique PhongLight
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_main();
		PixelShader = compile ps_2_0 ps_main();
	}
}

