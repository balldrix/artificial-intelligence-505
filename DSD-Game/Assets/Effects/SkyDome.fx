
uniform extern float4x4 gWVP;
uniform extern texture gTexture;

sampler CubeMap = sampler_state
{
   Texture = <gTexture>;
   MAGFILTER = LINEAR;
   MINFILTER = LINEAR;
   MIPFILTER = LINEAR;
   AddressU  = WRAP;
   AddressV  = WRAP;
};

struct VS_INPUT
{
	float3 Position : POSITION0;
};

struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float3 Texcoord : TEXCOORD0;
};

VS_OUTPUT vs_main( VS_INPUT input)
{
	VS_OUTPUT output;
	
	float3 pos = input.Position;
	
	output.Position = mul(float4(input.Position,1.0f),gWVP).xyww;
	output.Texcoord = input.Position;
	
	return output;
}

struct PS_INPUT
{
	float3 Texcoord : TEXCOORD0;
};

float4 ps_main(PS_INPUT input) : COLOR0
{
	return texCUBE(CubeMap,input.Texcoord);
	//return (1.0f,0.0f,0.0f,1.0f);
};

technique SkyDome
{
    pass P0
    {
        vertexShader = compile vs_2_0 vs_main();
        pixelShader  = compile ps_2_0 ps_main();
	CullMode = None;
		ZFunc = Always; // Always write sky to depth buffer
		StencilEnable = true;
		StencilFunc   = Always;
		StencilPass   = Replace;
		StencilRef    = 0; // clear to zero
    }
}

