uniform extern float4x4 gWVP;

struct VS_INPUT
{
	float4 Position : POSITION0;
};

struct VS_OUTPUT
{
	float4 Position : POSITION0;
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Position = mul(input.Position,gWVP);
	return (output);
}


float4 ps_main() : COLOR0
{
	return float4(1.0f,0.0f,0.0f,1.0f);
}

technique Amb
{
	pass P0
	{
		vertexShader = compile vs_2_0 vs_main();
		pixelShader = compile ps_2_0 ps_main();
	}
}