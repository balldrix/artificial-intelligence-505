uniform extern texture gTexture;
uniform extern texture gNoise;
uniform extern float gTime;
uniform extern bool gCutScene;
uniform extern bool ghurtPlayer;
uniform extern bool negative;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
	AddressV  = WRAP;
};

sampler NoiseS = sampler_state
{
	Texture = <gNoise>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU  = WRAP;
	AddressV  = WRAP;
};


struct VS_INPUT
{
	float3 Position : POSITION0;
	float2 Texcoord : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Texcoord : TEXCOORD0;
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	VS_OUTPUT output;
	
	output.Position = float4(input.Position,1.0f);
	
	output.Texcoord = input.Texcoord;
	
	return output;
}

struct PS_INPUT
{
	float2 Texcoord : TEXCOORD0;
};

float4 ps_main(PS_INPUT input) : COLOR
{

   float2 offset = tex2D(NoiseS,float2(input.Texcoord.x,2*input.Texcoord.y + gTime * 0.5f));
 
   offset  *= 0.01f;
   
   float2 Result = float2(input.Texcoord.x,input.Texcoord.y) + offset;
   
   if(Result.x > 0.999f)
   {
	   Result.x = 0.999f;
   }
   
    if(Result.y > 0.999f)
   {
	   Result.y = 0.999f;
   }
   
   if(gCutScene)
   {
	   if(input.Texcoord.y < 0.15f || input.Texcoord.y > 0.85f )
	   {
		   return float4(0,0,0,1);
	   }
   }
   float4 res = tex2D(TexS,Result);
   if(negative)
   {
		res = 1 - res;
 		res.a = 1.0f;
   }
   return res;
}

technique Texture
{
    pass P0
    {
        vertexShader = compile vs_2_0 vs_main();
        pixelShader  = compile ps_2_0 ps_main();
    }
}
