uniform extern float4x4 gWVP;
uniform extern float4x4 gView;
uniform extern float3 gEyePosition;
uniform extern texture gTexture;
uniform extern float4 gFogColour;
uniform extern float gFogMinDist;
uniform extern float gFogMaxDist;


sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};


struct VS_INPUT
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Texcoord : TEXCOORD0;
	float4 Color    : COLOR0;
};

struct VS_OUTPUT
{
	float4 Position : POSITION;
	float2 Texcoord : TEXCOORD0;
	float4 Color    : COLOR0;
	float fog : TEXCOORD1;
	
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Position = mul(input.Position,gWVP);
	output.Texcoord = input.Texcoord;
	
	float4 eye = mul(float4(gEyePosition,1.0f),gView);
	float dist = distance(output.Position,eye.xyz);
	output.fog = saturate(( dist - gFogMinDist)/ (gFogMaxDist-gFogMinDist));
	output.Color = input.Color;
	return output;
}

struct PS_INPUT
{
	float2 Texcoord : TEXCOORD0; 
	float4 Color    : COLOR0;
	float fog : TEXCOORD1;
};

float4 ps_main(PS_INPUT input) : COLOR0
{
	float4 colour = tex2D(TexS,input.Texcoord) * input.Color;

	return float4(lerp(colour,gFogColour,input.fog));

	
}

technique Fog
{
    pass P0
    {
        vertexShader = compile vs_2_0 vs_main();
        pixelShader  = compile ps_2_0 ps_main();
    }
}