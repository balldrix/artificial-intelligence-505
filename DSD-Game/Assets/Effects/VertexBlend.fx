uniform extern float4x4 gWorld;
uniform extern float4x4 gInvWorld;
uniform extern float4x4 gWVP;
uniform extern float4x4 gBoneMatrix[35];
uniform extern float4 gLightPosition = float4(0,0,0,0);
uniform extern float3 gEyePosition;
uniform extern texture gTexture;
uniform extern texture gNormalMap;
uniform extern float4x4 gView;
uniform extern float4x4 gRotation;
uniform extern bool flash;
uniform extern float4 gFogColour;
uniform extern bool gRender;
uniform extern bool negative;

uniform extern float gAlpha;
uniform extern float gFogMinDist;
uniform extern float gFogMaxDist;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

sampler TexNormal = sampler_state
{
	Texture = <gNormalMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

struct VS_INPUT
{
	float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float3 Binormal : BINORMAL0;
	float3 Tangent : TANGENT0;
	float2 Texcoord : TEXCOORD0;
	float weight0 : BLENDWEIGHT0;
	int4 boneIndex : BLENDINDICES0;
};	

struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Texcoord: TEXCOORD0;
	float3 ViewDirection : TEXCOORD1;
	float3 LightDirection : TEXCOORD2;
	float3 Normal : TEXCOORD3;
	float fog : TEXCOORD4;
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	
	float weight1 = 1.0f - input.weight0;
	
	float4x4 mat0 = gBoneMatrix[input.boneIndex[0]] * input.weight0;
	float4x4 mat1 = gBoneMatrix[input.boneIndex[1]] * weight1;
	
	float4 p = mul(input.Position, mat0);
	float3 n = mul(input.Normal, (float3x3)mat0);
	p += mul(input.Position, mat1);
	n += mul(input.Normal, (float3x3)mat1);
	n = normalize(n);
	
	float3 t = mul(input.Tangent, (float3x3)mat0);
	t += mul(input.Tangent, (float3x3)mat1);
	
	float3 b = mul(input.Binormal, (float3x3)mat0);
	b += mul(input.Binormal, (float3x3)mat1);

	
	float4 worldPosition = mul(p, gWorld);
	
	output.ViewDirection = normalize(gEyePosition - worldPosition);
	output.Position = mul(p, gWVP);
	
	if(gRender)
	{
		float dist = distance(worldPosition,gEyePosition);
		output.fog = saturate(( dist - gFogMinDist)/ (gFogMaxDist-gFogMinDist));
	}	
	else
	{
		output.fog = 0;
	}
	
	/// Create the tangent space matrix
	float3x3 tbnMatrix;
	tbnMatrix[0] = normalize(t);
	tbnMatrix[1] = normalize(b);
	tbnMatrix[2] = n;
	
	tbnMatrix=mul(tbnMatrix,(float3x3)gWorld);
	
	float3 LightDirection = normalize(float3(-1, 0.75f,0.1f)); /// TODO get this from an input parameter
	
	/// Transform the light & view vectors into tangent space
	output.ViewDirection = normalize(mul(tbnMatrix, output.ViewDirection));
	output.LightDirection = normalize(mul(tbnMatrix, LightDirection));

	output.Texcoord = input.Texcoord;

	return output;
}

struct PS_INPUT
{
	float2 Texcoord: TEXCOORD0;
	float3 ViewDirection : TEXCOORD1;
	float3 LightDirection : TEXCOORD2;
	float3 Normal : TEXCOORD3;
	float fog : TEXCOORD4;
};	

float3 
SampleNormalMap(float2 uv)
{   
	float3 normal=tex2D(TexNormal, uv);
	normal-=0.5f; /// Offset
	normal*=2; /// Scale
	return normalize(normal);
}


float4 
ps_main(PS_INPUT input) : COLOR0
{
	float3 normal = SampleNormalMap(input.Texcoord);
	
	/// Calculate n.l
	float nl = max(0,dot(normal,input.LightDirection));
	float3 reflectionVector = normalize(2*nl*normal-input.LightDirection); 
	float specularExponent = 10;
	float specularIntensity = saturate(4*nl) * pow(saturate(dot(reflectionVector, input.ViewDirection)), specularExponent); // R.V^n    
	
	float3 BaseColor = tex2D( TexS, input.Texcoord ).rgb;
	
	if (negative)
	{
		BaseColor = 1 - BaseColor;
	}
	   
	float3 TotalAmbient = 0.35f * BaseColor; 
	float3 TotalDiffuse =  0.88f * nl * BaseColor; 
	float3 TotalSpecular =  0.5f * specularIntensity;
	
	float3 returnvalue = float3(TotalAmbient + TotalDiffuse + TotalSpecular);
	
	if(flash)
	{
		returnvalue.r = 1.0f;		
	}
	
	float4 colour = float4(lerp(returnvalue,gFogColour,input.fog), gAlpha);

	return (colour);
}

technique VertexBlend
{
	
	pass P0
	{
		AlphaBlendEnable = TRUE;
		SrcBlend = SRCALPHA;
		DestBlend = INVSRCALPHA; 

		//SrcBlend     = One;
		//DestBlend    = One;
		
		VertexShader = compile vs_2_0 vs_main();
		PixelShader = compile ps_2_0 ps_main();
	}
}
