uniform extern float4x4 gWVP;
uniform extern float4x4 gInvWorld;
uniform extern float4 gLightDirection;
uniform extern texture gTexture;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

struct VS_INPUT
{
	float4 Position: POSITION0;
	float3 Normal: NORMAL;
	float2 Texcoord : TEXCOORD0;
};

struct VS_OUTPUT
{
	float4 Position: POSITION0;
	float2 Texcoord : TEXCOORD0;
	float3 Light: TEXCOORD1;
	float3 Normal: TEXCOORD2;
};

VS_OUTPUT vs_shader(VS_INPUT input)
{
	VS_OUTPUT output;
	output.Position = mul(input.Position,gWVP);
	output.Light = normalize(gLightDirection);
	output.Normal = normalize(mul(gInvWorld,input.Normal));
	output.Texcoord = input.Texcoord;
	return output;
}


struct PS_INPUT
{
	float2 Texcoord : TEXCOORD0;
	float3 Light: TEXCOORD1;
	float3 Normal: TEXCOORD2;
	
};

float4 ps_shader (PS_INPUT input) : COLOR
{
	float Ai = 0.8f;
	float4 Ac = float4(0.075,0.075,0.2,1.0);
	float Di = 1.0f;
	float4 Dc = float4(1.0,1.0,1.0,1.0);
	
	float4 base = tex2D( TexS, input.Texcoord );
	return Ai * Ac + Di * Dc * saturate(dot(input.Light,input.Normal)) * base;
	//return tex2D( TexS  +  Ai * Ac + Di * Dc * saturate(dot(input.Light,input.Normal)), input.Texcoord);
	//return tex2D( TexS, input.Texcoord );
}

technique DiffuseLight
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_shader();
		PixelShader = compile ps_2_0 ps_shader();
	}
}