//////////////////////////////////////////////
//The Shadow Volume Shader
//////////////////////////////////////////////

#define LIGHT_FALLOFF 2.5f

//Global Variables

//Ambient Colour
uniform extern float4 gAmbient;
//Light Position
uniform extern float3 gLightPosition;
//Light Colour
uniform extern float4 gLightColour;
//Shadow Colour
uniform extern float4 gShadowColour;
//Material
uniform extern float4 gMaterial;
//World Matrix
uniform extern float4x4 gWorld;
//World View Matrix
uniform extern float4x4 gWorldView;
//View Matrix
uniform extern float4x4 gView;
//Projection Matrix
uniform extern float4x4 gProj;
//World View Projection matrix
uniform extern float4x4 gWVP;
//Base map texture
uniform extern texture gTexture;
//Shadow Clip
uniform extern float gZClip;
//Light falloff range
uniform extern float gFalloff;

//Sampler to the base example
sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
};

//The Ambient Vertex Shaders input parameters
struct VS_AMBIENT_INPUT
{
	float4 Position : POSITION;
	float2 Texcoord: TEXCOORD0;
};

//the Scene Ambient vertex shader,
//the Ambient shader will render the environment in the currect position in world space
void vs_SceneAmbient(VS_AMBIENT_INPUT input,
				out float4 outPosition : POSITION,
				out float2 outTexcoord : TEXCOORD0)
{
	//transform the vert to the input world view projection matrix
	outPosition  = mul(input.Position, gWVP);
	//set texcoords as seen in input
	outTexcoord = input.Texcoord;
}

//The Scene Ambient pixel shader

float4 ps_SceneAmbient (float2 Texcoord : TEXCOORD0) : COLOR0
{
	//returns the base map texture colour
	return tex2D(TexS,Texcoord);
}

struct VS_SCENE_INPUT
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Texcoord : TEXCOORD0;
};

//The Scene vertex shader, used for per pixel lighting of the environment.

void vs_scene(VS_SCENE_INPUT input,
			out float4 outPosition : POSITION,
			out float4 outEyePosition : TEXCOORD0,
			out float3 outEyeNormal : TEXCOORD1,
			out float2 outTexcoord: TEXCOORD2,
			out float4 outDiffuse : TEXCOORD3)
{
	
    outPosition = mul( input.Position, gWVP );

    // transform view space
    outEyePosition = mul( input.Position, gWorldView );

    // Compute world space normal
    outEyeNormal = normalize( mul( input.Normal, (float3x3)gWorldView ) );

    // Create a diffuse colour from the light colour and the material colour of the object being operated on
    outDiffuse = gMaterial * gLightColour;

    //set texcoords as seen from input
    outTexcoord = input.Texcoord;

}


//Scene Pixel Shader, computes the light for the pixel
float4 ps_scene( float4 Position : POSITION,
			 float4 EyePosition : TEXCOORD0,
			 float3 EyeNormal : TEXCOORD1,
			 float2 Texcoord: TEXCOORD2,
			 float4 Diffuse : TEXCOORD3) : COLOR0
{
	
	//Compute pixel to light position, distance and normalise the vector
	float3 L = gLightPosition - EyePosition;
	float LenSq = dot( L, L );
	L = normalize( L );

	// Compute lighting amount required
	float4 I = saturate( dot( normalize( EyeNormal ), L ) ) * Diffuse *
	(LIGHT_FALLOFF * LIGHT_FALLOFF) / LenSq;

	// Lookup mesh texture and apply the calcuated diffuse value stored in I
	return float4( tex2D( TexS, Texcoord).xyz, 1.0f ) * I;
}


//The Shadow Vertex shader, this transforms the shadow mesh to give the appearance of a shadow,
//by streching the meshes added quads in the direction of the light.
void vs_shadow( float4 position : POSITION,
			float3 normal : NORMAL,
			out float4 outPosition : POSITION)
{
	//Get the input position and normals
	float4 pos =  position;
	float4 nor =  float4(normal,1.0f);

	//transform the normal to view space
	float3 N = mul( nor.xyz, (float3x3)gWorldView );

	// transform the position to view space 
	float4 PosView = mul( pos, gWorldView );

	// get the pixel to light vector using the viewspace vertex position
	float3 LightVecView = PosView - gLightPosition;

	// Perform reverse vertex extrusion
	// Extrude the vertex away from light if it is facing away from the light.
	
	if( dot( N, -LightVecView ) < 0.0f )
	{
		//move the vertex to the new position
		if( PosView.z > gLightPosition.z )
		{
			PosView.xyz += LightVecView * ( gZClip - PosView.z ) / LightVecView.z;
		}
		else
		{
			PosView = float4( LightVecView, 0.0f );
		}

		// Transform the position from view space to include the projection matrix,
		outPosition = mul( PosView, gProj );
	} 
	
	else
	{
		//else set the position of the vertex to the world view projection tranform
		outPosition = mul( pos, gWVP );
	}
	
}

//The Shadow Pixel Shader return the shadow colour only
float4 ps_shadow() : COLOR0
{
	 return float4( gShadowColour.xyz, 1.0f );
}

technique RenderSceneAmbient
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_SceneAmbient();
		PixelShader = compile ps_2_0 ps_SceneAmbient();
		StencilEnable = false;
		ZFunc = LessEqual;
	}
}

technique RenderShadow
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_shadow();
		PixelShader = compile ps_2_0 ps_shadow();
		 CullMode = Ccw;
		// Disable writing to the frame buffer
		AlphaBlendEnable = true;
		SrcBlend = Zero;
		DestBlend = One;
		// Disable writing to depth buffer
		ZWriteEnable = false;
		ZFunc = Less;
		// Setup stencil states
		StencilEnable = true;
		StencilRef = 1;
		StencilMask = 0xFFFFFFFF;
		StencilWriteMask = 0xFFFFFFFF;
		StencilFunc = Always;
		StencilZFail = Decr;
		StencilPass = Keep;
	}
	pass P1
	{
		VertexShader = compile vs_2_0 vs_shadow();
		PixelShader  = compile ps_2_0 ps_shadow();
		CullMode = Cw;
		StencilZFail = Incr;
	}
	
}

technique RenderShadow2Sided
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_shadow();
		PixelShader  = compile ps_2_0 ps_shadow();
		CullMode = None;
		// Disable writing to the frame buffer
		AlphaBlendEnable = true;
		SrcBlend = Zero;
		DestBlend = One;
		// Disable writing to depth buffer
		ZWriteEnable = false;
		ZFunc = Less;
		// Setup stencil states
		TwoSidedStencilMode = true;
		StencilEnable = true;
		StencilRef = 1;
		StencilMask = 0xFFFFFFFF;
		StencilWriteMask = 0xFFFFFFFF;
		Ccw_StencilFunc = Always;
		Ccw_StencilZFail = Incr;
		Ccw_StencilPass = Keep;
		StencilFunc = Always;
		StencilZFail = Decr;
		StencilPass = Keep;

	}
}

technique RenderScene
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_scene();
		PixelShader  = compile ps_2_0 ps_scene();
		ZEnable = true;
		ZFunc = LessEqual;
		StencilEnable = true;
		AlphaBlendEnable = true;
		BlendOp = Add;
		SrcBlend = One;
		DestBlend = One;
		StencilRef = 1;
		StencilFunc = Greater;
		StencilPass = Keep;
	}
	
	
}
