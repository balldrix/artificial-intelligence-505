/////////////////////////////////////////////////////////
//Shadow Mapping Shader
//
//This Shader Contains 2 effects, one to generate a 
//shadow map, the other to render the scene applying the
//shadow map to the pixel colour of the environment.
////////////////////////////////////////////////////////

//define image size and error range
#define SMAP_SIZE 2048
#define EPSILON 1.f / 256.f

//Global Variables

//World
uniform extern float4x4 gWorld;
//View
uniform extern float4x4 gView;
//Projection
uniform extern float4x4 gProjection;
//View to Light
uniform extern float4x4 gViewToLightProjection;
//Base Texture
uniform extern texture gTexture;
//Shadow Texture
uniform extern texture gShadowMap;
//Light Position
uniform extern float4 gLightPosition;
//Light Direction
uniform extern float4 gLightDirection;
//Colours
uniform extern float4 gLightDiffuse = float4 (1,1,1,1);
uniform extern float4 gLightAmbient =  float4(0.5f,0.5f,0.5f,1.0f);
//Range
uniform extern float gCosTheta;
//NormalMap
uniform extern texture gNormalMap;
//CameraPosition
uniform extern float3 gEyePos;
//Fog Colour
uniform extern float4 gFogColour;
uniform extern float gFogMinDist;
uniform extern float gFogMaxDist;

uniform extern float4x4 gBoneMatrix[35];

uniform extern float a;

//Texture Sampler for base map
sampler2D TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};

sampler TexNormal = sampler_state
{
	Texture = <gNormalMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

//Texture Sampler for Shadow Map
sampler2D TexShadow = sampler_state
{
    Texture = <gShadowMap>;
    MinFilter = Point;
    MagFilter = Point;
    MipFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};

//Shadow Input Structure
struct VS_ShadowInput
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
};

struct VS_ShadowInputSkinned
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float weight0 : BLENDWEIGHT0;
	int4 boneIndex : BLENDINDICES0;
};

//Shadow Output Structure
struct VS_ShadowOutput
{
	float4 Position : POSITION;
	float2 TexCoord : TEXCOORD0;
};

//Scene Input Structure
struct VS_SceneInput
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Texcoord : TEXCOORD0;
	float3 Binormal : BINORMAL0;
	float3 Tangent : TANGENT0;
};

struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Texcoord : TEXCOORD0;
	float3 LightPosition : TEXCOORD1;
	float3 ViewDirection : TEXCOORD2;
	float3 LightDirection : TEXCOORD3;
	float fog : TEXCOORD4;
};

//The Scene Shader
//This renders the environment applying the shadow map
//within the calculations, 
//
//Yhe Vertex Shader positions the object and returns normal and light information
//ScenePosition and SceneNormal are transformed values within the projection matrix
VS_OUTPUT
vs_scene(VS_SceneInput input)
{
	VS_OUTPUT output = (VS_OUTPUT)0;
	
	/// Transform by the world matrix, to get the world space co-ordinate then transform into view & projection space
	float4 worldPosition = mul(input.Position,gWorld); 
	float4 viewPosition =  mul(worldPosition,gView);
	output.Position = mul(viewPosition,gProjection);
	
	float3 n = mul(input.Normal,gWorld); 
	n = normalize(n);
	
	// Get Shadow map lookup position by projecting the world co-ordinate by the using the ScreenPosition and gViewToLightProjection
	output.LightPosition = mul(worldPosition, gViewToLightProjection);
	output.LightPosition.xy /= 2; // scale and offset into texture space
	output.LightPosition.xy += 0.5f;
	
	output.LightPosition.z = int(output.LightPosition.z * 65536.f) / 256.f; /// Quantise and scale the light depth value for when we compare with texture look up depth
	
	//Set Texcoord
	output.Texcoord = input.Texcoord;
	
	/// Create the tangent space matrix, to transform the lighting vectors by
	float3x3 tbnMatrix;
	tbnMatrix[0] = normalize(input.Tangent);
	tbnMatrix[1] = normalize(input.Binormal);
	tbnMatrix[2] = n;
	tbnMatrix=mul(tbnMatrix,(float3x3)gWorld);
	
	float3 LightDirection = normalize(float3(-1, 0.75f,0.1f));	
	output.ViewDirection = normalize(gEyePos - worldPosition);
	output.ViewDirection = normalize(mul(tbnMatrix, output.ViewDirection));
	output.LightDirection = normalize(mul(tbnMatrix, LightDirection));
	
	float dist = distance(worldPosition,gEyePos);	
	output.fog = saturate(( dist - gFogMinDist)/ (gFogMaxDist-gFogMinDist));
	
	return output;
}

//The Scene Pixel Shader Input Structure
struct PS_SceneInput
{
	float2 Texcoord : TEXCOORD0;
	float3 LightPosition : TEXCOORD1;
	float3 ViewDirection : TEXCOORD2;
	float3 LightDirection : TEXCOORD3;
	float fog : TEXCOORD4;
};

float3 
SampleNormalMap(float2 uv)
{   
	float3 normal=tex2D(TexNormal, uv);
	normal-=0.5f; /// Offset
	normal*=2; /// Scale
	return normalize(normal);
}

static const float HALF_PI = 3.14159265f / 2.f;

//Pixel Shader, this computes the pixel colour of the object using the shadow map
//as guide for calculating the pixel colour
float4 
ps_scene(PS_SceneInput input): COLOR0
{
	float2 ShadowTex = input.LightPosition.xy;
	ShadowTex.y = 1.f - ShadowTex.y;

	/// Calculate the factional value for the smoothing of the depth
	float2 lerps = frac((ShadowTex * SMAP_SIZE));

	//Do Shadow Checks
	float sourceval[4];
	
	/// Take a small value off the depth from the light source to stop a surface from making itself in shadow
	float depth =  input.LightPosition.z - EPSILON;
	
	//for each, read the shadow map texture for its value
	float inv = 1.0f / SMAP_SIZE;

	float4 shadowMapDepthColours[4];
	
	shadowMapDepthColours[0] = tex2D(TexShadow,ShadowTex);
	shadowMapDepthColours[1] = tex2D(TexShadow,ShadowTex + float2(inv, 0));
	shadowMapDepthColours[2] = tex2D(TexShadow,ShadowTex + float2(0, inv));
	shadowMapDepthColours[3] = tex2D(TexShadow,ShadowTex + float2(inv, inv));
	
	float shadowMapDepth[4];
	
	shadowMapDepth[0] = (shadowMapDepthColours[0].r * 256.f + shadowMapDepthColours[0].g < depth) ? 0.f : 1.f;
	shadowMapDepth[1] = (shadowMapDepthColours[1].r * 256.f + shadowMapDepthColours[1].g < depth) ? 0.f : 1.f;
	shadowMapDepth[2] = (shadowMapDepthColours[2].r * 256.f + shadowMapDepthColours[2].g < depth) ? 0.f : 1.f;
	shadowMapDepth[3] = (shadowMapDepthColours[3].r * 256.f + shadowMapDepthColours[3].g < depth) ? 0.f : 1.f;
	
	float LightAmount = lerp(lerp(shadowMapDepth[0],shadowMapDepth[1],lerps.x),lerp(shadowMapDepth[2],shadowMapDepth[3] ,lerps.x),lerps.y);

	
	float3 normal = SampleNormalMap(input.Texcoord);
	
	/// Calculate n.l
	float nl = max(0,dot(normal,input.LightDirection));
	float3 reflectionVector = normalize(2*nl*normal-input.LightDirection); 
	float specularExponent = 25;
	float specularIntensity = saturate(4*nl) * pow(saturate(dot(reflectionVector, input.ViewDirection)), specularExponent); // R.V^n    
	
	float3 BaseColor = tex2D( TexS, input.Texcoord ).rgb;

	float3 TotalAmbient = 0.35f * BaseColor; 
	float3 TotalDiffuse =  0.88f * nl * BaseColor * saturate(LightAmount + 0.5f); 
	float3 TotalSpecular =  0.5f * specularIntensity * LightAmount;
	
	float3 returnvalue = float3(TotalAmbient + TotalDiffuse + TotalSpecular);

	/// Apply the fog
	float4 colour = float4(lerp(returnvalue,gFogColour,input.fog), 1);
	return colour;
	//return float4(LightAmount, LightAmount, LightAmount, 1);
}


//The Shadow Shader
//This Calulates the depth of the scene 
void vs_shadow(	VS_ShadowInput input,
			out float4 Position : POSITION,
			out float2 Depth : TEXCOORD0)
{
	//Transform the world
	Position = mul(input.Position,gWorld);
	Position = mul(Position,gView);
	Position = mul(Position,gProjection);
	
	//Get the depth of the world by getting the positions z and w values
	Depth = Position.zw;
}

void vs_shadowSkinned(VS_ShadowInputSkinned input,
				out float4 Position : POSITION,
				out float2 Depth : TEXCOORD0)
{
	//Transform the world
	float weight1 = 1.0f - input.weight0;
	
	float4 p = input.weight0 * mul(input.Position,gBoneMatrix[input.boneIndex[0]]);
	p += weight1* mul(input.Position,gBoneMatrix[input.boneIndex[1]]);
	p.w = 1.0f;
	
	Position = mul(p,gWorld);
	Position = mul(Position,gView);
	Position = mul(Position,gProjection);
	
	//Get the depth of the world by getting the positions z and w values
	Depth = Position.zw;
}

//The Shadows Pixel Shader
//Returns the Depth of the environment as a colour
void ps_shadow(	float2 Depth : TEXCOORD0,
			out float4 Color : COLOR )
{
	float d = Depth.x;
	float a = int(d*256)/256.0f;
	float b = d*256 - int(d*256);
	Color = float4(a,b,0,1);
}

//The Scene Technique
technique RenderScene
{
	pass p0
	{
		VertexShader = compile vs_2_0 vs_scene();
		PixelShader = compile ps_2_0 ps_scene();
	}
}

//The Shadow Technique
technique RenderShadow
{
	pass p0
	{
		CullMode = None;
		VertexShader = compile vs_2_0 vs_shadow();
		PixelShader = compile ps_2_0 ps_shadow();
	
	}
}

//The Shadow Technique
technique RenderShadowSkinned
{
	pass p0
	{
		CullMode = None;
		VertexShader = compile vs_2_0 vs_shadowSkinned();
		PixelShader = compile ps_2_0 ps_shadow();
	
	}
}

