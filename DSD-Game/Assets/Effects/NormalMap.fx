uniform extern float4x4 gView;
uniform extern float4x4 gWVP;
uniform extern float3 gLightPosition;
uniform extern float3 gEyePosition;
uniform extern texture gTexture;
uniform extern texture gNormalMap;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

sampler TexNormal = sampler_state
{
	Texture = <gNormalMap>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

struct VS_INPUT
{
	float4 Position: POSITION0;
	float2 Texcoord : TEXCOORD0;
	float3 Normal : NORMAL0;
	float3 Binormal : BINORMAL0;
	float3 Tangent : TANGENT0;
};

struct VS_OUTPUT
{
	float4 Position : POSITION0;
	float2 Texcoord : TEXCOORD0;
	float3 ViewDirection : TEXCOORD1;
	float3 LightDirection : TEXCOORD2;
};

VS_OUTPUT vs_main (VS_INPUT input)
{
	VS_OUTPUT output;
	
	output.Position = mul(input.Position,gWVP);
	output.Texcoord = input.Texcoord;
	
	float3 ViewDirection = gEyePosition - output.Position;
	float3 LightDirection = gLightPosition - output.Position;
	
	float3 normal = mul(input.Normal, gView); 
	float3 binormal = mul(input.Binormal, gView); 
	float3 tangent = mul(input.Tangent, gView); 
	
	output.ViewDirection.x = dot(tangent,ViewDirection);
	output.ViewDirection.y = dot(binormal,ViewDirection);
	output.ViewDirection.z = dot(normal,ViewDirection);
	
	output.LightDirection.x = dot(tangent,LightDirection);
	output.LightDirection.y = dot(binormal,LightDirection);
	output.LightDirection.z = dot(normal,LightDirection);
	
	return output;
	
}

struct PS_INPUT
{
	float2 Texcoord : TEXCOORD0;
	float3 ViewDirection : TEXCOORD1;
	float3 LightDirection : TEXCOORD2;
};

float4 ps_main(PS_INPUT input) : COLOR
{
	float3 light = normalize(input.LightDirection);
	float3 normal = normalize((tex2D(TexNormal,input.Texcoord).xyz * 2.0f) - 1.0f);
	float nDOTl = dot(normal,light);
	
	float3 reflection = normalize( ( ( 2.0f * normal ) * ( nDOTl ) ) - light ); 
	float3 ViewDirection = normalize( input.ViewDirection );
	float  RDotV = max( 0.0f, dot( reflection, ViewDirection ) );
	   
	float4 BaseColor = tex2D( TexS, input.Texcoord );
	   
	float4 TotalAmbient = float4(0.35,0.35,0.35,1.0f) * BaseColor; 
	float4 TotalDiffuse =  float4(0.88,0.88,0.88,1.0f)  * nDOTl * BaseColor; 
	float4 TotalSpecular =  float4(0.5,0.5,0.5,1.0f)  * pow( RDotV, 10 );
	
	float4 returnvalue = saturate( TotalAmbient + TotalDiffuse + TotalSpecular );
	   
	return (returnvalue);
}

technique NormalMap
{
	pass P0
	{
		VertexShader = compile vs_2_0 vs_main();
		PixelShader = compile ps_2_0 ps_main();
	}
}


