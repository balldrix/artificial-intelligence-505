uniform extern float4x4 gWVP;
uniform extern float3 gEyePosition;
uniform extern texture gTexture;
uniform extern float4 gFogColour;
uniform extern float gFogMinDist;
uniform extern float gFogMaxDist;

sampler TexS = sampler_state
{
	Texture = <gTexture>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	addressU = WRAP;
	addressV = WRAP;
};

struct VS_INPUT
{
	float4 Position : POSITION;
	float3 quadPosW : TEXCOORD0;
	float2 Texcoord : TEXCOORD1;
};

struct VS_OUTPUT
{
	float4 Position : POSITION;
	float2 Texcoord : TEXCOORD0;
	float fog : TEXCOORD1;
};

VS_OUTPUT vs_main(VS_INPUT input)
{
	
	VS_OUTPUT output;

	float3 look = normalize(gEyePosition - input.quadPosW);
	float3 right = normalize(cross(float3(0.0f, 1.0f, 0.0f), look));
	float3 up    = cross(look, right);
	
	float4x4 lookAtMtx;
	lookAtMtx[0] = float4(right, 0.0f);
	lookAtMtx[1] = float4(up, 0.0f);
	lookAtMtx[2] = float4(look, 0.0f);
	lookAtMtx[3] = float4(input.quadPosW, 1.0f);

	float4 posW = mul(input.Position, lookAtMtx);
	
	output.Position = mul(posW,gWVP);
	
	output.Texcoord = input.Texcoord;
	
	float dist = distance(input.quadPosW,gEyePosition);
	
	output.fog = saturate(( dist - gFogMinDist)/ (gFogMaxDist-gFogMinDist));
	
	return output;
	
}

struct PS_INPUT
{
	float2 Texcoord : TEXCOORD0; 
	float fog : TEXCOORD1;
};

float4 ps_main(PS_INPUT input) : COLOR0
{
	float4 colour = tex2D(TexS,input.Texcoord);
	float a = colour.a;

	colour = lerp(colour,gFogColour,input.fog);
	colour.a = a;
	
	return colour;

}

technique Billboard
{
    pass P0
    {
        vertexShader = compile vs_2_0 vs_main();
        pixelShader  = compile ps_2_0 ps_main();
        
        AlphaRef = 75;
        AlphaFunc = GreaterEqual;
        AlphaTestEnable = true;
        
        CullMode = None;
    }
}


