#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "SharkBehaviors.h"
#include "SharkAttack.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

SharkAttack::SharkAttack() : 
	Behavior("SHARK_ATTACK")
{
	
}

/**********************************************************************************************************************************/

SharkAttack::~SharkAttack()
{

}

/**********************************************************************************************************************************/

void 
SharkAttack::OnEnter(Character* owner)
{
	Vector3D p = Vector3D(0,0,0);
	p.y += 5;
	p.z -= 10;

	owner->SetBoundOffset(p);
	SetAnimationAndCacheIndex(owner, "SharkAttack");
	m_playerPos = m_playerCharacter->GetPosition();
}

/**********************************************************************************************************************************/

void 
SharkAttack::Update(Character* owner, float elapsedTime)
{
	float tempAngle = AngleToTarget(owner,m_playerPos);

	TurnTo(owner,tempAngle,SHARKBANKSPEED,elapsedTime);
	
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();


	pAC->GetTrackAnimationSet( track, &pASTrack );
	if(strcmp(anim,"SharkAttack")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		
		if( td.Position >= period - 0.1f)
		{
			owner->SetBehavior("SHARK_CHASE");
		}		
	}
	
	//kill it
	if(owner->GetHitCount() > NUM_SHARK_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("SHARK_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::SHARK);
	}
}

/**********************************************************************************************************************************/

void	
SharkAttack::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
