#include "Types.h"
#include "CharacterManager.h"
#include "Character.h"
#include "PropItem.h"
#include "InputManager.h"
#include "Bullets.h"
#include "EnvironmentManager.h"
#include "Player.h"
#include "ParticleSystem.h"
#include "ParticleEmitter.h"
#include "CameraController.h"
#include "ThirdPersonCamera.h"

#include "SoundManager.h"
#include "SoundListener.h"
#include "ModelManager.h"
#include "SkinnedMesh.h"
#include "PropManager.h"
#include "PropItem.h"
#include "CutSceneController.h"

#include "UIManager.h"
#include "UIObject.h"
#include "UIText.h"

#include "SoundEmitter.h"
#include "AnimationInstance.h"
#include "GameStateManager.h"

#include "EffectManager.h"
#include "Effect.h"
#include "TargetIndicator.h"

#include "ModelManager.h"
#include "Model.h"

#include "ScoreManager.h"

#include "PlayerConstants.h"

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Player Constructor
//////////////////////////////////////////////////////////////////////////

Player::Player() : 
	m_airTimer(0),
	m_air(255),
	m_health(255),
	m_cargo(0),
	m_coins(0),
	m_allowCollision(true),
	m_ammo(50),
	m_hurtCounter(0.0f),
	m_flashTimer(0.0f),
	m_electicuteTimer(0.0f),
	m_velocity(Vector3D(0,0,0)),
	m_rotation(Vector3D(0,3.14f,0)),
	m_forward(Vector3D(0,0,-1)),
	m_idleTimer(0),
	m_prevState(ONFLOOR),
	m_state(ONFLOOR),
	m_noInputMovement(true),
	m_cheat(false),
	m_hurtPlayed(true),
	m_flash(false),
	m_hadFloorCollision(true),
	m_allowAirControl(false),
	m_nextHealthIncrement(0),
	m_coinsRequiredForHealth(0),
	m_jumpTimer(0.0f),
	m_jumpDelay(0.0f),
	m_shield(false),
	m_currentUpgrade(NONE),
	m_upgradeTimer(0.0f),
	m_rapidFireTimer(0.0f),
	m_lockedTarget(false),
	m_targetLock(NULL),
	m_demoMode(false),
	m_AIState(COLLECTING),
	m_targetPosition(Vector3D(0, 0, 0)),
	m_NPCTarget(nullptr),
	Behavior("DIVER")
{
	//cache the player to avoid continually looking for it in the character manager
	m_playerCharacter = CharacterManager::GetInstance()->GetCharacter("Player");
	m_playerSkeleton = CharacterManager::GetInstance()->GetCharacter("Skeleton");
	m_playerSkeleton->SetVisible(false);
	m_playerCharacter->SetVisible(true);
	//Set up the cargo compress
	m_cargoTarget = new TargetIndicator();
	m_cargoTarget->SetBaseMesh(ModelManager::GetInstance()->GetStaticMeshPointer("COMPASS"));
	m_cargoTarget->SetNeedleMesh(ModelManager::GetInstance()->GetStaticMeshPointer("COMPASSNEEDLE"));

	m_collectorID = m_playerCharacter->GetID();

	//set that this character can collect props
	PropManager::GetInstance()->RegisterCollector(this);
	m_position = m_playerCharacter->GetPosition();
	//create bullets
	for (int i = 0; i < NUMBEROFBULLETS; i++)
	{
		m_bullets[i] = new Bullets();
		MyString temp;
		temp.Format("Bullet%i",i);
		m_bullets[i]->CreateBullet(temp);
	}
	//set the initial camera position
	static_cast<ThirdPersonCamera*>(CameraController::GetInstance()->GetCurrentCamera())->SetTargetYaw(m_rotation.y);
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Player Destructor
//////////////////////////////////////////////////////////////////////////

Player::~Player()
{
	//Destory Bullets
	for (int i = 0; i < NUMBEROFBULLETS ; i++)
	{
		delete m_bullets[i];
		m_bullets[i] = 0;
	}

	//clear mesh pointers
	if(m_playerCharacter)
	{
		m_playerCharacter = NULL;
	}
	
	if(m_playerSkeleton)
	{
		m_playerSkeleton = NULL;
	}
	
	//delete cargo compress
	delete m_cargoTarget;

	m_targetLock = NULL;
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//SetState caches the previous state and set the current state the state passed into the function
//////////////////////////////////////////////////////////////////////////

void 
Player::SetState(CharacterState newState)
{
	m_prevState = m_state;
	m_state = newState;
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//GetPosition
//////////////////////////////////////////////////////////////////////////

const Vector3D&
Player::GetPosition() const
{
	return m_position;
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//GetCharacterState
//////////////////////////////////////////////////////////////////////////
Player::CharacterState
Player::GetCharacterState() const
{
	return m_state;
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//ProcessKeys, manages keyboard and mouse input
//////////////////////////////////////////////////////////////////////////
void
Player::ProcessKeys(float elapsedTime)
{
	bool noKey = true;
	m_noInputMovement = true;

	bool slow = false;
	float slowFactor = 1.0f;

	//if the user is in has been hurt
	if(m_state == HURT || m_state == ELECTROCUTED)
	{
		//if the player has not yet played the hurt animation
		if(!m_hurtPlayed)
		{
			m_hurtPlayed = true;
			SoundManager::GetInstance()->GetEmitter("HURT")->Play();
			SoundManager::GetInstance()->GetEmitter("HURT")->SetPosition(m_position);
			SetAnimationAndCacheIndex(m_playerCharacter, "GET_HIT");
			SetAnimationAndCacheIndex(m_playerSkeleton, "GET_HIT");
			m_idleTimer = 0;
		}
		if(m_hurtCounter < HURTFALLTIME || m_prevState == ROCKETJUMPING)
		{
			/*don't respond to keys for a short time
			if hurt in air wait till player lands before returning control*/
			if(!m_allowCollision)
			{
				slow = true;
				slowFactor = 0.333f;
			}
		}
	}

	if (m_hadFloorCollision)
	{
		m_velocity.x = 0.0f;
		m_velocity.z = 0.0f;
	}

	//if the 'w' key is pressed (move forward)
	if(InputManager::GetInstance()->IsKeyDown(true,'w'))
	{
		//move character
		if (m_hadFloorCollision || (m_state == ROCKETJUMPING && m_allowAirControl))
		{
			m_velocity.x = -m_forward.x * DISTANCEPERSECOND * slowFactor;
			m_velocity.z = -m_forward.z * DISTANCEPERSECOND * slowFactor;
		}

		//set animation
		if(m_state != ROCKETJUMPING && !slow)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "RUN");
			SetAnimationAndCacheIndex(m_playerSkeleton, "RUN");
		}

		m_idleTimer = 0;
		noKey = false;
		m_noInputMovement = false;
	}
	//if the 's' key is pressed (move backward)
	else if(InputManager::GetInstance()->IsKeyDown(true,'s'))
	{
		//move character
		if (m_hadFloorCollision || (m_state == ROCKETJUMPING && m_allowAirControl))
		{
			m_velocity.x = m_forward.x * DISTANCEPERSECOND * slowFactor / 1.5f;
			m_velocity.z = m_forward.z * DISTANCEPERSECOND * slowFactor / 1.5f;
		}
		//set animation
		if(m_state != ROCKETJUMPING && !slow)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "BACK");
			SetAnimationAndCacheIndex(m_playerSkeleton, "BACK");
		}
		m_idleTimer = 0;
		noKey = false;
		m_noInputMovement = false;
	}
	else
	{
		if (m_hadFloorCollision && m_state != ROCKETJUMPING)
		{
			m_velocity.y = 0.0f;
		}
	}

	//if the 's' key is pressed (turn right)
	if(InputManager::GetInstance()->IsKeyDown(true,'d'))
	{
		//rotate character
		m_rotation.y += ROTATIONPERSECOND * elapsedTime ;
		m_idleTimer = 0;

		//set animation
		if(noKey && m_state != ROCKETJUMPING && !slow)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_R");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_R");
		}
		
		if(m_rotation.y > 2 * D3DX_PI)
		{
			m_rotation.y = 0;
		}
		//recalculate rotations of character, movement and bullets
		CalculateRotations();
		noKey = false;
	}
	//if the 'a' key is pressed (turn left)
	if(InputManager::GetInstance()->IsKeyDown(true,'a'))
	{
		//rotate character
		m_rotation.y -= ROTATIONPERSECOND * elapsedTime ;

		m_idleTimer = 0;
		//set animation
		if(noKey && m_state != ROCKETJUMPING && !slow)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_L");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_L");
		}
		
		if(m_rotation.y < 0)
		{
			m_rotation.y = 2 * D3DX_PI;
		}
		//recalculate rotations of character, movement and bullets
		noKey = false;
		CalculateRotations();
	}

	//reposition camera is a key is pressed
	if (!noKey)
	{
		if(strcmp(CameraController::GetInstance()->GetCurrentCamera()->GetType(),"THIRDPERSON")==0)
		{
			static_cast<ThirdPersonCamera*>(CameraController::GetInstance()->GetCurrentCamera())->SetTargetYaw(m_rotation.y);
		}
	}
	//if a key is not pressed, set animation to idle and update m_idleTimer
	if(m_idleTimer < 5 && noKey && m_state != ROCKETJUMPING && !slow)
	{
		
	    SetAnimationAndCacheIndex(m_playerCharacter, "IDLE");
		SetAnimationAndCacheIndex(m_playerSkeleton, "IDLE");
		m_idleTimer += elapsedTime;
	}

	//if the timer is creater than five seconds, play the wait animation, then reset the timer
	else if(m_idleTimer > 5 && noKey && !slow)
	{
		SetAnimationAndCacheIndex(m_playerCharacter, "WAIT1");
		SetAnimationAndCacheIndex(m_playerSkeleton, "WAIT1");

		//get animation details
		LPD3DXANIMATIONCONTROLLER pAC = m_playerCharacter->GetAnimationInst()->GetController();
		LPD3DXANIMATIONSET pASTrack;

		int track = m_playerCharacter->GetAnimationInst()->GetTrack();

		const char* anim = m_playerCharacter->GetAnimationInst()->GetAnimationName();


		pAC->GetTrackAnimationSet( track, &pASTrack );
		if(strcmp(anim,"WAIT1")==0)
		{
			D3DXTRACK_DESC td;
			pAC->GetTrackDesc( track, &td );

			double period = pASTrack->GetPeriod();
			//test to see if the animation has completed a cycle
			if( td.Position >= period - 0.1f)
			{
				m_idleTimer = 0;
			}
		}
	}

	//Cheat key input
	static bool cheatLatch = true;
	if(InputManager::GetInstance()->IsKeyDown(true, '8') && 
	   InputManager::GetInstance()->IsKeyDown(true, '9') &&
	   InputManager::GetInstance()->IsKeyDown(true, '0'))
	{
		if (cheatLatch)
		{
			m_cheat = !m_cheat;
			cheatLatch = false;
		}
	}
	else
	{
		cheatLatch = true;
	}

	//if spacebar is pressed (jump)
	if(InputManager::GetInstance()->IsKeyDown(true, ' ') && (m_state != JUMPING && m_state != ROCKETJUMPING))
	{
		SetState(JUMPING);
		m_velocity.y = 20.0f;
		m_idleTimer = 0;
		SoundManager::GetInstance()->GetEmitter("JUMP")->Play();
		SoundManager::GetInstance()->GetEmitter("JUMP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("JUMP")->SetVelocity(m_velocity);
	}

	//if CTRL is pressed (rocket jump)
	if(InputManager::GetInstance()->IsKeyDown(false, CTRL) && (m_state != JUMPING && m_state != ROCKETJUMPING))
	{
		noKey = false;
		SetState(ROCKETJUMPING);
		m_idleTimer = 0;
		m_allowAirControl = true;
		SetAnimationAndCacheIndex(m_playerCharacter, "JET_START");
		SetAnimationAndCacheIndex(m_playerSkeleton, "JET_START");
	}

	static bool shootFlag;

	//is left mouse is pressed shoot bullet.
	if(InputManager::GetInstance()->IsMouseButtonDown(0))
	{
		// shoot depending on upgrade status
		switch(m_currentUpgrade)
		{
			case NONE:
				if(shootFlag == false && m_ammo > 0)
				{
					ShootBullet();
					shootFlag = true;
				}
				break;
			case HEATSEEKER:
				if(shootFlag == false && m_ammo > 0)
				{
					ShootBullet();
					shootFlag = true;
				}
				break;
			case SMARTMISSILE:
				if(shootFlag == false)
				{
					if(m_lockedTarget && m_ammo > 0)
					{
						ShootBullet();
					}
					else
					{
						SetTargetLock();
					}

					shootFlag = true;
				}
				break;
			case THREEWAY:
				if(shootFlag == false && m_ammo > 2)
				{
					ShootBullet();
					ShootBullet(D3DXToRadian(-20));
					ShootBullet(D3DXToRadian(20));
					shootFlag = true;
				}
				break;
			case EXPLOSIVE:
				if(shootFlag == false && m_ammo > 0)
				{
					ShootBullet();
					shootFlag = true;
				}
				break;
			case RAPIDFIRE:
				if(m_rapidFireTimer > RAPIDFIRE_DELAY)
				{
					ShootBullet();
				}
				break;
			default:
				break;
		}
		
		m_idleTimer = 0;
	}
	else if (!InputManager::GetInstance()->IsMouseButtonDown(0))
	{
		shootFlag = false;
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//ProcessAI, takes control of player
//////////////////////////////////////////////////////////////////////////
void
Player::ProcessAI(float elapsedTime)
{
	bool slow = false;
	float slowFactor = 1.0f;

	//if the user is in has been hurt
	if (m_state == HURT || m_state == ELECTROCUTED)
	{
		//if the Diver has not yet played the hurt animation
		if (!m_hurtPlayed)
		{
			m_hurtPlayed = true;
			SoundManager::GetInstance()->GetEmitter("HURT")->Play();
			SoundManager::GetInstance()->GetEmitter("HURT")->SetPosition(m_position);
			SetAnimationAndCacheIndex(m_playerCharacter, "GET_HIT");
			SetAnimationAndCacheIndex(m_playerSkeleton, "GET_HIT");
		}
		if (m_hurtCounter < HURTFALLTIME || m_prevState == ROCKETJUMPING)
		{
			/*don't respond to keys for a short time
			if hurt in air wait till Diver lands before returning control*/
			if (!m_allowCollision)
			{
				slow = true;
				slowFactor = 0.333f;
			}
		}
	}

	if (m_hadFloorCollision)
	{
		m_velocity.x = 0.0f;
		m_velocity.z = 0.0f;
	}

	// behaviour tree
	switch (m_AIState)
	{
	case Player::COLLECTING:
		m_AIState = Collecting(elapsedTime);
		break;
	case Player::ATTACKING:
		m_AIState = Attacking(elapsedTime);
		break;
	case Player::RETREATING:
		m_AIState = Retreating(elapsedTime);
		break;
	}

	// get character rotation after using the behavior method
	m_rotation = m_playerCharacter->GetRotation();

	// calculate model rotations
	CalculateRotations();

	//reposition camera
	if (strcmp(CameraController::GetInstance()->GetCurrentCamera()->GetType(), "THIRDPERSON") == 0)
	{
		static_cast<ThirdPersonCamera*>(CameraController::GetInstance()->GetCurrentCamera())->SetTargetYaw(m_rotation.y);
	}
}



//////////////////////////////////////////////////////////////////////////
//ProcessMovement update the movement of the character
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//ProcessDemoMovement update the movement of the character
//////////////////////////////////////////////////////////////////////////

void
Player::ProcessDemoMovement(float elapsedTime)
{
	const float radius = 1.25f;

	// Gravity
	m_velocity.y -= 40.0f * elapsedTime;

	//variables required for collision test
	float remaining = 1.0f;
	Vector3D newPosition = m_position;
	Vector3D oldVelocityDir = m_velocity;
	newPosition.y += radius;
	Vector3D noCollisionPosition = newPosition + m_velocity * elapsedTime;
	Vector3D intersectionPoint;
	float intersectionTime;

	m_hadFloorCollision = false;
	bool hadCollision = false;
	bool thisCollision = true;
	int maxIterations = 64;

	//test for collisions with the enviroment
	while (remaining > 0.0f && Vector3DDotProduct(&m_velocity, &oldVelocityDir) >= 0.0f && maxIterations--)
	{
		thisCollision = EnvironmentManager::GetInstance()->TestCollision(newPosition, newPosition + m_velocity * elapsedTime * remaining, radius, intersectionPoint, intersectionTime);

		//if in collision
		if (thisCollision)
		{
			hadCollision = true;
			//update position
			newPosition += m_velocity * elapsedTime * intersectionTime * remaining;
			Vector3D normal = newPosition - intersectionPoint;
			Vector3DNormalize(&normal, &normal);

			// Is this a floor surface?
			if (normal.y > 0.75f)
			{
				m_hadFloorCollision = true;
			}
			else
			{
				{
					SetState(JUMPING);
					m_velocity.y = 25.0f;
					SoundManager::GetInstance()->GetEmitter("JUMP")->Play();
					SoundManager::GetInstance()->GetEmitter("JUMP")->SetPosition(m_position);
					SoundManager::GetInstance()->GetEmitter("JUMP")->SetVelocity(m_velocity);
				}

				//if in rocket jump, prevent the user from having control of the character
				if (m_state == ROCKETJUMPING && m_jumpTimer > ROCKETJUMPDURATION)
				{
					m_allowAirControl = false;
				}
			}

			newPosition += normal * 0.0001f;
			m_velocity -= normal * Vector3DDotProduct(&m_velocity, &normal);

			remaining -= intersectionTime * remaining;
		}
		else
		{
			newPosition += m_velocity * elapsedTime * remaining;
			intersectionTime = 1.0f;
			remaining = 0.0f;
		}
	}

	//check creature collisions
	if (m_allowCollision && m_state != DEAD)
	{
		//for each character
		int numCharacters;
		Character** characters = CharacterManager::GetInstance()->GetCharacters(numCharacters);

		for (int index = 0; index < numCharacters; index++)
		{
			// pointer to character indexed
			Character& tempChar = *characters[index];

			// if character is not player, not dead and can damage player
			if (!tempChar.IsPlayer() && !tempChar.IsDead() && tempChar.GetCanDamagePlayer())
			{
				//if in collision with a character hurt the Diver
				if (tempChar.GetBoundingSphere()->CollidingWithSphere(*m_playerCharacter ->GetBoundingSphere()))
				{
					// for all characters bones
					for (int i = 0; i < tempChar.GetBoundingSphereList().size() - 3; i++)
					{
						// if collision with spheres detected
						if (tempChar.GetBoundingSphereList()[i].sphere->CollidingWithSphere(*m_playerCharacter->GetBoundingSphere()))
						{

							if (strcmp(tempChar.GetType(), "JELLY") == 0)
							{
								SetState(ELECTROCUTED);
								m_playerCharacter->SetVisible(true);
								m_playerSkeleton->SetVisible(false);
								m_electicuteTimer = 2.7f;
							}
							else
							{
								SetState(HURT);
							}
							m_flashTimer = 0.0f;
							m_allowCollision = false;
							m_hurtPlayed = false;
							m_health -= tempChar.GetDamageToPlayer();
						}
					}
				}
			}
		}
	}

	if (hadCollision && !m_hadFloorCollision)
	{
		Vector3D normal = newPosition - noCollisionPosition;
		Vector3DNormalize(&normal, &normal);

		// Is this a floor surface?
		if (normal.y > 0.75f)
		{
			m_hadFloorCollision = true;
		}
	}

	newPosition.y -= radius;

	m_position = newPosition;

	if (m_hadFloorCollision)
	{
		if (m_state != ROCKETJUMPING && m_state != HURT)
		{
			SetState(ONFLOOR);
		}
		if ((m_state == HURT || m_state == ELECTROCUTED) && m_prevState != ROCKETJUMPING && m_hurtCounter > HURTFALLTIME)
		{
			SetState(ONFLOOR);
		}
	}
	else if (m_state == ONFLOOR)
	{
		SetState(JUMPING);
	}
}

void
Player::ProcessMovement(float elapsedTime)
{
	const float radius = 1.25f;

	// Gravity
	m_velocity.y -= 40.0f * elapsedTime;

	//variables required for collision test
	float remaining = 1.0f;
	Vector3D newPosition = m_position;
	Vector3D oldVelocityDir = m_velocity;
	newPosition.y += radius;
	Vector3D noCollisionPosition = newPosition + m_velocity * elapsedTime;
	Vector3D intersectionPoint;
	float intersectionTime;

	m_hadFloorCollision = false;
	bool hadCollision = false;
	bool thisCollision = true;
	int maxIterations = 64;
	
	//test for collisions with the enviroment
	while (remaining > 0.0f && Vector3DDotProduct(&m_velocity, &oldVelocityDir) >= 0.0f && maxIterations--)
	{
		thisCollision = EnvironmentManager::GetInstance()->TestCollision(newPosition, newPosition + m_velocity * elapsedTime * remaining, radius, intersectionPoint, intersectionTime);

		//if in collision
		if (thisCollision)
		{
			hadCollision = true;
			//update position
			newPosition += m_velocity * elapsedTime * intersectionTime * remaining;
			Vector3D normal = newPosition - intersectionPoint;
			Vector3DNormalize(&normal, &normal);

			// Is this a floor surface?
			if (normal.y > 0.75f)
			{
				m_hadFloorCollision = true;

				if (m_noInputMovement)
				{
					normal.x = 0.0f;
					normal.y = 1.f;
					normal.z = 0.0f;
				}
			}
			else
			{
				//if in rocket jump, prevent the user from having control of the character
				if (m_state == ROCKETJUMPING && m_jumpTimer > ROCKETJUMPDURATION)
				{
					m_allowAirControl = false;
				}
			}

			newPosition += normal * 0.0001f;
			m_velocity -= normal * Vector3DDotProduct(&m_velocity, &normal);

			remaining -= intersectionTime * remaining;
		}
		else
		{
			newPosition += m_velocity * elapsedTime * remaining;
			intersectionTime = 1.0f;
			remaining = 0.0f;
		}
	}	

	// don't check collisions if shield is on
	if(m_shield == false)
	{	
	//check creature collisions
		if(m_allowCollision && m_state != DEAD && !CutSceneController::GetInstance()->IsCutSceneActive())
		{
			//for each character
			int numCharacters;
			Character** characters = CharacterManager::GetInstance()->GetCharacters(numCharacters);

			// for all characters
			for(int index = 0; index < numCharacters; index++)
			{
				// pointer to character indexed
				Character& tempChar = *characters[index];

				// if character is not player, not dead and can damage player
				if(!tempChar.IsPlayer() && !tempChar.IsDead() && tempChar.GetCanDamagePlayer())
				{
					//if in collision with a character hurt the player
					if(tempChar.GetBoundingSphere()->CollidingWithSphere(*m_playerCharacter->GetBoundingSphere()))
					{
					// for all characters bones
						for(unsigned int i = 0; i < tempChar.GetBoundingSphereList().size() - 3; i++)
						{
							// if collision with spheres detected
							if(tempChar.GetBoundingSphereList()[i].sphere->CollidingWithSphere(*m_playerCharacter->GetBoundingSphere()))
							{
								if(strcmp(tempChar.GetType(), "JELLY") == 0)
								{
									SetState(ELECTROCUTED);
									m_playerSkeleton->SetVisible(true);
									m_playerCharacter->SetVisible(false);
									m_electicuteTimer = 2.7f;
								}
								else
								{
									SetState(HURT);
								}
								m_flashTimer = 0.0f;
								m_allowCollision = false;
								m_hurtPlayed = false;
								m_health -= tempChar.GetDamageToPlayer();
							}
						}
					}
				}
			}
		}
	}

	if (hadCollision && !m_hadFloorCollision)
	{
		Vector3D normal = newPosition - noCollisionPosition;
		Vector3DNormalize(&normal, &normal);

		// Is this a floor surface?
		if (normal.y > 0.75f)
		{
			m_hadFloorCollision = true;
		}
	}

	newPosition.y -= radius;

	m_position = newPosition;

	
	if (m_hadFloorCollision)
	{
		if (m_state != ROCKETJUMPING && m_state != HURT)
		{
			SetState(ONFLOOR);			
		}
		if((m_state == HURT || m_state == ELECTROCUTED) && m_prevState != ROCKETJUMPING && m_hurtCounter > HURTFALLTIME)
		{
			SetState(ONFLOOR);
		}
	}
	else if (m_state == ONFLOOR)
	{
		SetState(JUMPING);
	}
	
	
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Update
//////////////////////////////////////////////////////////////////////////

bool 
Player::Update(float elapsedTime)
{
	//prevent health and air getting above the maximum
	if(m_health > 255)
	{
		m_health = 255;
	}

	if(m_air > 255)
	{
		m_air = 255;
	}
	
	//update bullets
	for (int i = 0; i < NUMBEROFBULLETS; i++)
	{
		if(m_bullets[i])
		{
			m_bullets[i]->Update(elapsedTime);
		}
	
	}

	//If the player is not dead or in a cutscene, process keyboard
	if(m_state != DEAD && !CutSceneController::GetInstance()->IsCutSceneActive())
	{
		// if demo mode processAI else keys
		if (m_demoMode)
		{
			ProcessAI(elapsedTime);
		}
		else
		{
			ProcessKeys(elapsedTime);
		}
	}
	else
	{
		m_velocity.x = 0.0f;
		m_velocity.z = 0.0f;
	}

	//if player is in ELECTROCUTED state, trigger animation
	if(m_state == ELECTROCUTED && m_electicuteTimer > 0.0f)
	{
		m_electicuteTimer -= elapsedTime;
		SetAnimationAndCacheIndex(m_playerSkeleton, "E_SHOCK");
		SetAnimationAndCacheIndex(m_playerCharacter, "E_SHOCK");
		m_velocity.x = 0.0f;
		m_velocity.z = 0.0f;
		m_velocity.y = 0.0f;
		//return true;
	}

	if(m_state != DEAD && !m_allowCollision)
	{
		m_hurtCounter += elapsedTime;
		m_flashTimer += elapsedTime;
		//if the player is hurt in the air wait till they fall to the ground before allowing more collisions
		if(m_hurtCounter > PLAYERHURTDURATION && m_prevState != ROCKETJUMPING)
		{
			m_allowCollision = true;
			m_hurtCounter = 0.0f;
			m_playerSkeleton->SetVisible(false);
			m_playerCharacter->SetVisible(true);
			m_playerSkeleton->SetFlash(false);
		}
		else
		{
			if(m_flashTimer > 0.25f)
			{
				m_flashTimer = 0.0f;
				m_flash = !m_flash;
			}
			m_playerCharacter->SetFlash(m_flash);
			
			if(m_state == ELECTROCUTED)
			{
				//need to flash between player and skeleton
				m_playerSkeleton->SetFlash(false);
				m_playerSkeleton->SetVisible(m_flashTimer >= 0.125f);
				m_playerCharacter->SetVisible(m_flashTimer < 0.125f);
			}
			
		}
	}
	
	// if demo mode process demo movement else
	if (m_demoMode)
	{
		ProcessDemoMovement(elapsedTime);
	}
	else
	{
		//update movement and particle positions
		ProcessMovement(elapsedTime);
	}
	
	UpdateParticlePositions();

	//if rocket jumping
	if((m_state == ROCKETJUMPING) ||((m_state == ELECTROCUTED || m_state == HURT) && m_prevState == ROCKETJUMPING))
	{
		RocketJump(elapsedTime);
	}

	//if player is above or below the level height
	if(m_position.y < DEATHHEIGHT)
	{
		m_health = 0;
	}

	if(m_position.y > CEILINGHEIGHT && m_velocity.y > 0)
	{
		m_velocity.y = 0;
	}

	if(!CutSceneController::GetInstance()->IsCutSceneActive())
	{
		//Update air and / or health
		m_airTimer += elapsedTime;
		//if the player is out of air, we effect there health
		if(m_airTimer > 1.0f && !m_cheat)
		{
			m_airTimer = 0;
			if(m_air > 0)
			{
				
				m_air-=2;
				
			}
			else
			{
				m_air = 0;
				m_health -= 20;
			}
		}
	}

	UpdateStats();
	
	//update cargo compress
	PropItem* closestCargo = PropManager::GetInstance()->GetClosestOfType("CARGO",m_position);
	
	if(closestCargo)
	{	
		m_cargoTarget->SetPosition(Vector3D(m_position.x,m_position.y,m_position.z));
		m_cargoTarget->SetTarget(closestCargo->GetPosition(),elapsedTime);
		m_cargoTarget->SetVisible(true);
	}
	else
	{
		m_cargoTarget->SetVisible(false);
	}
	
	//set mesh position and rotations
	m_playerCharacter->SetPosition(m_position);
	m_playerCharacter->SetRotation(m_rotation);
	m_playerSkeleton->SetPosition(m_position);
	m_playerSkeleton->SetRotation(m_rotation);
	//set sound emitters
	SoundManager::GetInstance()->GetListener("PlayerListener")->SetPosition(m_position);
	SoundManager::GetInstance()->GetListener("PlayerListener")->SetForward(m_forward);
	SoundManager::GetInstance()->GetEmitter("GAMEMUSIC")->SetPosition(m_position);
	SoundManager::GetInstance()->GetEmitter("ENDBOSS")->SetPosition(m_position);
	
	// update timers
	if(m_shield)
	{
		m_shieldTimer += elapsedTime;
	}

	if(m_currentUpgrade != NONE)
	{
		m_upgradeTimer += elapsedTime;
	}


	if(m_currentUpgrade == RAPIDFIRE)
	{
		m_rapidFireTimer += elapsedTime;
	}
	
	// if timer is up, reset upgrade status
	if(m_upgradeTimer > UPGRADE_TIMER)
	{
		m_currentUpgrade = NONE;
		m_targetLock = NULL;
	}

	if(m_shieldTimer > SHIELD_TIMER)
	{
		m_shield = false;
		m_shieldTimer = 0.0f;
		ParticleSystem::GetInstance()->GetEmitter("SHIELD")->SetLooping(false);
	}

	/*Killing the player should be the last thing that happens in the player
	update*/
	if(m_health <= 0 && !m_cheat)
	{
		m_health = 0;
		//Kick the bucket
		
		KillPlayer();
		return false;
		
	}
	return true;
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//CalculateRotations sets the forward movement vector
//////////////////////////////////////////////////////////////////////////

void
Player::CalculateRotations()
{
	Matrix tempRot;
	Matrix rotateX, rotateY, rotateZ;

	MatrixRotationX(&rotateX,m_rotation.x);
	MatrixRotationY(&rotateY,m_rotation.y);
	MatrixRotationZ(&rotateZ,m_rotation.z);

	MatrixMultiply(&tempRot, &rotateX, &rotateY);
	MatrixMultiply(&tempRot, &tempRot, &rotateZ);

	Vector3DTransformCoord(&m_forward,&FORWARD,&tempRot);
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//ShootBullet
//////////////////////////////////////////////////////////////////////////

void
Player::ShootBullet(float rotateMod)
{
	//compute Position 
	Vector3D pos, forward, rotation;
	Matrix tempRot;
	Matrix rotateX, rotateY, rotateZ;

	for(int i = 0; i < NUMBEROFBULLETS; i++)
	{
		//if the current bullet is not alive
		if(!m_bullets[i]->GetAlive())
		{
			//set it alive
			m_bullets[i]->SetAlive(true);

			// set bullet updgrade state
			m_bullets[i]->SetState(m_currentUpgrade);

			if(m_currentUpgrade == HEATSEEKER)
			{
				//check for closest enemy
				int numCharacters;

				Character** characters = CharacterManager::GetInstance()->GetCharacters(numCharacters);

				float maxDistance = 999.0f;

				for(int index = 0; index < numCharacters; index++)
				{
					Character& tempChar = *characters[index];

					if(!tempChar.IsPlayer() && !tempChar.IsDead() && tempChar.GetVulnrability())// no need to check player, dead or invuln characters
					{
						// get distance between
						Sphere& sphere = *tempChar.GetBoundingSphere();
						Vector3D vecTo = sphere.GetPosition() - m_position;
						float distance = Vector3DLengthSq(&vecTo);

						if(distance < maxDistance)
						{
							m_targetLock = &tempChar;
							maxDistance = distance;
						}
					}
				}

				// set target for heat seeker
				m_bullets[i]->SetTarget(m_targetLock);
				m_bullets[i]->SetLife(MISSILELIFE);
			}
			else if(m_currentUpgrade == SMARTMISSILE)
			{
				// set target and lifetime
				m_bullets[i]->SetTarget(m_targetLock);
				m_bullets[i]->SetLife(MISSILELIFE);
				m_lockedTarget = false;
			}
			else
			{
				// set harpoon life
				m_bullets[i]->SetLife(HARPOONLIFETIME);
			}

			//set positions and rotations for the bullets
			rotation = m_rotation;
			rotation.y += rotateMod;
			MatrixRotationX(&rotateX, rotation.x);
			MatrixRotationY(&rotateY, rotation.y);
			MatrixRotationZ(&rotateZ, rotation.z);
			MatrixMultiply(&tempRot, &rotateX, &rotateY);
			MatrixMultiply(&tempRot, &tempRot, &rotateZ);

			Vector3DTransformCoord(&pos, &GUNLOCATOR, &tempRot);
			Vector3DTransformCoord(&forward, &m_forward, &rotateY);

			pos += m_position;
			m_bullets[i]->SetPosition(pos);
			m_bullets[i]->SetRotation(rotation);
			m_bullets[i]->SetForward(forward);

			// don't use ammo for rapid fire updgrade
			if(m_currentUpgrade != RAPIDFIRE)
			{
				m_ammo -= 1;
			}

		// reset rapid fire delay timer
			m_rapidFireTimer = 0.0f;

			//Setup Particle Effect for shooting
			Vector3D minVol, maxVol;


			Vector3DTransformCoord(&minVol, &GUNPARTICLEMINVOL, &tempRot);
			Vector3DTransformCoord(&maxVol, &GUNPARTICLEMAXVOL, &tempRot);

			//Create a particle effect for the gun
			ParticleEmitter* e = new ParticleEmitter();
			e->SetUpEmitter(50, pos, pos, minVol, maxVol, 0.1f, 0.01f, 0.25f, 1);
			e->LoadTexture("partical1", "");
			e->SetLooping(false);
			ParticleSystem::GetInstance()->AddEmitter(e);

			//play shoot sound
			SoundManager::GetInstance()->GetEmitter("SHOOT")->Play();
			SoundManager::GetInstance()->GetEmitter("SHOOT")->SetPosition(m_position);
			SoundManager::GetInstance()->GetEmitter("SHOOT")->SetVolume(200);

			return;
		}
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//RocketJump
//////////////////////////////////////////////////////////////////////////

void 
Player::RocketJump(float elapsedTime)
{
	static float airTimer = 0.25f;
	m_jumpDelay += elapsedTime;

	//pre jump
	if (m_jumpDelay < 0.5f)
	{
		SetAnimationAndCacheIndex(m_playerCharacter, "JET_START");
		SetAnimationAndCacheIndex(m_playerSkeleton, "JET_START");
		SoundManager::GetInstance()->GetEmitter("ROCKETJUMP")->Play();
	}
	//in jump
	else
	{

		Matrix tempRot;
		Matrix rotateX, rotateY, rotateZ;

		Vector3D minVol,maxVol,loc;

		MatrixRotationX(&rotateX,m_rotation.x);
		MatrixRotationY(&rotateY,m_rotation.y);
		MatrixRotationZ(&rotateZ,m_rotation.z);
		MatrixMultiply(&tempRot, &rotateX, &rotateY);
		MatrixMultiply(&tempRot, &tempRot, &rotateZ);
		Vector3DTransformCoord(&loc,&JETPACKLOCATOR,&tempRot);

		//if jump is starting
		if(m_jumpTimer == 0)
		{

			Vector3D minVol,maxVol;

			Vector3DTransformCoord(&minVol,&JETPACKPARTICLEMINVOL,&tempRot);
			Vector3DTransformCoord(&maxVol,&JETPACKPARTICLEMAXVOL,&tempRot);
			int numParticles = 5;
			//if player has enough air
			if(m_air >= ROCKET_AIR_LIMIT || m_cheat)
			{
				//If there is enough air show big particle jet
				numParticles = 100;
			}
			
			//create particle effect
			ParticleEmitter* spray1;
			if (ParticleSystem::GetInstance()->GetEmitter("JetPack"))
			{
				spray1 = ParticleSystem::GetInstance()->GetEmitter("JetPack");
			}
			else
			{
				spray1 = new ParticleEmitter();
				spray1->SetUpEmitter(numParticles,m_position + loc,m_position + loc,minVol,maxVol,0.8f,0.1f,2.0f,2);
				spray1->LoadTexture("partical1","bubble_sprite.tga");
				spray1->SetID("JetPack");
				ParticleSystem::GetInstance()->AddEmitter(spray1);
			}
			spray1->SetLooping(true);
		}
		
		//if air is low or the player gets hurt
		if((m_air < ROCKET_AIR_LIMIT || m_hurtCounter > HURTFALLTIME))
		{
			m_jumpTimer = ROCKETJUMPDURATION + 1;			
		}
		
		m_jumpTimer += elapsedTime;

		//if in jump
		if (m_jumpTimer < ROCKETJUMPDURATION && !(m_jumpDelay > 1.0f && m_hadFloorCollision))
		{
			//update sound and particle positions
			SoundManager::GetInstance()->GetEmitter("ROCKETJUMP")->SetPosition(m_position + loc);
			ParticleSystem::GetInstance()->GetEmitter("JetPack")->SetEmitRegion(m_position + loc,m_position + loc);
			
			//set currect jet pack loop
			if(!InputManager::GetInstance()->IsKeyDown(true,'s'))
			{
				SetAnimationAndCacheIndex(m_playerCharacter, "JET_LOOP");
				SetAnimationAndCacheIndex(m_playerSkeleton, "JET_LOOP");
			}
			else
			{
				SetAnimationAndCacheIndex(m_playerCharacter, "JET_BACK");
				SetAnimationAndCacheIndex(m_playerSkeleton, "JET_BACK");
			}
			

			//update m_velocity
			float speed = (20.0f / ROCKETJUMPDURATION) * m_jumpTimer;

			m_velocity.y = 5.0f + speed;
			SoundManager::GetInstance()->GetEmitter("ROCKETJUMP")->SetVelocity(m_velocity);
		
			//if not cheating, deduct air
			if(!m_cheat)
			{
				airTimer -= elapsedTime;
				if(airTimer < 0.0f)
				{
					airTimer = 0.25f;
					m_air-= 10;
				}
				
			}
		}
		//rocket jump has ended
		else
		{
			//stop rocket jump sound and particle
			SoundManager::GetInstance()->GetEmitter("ROCKETJUMP")->Stop();
			if(ParticleSystem::GetInstance()->GetEmitter("JetPack"))
			{
				ParticleSystem::GetInstance()->GetEmitter("JetPack")->SetLooping(false);
			}
			//set the fall animation
			if((m_state == ELECTROCUTED && m_electicuteTimer < 0.0f) || m_state != ELECTROCUTED)
			{
				SetAnimationAndCacheIndex(m_playerCharacter, "FALL");
				SetAnimationAndCacheIndex(m_playerSkeleton, "FALL");	
			}	
		}
	}

	//reset variables when back on ground
	if (m_jumpDelay > 1.0f && m_hadFloorCollision)
	{
		m_jumpTimer = 0.0f;
		m_jumpDelay = 0.0f;	
		SetState(ONFLOOR);
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//UpdateParticlePositions
//////////////////////////////////////////////////////////////////////////

void
Player::UpdateParticlePositions()
{
	Vector3D pos;
	Matrix tempRot;
	Matrix rotateX, rotateY, rotateZ;

	MatrixRotationX(&rotateX,m_rotation.x);
	MatrixRotationY(&rotateY,m_rotation.y);
	MatrixRotationZ(&rotateZ,m_rotation.z);
	MatrixMultiply(&tempRot, &rotateX, &rotateY);
	MatrixMultiply(&tempRot, &tempRot, &rotateZ);
	Vector3DTransformCoord(&pos,&HELMETLOCATOR,&tempRot);
	pos += m_position;
	ParticleSystem::GetInstance()->GetEmitter("HelemetParticle")->SetEmitRegion(pos, pos);

	// update shield effect position if it's active
	if(m_shield)
	{
		pos.y -= 1.0f;
		ParticleSystem::GetInstance()->GetEmitter("SHIELD")->SetEmitRegion(pos, pos);
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//OnCollected, what happens when a particular prop is collected
//////////////////////////////////////////////////////////////////////////

void 
Player::OnCollected(PropItem* item)
{
	if(strcmp("CROWN",item->GetType()) ==0)
	{
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f,-1.5f,-1.9f),Vector3D(1.9f,1.5f,1.9f),0.1f,0.1f,1.2f,5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark","SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);

		// add points for crown collection
		ScoreManager::GetInstance()->AddPoints(PointSystem::CROWN);

	}
	else if(strcmp("CARGO",item->GetType()) ==0)
	{
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50,pos, pos, Vector3D(-1.9f,-1.5f,-1.9f),Vector3D(1.9f,1.5f,1.9f),0.1f,0.1f,1.2f,5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark","SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
		m_cargo++;

		// add points for cargo collection
		ScoreManager::GetInstance()->AddPoints(PointSystem::CARGO);
	}
	else if(strcmp("AIR",item->GetType()) ==0)
	{
		
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50,pos, pos,Vector3D(-1.9f,-1.5f,-1.9f),Vector3D(1.9f,1.5f,1.9f),0.1f,0.1f,1.2f,5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark","SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("AIRPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("AIRPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("AIRPICKUP")->SetVelocity(m_velocity);
		m_air += 63;
	}
	else if(strcmp("HARPOON",item->GetType()) ==0)
	{
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50,pos, pos,Vector3D(-1.9f,-1.5f,-1.9f),Vector3D(1.9f,1.5f,1.9f),0.1f,0.1f,1.2f,5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark","SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("AMMOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("AMMOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("AMMOPICKUP")->SetVelocity(m_velocity);
		m_ammo += 20;
	}
	else if(strcmp("RUM",item->GetType()) ==0)
	{
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50,pos, pos,Vector3D(-1.9f,-1.5f,-1.9f),Vector3D(1.9f,1.5f,1.9f),0.1f,0.1f,1.2f,5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark","SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("RUMPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("RUMPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("RUMPICKUP")->SetVelocity(m_velocity);
		
		m_health += 50;
	}
	else if(strcmp("COIN",item->GetType()) ==0)
	{
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50,pos, pos,Vector3D(-1.9f,-1.5f,-1.9f),Vector3D(1.9f,1.5f,1.9f),0.1f,0.1f,1.2f,5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark","SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("COINPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("COINPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("COINPICKUP")->SetVelocity(m_velocity);
		m_coins++;
		if(m_coins == m_nextHealthIncrement)
		{
			m_health = 255;
			m_air = 255;
			m_nextHealthIncrement += m_coinsRequiredForHealth;
		}

		// add points for coin collection
		ScoreManager::GetInstance()->AddPoints(PointSystem::COIN);
	}
	//check
	else if(strcmp("POTHOLE",item->GetType()) ==0)
	{	
		//This will fire each time the grate respawns
		item->SetCollected(true);
		ParticleEmitter* spray1 = new ParticleEmitter();
		Vector3D pos = item->GetBoundingSphere()->GetPosition();
		pos += Vector3D(GetRandomFloat(-0.5f,0.5f),0,GetRandomFloat(-0.5f,0.5f));
		spray1->SetUpEmitter(5,
							 pos, pos,
							 Vector3D(-0.2f,1.05f,-0.2f),
							 Vector3D(0.5f,6.0f,0.5f),
							 0.1f,
							 0.1f,
							 10.0f,
							 GetRandomFloat(1.0f,5.0f));
		spray1->LoadTexture("partical1","bubble_sprite.tga");
		spray1->SetLooping(false);
		spray1->SetID("OxgenBubble");
		ParticleSystem::GetInstance()->AddEmitter(spray1);
		SoundManager::GetInstance()->GetEmitter("BUBBLE")->Play();
		SoundManager::GetInstance()->GetEmitter("BUBBLE")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("BUBBLE")->SetVelocity(m_velocity);	
		
		if(m_position.y > item->GetBoundingSphere()->GetPosition().y)
		{
			m_air += 10;
		}
	}
	else if(strcmp("HSMISSILE", item->GetType()) == 0)
	{
		// heat seeker picked up
		m_currentUpgrade = HEATSEEKER;
		m_upgradeTimer = 0.0f;

		// do usual pickup code taken from above
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark", "SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
	}
	else if(strcmp("SMARTMISSILE", item->GetType()) == 0)
	{
		// smart missile picked up
		m_currentUpgrade = SMARTMISSILE;
		m_upgradeTimer = 0.0f;

		// do usual pickup code taken from above
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark", "SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
	}
	else if(strcmp("RAPIDFIRE", item->GetType()) == 0)
	{
		// rapid fire picked up
		m_currentUpgrade = RAPIDFIRE;
		m_upgradeTimer = 0.0f;

		// do usual pickup code taken from above
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark", "SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
	}
	else if(strcmp("THREEWAY", item->GetType()) == 0)
	{
		// three way picked up
		m_currentUpgrade = THREEWAY;
		m_upgradeTimer = 0.0f;

		// do usual pickup code taken from above
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark", "SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
	}
	else if(strcmp("EXPLOSIVE", item->GetType()) == 0)
	{
		// explosive picked up
		m_currentUpgrade = EXPLOSIVE;
		m_upgradeTimer = 0.0f;

		// do usual pickup code taken from above
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark", "SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
	}
	else if(strcmp("SHIELD", item->GetType()) == 0)
	{
		// shield picked up
		m_shield = true;
		m_shieldTimer = 0.0f;

		// set new effect for shield
		ParticleEmitter* shield = new ParticleEmitter();
		const Vector3D& pos = item->GetBoundingSphere()->GetPosition();
		shield->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		shield->SetID("SHIELD");
		shield->LoadTexture("shield", "shield.tga");
		shield->SetLooping(true);
		ParticleSystem::GetInstance()->AddEmitter(shield);

		// do usual pickup code taken from above
		item->SetCollected(true);
		ParticleEmitter* spray = new ParticleEmitter();
		spray->SetUpEmitter(50, pos, pos, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 0.1f, 0.1f, 1.2f, 5);
		spray->SetID("SPARKLE");
		spray->LoadTexture("spark", "SPARKLE_DIF_01.tga");
		spray->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(spray);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->Play();
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetPosition(m_position);
		SoundManager::GetInstance()->GetEmitter("CARGOPICKUP")->SetVelocity(m_velocity);
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//KillPlayer apply death sequence
//////////////////////////////////////////////////////////////////////////

void 
Player::KillPlayer()
{	
	SetState(DEAD);
	m_velocity.x = 0;
	m_velocity.z = 0;
	SetAnimationAndCacheIndex(m_playerCharacter, "DIE");
	SetAnimationAndCacheIndex(m_playerSkeleton, "DIE");
	SoundManager::GetInstance()->GetEmitter("DIE")->Play(false);
	SoundManager::GetInstance()->GetEmitter("DIE")->SetPosition(m_position);
	SoundManager::GetInstance()->GetEmitter("DIE")->SetVelocity(m_velocity);

	//need a dead animation
	LPD3DXANIMATIONCONTROLLER pAC = m_playerCharacter->GetAnimationInst()->GetController();	
	const char* anim = m_playerCharacter->GetAnimationInst()->GetAnimationName();

	if(strcmp(anim,"DIE")==0)
	{
		int track = m_playerCharacter->GetAnimationInst()->GetTrack();
		LPD3DXANIMATIONSET pASTrack;
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );
		pAC->GetTrackAnimationSet( track, &pASTrack );
		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.3f)
		{
			if (m_demoMode)
			{
				SpawnPlayer();
			}
			else
			{
				GameStateManager::GetInstance()->SwitchState("GAMEOVER");
			}
		}	
		if(pASTrack)
		{
			pASTrack->Release();
			pASTrack = 0;
		}
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//UpdateStats, Update data within the games UI
//////////////////////////////////////////////////////////////////////////

void 
Player::UpdateStats()
{
	UIManager* uiManager = UIManager::GetInstance();
	MyString airPecentage;
	if(m_air >= 255)
	{
		m_air = 255;
	}
	int airvalue = (int)((100.0f / 255.0f) * (m_air));
	if(airvalue == 100)
	{
		airvalue = 99;
	}
	airPecentage.Format("%i%%",airvalue);
	static_cast<UIText*>(uiManager->GetUIObject("Air"))->SetValue(airPecentage);

	static_cast<UIText*>(uiManager->GetUIObject("Ammo"))->SetValue(m_ammo);

	int healthValue = (int)((100.0f / 255.0f) * (m_health));
	static_cast<UIText*>(uiManager->GetUIObject("health"))->SetValue(healthValue);
	static_cast<UIText*>(uiManager->GetUIObject("Cargo"))->SetValue(m_cargo);
	static_cast<UIText*>(uiManager->GetUIObject("Coin"))->SetValue(m_coins);

	// update score to display on screen
	unsigned int score = ScoreManager::GetInstance()->GetScore();
	MyString buffer;
	buffer.Format("Score: %u", score);
	static_cast<UIText*>(uiManager->GetUIObject("Score"))->SetValue(buffer);

	if(healthValue < 20)
	{
		static_cast<UIText*>(uiManager->GetUIObject("health"))->SetTextColour(255,0,0);
	}
	else
	{
		static_cast<UIText*>(uiManager->GetUIObject("health"))->SetTextColour(255,255,255);
	}

	if(airvalue < 20)
	{
		static_cast<UIText*>(uiManager->GetUIObject("Air"))->SetTextColour(255,0,0);
	}
	else
	{
		static_cast<UIText*>(uiManager->GetUIObject("Air"))->SetTextColour(255,255,255);
	}
}

void 
Player::SpawnPlayer()
{
	// spawn diver at random prop location
	PropManager* manager = PropManager::GetInstance(); // pointer to prop manager
	CharacterManager* cm = CharacterManager::GetInstance();	// get pointer to character manager
	int maxProps = manager->GetNumProps(); // max props in list
	Vector3D spawnLocation;
	bool safe = false;

	// loop until spawn position is safe distance from an enemy
	do
	{
		int index = rand() % maxProps; // random prop index
		spawnLocation = PropManager::GetInstance()->GetPropItem(index)->GetPosition();

		// loop through characters
		for (int i = 0; i < cm->GetNumberCharacters(); i++)
		{
			// if character is not related to the player
			if (!cm->GetCharacter(i)->IsPlayer())
			{
				if (DistanceToTargetSq(cm->GetCharacter(i), spawnLocation) >  50.0f)
				{
					safe = true;
				}
				else
				{
					safe = false;
					break;
				}
			}
		}
	} while (!safe);

	m_position = spawnLocation;

	// create new target location for diver
	m_targetPosition = PropManager::GetInstance()->GetClosestProp(m_position)->GetPosition();

	// set new positions and rotation of character
	m_playerCharacter->SetPosition(m_position);
	m_playerSkeleton->SetPosition(m_position);
}

Player::AIState
Player::Collecting(float elapsedTime)
{
	// if player reaches target set new one
	if (DistanceToTargetSq(m_playerCharacter, m_targetPosition) < 0.5f)
	{
		// get pointer to prop manager
		PropManager* pm = PropManager::GetInstance();

		// if closet prop is a pothole
		if (pm->GetClosestProp(m_position)->GetType() == "POTHOLE")
		{
			// target nearest coin instead
			m_targetPosition = pm->GetClosestOfType("COIN", m_position)->GetPosition();
		}
		else
		{
			// get any close prop
			m_targetPosition = PropManager::GetInstance()->GetClosestProp(m_position)->GetPosition();
		}
	}

	// get prop height from diver to see if he needs to jump
	float height = m_targetPosition.y - m_position.y;
	if (height > 2.5f)
	{
		// rocket jump
		SetState(ROCKETJUMPING);
		m_allowAirControl = true;
		SetAnimationAndCacheIndex(m_playerCharacter, "JET_START");
		SetAnimationAndCacheIndex(m_playerSkeleton, "JET_START");
	}

	// Path find to target
	float tempAngle = GetPathAngleToTarget(m_playerCharacter, m_targetPosition);

	TurnDirection direction = TurnTo(m_playerCharacter, tempAngle, ROTATIONPERSECOND, elapsedTime);

	switch (direction)
	{
	case TD_LEFT:
		//set animation
		if (m_state != ROCKETJUMPING)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_L");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_L");
		}
		break;
	case TD_RIGHT:
		//set animation
		if (m_state != ROCKETJUMPING)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_R");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_R");
		}
		break;
	case TD_FACING:
		//move character
		if (m_hadFloorCollision || (m_state == ROCKETJUMPING && m_allowAirControl))
		{
			m_velocity.x = -m_forward.x * DISTANCEPERSECOND;
			m_velocity.z = -m_forward.z * DISTANCEPERSECOND;
			//set animation
			if (m_state != ROCKETJUMPING)
			{
				SetAnimationAndCacheIndex(m_playerCharacter, "RUN");
				SetAnimationAndCacheIndex(m_playerSkeleton, "RUN");
			}
		}
		break;
	}


	CharacterManager* cm = CharacterManager::GetInstance();	// get pointer to character manager

	for (int index = 0; index < cm->GetNumberCharacters(); index++)
	{
		// if character is not related to the player
		if (!cm->GetCharacter(index)->IsPlayer() &&
			!cm->GetCharacter(index)->IsDead())
		{
			// if an enemy is within range
			if (DistanceToTargetSq(m_playerCharacter, cm->GetCharacter(index)->GetPosition()) < DETECTION_RANGE)
			{
				// set enemy as target
				m_NPCTarget = cm->GetCharacter(index);
				m_targetPosition = m_NPCTarget->GetPosition();

				// return state ATTACKING
				return ATTACKING;
			}
		}
	}

	return COLLECTING;
}

Player::AIState
Player::Attacking(float elapsedTime)
{
	// set target to npc position
	m_targetPosition = m_NPCTarget->GetPosition();

	// Path find to target
	float tempAngle = GetPathAngleToTarget(m_playerCharacter, m_targetPosition);

	TurnDirection direction = TurnTo(m_playerCharacter, tempAngle, ROTATIONPERSECOND, elapsedTime);

	switch (direction)
	{
	case TD_LEFT:
		//set animation
		if (m_state != ROCKETJUMPING)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_L");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_L");
		}
		break;
	case TD_RIGHT:
		//set animation
		if (m_state != ROCKETJUMPING)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_R");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_R");
		}
		break;
	case TD_FACING:
		//move character
		// fire bullet
		SetAnimationAndCacheIndex(m_playerCharacter, "IDLE");
		SetAnimationAndCacheIndex(m_playerSkeleton, "IDLE");

		// get enemy height from diver to see if he needs to jump
		float height = m_targetPosition.y - m_position.y;
		if (height > 2.5f)
		{
			// rocket jump
			SetState(ROCKETJUMPING);
			m_allowAirControl = true;
			SetAnimationAndCacheIndex(m_playerCharacter, "JET_START");
			SetAnimationAndCacheIndex(m_playerSkeleton, "JET_START");
		}

		// if enemy is at height for shooting
		if (height < 1.0f)
		{
			// create random number of shots
			int shots = rand() % MAX_SHOTS + 1;

			// shoot random number of times
			for (int i = 0; i < shots; i++)
			{
				// only if enough ammo
				if (m_ammo > shots)
				{
					ShootBullet();
				}
			}

			// set target position to away from 
			Matrix rotation;
			Vector3D facing;
			MatrixRotationY(&rotation, D3DX_PI);
			Vector3DTransformCoord(&facing, &m_forward, &rotation);
			m_targetPosition = m_position - facing * DETECTION_RANGE;

			return RETREATING;
		}
		break;
	}

	return ATTACKING;
}

Player::AIState
Player::Retreating(float elapsedTime)
{
	// Path find to target
	float tempAngle = GetPathAngleToTarget(m_playerCharacter, m_targetPosition);

	TurnDirection direction = TurnTo(m_playerCharacter, tempAngle, ROTATIONPERSECOND, elapsedTime);

	switch (direction)
	{
	case TD_LEFT:
		//set animation
		if (m_state != ROCKETJUMPING)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_L");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_L");
		}
		break;
	case TD_RIGHT:
		//set animation
		if (m_state != ROCKETJUMPING)
		{
			SetAnimationAndCacheIndex(m_playerCharacter, "TURN_R");
			SetAnimationAndCacheIndex(m_playerSkeleton, "TURN_R");
		}
		break;
	case TD_FACING:

		//move character
		if (m_hadFloorCollision || (m_state == ROCKETJUMPING && m_allowAirControl))
		{
			m_velocity.x = -m_forward.x * DISTANCEPERSECOND;
			m_velocity.z = -m_forward.z * DISTANCEPERSECOND;
			//set animation
			if (m_state != ROCKETJUMPING)
			{
				SetAnimationAndCacheIndex(m_playerCharacter, "RUN");
				SetAnimationAndCacheIndex(m_playerSkeleton, "RUN");
			}
		}

		if (m_NPCTarget->IsDead() ||
			DistanceToTargetSq(m_playerCharacter, m_NPCTarget->GetPosition()) > DETECTION_RANGE)
		{
			// get any close prop
			m_targetPosition = PropManager::GetInstance()->GetClosestProp(m_position)->GetPosition();
			return COLLECTING;
		}
		break;
	}

	return RETREATING;
}

void 
Player::SetDemo(bool demo)
{
	m_demoMode = demo;
}

void
Player::SetTargetLock()
{
	// find target
	//check for collision with creatures
	int numCharacters;
	Character** characters = CharacterManager::GetInstance()->GetCharacters(numCharacters);

	float maxDistance = 999.0f;

	for(int index = 0; index < numCharacters; index++)
	{
		Character& tempChar = *characters[index];
	
		if(!tempChar.IsPlayer() && !tempChar.IsDead() && tempChar.GetVulnrability())// no need to check player, dead or invuln characters
		{
			Vector3D charPosition;
			Vector3DNormalize(&charPosition, &(m_position - tempChar.GetBoundingSphere()->GetPosition()));

			// check character is in view
			float dot = Vector3DDotProduct(&m_forward, &charPosition);
			if(dot > 0.6)
			{
					// get distance between
				Sphere& sphere = *tempChar.GetBoundingSphere();
				Vector3D vecTo = sphere.GetPosition() - m_position;
				float distance = Vector3DLengthSq(&vecTo);

				if(distance < maxDistance)
				{
					m_targetLock = &tempChar;
					maxDistance = distance;
					m_lockedTarget = true;
				}
			}
		}
	}
}

/************************************************************************/

void
Player::SetCoinsRequiredForHealthBoost(int coins)
{
	m_coinsRequiredForHealth = coins;
	m_nextHealthIncrement = coins;
}

void
Player::SetHurt(int damage)
{
	if(m_shield == false)
	{
		m_state = HURT;
		m_flashTimer = 0.0f;
		m_allowCollision = false;
		m_hurtPlayed = false;
		m_health -= damage;
	}
}

/************************************************************************/