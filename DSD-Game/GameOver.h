#ifndef _GAMEOVER_H_
#define _GAMEOVER_H_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif

class GameOver : public GameState
{
public:
	/**
	 * Constructor
	 */
						GameOver();
	/**
	 * Destructor
	 */
	virtual				~GameOver();
	/**
	 * On entry handler, called when you enter the game state
	 */
	virtual void		OnEntry();
	/**
	 * On exit handler, called when you exit the game state
	 */
	virtual void		OnExit();
	/**
	 * Update handler, called once a frame for the state to update its logic.
	 */
	virtual void		Update(
							float elapsedTime ///< The elapsed time this frame
							);
	/**
	 *	Render handler, handles renderering the game for its current state
	 */
	virtual void		Render();
private:

	float m_timer;
};

#endif