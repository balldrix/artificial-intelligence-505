
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CageEntrance.h"

/**********************************************************************************************************************************/

CageEntrance::CageEntrance() : 
	Behavior("CAGE_ENTER")
{

}

/**********************************************************************************************************************************/

CageEntrance::~CageEntrance()
{

}

/**********************************************************************************************************************************/

void 
CageEntrance::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "DROP");
	owner->SetVulnarability(false);
}

/**********************************************************************************************************************************/

void 
CageEntrance::Update(Character* owner, float elapsedTime)
{
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );
	
	if(strcmp(anim,"DROP")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{
			owner->SetBehavior("CAGE_IDLE");

		}		
	}

}

/**********************************************************************************************************************************/

void 
CageEntrance::OnExit(Character* owner)
{
	
}

/**********************************************************************************************************************************/
