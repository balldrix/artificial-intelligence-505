
#ifndef _CRABBEHAVIOR_H_
#define _CRABBEHAVIOR_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

const int NUM_CRAB_HITS = 5;

const float CRABDISTPERSEC = 3.0f;
const float CRABROTPERSEC = 1.0f;

const float CRABCHASEDIST = 15.0f;

const float CRABATTACKDIST = 5.0f;


/**
 * The base crab behaviour which extends the behaviour class to enable them to move like a crab
 */
class CrabBehavior : public Behavior
{
public:
	/**
	 * Constructor
	 */
					CrabBehavior(
						const char* ID ///< The name of the behaviour
						);
	/**
	 * Destructor
	 */
	virtual			~CrabBehavior();
protected:
	/**
	 * General Purpose crab behaviour helper which makes the crab walk sideways towards the target position
	 */
	virtual void	WalkSideways(
						Character* owner,
						const Vector3D& targetPos,
						float elapsedTime
						);
	/**
	 * General Purpose crab behaviour helper which returns the sideways angle to the target position
	 */
	virtual float	SideAngleToTarget(
						Character* owner, 
						const Vector3D& targetPos
						);
};

#endif //#ifndef _CRABBEHAVIOR_H_