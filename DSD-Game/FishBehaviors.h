#ifndef _FISHBEHAVIORS_H_
#define _FISHBEHAVIORS_H_

const int FISHCHASEDIST = 20.0f;

const int NUM_FISH_HITS = 2;
const float FISHROTPERSEC = 1.5f;
const float FISHDISTPERSEC = 2.0f;

#include "FishPatrol.h"
#include "FishDeath.h"
#include "FishChase.h"

#endif //#ifndef _FISHBEHAVIORS_H_