
#ifndef _CRABPATROL_H_
#define _CRABPATROL_H_

#ifndef _CRABBEHAVIOR_H_
#include "CrabBehavior.h"
#endif //#ifndef _CRABBEHAVIOR_H_

class CrabPatrol : public CrabBehavior
{
public:
	/**
	 * Constructor
	 */
					CrabPatrol();
	/**
	 * Destructor
	 */
	virtual			~CrabPatrol();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
};

#endif //#ifndef _CRABPATROL_H_