
#ifndef _KINGCRABENTRANCE_H_
#define _KINGCRABENTRANCE_H_

#ifndef _CRABBEHAVIOR_H_
#include "CrabBehavior.h"
#endif //#ifndef _CRABBEHAVIOR_H_

class KingCrabEntrance : public CrabBehavior
{
public:
	/**
	 * Constructor
	 */
					KingCrabEntrance();
	/**
	 * Destructor
	 */
	virtual			~KingCrabEntrance();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);

private:
	float m_timer;
};

#endif //#ifndef _KINGCRABENTRANCE_H_