
#ifndef _CAGEEND_H_
#define _CAGEEND_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class CageEnd : public Behavior
{
public:
	/**
	 * Constructor
	 */
					CageEnd();
	/**
	 * Destructor
	 */
	virtual			~CageEnd();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
private:
	Sphere* m_sphere;
};

#endif //#define _CAGEEND_H_