
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CrabChase.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

CrabChase::CrabChase() : 
	CrabBehavior("CRAB_CHASE")
{

}

/**********************************************************************************************************************************/

CrabChase::~CrabChase()
{

}

/**********************************************************************************************************************************/

void 
CrabChase::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "WALK_FORWARD");
}

/**********************************************************************************************************************************/

void 
CrabChase::Update(Character* owner, float elapsedTime)
{
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();

	pAC->GetTrackAnimationSet( track, &pASTrack );

	if(strcmp(anim,"WALK_FORWARD")==0)
	{
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{			 
			float distSq = DistanceToCharacterSq(owner,*m_playerCharacter);
			if( distSq > CRABCHASEDIST * CRABCHASEDIST)
			{		
				owner->SetBehavior("CRAB_PATROL");
				return;
			}
			else if(distSq < CRABATTACKDIST * CRABATTACKDIST)
			{
				owner->SetBehavior("CRAB_ATTACK");
				return;
			}
			pAC->SetTrackPosition(track,0);
		}		
	}

	//kill it
	if(owner->GetHitCount() > NUM_CRAB_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("CRAB_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::CRAB);

		return;
	}
	float tempAngle = GetPathAngleToTarget(owner,m_playerCharacter->GetPosition());

	TurnDirection dirToTurn  = TurnTo(owner,tempAngle,CRABROTPERSEC,elapsedTime);

	switch(dirToTurn)
	{
	case TD_FACING:
		SetAnimationAndCacheIndex(owner, "WALK_FORWARD");
		if(owner->GetPath().GetFirst())
		{
			MoveInDirection(owner, elapsedTime);
		}		
		break;
	case TD_LEFT:
		SetAnimationAndCacheIndex(owner, "WALK_LEFT");
		break;
	case TD_RIGHT:
		SetAnimationAndCacheIndex(owner, "WALK_RIGHT");
		break;
	default: 
		break;
	};
}

/**********************************************************************************************************************************/

void 
CrabChase::OnExit(Character* owner)
{
	
}

/**********************************************************************************************************************************/
