#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "SquidBehaviors.h"
#include "SquidPatrol.h"

#include "ScoreManager.h"
#include "ParticleEmitter.h"

/**********************************************************************************************************************************/

SquidPatrol::SquidPatrol() : 
	Behavior("SQUID_PATROL")
{

}

/**********************************************************************************************************************************/

SquidPatrol::~SquidPatrol()
{

}

/**********************************************************************************************************************************/

void
SquidPatrol::OnEnter(Character* owner)
{
	owner->SetSpeed(SQUID_SPEED);
	SetAnimationAndCacheIndex(owner, "SWIM");
}

/**********************************************************************************************************************************/

void 
SquidPatrol::Update(Character* owner, float elapsedTime)
{
	float tempAngle = GetPathAngleToTarget(owner,owner->GetTargetWayPoint()->m_Position);

	if(TurnTo(owner,tempAngle,SQUIDROTPERSEC,elapsedTime) == TD_FACING)
	{	
		if(owner->GetPath().GetFirst())
		{
			MoveInDirection(owner, elapsedTime);
		}
	}	

	if(DistanceToTargetSq(owner,owner->GetTargetWayPoint()->m_Position) < 0.1f)
	{
		owner->ArrivedAtWaypoint();
	}
	
	if(DistanceToCharacterSq(owner,*m_playerCharacter) < SQUIDCHASEDIST * SQUIDCHASEDIST)
	{
		//check player is in fov before chasing
		if(CharacterInFov(owner, m_playerCharacter, D3DXToRadian(90)))
		{
			owner->SetBehavior("SQUID_CHASE");
		}
	}
	
	if(owner->GetHitCount() > NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("SQUID_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::SQUID);
	}

	// if health is below 50%
	if(owner->GetHitCount() > (NUM_SQUID_HITS * ScoreManager::GetInstance()->GetDifficulty()) / 2)
	{
		// release ink cloud
		ParticleEmitter* ink = new ParticleEmitter();
		ink->SetUpEmitter(1000, owner->GetPosition(), owner->GetPosition(), Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 1.0f, 3.0f, 5.0f, 50);
		ink->SetID("INK");
		ink->LoadTexture("ink", "Ink.tga");
		ink->SetLooping(false);
		ParticleSystem::GetInstance()->AddEmitter(ink);

		// set to flee
		owner->SetBehavior("SQUID_FLEE");
		
		// set target position to away from 
		Matrix rotation;
		Vector3D facing = m_playerCharacter->GetBoundingSphere()->GetPosition() - owner->GetBoundingSphere()->GetPosition();
		Vector3DNormalize(&facing, &facing);
		MatrixRotationY(&rotation, D3DX_PI);
		Vector3DTransformCoord(&facing, &facing, &rotation);

		owner->SetDestination(owner->GetPosition() + facing * SQUIDCHASEDIST);
	}
}

/**********************************************************************************************************************************/

void	
SquidPatrol::OnExit(Character* owner)
{
}

bool 
SquidPatrol::CharacterInFov(Character* owner, Character* player, float fov)
{
	// get squid forward facing vector
	Vector3D facing;
	Matrix rot;
	MatrixRotationY(&rot, owner->GetRotation().y);
	Vector3DTransformCoord(&facing, &Vector3D(0.0f, 0.0f, 1.0f), &rot);
	Vector3DNormalize(&facing, &facing);

	// find player direction from squid
	Vector3D playerPosition;
	Vector3DNormalize(&playerPosition, &(player->GetPosition() - owner->GetPosition()));

	// get dot product of angle
	float angle = Vector3DDotProduct(&facing, &playerPosition);
	
	fov = 1 - (fov / D3DX_PI);

	// check if angle is within fov
	if(angle > fov)
	{
		return true;
	}
	return false;
}

/**********************************************************************************************************************************/
