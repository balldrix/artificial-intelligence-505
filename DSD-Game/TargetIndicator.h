#ifndef _TARGETINDICATOR_H_
#define _TARGETINDICATOR_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif //_RENDEREVENT_H_

class Model;
class Character;

class TargetIndicator : public RenderEvent
{
public:
	/**
	 * Constructor
	 */
							TargetIndicator();
	/**
	 * Destructor
	 */
							~TargetIndicator();
	/**
	 * Sets the model to render as the compasses background
	 */
	void					SetBaseMesh(
								Model* mesh
								)		{m_baseModelPtr = mesh;}
	/**
	 * Sets the model to render as the needle of the compass
	 */
	void					SetNeedleMesh(
								Model* mesh
								)		{m_modelPtr = mesh;}
	/**
	 * Sets the position of the compass in the world
	 */
	void					SetPosition(
								const Vector3D& position
								){m_position = position;}
	/**
	 * Handles the updating the rotation of the compass to the target co-ord 
	 * from its current location.
	 */
	void					SetTarget(
								const Vector3D& target, 
								float elapsedTime
								);
	/**
	 * Renders the compass
	 */
	void					Render();
	/**
	 * Sets the compass visibility
	 */
	void					SetVisible(
								bool visible
								){m_isVisible = visible;}
private:
	Model*	 m_modelPtr;
	Model*	 m_baseModelPtr;
	bool m_isVisible;
	float m_rotation;
	float m_randOffset;

	Vector3D m_position;
	Character* m_playerCharacter;
};

#endif