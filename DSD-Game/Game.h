#ifndef _GAME_H_
#define _GAME_H_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif

class GameState;
class Stats;
class SkinnedMesh;
class Player;
class BillBoard;
class Effect;
class SkyDome;
class Model;
class SprayEmitter;
class RenderTargetSurface;

class Game : public GameState
{
public:
	/**
	 * Constructor
	 */
						Game();
	/**
	 * Destructor
	 */
	virtual				~Game();
	/**
	 * On entry handler, called when you enter the game state
	 */
	virtual void		OnEntry();
	/**
	 * On exit handler, called when you exit the game state
	 */
	virtual void		OnExit();
	/**
	 * Update handler, called once a frame for the state to update its logic.
	 */
	virtual void		Update(
							float elapsedTime ///< The elapsed time this frame
							);
	/**
	 *	Render handler, handles renderering the game for its current state
	 */
	virtual void		Render();
private:
	void				CreateBehaviors();

	void				TriggerGameEvents();

	Player* m_player;
	Stats* m_stats;
	
	RenderTargetSurface* m_renderTarget;
	IDirect3DVertexBuffer9* m_renderTargetVertex;

	int m_treasure;

	bool m_KingCrabTriggered;
	bool m_KingCrabKilled;
	bool m_CrownShown;
	bool m_CrownCollected;
};

#endif
