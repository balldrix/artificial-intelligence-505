#ifndef _ENEMY_PROJECTILE_H_
#define _ENEMY_PROJECTILE_H_

#ifndef _RENDEREVENT_H_
#include "RenderEvent.h"
#endif //_RENDEREVENT_H_

class Model;
class Player;

class EnemyProjectile : public RenderEvent
{
public:
	/**
	* Constructor
	*/
						EnemyProjectile();
	/**
	* Destructor
	*/
						~EnemyProjectile();
	/**
	* Sets up the model and sets the ink projectile id
	*/
	void				CreateProjectile(const char* ID, const char* type, Player* player);

	/**
	* Renders the projectile if it is active
	*/
	virtual void		Render();
	/**
	* Updates the movement of the projectile and detects if it has hit any characters
	* if it is still active
	*/
	void				Update(float elapsedTime);
	/**
	* Sets the position of the projectile
	*/
	void				SetPosition(const Vector3D& pos);
	/**
	* Gets current position of the projectile
	*/
	const Vector3D&		GetPosition() const;
	/**
	* Sets the current rotation of the projectile
	*/
	void				SetRotation(
		const Vector3D& rot
	);
	/**
	* Gets the current rotation of the projectile
	*/
	const Vector3D&		GetRotation() const;
	/**
	* Sets the direction the projectile should move
	*/
	void				SetForward(const Vector3D& forward);
	/**
	* Gets the direction of the projectile
	*/
	const Vector3D&		GetForward() const;
	/**
	* Sets the projectile active
	*/
	void				SetAlive(bool alive);
	/**
	* Returns if the projectile is currently active
	*/
	bool				GetAlive() const;

private:
	MyString m_ID;

	Model* m_model;
	Player* m_player;
	Vector3D m_position;
	Vector3D m_rotation;
	Vector3D m_forward;
	float m_life;

	bool m_alive;
};

#endif //_ENEMY_PROJECTILE_H_
