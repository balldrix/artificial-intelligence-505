// Demo.h
// Christopher Ball 2017
// modified Game class to use as attract mode

#ifndef _DEMO_H_
#define _DEMO_H_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif

class GameState;
class Stats;
class SkinnedMesh;
class BillBoard;
class Effect;
class SkyDome;
class Model;
class SprayEmitter;
class RenderTargetSurface;

class Player;

// time allowed for flashing text
const float FLASHING_TEXT_TIME = 2.0f;

// time allowed for demo to be displayed
const float DEMO_MODE_TIME = 30.0f;

class Demo : public GameState
{
public:
	/**
	* Constructor
	*/
	Demo();
	/**
	* Destructor
	*/
	virtual				~Demo();
	/**
	* On entry handler, called when you enter the game state
	*/
	virtual void		OnEntry();
	/**
	* On exit handler, called when you exit the game state
	*/
	virtual void		OnExit();
	/**
	* Update handler, called once a frame for the state to update its logic.
	*/
	virtual void		Update(
		float elapsedTime ///< The elapsed time this frame
	);
	/**
	*	Render handler, handles renderering the game for its current state
	*/
	virtual void		Render();
private:
	void				CreateBehaviors();

	Player* m_player;
	Stats* m_stats;

	RenderTargetSurface* m_renderTarget;
	IDirect3DVertexBuffer9* m_renderTargetVertex;

	int m_treasure;

	float m_demoTimer; // times how long demo has been active
	float m_flashingTextTimer; // times how long flashing text has toggled

	bool m_textToggle; // toggle text display
};

#endif
