#include "Singleton.h"
#include "ScoreManager.h"

DefineSingleton(ScoreManager);

ScoreManager::ScoreManager() :
	m_currentScore(0),
	m_difficulty(1.0f)
{
}

ScoreManager::~ScoreManager()
{
}

void
ScoreManager::AddPoints(unsigned int points)
{	
	// add points to current score
	m_currentScore += points * m_difficulty;
}

void 
ScoreManager::ResetScore()
{
	// reset score to zero
	m_currentScore = 0;
}

void 
ScoreManager::SetDifficulty(float multiplier)
{	
	// set difficulty multiplier value
	m_difficulty = multiplier;
}
