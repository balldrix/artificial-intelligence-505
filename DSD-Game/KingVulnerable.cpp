
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "KingBehaviors.h"
#include "KingVulnerable.h"

#include "ScoreManager.h"

/**********************************************************************************************************************************/

KingVulnerable::KingVulnerable() : 
	CrabBehavior("KING_VULNARABLE")
{
	m_normalRadius = 0;
	m_attackRadius = 0;
}

/**********************************************************************************************************************************/

KingVulnerable::~KingVulnerable()
{

}

/**********************************************************************************************************************************/

void 
KingVulnerable::OnEnter(Character* owner)
{
	if(m_normalRadius == 0)
	{
		m_normalRadius = owner->GetBoundingSphere()->GetRadius();
		m_attackRadius = m_normalRadius * 1.5f;
	}
	SetAnimationAndCacheIndex(owner, "ATTACK_VUL_A");
}

/**********************************************************************************************************************************/

void 
KingVulnerable::Update(Character* owner, float elapsedTime)
{
	//attack player
	float tempAngle = AngleToTarget(owner,m_playerCharacter->GetPosition());

	TurnTo(owner,tempAngle,KINGROTPERSEC,elapsedTime);

	//Place increase / decrease of bounding sphere here.
	LPD3DXANIMATIONCONTROLLER pAC = owner->GetAnimationInst()->GetController();
	LPD3DXANIMATIONSET pASTrack;

	int track = owner->GetAnimationInst()->GetTrack();

	const char* anim = owner->GetAnimationInst()->GetAnimationName();


	pAC->GetTrackAnimationSet( track, &pASTrack );
	if(strcmp(anim,"ATTACK_VUL_A")==0)
	{
		owner->SetVulnarability(false);
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position > (period/2) - 0.1f && td.Position < (period/2) + 0.1f)
		{
			owner->GetBoundingSphere()->SetRadius(m_attackRadius);
		}
		
		else
		{
			owner->GetBoundingSphere()->SetRadius(m_normalRadius);
		}
		
		if( td.Position >= period - 0.1f)
		{
			SetAnimationAndCacheIndex(owner, "ATTACK_VUL_B");
		}		
	}
	else if(strcmp(anim,"ATTACK_VUL_B")==0)
	{
		////////////////////////////////////////////////////
		//THIS IS THE ONLY PLACE THE KING CRAB SHOULD TAKE HITS
		////////////////////////////////////////////////////
		owner->SetVulnarability(true);
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{			
			SetAnimationAndCacheIndex(owner, "ATTACK_VUL_C");
		}		
	}
	else if(strcmp(anim,"ATTACK_VUL_C")==0)
	{
		owner->SetVulnarability(false);
		D3DXTRACK_DESC td;
		pAC->GetTrackDesc( track, &td );

		double period = pASTrack->GetPeriod();
		if( td.Position >= period - 0.1f)
		{
			int randomAttack = rand() % 2;
			if(randomAttack == 1)
			{
				owner->SetBehavior("KING_VULNARABLE");
			}
			else
			{
				owner->SetBehavior("KING_ATTACK");
			}
			
			if(DistanceToCharacterSq(owner,*m_playerCharacter) > KINGATTACKDIST * KINGATTACKDIST)
			{		
				owner->SetBehavior("KING_CHASE");	
			}
		}		
	}

	if(pASTrack)
	{
		pASTrack->Release();
		pASTrack = 0;
	}

	//kill it
	if(owner->GetHitCount() > NUM_KING_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("CRAB_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::KING_CRAB);
	}
}

/**********************************************************************************************************************************/

void 
KingVulnerable::OnExit(Character *owner)
{
	owner->SetVulnarability(false);
}

/**********************************************************************************************************************************/