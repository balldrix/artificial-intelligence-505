
#ifndef _CRABCHASE_H_
#define _CRABCHASE_H_

#ifndef _CRABBEHAVIOR_H_
#include "CrabBehavior.h"
#endif //#ifndef _CRABBEHAVIOR_H_

class CrabChase : public CrabBehavior
{
public:
	/**
	 * Constructor
	 */
					CrabChase();
	/**
	 * Destructor
	 */
	virtual			~CrabChase();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
};

#endif //#ifndef _CRABCHASE_H_