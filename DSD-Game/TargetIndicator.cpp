#include "Types.h"
#include "TargetIndicator.h"
#include "RenderManager.h"
#include "Model.h"
#include "ThirdPersonCamera.h"
#include "CameraController.h"
#include "UIManager.h"
#include "UIObject.h"
#include "UISprite.h"
#include "Character.h"
#include "CharacterManager.h"
#include "CutSceneController.h"

const float TURNSPEED = 1.3f;

/**********************************************************************************************************************************/

TargetIndicator::TargetIndicator() :
	m_isVisible(true),
	m_randOffset(0.0f)
{
	m_flags = UIEffect;
	RenderManager::GetInstance()->AddEvent(this, NULL);
	m_playerCharacter =  CharacterManager::GetInstance()->GetCharacter("Player");
}

/**********************************************************************************************************************************/

TargetIndicator::~TargetIndicator()
{
	RenderManager::GetInstance()->RemoveEvent(this);
	m_modelPtr = 0;
	m_baseModelPtr = 0;
	m_playerCharacter = 0;
}

/**********************************************************************************************************************************/

void 
TargetIndicator::SetTarget(const Vector3D& target, float elapsedTime)
{
	m_rotation = atan2f(m_position.x - target.x, m_position.z - target.z) - m_playerCharacter->GetRotation().y;

	float u = clamp(elapsedTime, 0.0f, 1.0f);
	m_randOffset = (1.0f - u) * (m_randOffset + GetRandomFloat(-elapsedTime/2,elapsedTime/2));
}

/**********************************************************************************************************************************/

void 
TargetIndicator::Render()
{
	if (!CutSceneController::GetInstance()->IsCutSceneActive() &&
		m_isVisible && m_modelPtr  && m_baseModelPtr)
	{
		Matrix compassView;
		MatrixLookAtLH(&compassView,&Vector3D(0,2,-2),&Vector3D(0,0,0),&Vector3D(0,1,0));

		Matrix oldView;
		oldView = CameraController::GetInstance()->GetView();				

		CameraController::GetInstance()->SetView(compassView);
		m_D3DDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);

		m_baseModelPtr->SetPosition(Vector3D(-2,-2,0));
		m_baseModelPtr->SetRotation(Vector3D(0,0,0));
		m_baseModelPtr->SetScale(0.25f);				
		m_baseModelPtr->Render();

		m_modelPtr->SetPosition(Vector3D(-2,-2,0));				
		m_modelPtr->SetRotation(Vector3D(0, m_rotation + m_randOffset, 0));
		m_modelPtr->SetScale(0.25f);
		m_modelPtr->Render();

		m_D3DDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
		CameraController::GetInstance()->SetView(oldView);
	}
}

/**********************************************************************************************************************************/
