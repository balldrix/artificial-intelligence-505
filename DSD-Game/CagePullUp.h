
#ifndef _CAGEPULLUP_H_
#define _CAGEPULLUP_H_

#ifndef _BEHAVIOR_H_
#include "Behavior.h"
#endif //#ifndef _BEHAVIOR_H_

class CagePullUp : public Behavior
{
public:
	/**
	 * Constructor
	 */
					CagePullUp();
	/**
	 * Destructor
	 */
	virtual			~CagePullUp();
	/**
	 * Notifies the behaviour that the following character has now entered the behaviour
	 */
	virtual void	OnEnter(
						Character* owner
						);
	/**
	 * Updates the characters logic for this behaviour
	 */
	virtual void	Update(
						Character* owner, 
						float elapsedTime
						);
	/**
	 * Notifies the behaviour that the following character has now left the behaviour
	 */
	virtual void	OnExit(
						Character* owner
						);
private:
	float m_timer;
};

#endif //#ifndef _CAGEPULLUP_H_