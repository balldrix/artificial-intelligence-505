#include "Types.h"
#include "Bullets.h"
#include "Model.h"
#include "RenderManager.h"
#include "ModelManager.h"
#include "CharacterManager.h"
#include "Character.h"
#include "Sphere.h"
#include "SoundManager.h"
#include "SoundEmitter.h"
#include "PlayerConstants.h"
#include "EnvironmentManager.h"
#include "CollisionMesh.h"
#include "ParticleEmitter.h"

/************************************************************************/

const float BULLETROTATIONTIME = 2.f;
const float BULLETSPEED = 30.0f;
const float MISSILESPEED = 10.0f;

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Bullets Constructor
//////////////////////////////////////////////////////////////////////////

Bullets::Bullets() : 
	m_position(0,2,0),
	m_rotation(0,0,0),
	m_forward(0,0,1),
	m_up(0,1,0),
	m_life(0.0f),
	m_alive(false),
	m_model(0),
	m_currentUpgrade(NONE),
	m_target(NULL),
	m_speed(0.0f)

{
	m_flags |= NoEffects;
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Bullets Destructor
//////////////////////////////////////////////////////////////////////////

Bullets::~Bullets()
{
	m_model = 0;
	m_target = NULL;
	RenderManager::GetInstance()->RemoveEvent(this);
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//CreateBullet initializes a bullet
//////////////////////////////////////////////////////////////////////////

void
Bullets::CreateBullet(const char* ID)
{
	m_ID = ID;
	m_model = ModelManager::GetInstance()->GetStaticMeshPointer("BULLET");
	m_model->SetID(m_ID);
	m_model->SetPosition(m_position);
	m_model->SetRotation(m_rotation);
	m_model->SetScale(3.0f);
	RenderManager::GetInstance()->AddEvent(this, NULL);
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Render
//////////////////////////////////////////////////////////////////////////
void
Bullets::Render()
{
	if(m_alive)
	{
		m_model->SetPosition(m_position);
		m_model->SetRotation(m_rotation);
		m_model->Render();
	}
}

/************************************************************************/

//////////////////////////////////////////////////////////////////////////
//Update
//////////////////////////////////////////////////////////////////////////

void
Bullets::Update(float elapsedTime)
{
	if(m_alive)
	{
		//check for collision with creatures
		int numCharacters;
		Character** characters = CharacterManager::GetInstance()->GetCharacters(numCharacters);

		for (int index = 0; index < numCharacters; index ++)
		{
			Character& tempChar = *characters[index];
			
			if(!tempChar.IsPlayer() && !tempChar.IsDead() && tempChar.GetVulnrability())//don't collide with player or cage
			{
				Sphere& sphere = *tempChar.GetBoundingSphere();
				Vector3D vecTo =  sphere.GetPosition() - m_position;
				float radius = sphere.GetRadius();
				float distance = Vector3DLengthSq(&vecTo);

				if(distance < radius * radius)

				{
					// for all characters bones
					for(int i = 0; i < tempChar.GetBoundingSphereList().size() - 3; i++)
					{

						sphere = *tempChar.GetBoundingSphereList()[i].sphere;
						vecTo = sphere.GetPosition() - m_position;
						radius = sphere.GetRadius();
						distance = Vector3DLengthSq(&vecTo);

						if(distance < radius * radius)
						{
							// when explosive is active
							if(m_currentUpgrade == EXPLOSIVE)
							{
								// double damage
								tempChar.HurtCharacter(3);

								// make explosion effect
								//Create a particle effect for the gun
								ParticleEmitter* explode = new ParticleEmitter();
								explode->SetUpEmitter(200, m_position, m_position, Vector3D(-1.9f, -1.5f, -1.9f), Vector3D(1.9f, 1.5f, 1.9f), 1.0f, 0.5f, 2.0f, 50);
								explode->LoadTexture("explosion", "explosion.tga");
								explode->SetLooping(false);
								ParticleSystem::GetInstance()->AddEmitter(explode);

								//play shoot sound
								SoundManager::GetInstance()->GetEmitter("EXPLOSION")->Play();
								SoundManager::GetInstance()->GetEmitter("EXPLOSION")->SetPosition(m_position);
								SoundManager::GetInstance()->GetEmitter("EXPLOSION")->SetVolume(200);
							}
							else
							{
								//tell character it's been hit
								tempChar.HurtCharacter();
							}

							SoundManager::GetInstance()->GetEmitter("HITENEMY")->Play();
							SoundManager::GetInstance()->GetEmitter("HITENEMY")->SetPosition(m_position);
							//remove the bullet
							m_alive = false;
							return;
						}
					}
				}
			}
		}

		// process movement depending on updgrade status
		if(m_currentUpgrade == HEATSEEKER)
		{
			if(m_target != NULL &&
				!m_target->IsDead())
			{
				TurnToTargetY(elapsedTime);
				AdjustHeight(elapsedTime);
			}

			m_speed = MISSILESPEED;
		}
		else if(m_currentUpgrade == SMARTMISSILE)
		{
			m_speed = MISSILESPEED;

			if(m_target != NULL && 
				!m_target->IsDead())
			{
				TurnToTargetY(elapsedTime);
				AdjustHeight(elapsedTime);

				Vector3D intersection;
				Vector3D destination(m_forward);
				if(EnvironmentManager::GetInstance()->TestCollision(m_position, 
																	m_position - destination * 2.0,
																	intersection))
				{
					Matrix rotationMatrixY;
					MatrixRotationY(&rotationMatrixY, D3DX_PI / 4);
					Vector3DTransformCoord(&destination, &Vector3D(0.0f, 0.0f, 1.0f), &rotationMatrixY);
					if(EnvironmentManager::GetInstance()->TestCollision(m_position, 
																		m_position - destination * 2.0,
																		intersection))
					{
						m_rotation.y = float(m_rotation.y - D3DX_PI / 4);
					}
					else
					{
						m_rotation.y = float(m_rotation.y + D3DX_PI / 4);
					}
				}
			}
		}
		else
		{
			m_speed = BULLETSPEED;
		}

		// rotate forward direction with new rotation matrix
		Matrix rotationMatrixY;
		MatrixRotationY(&rotationMatrixY, m_rotation.y);
		Vector3DTransformCoord(&m_forward, &Vector3D(0.0f, 0.0f, 1.0f), &rotationMatrixY);

		//update position
		Vector3D newPosition = m_position;
		newPosition -= m_forward * (m_speed * elapsedTime);
		m_position = newPosition;

		//update rotation
		float move = (2 * D3DX_PI) *  (BULLETROTATIONTIME * elapsedTime);
		m_rotation = Vector3D(m_rotation.x, m_rotation.y, m_rotation.z + move);

		m_life -= elapsedTime;

		if(m_life < 0)
		{
			m_alive = false;
		}
	}
}

/************************************************************************/

void
Bullets::SetPosition(const Vector3D& pos)
{
	m_position = pos;
}

/************************************************************************/

const Vector3D&
Bullets::GetPosition() const
{
	return m_position;
}

/************************************************************************/

void
Bullets::SetRotation(const Vector3D& rot)
{
	m_rotation = rot;
}

/************************************************************************/

const Vector3D&
Bullets::GetRotation() const
{
	return m_rotation;
}

/************************************************************************/

void
Bullets::SetForward(const Vector3D& forward)
{
	m_forward = forward;
}

/************************************************************************/

const Vector3D&
Bullets::GetForward() const
{
	return m_forward;
}

/************************************************************************/

void
Bullets::SetAlive(bool alive)
{
	m_alive = alive;
}

/************************************************************************/

bool
Bullets::GetAlive() const
{
	return m_alive;
}

void
Bullets::SetState(UpgradeState state)
{
	m_currentUpgrade = state;
}

void 
Bullets::SetTarget(Character* target)
{
	m_target = target;
}

void Bullets::TurnToTargetY(float elapsedTime)
{
	float angle = atan2f(-(m_target->GetBoundingSphere()->GetPosition().x - m_position.x), -(m_target->GetBoundingSphere()->GetPosition().z - m_position.z));

	float targetAngle = angle;

	if(angle > m_rotation.y)
	{
		targetAngle -= 2 * D3DX_PI;
	}

	float difference = m_rotation.y - targetAngle;

	if(difference < 0.001f)
	{
		return;
	}

	if(difference < D3DX_PI)
	{
		if(m_rotation.y - BULLETROTATIONTIME * elapsedTime <= targetAngle)
		{
			m_rotation.y = angle;
		}
		else
		{
			m_rotation.y = float(m_rotation.y - BULLETROTATIONTIME * elapsedTime);
		}
	}
	else
	{
		if(m_rotation.y + BULLETROTATIONTIME * elapsedTime - 2 * D3DX_PI >= targetAngle)
		{
			m_rotation.y = angle;
		}
		else
		{
			m_rotation.y = float(m_rotation.y + BULLETROTATIONTIME * elapsedTime);
		}
	}
}

void
Bullets::AdjustHeight(float elapsedTime)
{
	float height = m_target->GetBoundingSphere()->GetPosition().y - m_position.y;
	if(height > 0.02f)
	{
		m_position.y += 0.02f;
	}
	else if(height < -0.02f)
	{
		m_position.y -= 0.02f;
	}
}

void 
Bullets::SetLife(float life)
{
	m_life = life;
}

/************************************************************************/