// MissionBrief.h
// Christopher Ball 2016
// Mission brief class added to display before the game starts

#ifndef _MISSION_BRIEF_
#define _MISSION_BRIEF_

#ifndef _GAMESTATE_H_
#include "GameState.h"
#endif //#ifndef _GAMESTATE_H_

class MissionBrief : public GameState
{
public:
	/**
	* Constructor
	*/
	MissionBrief();
	/**
	* Destructor
	*/
	~MissionBrief();
	/**
	* On entry handler, called when you enter the game state
	*/
	virtual void		OnEntry();
	/**
	* On exit handler, called when you exit the game state
	*/
	virtual void		OnExit();
	/**
	* Update handler, called once a frame for the state to update its logic.
	*/
	virtual void		Update(
		float elapsedTime ///< The elapsed time this frame
	);
	/**
	*	Render handler, handles renderering the game for its current state
	*/
	virtual void		Render();

private:
	
	// Change screen method, changes the visible mission brief to the next 
	void ChangeScreen(); 	
	
	// stores current screen counter as int
	int m_currentScreen;
};

#endif _MISSION_BRIEF_