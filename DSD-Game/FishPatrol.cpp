
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "FishPatrol.h"
#include "FishBehaviors.h"

#include "ScoreManager.h"


/**********************************************************************************************************************************/

FishPatrol::FishPatrol() : 
	Behavior("FISH_PATROL")
{
}

/**********************************************************************************************************************************/

FishPatrol::~FishPatrol()
{
}

/**********************************************************************************************************************************/

void 
FishPatrol::OnEnter(Character* owner)
{
	owner->SetSpeed(FISHDISTPERSEC);
	SetAnimationAndCacheIndex(owner, "SWIM");
}

/**********************************************************************************************************************************/

void 
FishPatrol::Update(Character* owner, float elapsedTime)
{
	float tempAngle = GetPathAngleToTarget(owner,owner->GetTargetWayPoint()->m_Position);
	
	if(TurnTo(owner,tempAngle,FISHROTPERSEC,elapsedTime) == TD_FACING)
	{	
		MoveInDirection(owner, elapsedTime);
	}	

	if(DistanceToTargetSq(owner,owner->GetTargetWayPoint()->m_Position) < 0.1f)
	{
		owner->ArrivedAtWaypoint();
	}

	// check if player is in distance to chase
	if(DistanceToCharacterSq(owner, *m_playerCharacter) < FISHCHASEDIST * FISHCHASEDIST)
	{
		owner->SetBehavior("FISH_CHASE");
	}

	//FISH are one hit kills so kill it
	if(owner->GetHitCount() > NUM_FISH_HITS * ScoreManager::GetInstance()->GetDifficulty())
	{
		owner->SetBehavior("FISH_DEATH");
		owner->SetDead(true);

		// add points
		ScoreManager::GetInstance()->AddPoints(PointSystem::FISH);
	}
}

/**********************************************************************************************************************************/

void	
FishPatrol::OnExit(Character* owner)
{
}

/**********************************************************************************************************************************/
