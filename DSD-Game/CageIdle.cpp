
#include "Types.h"
#include "Character.h"
#include "AnimationInstance.h"
#include "CutSceneController.h"
#include "CageIdle.h"

/**********************************************************************************************************************************/

CageIdle::CageIdle() : 
	Behavior("CAGE_IDLE"),
	m_boundingSphere(0)
{
}

/**********************************************************************************************************************************/

CageIdle::~CageIdle()
{
	if(m_boundingSphere)
	{
		delete m_boundingSphere;
	}
}

/**********************************************************************************************************************************/

void 
CageIdle::OnEnter(Character* owner)
{
	SetAnimationAndCacheIndex(owner, "IDLE");
	m_triggered = false;

	//Create Bounding Sphere;
	m_boundingSphere = new Sphere(owner->GetPosition(),30.0f);
}

/**********************************************************************************************************************************/

void 
CageIdle::Update(Character* owner, float elapsedTime)
{
	if(!CutSceneController::GetInstance()->IsCutSceneActive() && !m_triggered)
	{
		if(m_boundingSphere->CollidingWithSphere(*m_playerCharacter->GetBoundingSphere()))
		{
			CutSceneController::GetInstance()->BeginCutScene("CutScene5");
			m_triggered = true;
			//owner->SetBehavior("CAGE_PULLUP");	
		}
	}
}

/**********************************************************************************************************************************/

void 
CageIdle::OnExit(Character* owner)
{
	if(m_boundingSphere)
	{
		delete m_boundingSphere;
		m_boundingSphere = 0;
	}
}

/**********************************************************************************************************************************/

