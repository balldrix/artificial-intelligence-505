#include "GameCompleted.h"
#include "UIManager.h"
#include "UIObject.h"
#include "UISprite.h"
#include "UIText.h"
#include "InputManager.h"
#include "GameStateManager.h"
#include "Program.h"
#include "Game.h"
#include "SoundManager.h"
#include "SoundEmitter.h"

#include "ScoreManager.h"
#include "HighScores.h"

/*****************************************************************************************/

GameCompleted::GameCompleted() :
	GameState("GAMECOMPLETED")
{
}

	/*****************************************************************************************/

GameCompleted::~GameCompleted()
{

}

void 
GameCompleted::OnEntry()
{
	UIManager::GetInstance()->SetCurrentUI("GAMECOMPETED");
	m_timer = 0.0f;
	SoundManager::GetInstance()->GetEmitter("GAMEOVER")->SetPosition(Vector3D(0,0,0));
	SoundManager::GetInstance()->GetEmitter("GAMEOVER")->Play(true);

	// update score to display on screen
	UIManager* uiManager = UIManager::GetInstance();
	unsigned int score = ScoreManager::GetInstance()->GetScore();
	MyString buffer;
	buffer.Format("You scored: %u", score);
	static_cast<UIText*>(uiManager->GetUIObject("WinScore"))->SetValue(buffer);
}

/*****************************************************************************************/

void 
GameCompleted::OnExit()
{
	UIManager::GetInstance()->SetColor(0xFFFFFFFF);
	SoundManager::GetInstance()->GetEmitter("GAMEOVER")->Stop();
}

/*****************************************************************************************/

void 
GameCompleted::Update(float elapsedTime)
{
	m_timer += elapsedTime;

	if(m_timer > 3.0f)
	{
		SoundManager::GetInstance()->GetEmitter("GAMEOVER")->Stop();
	}

	if(m_timer > 5.0f)
	{
		// check if player has a new high score
		if(HighScores::GetInstance()->IsHighScore(ScoreManager::GetInstance()->GetScore()))
		{
			GameStateManager::GetInstance()->SwitchState("ENTERNAME");
		}
		else
		{
			GameStateManager::GetInstance()->SwitchState("FRONTEND");
		}
	}

	InputManager::GetInstance()->Update();
	UIManager::GetInstance()->Update(elapsedTime);
	SoundManager::GetInstance()->Update();
}

/*****************************************************************************************/

void 
GameCompleted::Render()
{
	UIManager::GetInstance()->Render();
}

/*****************************************************************************************/